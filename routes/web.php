<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/tentangkami', 'HomeController@aboutus')->name('aboutus');
Route::get('/post/detail/{id}', 'HomeController@post_detail')->name('post.detail');
Route::get('/tempat', 'HomeController@place')->name('place');
Route::get('/acara', 'HomeController@event')->name('event');
Route::get('/berita', 'HomeController@news')->name('news');
Route::get('/komunitas', 'HomeController@community')->name('community');
Route::get('/komunitas/{id}', 'HomeController@community_detail')->name('community.detail');
Route::get('/tempat/{id}', 'HomeController@place_detail')->name('place.detail');
Route::get('/berita/{id}', 'HomeController@news_detail')->name('news.detail');
Route::get('/acara/{id}', 'HomeController@event_detail')->name('event.detail');
Route::post('/content/based', 'User\HomeController@content_based_post')->name('user.content.based.post');

Route::group(array('prefix' => 'admin'), function () {
  Route::get('/dashboard', 'Admin\DashboardController@index')->name('admin.dashboard');
  Route::get('/method', 'HomeController@method')->name('admin.method');
  Route::resource('/dashboard/berita', 'Admin\BeritaController', ['as' => 'admin'])->except([
      'show'
  ]);
});

Route::group(array('prefix' => 'user'), function () {
  Route::get('/dashboard', 'User\Dashboard\DashboardController@index')->name('user.dashboard');
  Route::resource('/dashboard/tempat', 'User\TempatController',  ['as' => 'user'])->except([
      'show'
  ]);
  Route::resource('/dashboard/acara', 'User\AcaraController',  ['as' => 'user'])->except([
      'show'
  ]);
  Route::resource('/dashboard/komunitas', 'User\KomunitasController',  ['as' => 'user'])->except([
      'show'
  ]);
  Route::resource('/ulasan', 'User\UlasanController', ['as' => 'user'])->except([
      'show', 'index', 'create', 'edit'
  ]);
  Route::get('/dashboard/profile', 'User\Dashboard\DashboardProfileController@index')->name('user.profile');
  Route::resource('/dashboard/hobby', 'User\Dashboard\MyHobbyController', ['as' => 'user'])->except([
      'show'
  ]);
});
