<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name' => 'Abdul',
                'username' => 'adminabdul',
                'role' => '0',
                'password' => bcrypt('admin1234')
             ],
             [
                 'name' => 'Wildan',
                 'username' => 'userwildan',
                 'role' => '1',
                 'password' => bcrypt('user1234')
              ],
           ]);
    }
}
