(function($) {
	var
		// CSS EVENT DETECT
		csse = {
			t : 'transitionend webkitTransitionEnd oTransitionEnd otransitionend MSTransitionEnd',
			a : 'webkitAnimationEnd mozAnimationEnd oAnimationEnd oanimationend animationend'
		},
		// I18N
		i18n = {
			'en' : {
				name : 'English',
				gregorian : false,
				months : {
					short: [
						'Jan',
						'Feb',
						'Mar',
						'Apr',
						'May',
						'June',
						'July',
						'Aug',
						'Sept',
						'Oct',
						'Nov',
						'Dec'
					],
					full : [
						'January',
						'February',
						'March',
						'April',
						'May',
						'June',
						'July',
						'August',
						'September',
						'October',
						'November',
						'December'
					]
				},
				weekdays : {
					short : [
						'S',
						'M',
						'T',
						'W',
						'T',
						'F',
						'S'
					],
					full : [
						'Sunday',
						'Monday',
						'Tuesday',
						'Wednesday',
						'Thursday',
						'Friday',
						'Saturday'
					]
				}
			},
			'ka' : {
				name : 'Georgian',
				gregorian : false,
				months : {
					short: [
						'იან',
						'თებ',
						'მარტ',
						'აპრ',
						'მაი',
						'ივნ',
						'ივლ',
						'აგვ',
						'სექტ',
						'ოქტ',
						'ნოემბ',
						'დეკ'
					],
					full : [
						'იანვარი',
						'თებერვალი',
						'მარტი',
						'აპრილი',
						'მაისი',
						'ივნისი',
						'ივლისი',
						'აგვისტო',
						'სექტემბერი',
						'ოქტომბერი',
						'ნოემბერი',
						'დეკემბერი'
					]
				},
				weekdays : {
					short : [
						'კვ',
						'ორ',
						'სამ',
						'ოთხ',
						'ხუთ',
						'პარ',
						'შაბ'
					],
					full : [
						'კვირა',
						'ორშაბათი',
						'სამშაბათი',
						'ოთხშაბათი',
						'ხუთშაბათი',
						'პარასკევი',
						'შაბათი'
					]
				}
			},//
			'it' : {
				name : 'Italiano',
				gregorian : true,
				months : {
					short: [
						'Gen',
						'Feb',
						'Mar',
						'Apr',
						'Mag',
						'Giu',
						'Lug',
						'Ago',
						'Set',
						'Ott',
						'Nov',
						'Dic'
					],
					full : [
						'Gennaio',
						'Febbraio',
						'Marzo',
						'Aprile',
						'Maggio',
						'Giugno',
						'Luglio',
						'Agosto',
						'Settembre',
						'Ottobre',
						'Novembre',
						'Dicembre'
					]
				},
				weekdays : {
					short : [
						'D',
						'L',
						'M',
						'M',
						'G',
						'V',
						'S'
					],
					full : [
						'Domenica',
						'Lunedì',
						'Martedì',
						'Mercoledì',
						'Giovedì',
						'Venerdì',
						'Sabato'
					]
				}
			},
			'fr' : {
				name : 'Français',
				gregorian : true,
				months : {
					short: [
						'Jan',
						'Fév',
						'Mar',
						'Avr',
						'Mai',
						'Jui',
						'Jui',
						'Aoû',
						'Sep',
						'Oct',
						'Nov',
						'Déc'
					],
					full : [
						'Janvier',
						'Février',
						'Mars',
						'Avril',
						'Mai',
						'Juin',
						'Juillet',
						'Août',
						'Septembre',
						'Octobre',
						'Novembre',
						'Décembre'
					]
				},
				weekdays : {
					short : [
						'D',
						'L',
						'M',
						'M',
						'J',
						'V',
						'S'
					],
					full : [
						'Dimanche',
						'Lundi',
						'Mardi',
						'Mercredi',
						'Jeudi',
						'Vendredi',
						'Samedi'
					]
				}
			},
			'zh' : {
				name : '中文',
				gregorian : true,
				months : {
					short: [
						'一月',
						'二月',
						'三月',
						'四月',
						'五月',
						'六月',
						'七月',
						'八月',
						'九月',
						'十月',
						'十一月',
						'十二月'
					],
					full : [
						'一月',
						'二月',
						'三月',
						'四月',
						'五月',
						'六月',
						'七月',
						'八月',
						'九月',
						'十月',
						'十一月',
						'十二月'
					]
				},
				weekdays : {
					short : [
						'天',
						'一',
						'二',
						'三',
						'四',
						'五',
						'六'
					],
					full : [
						'星期天',
						'星期一',
						'星期二',
						'星期三',
						'星期四',
						'星期五',
						'星期六'
					]
				}
			},
			'ar' : {
				name : 'العَرَبِيَّة',
				gregorian : false,
				months : {
					short: [
						'جانفي',
						'فيفري',
						'مارس',
						'أفريل',
						'ماي',
						'جوان',
						'جويلية',
						'أوت',
						'سبتمبر',
						'أكتوبر',
						'نوفمبر',
						'ديسمبر'
					],
					full : [
						'جانفي',
						'فيفري',
						'مارس',
						'أفريل',
						'ماي',
						'جوان',
						'جويلية',
						'أوت',
						'سبتمبر',
						'أكتوبر',
						'نوفمبر',
						'ديسمبر'
					]
				},
				weekdays : {
					short : [
						'S',
						'M',
						'T',
						'W',
						'T',
						'F',
						'S'
					],
					full : [
						'الأحد',
						'الإثنين',
						'الثلثاء',
						'الأربعاء',
						'الخميس',
						'الجمعة',
						'السبت'
					]
				}
			},
			'fa' : {
				name : 'فارسی',
				gregorian : false,
				months : {
					short: [
						'ژانویه',
						'فووریه',
						'مارچ',
						'آپریل',
						'می',
						'جون',
						'جولای',
						'آگوست',
						'سپتامبر',
						'اکتبر',
						'نوامبر',
						'دسامبر'
					],
					full : [
						'ژانویه',
						'فووریه',
						'مارچ',
						'آپریل',
						'می',
						'جون',
						'جولای',
						'آگوست',
						'سپتامبر',
						'اکتبر',
						'نوامبر',
						'دسامبر'
					]
				},
				weekdays : {
					short : [
						'S',
						'M',
						'T',
						'W',
						'T',
						'F',
						'S'
					],
					full : [
						'یکشنبه',
						'دوشنبه',
						'سه شنبه',
						'چهارشنبه',
						'پنج شنبه',
						'جمعه',
						'شنبه'
					]
				}
			},
			'hu' : {
				name : 'Hungarian',
				gregorian : true,
				months : {
					short: [
						"jan",
						"feb",
						"már",
						"ápr",
						"máj",
						"jún",
						"júl",
						"aug",
						"sze",
						"okt",
						"nov",
						"dec"
					],
					full : [
						"január",
						"február",
						"március",
						"április",
						"május",
						"június",
						"július",
						"augusztus",
						"szeptember",
						"október",
						"november",
						"december"
					]
				},
				weekdays : {
					short : [
						'v',
						'h',
						'k',
						's',
						'c',
						'p',
						's'
					],
					full : [
						'vasárnap',
						'hétfő',
						'kedd',
						'szerda',
						'csütörtök',
						'péntek',
						'szombat'
					]
				}
			},
			'gr' : {
				name : 'Ελληνικά',
				gregorian : true,
				months : {
					short: [
						"Ιαν",
						"Φεβ",
						"Μάρ",
						"Απρ",
						"Μάι",
						"Ιούν",
						"Ιούλ",
						"Αύγ",
						"Σεπ",
						"Οκτ",
						"Νοέ",
						"Δεκ"
					],
					full : [
						"Ιανουάριος",
						"Φεβρουάριος",
						"Μάρτιος",
						"Απρίλιος",
						"Μάιος",
						"Ιούνιος",
						"Ιούλιος",
						"Αύγουστος",
						"Σεπτέμβριος",
						"Οκτώβριος",
						"Νοέμβριος",
						"Δεκέμβριος"
					]
				},
				weekdays : {
					short : [
						'Κ',
						'Δ',
						'Τ',
						'Τ',
						'Π',
						'Π',
						'Σ'
					],
					full : [
						'Κυριακή',
						'Δευτέρα',
						'Τρίτη',
						'Τετάρτη',
						'Πέμπτη',
						'Παρασκευή',
						'Σάββατο'
					]
				}
			},
			'es' : {
				name : 'Español',
				gregorian : true,
				months : {
					short: [
						"Ene",
						"Feb",
						"Mar",
						"Abr",
						"May",
						"Jun",
						"Jul",
						"Ago",
						"Sep",
						"Oct",
						"Nov",
						"Dic"
					],
					full : [
						"Enero",
						"Febrero",
						"Marzo",
						"Abril",
						"Mayo",
						"Junio",
						"Julio",
						"Agosto",
						"Septiembre",
						"Octubre",
						"Noviembre",
						"Diciembre"
					]
				},
				weekdays : {
					short : [
						'D',
						'L',
						'M',
						'X',
						'J',
						'V',
						'S'
					],
					full : [
						'Domingo',
						'Lunes',
						'Martes',
						'Miércoles',
						'Jueves',
						'Viernes',
						'Sábado'
					]
				}
			},
			'da' : {
				name : 'Dansk',
				gregorian : true,
				months : {
					short: [
						"jan",
						"feb",
						"mar",
						"apr",
						"maj",
						"jun",
						"jul",
						"aug",
						"sep",
						"okt",
						"nov",
						"dec"
					],
					full : [
						"januar",
						"februar",
						"marts",
						"april",
						"maj",
						"juni",
						"juli",
						"august",
						"september",
						"oktober",
						"november",
						"december"
					]
				},
				weekdays : {
					short : [
						's',
						'm',
						't',
						'o',
						't',
						'f',
						'l'
					],
					full : [
						'søndag',
						'mandag',
						'tirsdag',
						'onsdag',
						'torsdag',
						'fredag',
						'lørdag'
					]
				}
			},
			'de' : {
				name : 'Deutsch',
				gregorian : true,
				months : {
					short: [
						"Jan",
						"Feb",
						"Mär",
						"Apr",
						"Mai",
						"Jun",
						"Jul",
						"Aug",
						"Sep",
						"Okt",
						"Nov",
						"Dez"
					],
					full : [
						"Januar",
						"Februar",
						"März",
						"April",
						"Mai",
						"Juni",
						"Juli",
						"August",
						"September",
						"Oktober",
						"November",
						"Dezember"
					]
				},
				weekdays : {
					short : [
						'S',
						'M',
						'D',
						'M',
						'D',
						'F',
						'S'
					],
					full : [
						'Sonntag',
						'Montag',
						'Dienstag',
						'Mittwoch',
						'Donnerstag',
						'Freitag',
						'Samstag'
					]
				}
			},
			'nl' : {
				name : 'Nederlands',
				gregorian : true,
				months : {
					short: [
						"jan",
						"feb",
						"maa",
						"apr",
						"mei",
						"jun",
						"jul",
						"aug",
						"sep",
						"okt",
						"nov",
						"dec"
					],
					full : [
						"januari",
						"februari",
						"maart",
						"april",
						"mei",
						"juni",
						"juli",
						"augustus",
						"september",
						"oktober",
						"november",
						"december"
					]
				},
				weekdays : {
					short : [
						'z',
						'm',
						'd',
						'w',
						'd',
						'v',
						'z'
					],
					full : [
						'zondag',
						'maandag',
						'dinsdag',
						'woensdag',
						'donderdag',
						'vrijdag',
						'zaterdag'
					]
				}
			},
			'pl' : {
				name : 'język polski',
				gregorian : true,
				months : {
					short: [
						"sty",
						"lut",
						"mar",
						"kwi",
						"maj",
						"cze",
						"lip",
						"sie",
						"wrz",
						"paź",
						"lis",
						"gru"
					],
					full : [
						"styczeń",
						"luty",
						"marzec",
						"kwiecień",
						"maj",
						"czerwiec",
						"lipiec",
						"sierpień",
						"wrzesień",
						"październik",
						"listopad",
						"grudzień"
					]
				},
				weekdays : {
					short : [
						'n',
						'p',
						'w',
						'ś',
						'c',
						'p',
						's'
					],
					full : [
						'niedziela',
						'poniedziałek',
						'wtorek',
						'środa',
						'czwartek',
						'piątek',
						'sobota'
					]
				}
			},
			'pt' : {
				name : 'Português',
				gregorian : true,
				months : {
					short: [
						"Janeiro",
						"Fevereiro",
						"Março",
						"Abril",
						"Maio",
						"Junho",
						"Julho",
						"Agosto",
						"Setembro",
						"Outubro",
						"Novembro",
						"Dezembro"
					],
					full : [
						"Janeiro",
						"Fevereiro",
						"Março",
						"Abril",
						"Maio",
						"Junho",
						"Julho",
						"Agosto",
						"Setembro",
						"Outubro",
						"Novembro",
						"Dezembro"
					]
				},
				weekdays : {
					short : [
						"D",
						"S",
						"T",
						"Q",
						"Q",
						"S",
						"S"
					],
					full : [
						"Domingo",
						"Segunda",
						"Terça",
						"Quarta",
						"Quinta",
						"Sexta",
						"Sábado"
					]
				}
			},
			'si' : {
				name : 'Slovenščina',
				gregorian : true,
				months : {
					short: [
						"jan",
						"feb",
						"mar",
						"apr",
						"maj",
						"jun",
						"jul",
						"avg",
						"sep",
						"okt",
						"nov",
						"dec"
					],
					full : [
						"januar",
						"februar",
						"marec",
						"april",
						"maj",
						"junij",
						"julij",
						"avgust",
						"september",
						"oktober",
						"november",
						"december"
					]
				},
				weekdays : {
					short : [
						'n',
						'p',
						't',
						's',
						'č',
						'p',
						's'
					],
					full : [
						'nedelja',
						'ponedeljek',
						'torek',
						'sreda',
						'četrtek',
						'petek',
						'sobota'
					]
				}
			},
			'uk' : {
				name : 'українська мова',
				gregorian : true,
				months : {
					short: [
						"січень",
						"лютий",
						"березень",
						"квітень",
						"травень",
						"червень",
						"липень",
						"серпень",
						"вересень",
						"жовтень",
						"листопад",
						"грудень"
					],
					full : [
						"січень",
						"лютий",
						"березень",
						"квітень",
						"травень",
						"червень",
						"липень",
						"серпень",
						"вересень",
						"жовтень",
						"листопад",
						"грудень"
					]
				},
				weekdays : {
					short : [
						'н',
						'п',
						'в',
						'с',
						'ч',
						'п',
						'с'
					],
					full : [
						'неділя',
						'понеділок',
						'вівторок',
						'середа',
						'четвер',
						'п\'ятниця',
						'субота'
					]
				}
			},
			'ru' : {
				name : 'русский язык',
				gregorian : true,
				months : {
					short: [
						"январь",
						"февраль",
						"март",
						"апрель",
						"май",
						"июнь",
						"июль",
						"август",
						"сентябрь",
						"октябрь",
						"ноябрь",
						"декабрь"
					],
					full : [
						"январь",
						"февраль",
						"март",
						"апрель",
						"май",
						"июнь",
						"июль",
						"август",
						"сентябрь",
						"октябрь",
						"ноябрь",
						"декабрь"
					]
				},
				weekdays : {
					short : [
						'в',
						'п',
						'в',
						'с',
						'ч',
						'п',
						'с'
					],
					full : [
						'воскресенье',
						'понедельник',
						'вторник',
						'среда',
						'четверг',
						'пятница',
						'суббота'
					]
				}
			},
			'tr' : {
				name : 'Türkçe',
				gregorian : true,
				months : {
					short: [
						"Oca",
						"Şub",
						"Mar",
						"Nis",
						"May",
						"Haz",
						"Tem",
						"Ağu",
						"Eyl",
						"Eki",
						"Kas",
						"Ara"
					],
					full : [
						"Ocak",
						"Şubat",
						"Mart",
						"Nisan",
						"Mayıs",
						"Haziran",
						"Temmuz",
						"Ağustos",
						"Eylül",
						"Ekim",
						"Kasım",
						"Aralık"
					]
				},
				weekdays : {
					short : [
						'P',
						'P',
						'S',
						'Ç',
						'P',
						'C',
						'C'
					],
					full : [
						'Pazar',
						'Pazartesi',
						'Sali',
						'Çarşamba',
						'Perşembe',
						'Cuma',
						'Cumartesi'
					]
				}
			},
			'ko' : {
				name : '조선말',
				gregorian : true,
				months : {
					short: [
						"1월",
						"2월",
						"3월",
						"4월",
						"5월",
						"6월",
						"7월",
						"8월",
						"9월",
						"10월",
						"11월",
						"12월"
					],
					full : [
						"1월",
						"2월",
						"3월",
						"4월",
						"5월",
						"6월",
						"7월",
						"8월",
						"9월",
						"10월",
						"11월",
						"12월"
					]
				},
				weekdays : {
					short : [
						'일',
						'월',
						'화',
						'수',
						'목',
						'금',
						'토'
					],
					full : [
						'일요일',
						'월요일',
						'화요일',
						'수요일',
						'목요일',
						'금요일',
						'토요일'
					]
				}
			},
			'fi' : {
				name : 'suomen kieli',
				gregorian : true,
				months : {
					short: [
						"Tam",
						"Hel",
						"Maa",
						"Huh",
						"Tou",
						"Kes",
						"Hei",
						"Elo",
						"Syy",
						"Lok",
						"Mar",
						"Jou"
					],
					full : [
						"Tammikuu",
						"Helmikuu",
						"Maaliskuu",
						"Huhtikuu",
						"Toukokuu",
						"Kesäkuu",
						"Heinäkuu",
						"Elokuu",
						"Syyskuu",
						"Lokakuu",
						"Marraskuu",
						"Joulukuu"
					]
				},
				weekdays : {
					short : [
						'S',
						'M',
						'T',
						'K',
						'T',
						'P',
						'L'
					],
					full : [
						'Sunnuntai',
						'Maanantai',
						'Tiistai',
						'Keskiviikko',
						'Torstai',
						'Perjantai',
						'Lauantai'
					]
				}
			},
			'vi':{
				name:'Tiếng việt',
				gregorian:false,
				months:{
					short:[
						'Th.01',
						'Th.02',
						'Th.03',
						'Th.04',
						'Th.05',
						'Th.06',
						'Th.07',
						'Th.08',
						'Th.09',
						'Th.10',
						'Th.11',
						'Th.12'
					],
					full:[
						'Tháng 01',
						'Tháng 02',
						'Tháng 03',
						'Tháng 04',
						'Tháng 05',
						'Tháng 06',
						'Tháng 07',
						'Tháng 08',
						'Tháng 09',
						'Tháng 10',
						'Tháng 11',
						'Tháng 12'
					]
				},
				weekdays:{
					short:[
						'CN',
						'T2',
						'T3',
						'T4',
						'T5',
						'T6',
						'T7'
					],
					full:[
						'Chủ nhật',
						'Thứ hai',
						'Thứ ba',
						'Thứ tư',
						'Thứ năm',
						'Thứ sáu',
						'Thứ bảy'
					]
				}
			}
		},

		// MAIN VARS

		pickers = {},
		picker = null,
		picker_ctrl = false,
		pick_dragged = null,
		pick_drag_offset = null,
		pick_drag_temp = null,

		// CHECK FUNCTIONS

		is_click = false,
		is_ie = function() {
			var
				n = navigator.userAgent.toLowerCase();
			return (n.indexOf('msie') != -1) ? parseInt(n.split('msie')[1]) : false;
		},
		is_touch = function() {
			if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent))
				return true;
			else
				return false;
		},
		is_fx_mobile = function() {
			if(picker&&pickers[picker.id].fx&&!pickers[picker.id].fxmobile) {
				if($(window).width()<480)
					picker.element.removeClass('picker-fxs');
				else
					picker.element.addClass('picker-fxs')
			}
		},
		is_jumpable = function() {
			if( pickers[picker.id].jump >= pickers[picker.id].key.y.max - pickers[picker.id].key.y.min )
				return false;
			else
				return true;
		},
		is_locked = function() {
			var
				unix_current = get_unix(get_current_full()),
				unix_today = get_unix(get_today_full());

			if(pickers[picker.id].lock) {
				if(pickers[picker.id].lock=='from') {
					if(unix_current<unix_today) {
						picker_alrt();
						picker.element.addClass('picker-lkd');
						return true;
					}
					else {
						picker.element.removeClass('picker-lkd');
						return false;
					}
				}
				if(pickers[picker.id].lock=='to') {
					if(unix_current>unix_today) {
						picker_alrt();
						picker.element.addClass('picker-lkd');
						return true;
					}
					else {
						picker.element.removeClass('picker-lkd');
						return false;
					}
				}
			}

			if(pickers[picker.id].disabledays) {
				if(pickers[picker.id].disabledays.indexOf(unix_current) != -1) {
					picker_alrt();
					picker.element.addClass('picker-lkd');
					return true;
				}
				else {
					picker.element.removeClass('picker-lkd');
					return false;
				}
			}
		},
		is_int = function(n) {
			return n % 1 === 0;
		},
		is_date = function(value) {
			var
				format = /(^\d{1,4}[\.|\\/|-]\d{1,2}[\.|\\/|-]\d{1,4})(\s*(?:0?[1-9]:[0-5]|1(?=[012])\d:[0-5])\d\s*[ap]m)?$/;
			return format.test(value);
		},

		// REST FUNCTIONS

		get_current = function(k){
			return parseInt(pickers[picker.id].key[k].current);
		},
		get_today = function(k){
			return parseInt(pickers[picker.id].key[k].today);
		},
		get_today_full = function() {
			return get_today('m')+'/'+get_today('d')+'/'+get_today('y');
		},
		get_current_full = function() {
			return get_current('m')+'/'+get_current('d')+'/'+get_current('y');
		},
		get_jumped = function(k,val) {
			var
				a = [],
				key_values = pickers[picker.id].key[k];
			for (var i = key_values.min; i <= key_values.max; i++)
				if (i%val == 0)
					a.push(i);
			return a;
		},
		get_closest_jumped = function(int,arr) {
			var c = arr[0];
			var d = Math.abs (int - c);
			for (var i = 0; i < arr.length; i++) {
				var n = Math.abs (int - arr[i]);
				if (n < d) {
					d = n;
					c = arr[i];
				}
			}
			return c;
		},
		get_clear = function(k,n){
			var
				key_values = pickers[picker.id].key[k];
			if( n > key_values.max )
				return get_clear( k , (n-key_values.max)+(key_values.min-1) );
			else if( n < key_values.min )
				return get_clear( k , (n+1) + (key_values.max - key_values.min));
			else
				return n;
		},
		get_days_array = function() {
			if(i18n[pickers[picker.id].lang].gregorian)
				return [1,2,3,4,5,6,0];
			else
				return [0,1,2,3,4,5,6];
		},
		get_ul = function(k) {
			return get_picker_els('ul.pick[data-k="'+k+'"]');
		},
		get_eq = function(k,d) {
			ul = get_ul(k);
			var
				o = [];

			ul.find('li').each(function(){
				o.push($(this).attr('value'));
			});

			if(d=='last')
				return o[o.length-1];
			else
				return o[0];

		},
		get_picker_els = function(el) {
			if(picker)
				return picker.element.find(el);
		},
		get_unix = function(d) {
			return Date.parse(d) / 1000;
		},

		// RENDER FUNCTIONS

		picker_large_onoff = function() {
			if(pickers[picker.id].large) {
				picker.element.toggleClass('picker-lg');
				picker_render_calendar();
			}
		},
		picker_translate_onoff = function() {
			get_picker_els('ul.pick.pick-l').toggleClass('visible');
		},
		picker_offset = function(){
			if(!picker.element.hasClass('picker-modal')){
				var
					input = picker.input,
					left = input.offset().left + input.outerWidth()/2,
					top = input.offset().top + input.outerHeight();
				picker.element.css({
					'left' : left,
					'top' : top
				});
			}
		},
		picker_translate = function(v) {
			pickers[picker.id].lang = Object.keys(i18n)[v];
			picker_set_lang();
			picker_set();
		},
		picker_set_lang = function() {
			var
				picker_day_offset = get_days_array();
			get_picker_els('.pick-lg .pick-lg-h li').each(function(i){
				$(this).html(i18n[pickers[picker.id].lang].weekdays.short[picker_day_offset[i]]);
			});
			get_picker_els('ul.pick.pick-m li').each(function(){
				$(this).html(i18n[pickers[picker.id].lang].months.short[$(this).attr('value')-1]);
			});
		},
		picker_show = function() {
			picker.element.addClass('picker-focus');
		},
		picker_hide = function() {
			if(!is_locked()) {
				picker.element.removeClass('picker-focus');
				if(picker.element.hasClass('picker-modal'))
					$('.picker-modal-overlay').addClass('tohide');
				picker = null;
			}
			picker_ctrl = false;
		},
		picker_render_ul = function(k){
			var
				ul = get_ul(k),
				key_values = pickers[picker.id].key[k];

			//CURRENT VALUE
			pickers[picker.id].key[k].current = key_values.today < key_values.min && key_values.min || key_values.today;

			for (i = key_values.min; i <= key_values.max; i++) {
				var
					html = i;

				if(k=='m')
					html = i18n[pickers[picker.id].lang].months.short[i-1];
				if(k=='l')
					html = i18n[Object.keys(i18n)[i]].name;

				html += k=='d' ? '<span></span>' : '';

				$('<li>', {
					value: i,
					html: html
				})
				.appendTo(ul)
			}

			//PREV BUTTON
			$('<div>', {
				class: 'pick-arw pick-arw-s1 pick-arw-l',
				html: $('<i>', {
					class: 'pick-i-l'
				})
			})
			.appendTo(ul);

			//NEXT BUTTON
			$('<div>', {
				class: 'pick-arw pick-arw-s1 pick-arw-r',
				html: $('<i>', {
					class: 'pick-i-r'
				})
			})
			.appendTo(ul);

			if(k=='y') {

				//PREV BUTTON
				$('<div>', {
					class: 'pick-arw pick-arw-s2 pick-arw-l',
					html: $('<i>', {
						class: 'pick-i-l'
					})
				})
				.appendTo(ul);

				//NEXT BUTTON
				$('<div>', {
					class: 'pick-arw pick-arw-s2 pick-arw-r',
					html: $('<i>', {
						class: 'pick-i-r'
					})
				})
				.appendTo(ul);

			}

			picker_ul_transition(k,get_current(k));

		},
		picker_render_calendar = function() {

			var
				index = 0,
				w = get_picker_els('.pick-lg-b');

			w.find('li')
			.empty()
			.removeClass('pick-n pick-b pick-a pick-v pick-lk pick-sl pick-h')
			.attr('data-value','');

			var
				_C = new Date(get_current_full()),
				_S = new Date(get_current_full()),
				_L = new Date(get_current_full()),
				_NUM = function(d){
					var
						m = d.getMonth(),
						y = d.getFullYear();
					var l = ((y % 4) == 0 && ((y % 100) != 0 || (y % 400) == 0));
					return [31, (l ? 29 : 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][m];
				};

			_L.setMonth(_L.getMonth()-1);
			_S.setDate(1);

			var
				o = _S.getDay()-1;
				if(o<0)
					o = 6;
				if(i18n[pickers[picker.id].lang].gregorian) {
					o--;
					if(o<0)
						o=6;
				}

			//before
			for(var i = _NUM(_L)-o ; i <= _NUM(_L) ; i++) {
				w.find('li').eq(index)
				.html(i)
				.addClass('pick-b pick-n pick-h');
				index++;
			}
			//current
			for(var i = 1 ; i <= _NUM(_S) ; i++) {
				w.find('li').eq(index)
				.html(i)
				.addClass('pick-n pick-v')
				.attr('data-value',i);
				index++;
			}
			//after
			if(w.find('li.pick-n').length < 42) {
				var
					e = 42 - w.find('li.pick-n').length;
				for(var i = 1 ; i <= e; i++) {
					w.find('li').eq(index).html(i)
					.addClass('pick-a pick-n pick-h');
					index++;
				}
			}
			if(pickers[picker.id].lock) {
				if(pickers[picker.id].lock==='from') {
					if(get_current('y')<=get_today('y')) {
						if(get_current('m')==get_today('m')) {
							get_picker_els('.pick-lg .pick-lg-b li.pick-v[data-value="'+get_today('d')+'"]')
							.prevAll('li')
							.addClass('pick-lk')
						}
						else {
							if(get_current('m')<get_today('m')) {
								get_picker_els('.pick-lg .pick-lg-b li')
								.addClass('pick-lk')
							}
							else if(get_current('m')>get_today('m')&&get_current('y')<get_today('y')) {
								get_picker_els('.pick-lg .pick-lg-b li')
								.addClass('pick-lk')
							}
						}
					}
				}
				else {
					if(get_current('y')>=get_today('y')) {
						if(get_current('m')==get_today('m')) {
							get_picker_els('.pick-lg .pick-lg-b li.pick-v[data-value="'+get_today('d')+'"]')
							.nextAll('li')
							.addClass('pick-lk')
						}
						else {
							if(get_current('m')>get_today('m')) {
								get_picker_els('.pick-lg .pick-lg-b li')
								.addClass('pick-lk')
							}
							else if(get_current('m')<get_today('m')&&get_current('y')>get_today('y')) {
								get_picker_els('.pick-lg .pick-lg-b li')
								.addClass('pick-lk')
							}
						}
					}
				}
			}
			if(pickers[picker.id].disabledays) {
				$.each(pickers[picker.id].disabledays, function( i, v ) {
					if(v&&is_date(v)) {
						var
							d = new Date(v*1000);
						if(d.getMonth()+1==get_current('m')&&d.getFullYear()==get_current('y'))
							get_picker_els('.pick-lg .pick-lg-b li.pick-v[data-value="'+d.getDate()+'"]')
							.addClass('pick-lk');
					}
				});
			}

			get_picker_els('.pick-lg-b li.pick-v[data-value='+get_current('d')+']').addClass('pick-sl');

		},
		picker_fills = function() {

			var
				m = get_current('m'),
				y = get_current('y'),
				l = ((y % 4) == 0 && ((y % 100) != 0 || (y % 400) == 0));

			pickers[picker.id].key['d'].max =  [31, (l ? 29 : 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][m-1];

			if(get_current('d')>pickers[picker.id].key['d'].max) {
				pickers[picker.id].key['d'].current = pickers[picker.id].key['d'].max;
				picker_ul_transition('d',get_current('d'));
			}

			get_picker_els('.pick-d li')
			.removeClass('pick-wke')
			.each(function() {
				var
					d = new Date(m+"/"+$(this).attr('value')+"/"+y).getDay();

				$(this)
				.find('span')
				.html(i18n[pickers[picker.id].lang].weekdays.full[d]);

				if(d==0||d==6)
					$(this).addClass('pick-wke');

			});

			if(picker.element.hasClass('picker-lg')) {
				get_picker_els('.pick-lg-b li').removeClass('pick-wke');
				get_picker_els('.pick-lg-b li.pick-v')
				.each(function() {
					var
						d = new Date(m+"/"+$(this).attr('data-value')+"/"+y).getDay();
					if(d==0||d==6)
						$(this).addClass('pick-wke');

				});
			}

		},
		picker_set = function() {
			if(picker.element.hasClass('picker-lg'))
				picker_render_calendar();
			picker_fills();
			input_change_value();
		},

		// ACTION FUNCTIONS

		picker_ul_transition = function(k,i) {

			var
				ul = get_ul(k);

			ul.find('li').removeClass('pick-sl pick-bfr pick-afr');

			if(i==get_eq(k,'last')) {
				var li = ul.find('li[value="'+get_eq(k,'first')+'"]');
				li.clone().insertAfter(ul.find('li[value='+i+']'));
				li.remove();
			}
			if(i==get_eq(k,'first')) {
				var li = ul.find('li[value="'+get_eq(k,'last')+'"]');
				li.clone().insertBefore(ul.find('li[value='+i+']'));
				li.remove();
			}

			ul.find('li[value='+i+']').addClass('pick-sl');
			ul.find('li.pick-sl').nextAll('li').addClass('pick-afr');
			ul.find('li.pick-sl').prevAll('li').addClass('pick-bfr');

		},
		picker_values_increase = function(k,v) {

			var
				key_values = pickers[picker.id].key[k];

			if(v>key_values.max) {
				if(k=='d')
					picker_ul_turn('m','right');
				if(k=='m')
					picker_ul_turn('y','right');
				v = key_values.min;
			}
			if(v<key_values.min) {
				if(k=='d')
					picker_ul_turn('m','left');
				if(k=='m')
					picker_ul_turn('y','left');
				v = key_values.max;
			}
			pickers[picker.id].key[k].current = v;
			picker_ul_transition(k,v);

		},
		picker_ul_turn = function(k,d) {
			var
				v = get_current(k);
			if(d=='right')
				v++;
			else
				v--;
			picker_values_increase(k,v);
		},
		picker_alrt = function() {
			picker.element
			.addClass('picker-rmbl');
		},

		/* INPUT FUNCTIONS */

		input_fill = function(n) {
			return n < 10 ? '0' + n : n
		},
		input_ordinal_suffix = function(n) {
			var
				s=["th","st","nd","rd"],
				v=n%100;
			return n+(s[(v-20)%10]||s[v]||s[0]);
		},
		input_change_value = function() {

			if(!is_locked()&&picker_ctrl) {

				var
					d = get_current('d'),
					m = get_current('m'),
					y = get_current('y'),
					get_day = new Date(m+"/"+d+"/"+y).getDay(),

					str =
					pickers[picker.id].format
					.replace(/\b(d)\b/g, input_fill(d))
					.replace(/\b(m)\b/g, input_fill(m))
					.replace(/\b(S)\b/g, input_ordinal_suffix(d)) //new
					.replace(/\b(Y)\b/g, y)
					.replace(/\b(U)\b/g, get_unix(get_current_full())) //new
					.replace(/\b(D)\b/g, i18n[pickers[picker.id].lang].weekdays.short[get_day])
					.replace(/\b(l)\b/g, i18n[pickers[picker.id].lang].weekdays.full[get_day])
					.replace(/\b(F)\b/g, i18n[pickers[picker.id].lang].months.full[m-1])
					.replace(/\b(M)\b/g, i18n[pickers[picker.id].lang].months.short[m-1])
					.replace(/\b(n)\b/g, m)
					.replace(/\b(j)\b/g, d);

				picker
				.input
				.val(str)
				.change();

				picker_ctrl = false;

			}

		};

	// GET UI EVENT

	if(is_touch())
		var
			ui_event = {
				i : 'touchstart',
				m	: 'touchmove',
				e : 'touchend'
			}
	else
		var
			ui_event = {
				i : 'mousedown',
				m	: 'mousemove',
				e : 'mouseup'
			}


	var
		picker_node_el = 'div.datedropper.picker-focus';

	$(document)


	//CLOSE PICKER
	.on('click',function(e) {
		if(picker) {
			if(!picker.input.is(e.target) && !picker.element.is(e.target) && picker.element.has(e.target).length === 0) {
				picker_hide();
				pick_dragged = null;
			}
		}
	})

	//LOCK ANIMATION
	.on(csse.a,picker_node_el + '.picker-rmbl',function(){
		if(picker.element.hasClass('picker-rmbl'))
			$(this).removeClass('picker-rmbl');
	})

	//HIDE MODAL OVERLAY
	.on(csse.t,'.picker-modal-overlay',function(){
		$(this).remove();
	})


	//LARGE-MODE DAY CLICK
	.on(ui_event.i,picker_node_el+' .pick-lg li.pick-v',function(){
		get_picker_els('.pick-lg-b li').removeClass('pick-sl');
		$(this).addClass('pick-sl');
		pickers[picker.id].key['d'].current = $(this).attr('data-value');
		picker_ul_transition('d',$(this).attr('data-value'));
		picker_ctrl = true;
	})

	//BUTTON LARGE-MODE
	.on('click',picker_node_el+' .pick-btn-sz',function(){
		picker_large_onoff();
	})

	//BUTTON TRANSLATE-MODE
	.on('click',picker_node_el+' .pick-btn-lng',function(){
		picker_translate_onoff();
	})

	//JUMP
	.on(ui_event.i,picker_node_el+' .pick-arw.pick-arw-s2',function(e){

		e.preventDefault();
		pick_dragged = null;

		var
			i,
			k = $(this).closest('ul').data('k'),
			jump = pickers[picker.id].jump;

		if($(this).hasClass('pick-arw-r'))
			i = get_current('y') + jump;
		else
			i = get_current('y') - jump;

		var
			jumped_array = get_jumped('y',jump);

		if(i>jumped_array[jumped_array.length-1])
			i = jumped_array[0];
		if(i<jumped_array[0])
			i = jumped_array[jumped_array.length-1];

		pickers[picker.id].key['y'].current = i;
		picker_ul_transition('y',get_current('y'));

		picker_ctrl = true;

	})

	//DEFAULT ARROW
	.on(ui_event.i,picker_node_el+' .pick-arw.pick-arw-s1',function(e){
		e.preventDefault();
		pick_dragged = null;
		var
			k = $(this).closest('ul').data('k');
		if($(this).hasClass('pick-arw-r'))
			picker_ul_turn(k,'right');
		else
			picker_ul_turn(k,'left');

		picker_ctrl = true;

	})

	// JUMP
	.on(ui_event.i,picker_node_el+' ul.pick.pick-y li',function(){
		is_click = true;
	})
	.on(ui_event.e,picker_node_el+' ul.pick.pick-y li',function(){
		if(is_click&&is_jumpable()) {
			$(this).closest('ul').toggleClass('pick-jump');
			var
				jumped = get_closest_jumped(get_current('y'),get_jumped('y',pickers[picker.id].jump));
			pickers[picker.id].key['y'].current = jumped;
			picker_ul_transition('y',get_current('y'));
			is_click = false;
		}
	})

	//TOGGLE CALENDAR
	.on(ui_event.i,picker_node_el+' ul.pick.pick-d li',function(){
		is_click = true;
	})
	.on(ui_event.e,picker_node_el+' ul.pick.pick-d li',function(){
		if(is_click) {
			picker_large_onoff();
			is_click = false;
		}
	})

	//TOGGLE TRANSLATE MODE
	.on(ui_event.i,picker_node_el+' ul.pick.pick-l li',function(){
		is_click = true;
	})
	.on(ui_event.e,picker_node_el+' ul.pick.pick-l li',function(){
		if(is_click) {
			picker_translate_onoff();
			picker_translate($(this).val());
			is_click = false;
		}
	})

	//MOUSEDOWN ON UL
	.on(ui_event.i,picker_node_el+' ul.pick',function(e){
		pick_dragged = $(this);
		if(pick_dragged) {
			var
				k = pick_dragged.data('k');
			pick_drag_offset = is_touch() ? e.originalEvent.touches[0].pageY : e.pageY;
			pick_drag_temp = get_current(k);
		}
	})

	//MOUSEMOVE ON UL
	.on(ui_event.m,function(e){

		is_click = false;

		if(pick_dragged) {
			e.preventDefault();
			var
				k = pick_dragged.data('k');
				o = is_touch() ? e.originalEvent.touches[0].pageY : e.pageY;
			o = pick_drag_offset - o;
			o = Math.round(o * .026);
			i = pick_drag_temp + o;
			var
				int = get_clear(k,i);
			if(int!=pickers[picker.id].key[k].current)
				picker_values_increase(k,int);

			picker_ctrl = true;
		}
	})

	//MOUSEUP ON UL
	.on(ui_event.e,function(e){
		if( pick_dragged )
			pick_dragged = null,
			pick_drag_offset = null,
			pick_drag_temp = null;
		if(picker)
			picker_set();
	})

	//CLICK SUBMIT
	.on(ui_event.i,picker_node_el+' .pick-submit',function(){
		picker_hide();
	});

	$(window).resize(function(){
		if(picker) {
			picker_offset();
			is_fx_mobile();
		}
	});

	$.fn.dateDropper = function(options) {
		return $(this).each(function(){
			if($(this).is('input')&&!$(this).hasClass('picker-input')) {

				var
					input = $(this),
					id = 'datedropper-' + Object.keys(pickers).length;

				input
				.attr('data-id',id)
				.addClass('picker-input')
				.prop({
					'type':'text',
					'readonly' : true
				});

				var
					picker_default_date = (input.data('default-date')&&is_date(input.data('default-date'))) ? input.data('default-date') : null,
					picker_disabled_days = (input.data('disabled-days')) ? input.data('disabled-days').split(',') : null,
					picker_format = input.data('format') || 'm/d/Y',
					picker_fx = (input.data('fx')===false) ? input.data('fx') : true,
					picker_fx_class = (input.data('fx')===false) ? '' : 'picker-fxs',
					picker_fx_mobile = (input.data('fx-mobile')===false) ? input.data('fx-mobile') : true,
					picker_init_set = (input.data('init-set')===false) ? false : true,
					picker_lang = (input.data('lang')&&(input.data('lang') in i18n)) ? input.data('lang') : 'en',
					picker_large = (input.data('large-mode')===true) ? true : false,
					picker_large_class = (input.data('large-default')===true && picker_large===true) ? 'picker-lg' : '',
					picker_lock = (input.data('lock')=='from'||input.data('lock')=='to') ? input.data('lock') : false,
					picker_jump = (input.data('jump')&&is_int(input.data('jump'))) ? input.data('jump') : 10,
					picker_max_year = (input.data('max-year')&&is_int(input.data('max-year'))) ? input.data('max-year') : new Date().getFullYear(),
					picker_min_year = (input.data('min-year')&&is_int(input.data('min-year'))) ? input.data('min-year') : 1970,

					picker_modal = (input.data('modal')===true) ? 'picker-modal' : '',
					picker_theme = input.data('theme') || 'primary',
					picker_translate_mode = (input.data('translate-mode')===true) ? true : false;

				if(picker_disabled_days) {
					$.each(picker_disabled_days, function( index, value ) {
						if(value&&is_date(value))
							picker_disabled_days[index] = get_unix(value);
					});
				}

				pickers[id] = {
					disabledays : picker_disabled_days,
					format : picker_format,
					fx : picker_fx,
					fxmobile : picker_fx_mobile,
					lang : picker_lang,
					large : picker_large,
					lock : picker_lock,
					jump : picker_jump,
					key : {
						m : {
							min : 1,
							max : 12,
							current : 1,
							today : (new Date().getMonth()+1)
						},
						d : {
							min : 1,
							max : 31,
							current : 1,
							today : new Date().getDate()
						},
						y : {
							min : picker_min_year,
							max : picker_max_year,
							current : picker_min_year,
							today : new Date().getFullYear()
						},
						l : {
							min : 0,
							max : Object.keys(i18n).length-1,
							current : 0,
							today : 0
						}
					},
					translate : picker_translate_mode
				};

				if(picker_default_date) {

					var regex = /\d+/g;
					var string = picker_default_date;
					var matches = string.match(regex);

					$.each(matches, function( index, value ) {
						matches[index] = parseInt(value);
					});

					pickers[id].key.m.today = (matches[0]&&matches[0]<=12) ? matches[0] : pickers[id].key.m.today;
					pickers[id].key.d.today = (matches[1]&&matches[1]<=31) ? matches[1] : pickers[id].key.d.today;
					pickers[id].key.y.today = (matches[2]) ? matches[2] : pickers[id].key.y.today;

					if(pickers[id].key.y.today>pickers[id].key.y.max)
						pickers[id].key.y.max = pickers[id].key.y.today;
					if(pickers[id].key.y.today<pickers[id].key.y.min)
						pickers[id].key.y.min = pickers[id].key.y.today;

				}

				$('<div>', {
					class: 'datedropper ' + picker_modal + ' ' + picker_theme + ' ' + picker_fx_class + ' ' + picker_large_class,
					id: id,
					html: $('<div>', {
						class: 'picker'
					})
				})
				.appendTo('body');

				picker = {
					id : id,
					input : input,
					element : $('#' + id)
				};

				for( var k in pickers[id].key ) {
					$('<ul>', {
						class: 'pick pick-' + k,
						'data-k' : k
					})
					.appendTo(get_picker_els('.picker'));
					picker_render_ul(k);
				}

				if(pickers[id].large) {

					//calendar
					$('<div>', {
						class: 'pick-lg'
					})
					.insertBefore(get_picker_els('.pick-d'));

					$('<ul class="pick-lg-h"></ul><ul class="pick-lg-b"></ul>')
					.appendTo(get_picker_els('.pick-lg'));

					var
						picker_day_offset = get_days_array();

					for(var i = 0; i < 7 ; i++) {
						$('<li>', {
							html: i18n[pickers[picker.id].lang].weekdays.short[picker_day_offset[i]]
						})
						.appendTo(get_picker_els('.pick-lg .pick-lg-h'))
					}
					for(var i = 0; i < 42 ; i++) {
						$('<li>')
						.appendTo(get_picker_els('.pick-lg .pick-lg-b'))
					}
				}

				//buttons
				$('<div>', {
					class: 'pick-btns'
				})
				.appendTo(get_picker_els('.picker'));

				$('<div>', {
					class: 'pick-submit'
				})
				.appendTo(get_picker_els('.pick-btns'));

				if(pickers[picker.id].translate) {
					$('<div>', {
						class: 'pick-btn pick-btn-lng'
					})
					.appendTo(get_picker_els('.pick-btns'));
				}
				if(pickers[picker.id].large) {
					$('<div>', {
						class: 'pick-btn pick-btn-sz'
					})
					.appendTo(get_picker_els('.pick-btns'));
				}

				if(picker_format=='Y'||picker_format=='m') {
					get_picker_els('.pick-d,.pick-btn-sz').hide();
					picker.element.addClass('picker-tiny');
					if(picker_format=='Y')
						get_picker_els('.pick-m,.pick-btn-lng').hide();
					if(picker_format=='m')
						get_picker_els('.pick-y').hide();
				}

				if(picker_init_set) {
					picker_ctrl = true;
					input_change_value();
				}

				picker = null;

			}

		})
		.focus(function(e){

			e.preventDefault();
			$(this).blur();

			if(picker)
				picker_hide();

			picker = {
				id : $(this).data('id'),
				input : $(this),
				element : $('#'+$(this).data('id'))
			};

			is_fx_mobile();
			picker_offset();
			picker_set();
			picker_show();

			if(picker.element.hasClass('picker-modal'))
				$('body').append('<div class="picker-modal-overlay"></div>')

		});
	};
}(jQuery));


( function($) {
	$(document).ready(function(){
	
		$('#price-range-submit').hide();

		$("#min_price,#max_price").on('change', function () {

		  $('#price-range-submit').show();

		  var min_price_range = parseInt($("#min_price").val());

		  var max_price_range = parseInt($("#max_price").val());

		  if (min_price_range > max_price_range) {
			$('#max_price').val(min_price_range);
		  }

		  $("#slider-range").slider({
			values: [min_price_range, max_price_range]
		  });
		  
		});


		$("#min_price,#max_price").on("paste keyup", function () {                                        

		  $('#price-range-submit').show();

		  var min_price_range = parseInt($("#min_price").val());

		  var max_price_range = parseInt($("#max_price").val());
		  
		  if(min_price_range == max_price_range){

				max_price_range = min_price_range + 100;
				
				$("#min_price").val(min_price_range);		
				$("#max_price").val(max_price_range);
		  }

		  $("#slider-range").slider({
			values: [min_price_range, max_price_range]
		  });

		});


		$(function () {
		  $("#slider-range").slider({
			range: true,
			orientation: "horizontal",
			min: 0,
			max: 10000,
			values: [0, 10000],
			step: 100,

			slide: function (event, ui) {
				if (ui.values[0] == ui.values[1]) {
				  return false;
			  }
			  
			  $("#min_price").val(ui.values[0]);
			  $("#max_price").val(ui.values[1]);
			}
		  });

		});

		$("#slider-range,#price-range-submit").click(function () {

		  var min_price = $('#min_price').val();
		  var max_price = $('#max_price').val();

		  $("#searchResults").text("From $" + min_price  +" "+ "to $" + " "+ max_price + ".");
		});

	});
}(jQuery));
( function($){
  clientWidth = document.body.clientWidth;
  $navToggle = $('.js-nav-toggle');
  $navigation = $('.main-navigation');
  
  function adjustMenu() {
    if (clientWidth < 992) {
      $('.main-navigation li').unbind('mouseenter mouseleave');
      $('a.parent')
        .unbind('click')
        .bind('click', function() {
          $(this)
            .parent('li')
            .toggleClass('in-active');
          $(this).toggleClass('in-active');
          return false;
        });
    } else if (clientWidth >= 992) {
      $('.main-navigation li').removeClass('in-active');
      $('.main-navigation li a').unbind('click');
      $('a.parent')
        .removeClass('in-active')
        .on('click', e => e.preventDefault());
      $('.main-navigation li')
        .unbind('mouseenter mouseleave')
        .bind('mouseenter mouseleave', function() {
          $(this).toggleClass('in-active');
        });
    }
  }

  $(document).ready(() => {
    $('.main-navigation a:not(:only-child)').each(function() {
      $(this).addClass('parent');
    });
    $navToggle.on('click', e => {
      e.preventDefault();
      $navToggle.toggleClass('in-active');
      $navigation.slideToggle(300);
    });

    adjustMenu();
  });

  $(window).on('resize orientationchange', () => {
    clientWidth = document.body.clientWidth;
    adjustMenu();
  });

} (jQuery));

( function($) {
	$( ".faq-toggle" ).on( 'click', function() {
		var t      = $( this );

		if ( t.hasClass( 'active' ) ) {
			t.removeClass( 'active' );
			t.find( '.none-faq' ).slideUp( 350 );
		} else {
			t.siblings().removeClass( 'active' );
			t.parent().find( '.none-faq' ).slideUp( 350 );
			t.toggleClass( 'active' );
			t.find( '.none-faq' ).slideToggle( 350 );
		}
	});
} (jQuery));
jQuery(document).ready(function($){
    $(".all").isotope({
        itemSelector: '.isotope-item',
        layoutMode: 'fitRows',
    });
    
    $('ul.filter li').click(function(){ 
        
      $("ul.filter li").removeClass("active");
      $(this).addClass("active");        

        var selector = $(this).attr('data-filter'); 
        $(".all").isotope({ 
            filter: selector, 
            animationOptions: { 
                duration: 750, 
                easing: 'linear', 
                queue: false, 
            } 
        }); 
      return false; 
    }); 
    
});
!function(t,e){"function"==typeof define&&define.amd?define(e):"object"==typeof module&&module.exports?module.exports=e():t.getSize=e()}(window,function(){"use strict";function t(t){var e=parseFloat(t),i=t.indexOf("%")==-1&&!isNaN(e);return i&&e}function e(){}function i(){for(var t={width:0,height:0,innerWidth:0,innerHeight:0,outerWidth:0,outerHeight:0},e=0;e<g;e++){var i=a[e];t[i]=0}return t}function o(t){var e=getComputedStyle(t);return e||h("Style returned "+e+". Are you running this code in a hidden iframe on Firefox? See https://bit.ly/getsizebug1"),e}function r(){if(!p){p=!0;var e=document.createElement("div");e.style.width="200px",e.style.padding="1px 2px 3px 4px",e.style.borderStyle="solid",e.style.borderWidth="1px 2px 3px 4px",e.style.boxSizing="border-box";var i=document.body||document.documentElement;i.appendChild(e);var r=o(e);n=200==Math.round(t(r.width)),d.isBoxSizeOuter=n,i.removeChild(e)}}function d(e){if(r(),"string"==typeof e&&(e=document.querySelector(e)),e&&"object"==typeof e&&e.nodeType){var d=o(e);if("none"==d.display)return i();var h={};h.width=e.offsetWidth,h.height=e.offsetHeight;for(var p=h.isBorderBox="border-box"==d.boxSizing,u=0;u<g;u++){var f=a[u],m=d[f],s=parseFloat(m);h[f]=isNaN(s)?0:s}var l=h.paddingLeft+h.paddingRight,c=h.paddingTop+h.paddingBottom,b=h.marginLeft+h.marginRight,x=h.marginTop+h.marginBottom,y=h.borderLeftWidth+h.borderRightWidth,v=h.borderTopWidth+h.borderBottomWidth,W=p&&n,w=t(d.width);w!==!1&&(h.width=w+(W?0:l+y));var B=t(d.height);return B!==!1&&(h.height=B+(W?0:c+v)),h.innerWidth=h.width-(l+y),h.innerHeight=h.height-(c+v),h.outerWidth=h.width+b,h.outerHeight=h.height+x,h}}var n,h="undefined"==typeof console?e:function(t){console.error(t)},a=["paddingLeft","paddingRight","paddingTop","paddingBottom","marginLeft","marginRight","marginTop","marginBottom","borderLeftWidth","borderRightWidth","borderTopWidth","borderBottomWidth"],g=a.length,p=!1;return d});
!function(e,t){"use strict";"function"==typeof define&&define.amd?define(t):"object"==typeof module&&module.exports?module.exports=t():e.matchesSelector=t()}(window,function(){"use strict";var e=function(){var e=window.Element.prototype;if(e.matches)return"matches";if(e.matchesSelector)return"matchesSelector";for(var t=["webkit","moz","ms","o"],o=0;o<t.length;o++){var r=t[o],n=r+"MatchesSelector";if(e[n])return n}}();return function(t,o){return t[e](o)}});
!function(e,t){"function"==typeof define&&define.amd?define(t):"object"==typeof module&&module.exports?module.exports=t():e.EvEmitter=t()}("undefined"!=typeof window?window:this,function(){"use strict";function e(){}var t=e.prototype;return t.on=function(e,t){if(e&&t){var n=this._events=this._events||{},i=n[e]=n[e]||[];return i.indexOf(t)==-1&&i.push(t),this}},t.once=function(e,t){if(e&&t){this.on(e,t);var n=this._onceEvents=this._onceEvents||{},i=n[e]=n[e]||{};return i[t]=!0,this}},t.off=function(e,t){var n=this._events&&this._events[e];if(n&&n.length){var i=n.indexOf(t);return i!=-1&&n.splice(i,1),this}},t.emitEvent=function(e,t){var n=this._events&&this._events[e];if(n&&n.length){n=n.slice(0),t=t||[];for(var i=this._onceEvents&&this._onceEvents[e],s=0;s<n.length;s++){var o=n[s],f=i&&i[o];f&&(this.off(e,o),delete i[o]),o.apply(this,t)}return this}},t.allOff=function(){delete this._events,delete this._onceEvents},e});
!function(e,t){"function"==typeof define&&define.amd?define(["desandro-matches-selector/matches-selector"],function(r){return t(e,r)}):"object"==typeof module&&module.exports?module.exports=t(e,require("desandro-matches-selector")):e.fizzyUIUtils=t(e,e.matchesSelector)}(window,function(e,t){"use strict";var r={};r.extend=function(e,t){for(var r in t)e[r]=t[r];return e},r.modulo=function(e,t){return(e%t+t)%t};var n=Array.prototype.slice;r.makeArray=function(e){if(Array.isArray(e))return e;if(null===e||void 0===e)return[];var t="object"==typeof e&&"number"==typeof e.length;return t?n.call(e):[e]},r.removeFrom=function(e,t){var r=e.indexOf(t);r!=-1&&e.splice(r,1)},r.getParent=function(e,r){for(;e.parentNode&&e!=document.body;)if(e=e.parentNode,t(e,r))return e},r.getQueryElement=function(e){return"string"==typeof e?document.querySelector(e):e},r.handleEvent=function(e){var t="on"+e.type;this[t]&&this[t](e)},r.filterFindElements=function(e,n){e=r.makeArray(e);var o=[];return e.forEach(function(e){if(e instanceof HTMLElement){if(!n)return void o.push(e);t(e,n)&&o.push(e);for(var r=e.querySelectorAll(n),u=0;u<r.length;u++)o.push(r[u])}}),o},r.debounceMethod=function(e,t,r){r=r||100;var n=e.prototype[t],o=t+"Timeout";e.prototype[t]=function(){var e=this[o];clearTimeout(e);var t=arguments,u=this;this[o]=setTimeout(function(){n.apply(u,t),delete u[o]},r)}},r.docReady=function(e){var t=document.readyState;"complete"==t||"interactive"==t?setTimeout(e):document.addEventListener("DOMContentLoaded",e)},r.toDashed=function(e){return e.replace(/(.)([A-Z])/g,function(e,t,r){return t+"-"+r}).toLowerCase()};var o=e.console;return r.htmlInit=function(t,n){r.docReady(function(){var u=r.toDashed(n),a="data-"+u,i=document.querySelectorAll("["+a+"]"),c=document.querySelectorAll(".js-"+u),d=r.makeArray(i).concat(r.makeArray(c)),f=a+"-options",s=e.jQuery;d.forEach(function(e){var r,u=e.getAttribute(a)||e.getAttribute(f);try{r=u&&JSON.parse(u)}catch(i){return void(o&&o.error("Error parsing "+a+" on "+e.className+": "+i))}var c=new t(e,r);s&&s.data(e,n,c)})})},r});
!function(t,n){"function"==typeof define&&define.amd?define(["jquery"],function(i){return n(t,i)}):"object"==typeof module&&module.exports?module.exports=n(t,require("jquery")):t.jQueryBridget=n(t,t.jQuery)}(window,function(t,n){"use strict";function i(i,r,a){function f(t,n,e){var o,r="$()."+i+'("'+n+'")';return t.each(function(t,f){var d=a.data(f,i);if(!d)return void u(i+" not initialized. Cannot call methods, i.e. "+r);var c=d[n];if(!c||"_"==n.charAt(0))return void u(r+" is not a valid method");var p=c.apply(d,e);o=void 0===o?p:o}),void 0!==o?o:t}function d(t,n){t.each(function(t,e){var o=a.data(e,i);o?(o.option(n),o._init()):(o=new r(e,n),a.data(e,i,o))})}a=a||n||t.jQuery,a&&(r.prototype.option||(r.prototype.option=function(t){a.isPlainObject(t)&&(this.options=a.extend(!0,this.options,t))}),a.fn[i]=function(t){if("string"==typeof t){var n=o.call(arguments,1);return f(this,t,n)}return d(this,t),this},e(a))}function e(t){!t||t&&t.bridget||(t.bridget=i)}var o=Array.prototype.slice,r=t.console,u="undefined"==typeof r?function(){}:function(t){r.error(t)};return e(n||t.jQuery),i});
!function(t,i){"function"==typeof define&&define.amd?define(["ev-emitter/ev-emitter","get-size/get-size"],i):"object"==typeof module&&module.exports?module.exports=i(require("ev-emitter"),require("get-size")):(t.Outlayer={},t.Outlayer.Item=i(t.EvEmitter,t.getSize))}(window,function(t,i){"use strict";function n(t){for(var i in t)return!1;return i=null,!0}function o(t,i){t&&(this.element=t,this.layout=i,this.position={x:0,y:0},this._create())}function e(t){return t.replace(/([A-Z])/g,function(t){return"-"+t.toLowerCase()})}var s=document.documentElement.style,r="string"==typeof s.transition?"transition":"WebkitTransition",a="string"==typeof s.transform?"transform":"WebkitTransform",h={WebkitTransition:"webkitTransitionEnd",transition:"transitionend"}[r],l={transform:a,transition:r,transitionDuration:r+"Duration",transitionProperty:r+"Property",transitionDelay:r+"Delay"},u=o.prototype=Object.create(t.prototype);u.constructor=o,u._create=function(){this._transn={ingProperties:{},clean:{},onEnd:{}},this.css({position:"absolute"})},u.handleEvent=function(t){var i="on"+t.type;this[i]&&this[i](t)},u.getSize=function(){this.size=i(this.element)},u.css=function(t){var i=this.element.style;for(var n in t){var o=l[n]||n;i[o]=t[n]}},u.getPosition=function(){var t=getComputedStyle(this.element),i=this.layout._getOption("originLeft"),n=this.layout._getOption("originTop"),o=t[i?"left":"right"],e=t[n?"top":"bottom"],s=parseFloat(o),r=parseFloat(e),a=this.layout.size;o.indexOf("%")!=-1&&(s=s/100*a.width),e.indexOf("%")!=-1&&(r=r/100*a.height),s=isNaN(s)?0:s,r=isNaN(r)?0:r,s-=i?a.paddingLeft:a.paddingRight,r-=n?a.paddingTop:a.paddingBottom,this.position.x=s,this.position.y=r},u.layoutPosition=function(){var t=this.layout.size,i={},n=this.layout._getOption("originLeft"),o=this.layout._getOption("originTop"),e=n?"paddingLeft":"paddingRight",s=n?"left":"right",r=n?"right":"left",a=this.position.x+t[e];i[s]=this.getXValue(a),i[r]="";var h=o?"paddingTop":"paddingBottom",l=o?"top":"bottom",u=o?"bottom":"top",d=this.position.y+t[h];i[l]=this.getYValue(d),i[u]="",this.css(i),this.emitEvent("layout",[this])},u.getXValue=function(t){var i=this.layout._getOption("horizontal");return this.layout.options.percentPosition&&!i?t/this.layout.size.width*100+"%":t+"px"},u.getYValue=function(t){var i=this.layout._getOption("horizontal");return this.layout.options.percentPosition&&i?t/this.layout.size.height*100+"%":t+"px"},u._transitionTo=function(t,i){this.getPosition();var n=this.position.x,o=this.position.y,e=t==this.position.x&&i==this.position.y;if(this.setPosition(t,i),e&&!this.isTransitioning)return void this.layoutPosition();var s=t-n,r=i-o,a={};a.transform=this.getTranslate(s,r),this.transition({to:a,onTransitionEnd:{transform:this.layoutPosition},isCleaning:!0})},u.getTranslate=function(t,i){var n=this.layout._getOption("originLeft"),o=this.layout._getOption("originTop");return t=n?t:-t,i=o?i:-i,"translate3d("+t+"px, "+i+"px, 0)"},u.goTo=function(t,i){this.setPosition(t,i),this.layoutPosition()},u.moveTo=u._transitionTo,u.setPosition=function(t,i){this.position.x=parseFloat(t),this.position.y=parseFloat(i)},u._nonTransition=function(t){this.css(t.to),t.isCleaning&&this._removeStyles(t.to);for(var i in t.onTransitionEnd)t.onTransitionEnd[i].call(this)},u.transition=function(t){if(!parseFloat(this.layout.options.transitionDuration))return void this._nonTransition(t);var i=this._transn;for(var n in t.onTransitionEnd)i.onEnd[n]=t.onTransitionEnd[n];for(n in t.to)i.ingProperties[n]=!0,t.isCleaning&&(i.clean[n]=!0);if(t.from){this.css(t.from);var o=this.element.offsetHeight;o=null}this.enableTransition(t.to),this.css(t.to),this.isTransitioning=!0};var d="opacity,"+e(a);u.enableTransition=function(){if(!this.isTransitioning){var t=this.layout.options.transitionDuration;t="number"==typeof t?t+"ms":t,this.css({transitionProperty:d,transitionDuration:t,transitionDelay:this.staggerDelay||0}),this.element.addEventListener(h,this,!1)}},u.onwebkitTransitionEnd=function(t){this.ontransitionend(t)},u.onotransitionend=function(t){this.ontransitionend(t)};var p={"-webkit-transform":"transform"};u.ontransitionend=function(t){if(t.target===this.element){var i=this._transn,o=p[t.propertyName]||t.propertyName;if(delete i.ingProperties[o],n(i.ingProperties)&&this.disableTransition(),o in i.clean&&(this.element.style[t.propertyName]="",delete i.clean[o]),o in i.onEnd){var e=i.onEnd[o];e.call(this),delete i.onEnd[o]}this.emitEvent("transitionEnd",[this])}},u.disableTransition=function(){this.removeTransitionStyles(),this.element.removeEventListener(h,this,!1),this.isTransitioning=!1},u._removeStyles=function(t){var i={};for(var n in t)i[n]="";this.css(i)};var f={transitionProperty:"",transitionDuration:"",transitionDelay:""};return u.removeTransitionStyles=function(){this.css(f)},u.stagger=function(t){t=isNaN(t)?0:t,this.staggerDelay=t+"ms"},u.removeElem=function(){this.element.parentNode.removeChild(this.element),this.css({display:""}),this.emitEvent("remove",[this])},u.remove=function(){return r&&parseFloat(this.layout.options.transitionDuration)?(this.once("transitionEnd",function(){this.removeElem()}),void this.hide()):void this.removeElem()},u.reveal=function(){delete this.isHidden,this.css({display:""});var t=this.layout.options,i={},n=this.getHideRevealTransitionEndProperty("visibleStyle");i[n]=this.onRevealTransitionEnd,this.transition({from:t.hiddenStyle,to:t.visibleStyle,isCleaning:!0,onTransitionEnd:i})},u.onRevealTransitionEnd=function(){this.isHidden||this.emitEvent("reveal")},u.getHideRevealTransitionEndProperty=function(t){var i=this.layout.options[t];if(i.opacity)return"opacity";for(var n in i)return n},u.hide=function(){this.isHidden=!0,this.css({display:""});var t=this.layout.options,i={},n=this.getHideRevealTransitionEndProperty("hiddenStyle");i[n]=this.onHideTransitionEnd,this.transition({from:t.visibleStyle,to:t.hiddenStyle,isCleaning:!0,onTransitionEnd:i})},u.onHideTransitionEnd=function(){this.isHidden&&(this.css({display:"none"}),this.emitEvent("hide"))},u.destroy=function(){this.css({position:"",left:"",right:"",top:"",bottom:"",transition:"",transform:""})},o});
!function(t,e){"use strict";"function"==typeof define&&define.amd?define(["ev-emitter/ev-emitter","get-size/get-size","fizzy-ui-utils/utils","./item"],function(i,n,s,o){return e(t,i,n,s,o)}):"object"==typeof module&&module.exports?module.exports=e(t,require("ev-emitter"),require("get-size"),require("fizzy-ui-utils"),require("./item")):t.Outlayer=e(t,t.EvEmitter,t.getSize,t.fizzyUIUtils,t.Outlayer.Item)}(window,function(t,e,i,n,s){"use strict";function o(t,e){var i=n.getQueryElement(t);if(!i)return void(h&&h.error("Bad element for "+this.constructor.namespace+": "+(i||t)));this.element=i,u&&(this.$element=u(this.element)),this.options=n.extend({},this.constructor.defaults),this.option(e);var s=++c;this.element.outlayerGUID=s,f[s]=this,this._create();var o=this._getOption("initLayout");o&&this.layout()}function r(t){function e(){t.apply(this,arguments)}return e.prototype=Object.create(t.prototype),e.prototype.constructor=e,e}function a(t){if("number"==typeof t)return t;var e=t.match(/(^\d*\.?\d*)(\w*)/),i=e&&e[1],n=e&&e[2];if(!i.length)return 0;i=parseFloat(i);var s=d[n]||1;return i*s}var h=t.console,u=t.jQuery,m=function(){},c=0,f={};o.namespace="outlayer",o.Item=s,o.defaults={containerStyle:{position:"relative"},initLayout:!0,originLeft:!0,originTop:!0,resize:!0,resizeContainer:!0,transitionDuration:"0.4s",hiddenStyle:{opacity:0,transform:"scale(0.001)"},visibleStyle:{opacity:1,transform:"scale(1)"}};var l=o.prototype;n.extend(l,e.prototype),l.option=function(t){n.extend(this.options,t)},l._getOption=function(t){var e=this.constructor.compatOptions[t];return e&&void 0!==this.options[e]?this.options[e]:this.options[t]},o.compatOptions={initLayout:"isInitLayout",horizontal:"isHorizontal",layoutInstant:"isLayoutInstant",originLeft:"isOriginLeft",originTop:"isOriginTop",resize:"isResizeBound",resizeContainer:"isResizingContainer"},l._create=function(){this.reloadItems(),this.stamps=[],this.stamp(this.options.stamp),n.extend(this.element.style,this.options.containerStyle);var t=this._getOption("resize");t&&this.bindResize()},l.reloadItems=function(){this.items=this._itemize(this.element.children)},l._itemize=function(t){for(var e=this._filterFindItemElements(t),i=this.constructor.Item,n=[],s=0;s<e.length;s++){var o=e[s],r=new i(o,this);n.push(r)}return n},l._filterFindItemElements=function(t){return n.filterFindElements(t,this.options.itemSelector)},l.getItemElements=function(){return this.items.map(function(t){return t.element})},l.layout=function(){this._resetLayout(),this._manageStamps();var t=this._getOption("layoutInstant"),e=void 0!==t?t:!this._isLayoutInited;this.layoutItems(this.items,e),this._isLayoutInited=!0},l._init=l.layout,l._resetLayout=function(){this.getSize()},l.getSize=function(){this.size=i(this.element)},l._getMeasurement=function(t,e){var n,s=this.options[t];s?("string"==typeof s?n=this.element.querySelector(s):s instanceof HTMLElement&&(n=s),this[t]=n?i(n)[e]:s):this[t]=0},l.layoutItems=function(t,e){t=this._getItemsForLayout(t),this._layoutItems(t,e),this._postLayout()},l._getItemsForLayout=function(t){return t.filter(function(t){return!t.isIgnored})},l._layoutItems=function(t,e){if(this._emitCompleteOnItems("layout",t),t&&t.length){var i=[];t.forEach(function(t){var n=this._getItemLayoutPosition(t);n.item=t,n.isInstant=e||t.isLayoutInstant,i.push(n)},this),this._processLayoutQueue(i)}},l._getItemLayoutPosition=function(){return{x:0,y:0}},l._processLayoutQueue=function(t){this.updateStagger(),t.forEach(function(t,e){this._positionItem(t.item,t.x,t.y,t.isInstant,e)},this)},l.updateStagger=function(){var t=this.options.stagger;return null===t||void 0===t?void(this.stagger=0):(this.stagger=a(t),this.stagger)},l._positionItem=function(t,e,i,n,s){n?t.goTo(e,i):(t.stagger(s*this.stagger),t.moveTo(e,i))},l._postLayout=function(){this.resizeContainer()},l.resizeContainer=function(){var t=this._getOption("resizeContainer");if(t){var e=this._getContainerSize();e&&(this._setContainerMeasure(e.width,!0),this._setContainerMeasure(e.height,!1))}},l._getContainerSize=m,l._setContainerMeasure=function(t,e){if(void 0!==t){var i=this.size;i.isBorderBox&&(t+=e?i.paddingLeft+i.paddingRight+i.borderLeftWidth+i.borderRightWidth:i.paddingBottom+i.paddingTop+i.borderTopWidth+i.borderBottomWidth),t=Math.max(t,0),this.element.style[e?"width":"height"]=t+"px"}},l._emitCompleteOnItems=function(t,e){function i(){s.dispatchEvent(t+"Complete",null,[e])}function n(){r++,r==o&&i()}var s=this,o=e.length;if(!e||!o)return void i();var r=0;e.forEach(function(e){e.once(t,n)})},l.dispatchEvent=function(t,e,i){var n=e?[e].concat(i):i;if(this.emitEvent(t,n),u)if(this.$element=this.$element||u(this.element),e){var s=u.Event(e);s.type=t,this.$element.trigger(s,i)}else this.$element.trigger(t,i)},l.ignore=function(t){var e=this.getItem(t);e&&(e.isIgnored=!0)},l.unignore=function(t){var e=this.getItem(t);e&&delete e.isIgnored},l.stamp=function(t){t=this._find(t),t&&(this.stamps=this.stamps.concat(t),t.forEach(this.ignore,this))},l.unstamp=function(t){t=this._find(t),t&&t.forEach(function(t){n.removeFrom(this.stamps,t),this.unignore(t)},this)},l._find=function(t){if(t)return"string"==typeof t&&(t=this.element.querySelectorAll(t)),t=n.makeArray(t)},l._manageStamps=function(){this.stamps&&this.stamps.length&&(this._getBoundingRect(),this.stamps.forEach(this._manageStamp,this))},l._getBoundingRect=function(){var t=this.element.getBoundingClientRect(),e=this.size;this._boundingRect={left:t.left+e.paddingLeft+e.borderLeftWidth,top:t.top+e.paddingTop+e.borderTopWidth,right:t.right-(e.paddingRight+e.borderRightWidth),bottom:t.bottom-(e.paddingBottom+e.borderBottomWidth)}},l._manageStamp=m,l._getElementOffset=function(t){var e=t.getBoundingClientRect(),n=this._boundingRect,s=i(t),o={left:e.left-n.left-s.marginLeft,top:e.top-n.top-s.marginTop,right:n.right-e.right-s.marginRight,bottom:n.bottom-e.bottom-s.marginBottom};return o},l.handleEvent=n.handleEvent,l.bindResize=function(){t.addEventListener("resize",this),this.isResizeBound=!0},l.unbindResize=function(){t.removeEventListener("resize",this),this.isResizeBound=!1},l.onresize=function(){this.resize()},n.debounceMethod(o,"onresize",100),l.resize=function(){this.isResizeBound&&this.needsResizeLayout()&&this.layout()},l.needsResizeLayout=function(){var t=i(this.element),e=this.size&&t;return e&&t.innerWidth!==this.size.innerWidth},l.addItems=function(t){var e=this._itemize(t);return e.length&&(this.items=this.items.concat(e)),e},l.appended=function(t){var e=this.addItems(t);e.length&&(this.layoutItems(e,!0),this.reveal(e))},l.prepended=function(t){var e=this._itemize(t);if(e.length){var i=this.items.slice(0);this.items=e.concat(i),this._resetLayout(),this._manageStamps(),this.layoutItems(e,!0),this.reveal(e),this.layoutItems(i)}},l.reveal=function(t){if(this._emitCompleteOnItems("reveal",t),t&&t.length){var e=this.updateStagger();t.forEach(function(t,i){t.stagger(i*e),t.reveal()})}},l.hide=function(t){if(this._emitCompleteOnItems("hide",t),t&&t.length){var e=this.updateStagger();t.forEach(function(t,i){t.stagger(i*e),t.hide()})}},l.revealItemElements=function(t){var e=this.getItems(t);this.reveal(e)},l.hideItemElements=function(t){var e=this.getItems(t);this.hide(e)},l.getItem=function(t){for(var e=0;e<this.items.length;e++){var i=this.items[e];if(i.element==t)return i}},l.getItems=function(t){t=n.makeArray(t);var e=[];return t.forEach(function(t){var i=this.getItem(t);i&&e.push(i)},this),e},l.remove=function(t){var e=this.getItems(t);this._emitCompleteOnItems("remove",e),e&&e.length&&e.forEach(function(t){t.remove(),n.removeFrom(this.items,t)},this)},l.destroy=function(){var t=this.element.style;t.height="",t.position="",t.width="",this.items.forEach(function(t){t.destroy()}),this.unbindResize();var e=this.element.outlayerGUID;delete f[e],delete this.element.outlayerGUID,u&&u.removeData(this.element,this.constructor.namespace)},o.data=function(t){t=n.getQueryElement(t);var e=t&&t.outlayerGUID;return e&&f[e]},o.create=function(t,e){var i=r(o);return i.defaults=n.extend({},o.defaults),n.extend(i.defaults,e),i.compatOptions=n.extend({},o.compatOptions),i.namespace=t,i.data=o.data,i.Item=r(s),n.htmlInit(i,t),u&&u.bridget&&u.bridget(t,i),i};var d={ms:1,s:1e3};return o.Item=s,o});
!function(e,t){"function"==typeof define&&define.amd?define(["get-size/get-size","outlayer/outlayer"],t):"object"==typeof module&&module.exports?module.exports=t(require("get-size"),require("outlayer")):(e.Isotope=e.Isotope||{},e.Isotope.LayoutMode=t(e.getSize,e.Outlayer))}(window,function(e,t){"use strict";function i(e){this.isotope=e,e&&(this.options=e.options[this.namespace],this.element=e.element,this.items=e.filteredItems,this.size=e.size)}var o=i.prototype,s=["_resetLayout","_getItemLayoutPosition","_manageStamp","_getContainerSize","_getElementOffset","needsResizeLayout","_getOption"];return s.forEach(function(e){o[e]=function(){return t.prototype[e].apply(this.isotope,arguments)}}),o.needsVerticalResizeLayout=function(){var t=e(this.isotope.element),i=this.isotope.size&&t;return i&&t.innerHeight!=this.isotope.size.innerHeight},o._getMeasurement=function(){this.isotope._getMeasurement.apply(this,arguments)},o.getColumnWidth=function(){this.getSegmentSize("column","Width")},o.getRowHeight=function(){this.getSegmentSize("row","Height")},o.getSegmentSize=function(e,t){var i=e+t,o="outer"+t;if(this._getMeasurement(i,o),!this[i]){var s=this.getFirstItemSize();this[i]=s&&s[o]||this.isotope.size["inner"+t]}},o.getFirstItemSize=function(){var t=this.isotope.filteredItems[0];return t&&t.element&&e(t.element)},o.layout=function(){this.isotope.layout.apply(this.isotope,arguments)},o.getSize=function(){this.isotope.getSize(),this.size=this.isotope.size},i.modes={},i.create=function(e,t){function s(){i.apply(this,arguments)}return s.prototype=Object.create(o),s.prototype.constructor=s,t&&(s.options=t),s.prototype.namespace=e,i.modes[e]=s,s},i});
!function(t,o){"function"==typeof define&&define.amd?define(["outlayer/outlayer"],o):"object"==typeof module&&module.exports?module.exports=o(require("outlayer")):(t.Isotope=t.Isotope||{},t.Isotope.Item=o(t.Outlayer))}(window,function(t){"use strict";function o(){t.Item.apply(this,arguments)}var e=o.prototype=Object.create(t.Item.prototype),i=e._create;e._create=function(){this.id=this.layout.itemGUID++,i.call(this),this.sortData={}},e.updateSortData=function(){if(!this.isIgnored){this.sortData.id=this.id,this.sortData["original-order"]=this.id,this.sortData.random=Math.random();var t=this.layout.options.getSortData,o=this.layout._sorters;for(var e in t){var i=o[e];this.sortData[e]=i(this.element,this)}}};var a=e.destroy;return e.destroy=function(){a.apply(this,arguments),this.css({display:""})},o});
!function(t,e){"function"==typeof define&&define.amd?define(["outlayer/outlayer","get-size/get-size","desandro-matches-selector/matches-selector","fizzy-ui-utils/utils","./item","./layout-mode","./layout-modes/masonry","./layout-modes/fit-rows","./layout-modes/vertical"],function(i,r,o,s,n,a){return e(t,i,r,o,s,n,a)}):"object"==typeof module&&module.exports?module.exports=e(t,require("outlayer"),require("get-size"),require("desandro-matches-selector"),require("fizzy-ui-utils"),require("./item"),require("./layout-mode"),require("./layout-modes/masonry"),require("./layout-modes/fit-rows"),require("./layout-modes/vertical")):t.Isotope=e(t,t.Outlayer,t.getSize,t.matchesSelector,t.fizzyUIUtils,t.Isotope.Item,t.Isotope.LayoutMode)}(window,function(t,e,i,r,o,s,n){"use strict";function a(t,e){return function(i,r){for(var o=0;o<t.length;o++){var s=t[o],n=i.sortData[s],a=r.sortData[s];if(n>a||n<a){var u=void 0!==e[s]?e[s]:e,h=u?1:-1;return(n>a?1:-1)*h}}return 0}}var u=t.jQuery,h=String.prototype.trim?function(t){return t.trim()}:function(t){return t.replace(/^\s+|\s+$/g,"")},l=e.create("isotope",{layoutMode:"masonry",isJQueryFiltering:!0,sortAscending:!0});l.Item=s,l.LayoutMode=n;var m=l.prototype;m._create=function(){this.itemGUID=0,this._sorters={},this._getSorters(),e.prototype._create.call(this),this.modes={},this.filteredItems=this.items,this.sortHistory=["original-order"];for(var t in n.modes)this._initLayoutMode(t)},m.reloadItems=function(){this.itemGUID=0,e.prototype.reloadItems.call(this)},m._itemize=function(){for(var t=e.prototype._itemize.apply(this,arguments),i=0;i<t.length;i++){var r=t[i];r.id=this.itemGUID++}return this._updateItemsSortData(t),t},m._initLayoutMode=function(t){var e=n.modes[t],i=this.options[t]||{};this.options[t]=e.options?o.extend(e.options,i):i,this.modes[t]=new e(this)},m.layout=function(){return!this._isLayoutInited&&this._getOption("initLayout")?void this.arrange():void this._layout()},m._layout=function(){var t=this._getIsInstant();this._resetLayout(),this._manageStamps(),this.layoutItems(this.filteredItems,t),this._isLayoutInited=!0},m.arrange=function(t){this.option(t),this._getIsInstant();var e=this._filter(this.items);this.filteredItems=e.matches,this._bindArrangeComplete(),this._isInstant?this._noTransition(this._hideReveal,[e]):this._hideReveal(e),this._sort(),this._layout()},m._init=m.arrange,m._hideReveal=function(t){this.reveal(t.needReveal),this.hide(t.needHide)},m._getIsInstant=function(){var t=this._getOption("layoutInstant"),e=void 0!==t?t:!this._isLayoutInited;return this._isInstant=e,e},m._bindArrangeComplete=function(){function t(){e&&i&&r&&o.dispatchEvent("arrangeComplete",null,[o.filteredItems])}var e,i,r,o=this;this.once("layoutComplete",function(){e=!0,t()}),this.once("hideComplete",function(){i=!0,t()}),this.once("revealComplete",function(){r=!0,t()})},m._filter=function(t){var e=this.options.filter;e=e||"*";for(var i=[],r=[],o=[],s=this._getFilterTest(e),n=0;n<t.length;n++){var a=t[n];if(!a.isIgnored){var u=s(a);u&&i.push(a),u&&a.isHidden?r.push(a):u||a.isHidden||o.push(a)}}return{matches:i,needReveal:r,needHide:o}},m._getFilterTest=function(t){return u&&this.options.isJQueryFiltering?function(e){return u(e.element).is(t)}:"function"==typeof t?function(e){return t(e.element)}:function(e){return r(e.element,t)}},m.updateSortData=function(t){var e;t?(t=o.makeArray(t),e=this.getItems(t)):e=this.items,this._getSorters(),this._updateItemsSortData(e)},m._getSorters=function(){var t=this.options.getSortData;for(var e in t){var i=t[e];this._sorters[e]=d(i)}},m._updateItemsSortData=function(t){for(var e=t&&t.length,i=0;e&&i<e;i++){var r=t[i];r.updateSortData()}};var d=function(){function t(t){if("string"!=typeof t)return t;var i=h(t).split(" "),r=i[0],o=r.match(/^\[(.+)\]$/),s=o&&o[1],n=e(s,r),a=l.sortDataParsers[i[1]];return t=a?function(t){return t&&a(n(t))}:function(t){return t&&n(t)}}function e(t,e){return t?function(e){return e.getAttribute(t)}:function(t){var i=t.querySelector(e);return i&&i.textContent}}return t}();l.sortDataParsers={parseInt:function(t){return parseInt(t,10)},parseFloat:function(t){return parseFloat(t)}},m._sort=function(){if(this.options.sortBy){var t=o.makeArray(this.options.sortBy);this._getIsSameSortBy(t)||(this.sortHistory=t.concat(this.sortHistory));var e=a(this.sortHistory,this.options.sortAscending);this.filteredItems.sort(e)}},m._getIsSameSortBy=function(t){for(var e=0;e<t.length;e++)if(t[e]!=this.sortHistory[e])return!1;return!0},m._mode=function(){var t=this.options.layoutMode,e=this.modes[t];if(!e)throw new Error("No layout mode: "+t);return e.options=this.options[t],e},m._resetLayout=function(){e.prototype._resetLayout.call(this),this._mode()._resetLayout()},m._getItemLayoutPosition=function(t){return this._mode()._getItemLayoutPosition(t)},m._manageStamp=function(t){this._mode()._manageStamp(t)},m._getContainerSize=function(){return this._mode()._getContainerSize()},m.needsResizeLayout=function(){return this._mode().needsResizeLayout()},m.appended=function(t){var e=this.addItems(t);if(e.length){var i=this._filterRevealAdded(e);this.filteredItems=this.filteredItems.concat(i)}},m.prepended=function(t){var e=this._itemize(t);if(e.length){this._resetLayout(),this._manageStamps();var i=this._filterRevealAdded(e);this.layoutItems(this.filteredItems),this.filteredItems=i.concat(this.filteredItems),this.items=e.concat(this.items)}},m._filterRevealAdded=function(t){var e=this._filter(t);return this.hide(e.needHide),this.reveal(e.matches),this.layoutItems(e.matches,!0),e.matches},m.insert=function(t){var e=this.addItems(t);if(e.length){var i,r,o=e.length;for(i=0;i<o;i++)r=e[i],this.element.appendChild(r.element);var s=this._filter(e).matches;for(i=0;i<o;i++)e[i].isLayoutInstant=!0;for(this.arrange(),i=0;i<o;i++)delete e[i].isLayoutInstant;this.reveal(s)}};var f=m.remove;return m.remove=function(t){t=o.makeArray(t);var e=this.getItems(t);f.call(this,t);for(var i=e&&e.length,r=0;i&&r<i;r++){var s=e[r];o.removeFrom(this.filteredItems,s)}},m.shuffle=function(){for(var t=0;t<this.items.length;t++){var e=this.items[t];e.sortData.random=Math.random()}this.options.sortBy="random",this._sort(),this._layout()},m._noTransition=function(t,e){var i=this.options.transitionDuration;this.options.transitionDuration=0;var r=t.apply(this,e);return this.options.transitionDuration=i,r},m.getFilteredItemElements=function(){return this.filteredItems.map(function(t){return t.element})},l});
!function(t,e){"function"==typeof define&&define.amd?define(["../layout-mode"],e):"object"==typeof exports?module.exports=e(require("../layout-mode")):e(t.Isotope.LayoutMode)}(window,function(t){"use strict";var e=t.create("fitRows"),i=e.prototype;return i._resetLayout=function(){this.x=0,this.y=0,this.maxY=0,this._getMeasurement("gutter","outerWidth")},i._getItemLayoutPosition=function(t){t.getSize();var e=t.size.outerWidth+this.gutter,i=this.isotope.size.innerWidth+this.gutter;0!==this.x&&e+this.x>i&&(this.x=0,this.y=this.maxY);var o={x:this.x,y:this.y};return this.maxY=Math.max(this.maxY,this.y+t.size.outerHeight),this.x+=e,o},i._getContainerSize=function(){return{height:this.maxY}},e});
!function(t,e){"function"==typeof define&&define.amd?define(["../layout-mode"],e):"object"==typeof module&&module.exports?module.exports=e(require("../layout-mode")):e(t.Isotope.LayoutMode)}(window,function(t){"use strict";var e=t.create("vertical",{horizontalAlignment:0}),o=e.prototype;return o._resetLayout=function(){this.y=0},o._getItemLayoutPosition=function(t){t.getSize();var e=(this.isotope.size.innerWidth-t.size.outerWidth)*this.options.horizontalAlignment,o=this.y;return this.y+=t.size.outerHeight,{x:e,y:o}},o._getContainerSize=function(){return{height:this.y}},e});
!function(t,i){"function"==typeof define&&define.amd?define(["outlayer/outlayer","get-size/get-size"],i):"object"==typeof module&&module.exports?module.exports=i(require("outlayer"),require("get-size")):t.Masonry=i(t.Outlayer,t.getSize)}(window,function(t,i){"use strict";var o=t.create("masonry");o.compatOptions.fitWidth="isFitWidth";var e=o.prototype;return e._resetLayout=function(){this.getSize(),this._getMeasurement("columnWidth","outerWidth"),this._getMeasurement("gutter","outerWidth"),this.measureColumns(),this.colYs=[];for(var t=0;t<this.cols;t++)this.colYs.push(0);this.maxY=0,this.horizontalColIndex=0},e.measureColumns=function(){if(this.getContainerWidth(),!this.columnWidth){var t=this.items[0],o=t&&t.element;this.columnWidth=o&&i(o).outerWidth||this.containerWidth}var e=this.columnWidth+=this.gutter,h=this.containerWidth+this.gutter,n=h/e,s=e-h%e,r=s&&s<1?"round":"floor";n=Math[r](n),this.cols=Math.max(n,1)},e.getContainerWidth=function(){var t=this._getOption("fitWidth"),o=t?this.element.parentNode:this.element,e=i(o);this.containerWidth=e&&e.innerWidth},e._getItemLayoutPosition=function(t){t.getSize();var i=t.size.outerWidth%this.columnWidth,o=i&&i<1?"round":"ceil",e=Math[o](t.size.outerWidth/this.columnWidth);e=Math.min(e,this.cols);for(var h=this.options.horizontalOrder?"_getHorizontalColPosition":"_getTopColPosition",n=this[h](e,t),s={x:this.columnWidth*n.col,y:n.y},r=n.y+t.size.outerHeight,a=e+n.col,u=n.col;u<a;u++)this.colYs[u]=r;return s},e._getTopColPosition=function(t){var i=this._getTopColGroup(t),o=Math.min.apply(Math,i);return{col:i.indexOf(o),y:o}},e._getTopColGroup=function(t){if(t<2)return this.colYs;for(var i=[],o=this.cols+1-t,e=0;e<o;e++)i[e]=this._getColGroupY(e,t);return i},e._getColGroupY=function(t,i){if(i<2)return this.colYs[t];var o=this.colYs.slice(t,t+i);return Math.max.apply(Math,o)},e._getHorizontalColPosition=function(t,i){var o=this.horizontalColIndex%this.cols,e=t>1&&o+t>this.cols;o=e?0:o;var h=i.size.outerWidth&&i.size.outerHeight;return this.horizontalColIndex=h?o+t:this.horizontalColIndex,{col:o,y:this._getColGroupY(o,t)}},e._manageStamp=function(t){var o=i(t),e=this._getElementOffset(t),h=this._getOption("originLeft"),n=h?e.left:e.right,s=n+o.outerWidth,r=Math.floor(n/this.columnWidth);r=Math.max(0,r);var a=Math.floor(s/this.columnWidth);a-=s%this.columnWidth?0:1,a=Math.min(this.cols-1,a);for(var u=this._getOption("originTop"),l=(u?e.top:e.bottom)+o.outerHeight,c=r;c<=a;c++)this.colYs[c]=Math.max(l,this.colYs[c])},e._getContainerSize=function(){this.maxY=Math.max.apply(Math,this.colYs);var t={height:this.maxY};return this._getOption("fitWidth")&&(t.width=this._getContainerFitWidth()),t},e._getContainerFitWidth=function(){for(var t=0,i=this.cols;--i&&0===this.colYs[i];)t++;return(this.cols-t)*this.columnWidth-this.gutter},e.needsResizeLayout=function(){var t=this.containerWidth;return this.getContainerWidth(),t!=this.containerWidth},o});
!function(t,o){"function"==typeof define&&define.amd?define(["../layout-mode","masonry-layout/masonry"],o):"object"==typeof module&&module.exports?module.exports=o(require("../layout-mode"),require("masonry-layout")):o(t.Isotope.LayoutMode,t.Masonry)}(window,function(t,o){"use strict";var e=t.create("masonry"),i=e.prototype,s={_getElementOffset:!0,layout:!0,_getMeasurement:!0};for(var n in o.prototype)s[n]||(i[n]=o.prototype[n]);var r=i.measureColumns;i.measureColumns=function(){this.items=this.isotope.filteredItems,r.call(this)};var u=i._getOption;return i._getOption=function(t){return"fitWidth"==t?void 0!==this.options.isFitWidth?this.options.isFitWidth:this.options.fitWidth:u.apply(this.isotope,arguments)},e});
!function(t,e){"function"==typeof define&&define.amd?define(["isotope-layout/js/layout-mode"],e):"object"==typeof exports?module.exports=e(require("isotope-layout/js/layout-mode")):e(t.Isotope.LayoutMode)}(window,function(t){"use strict";var e=t.create("cellsByColumn"),i=e.prototype;return i._resetLayout=function(){this.itemIndex=0,this.getColumnWidth(),this.getRowHeight(),this.rows=Math.floor(this.isotope.size.innerHeight/this.rowHeight),this.rows=Math.max(this.rows,1)},i._getItemLayoutPosition=function(t){t.getSize();var e=Math.floor(this.itemIndex/this.rows),i=this.itemIndex%this.rows,o=(e+.5)*this.columnWidth-t.size.outerWidth/2,s=(i+.5)*this.rowHeight-t.size.outerHeight/2;return this.itemIndex++,{x:o,y:s}},i._getContainerSize=function(){return{width:Math.ceil(this.itemIndex/this.rows)*this.columnWidth}},i.needsResizeLayout=function(){return this.needsVerticalResizeLayout()},e});
!function(t,e){"function"==typeof define&&define.amd?define(["isotope-layout/js/layout-mode"],e):"object"==typeof exports?module.exports=e(require("isotope-layout/js/layout-mode")):e(t.Isotope.LayoutMode)}(window,function(t){"use strict";var e=t.create("cellsByRow"),i=e.prototype;return i._resetLayout=function(){this.itemIndex=0,this.getColumnWidth(),this.getRowHeight(),this.cols=Math.floor(this.isotope.size.innerWidth/this.columnWidth),this.cols=Math.max(this.cols,1)},i._getItemLayoutPosition=function(t){t.getSize();var e=this.itemIndex%this.cols,i=Math.floor(this.itemIndex/this.cols),o=(e+.5)*this.columnWidth-t.size.outerWidth/2,s=(i+.5)*this.rowHeight-t.size.outerHeight/2;return this.itemIndex++,{x:o,y:s}},i._getContainerSize=function(){return{height:Math.ceil(this.itemIndex/this.cols)*this.rowHeight}},e});
!function(t,e){"function"==typeof define&&define.amd?define(["isotope-layout/js/layout-mode"],e):"object"==typeof exports?module.exports=e(require("isotope-layout/js/layout-mode")):e(t.Isotope.LayoutMode)}(window,function(t){"use strict";var e=t.create("fitColumns"),i=e.prototype;return i._resetLayout=function(){this.x=0,this.y=0,this.maxX=0},i._getItemLayoutPosition=function(t){t.getSize(),0!==this.y&&t.size.outerHeight+this.y>this.isotope.size.innerHeight&&(this.y=0,this.x=this.maxX);var e={x:this.x,y:this.y};return this.maxX=Math.max(this.maxX,this.x+t.size.outerWidth),this.y+=t.size.outerHeight,e},i._getContainerSize=function(){return{width:this.maxX}},i.needsResizeLayout=function(){return this.needsVerticalResizeLayout()},e});
!function(e,t){"function"==typeof define&&define.amd?define(["isotope-layout/js/layout-mode"],t):"object"==typeof module&&module.exports?module.exports=t(require("isotope-layout/js/layout-mode")):t(e.Isotope.LayoutMode)}(window,function(e){"use strict";var t=e.create("horiz",{verticalAlignment:0}),o=t.prototype;return o._resetLayout=function(){this.x=0},o._getItemLayoutPosition=function(e){e.getSize();var t=(this.isotope.size.innerHeight-e.size.outerHeight)*this.options.verticalAlignment,o=this.x;return this.x+=e.size.outerWidth,{x:o,y:t}},o._getContainerSize=function(){return{width:this.x}},o.needsResizeLayout=function(){return this.needsVerticalResizeLayout()},t});
!function(t,e){"use strict";"function"==typeof define&&define.amd?define(["get-size/get-size","isotope-layout/js/layout-mode"],e):"object"==typeof module&&module.exports?module.exports=e(require("get-size"),require("isotope-layout/js/layout-mode")):e(t.getSize,t.Isotope.LayoutMode)}(window,function(t,e){"use strict";var i=e.create("masonryHorizontal"),o=i.prototype;return o._resetLayout=function(){this.getRowHeight(),this._getMeasurement("gutter","outerHeight"),this.rowHeight+=this.gutter,this.rows=Math.floor((this.isotope.size.innerHeight+this.gutter)/this.rowHeight),this.rows=Math.max(this.rows,1);var t=this.rows;for(this.rowXs=[];t--;)this.rowXs.push(0);this.maxX=0},o._getItemLayoutPosition=function(t){t.getSize();var e=t.size.outerHeight%this.rowHeight,i=e&&e<1?"round":"ceil",o=Math[i](t.size.outerHeight/this.rowHeight);o=Math.min(o,this.rows);for(var r=this._getRowGroup(o),s=Math.min.apply(Math,r),h=r.indexOf(s),a={x:s,y:this.rowHeight*h},n=s+t.size.outerWidth,u=this.rows+1-r.length,g=0;g<u;g++)this.rowXs[h+g]=n;return a},o._getRowGroup=function(t){if(t<2)return this.rowXs;for(var e=[],i=this.rows+1-t,o=0;o<i;o++){var r=this.rowXs.slice(o,o+t);e[o]=Math.max.apply(Math,r)}return e},o._manageStamp=function(e){var i=t(e),o=this.isotope._getElementOffset(e),r=this._getOption("originTop")?o.top:o.bottom,s=r+i.outerHeight,h=Math.floor(r/this.rowHeight);h=Math.max(0,h);var a=Math.floor(s/this.rowHeight);a=Math.min(this.rows-1,a);for(var n=(this._getOption("originLeft")?o.left:o.right)+i.outerWidth,u=h;u<=a;u++)this.rowXs[u]=Math.max(n,this.rowXs[u])},o._getContainerSize=function(){return this.maxX=Math.max.apply(Math,this.rowXs),{width:this.maxX}},o.needsResizeLayout=function(){return this.needsVerticalResizeLayout()},i});
!function(t,h){"function"==typeof define&&define.amd?define(h):"object"==typeof module&&module.exports?module.exports=h():(t.Packery=t.Packery||{},t.Packery.Rect=h())}(window,function(){"use strict";function t(h){for(var i in t.defaults)this[i]=t.defaults[i];for(i in h)this[i]=h[i]}t.defaults={x:0,y:0,width:0,height:0};var h=t.prototype;return h.contains=function(t){var h=t.width||0,i=t.height||0;return this.x<=t.x&&this.y<=t.y&&this.x+this.width>=t.x+h&&this.y+this.height>=t.y+i},h.overlaps=function(t){var h=this.x+this.width,i=this.y+this.height,e=t.x+t.width,s=t.y+t.height;return this.x<e&&h>t.x&&this.y<s&&i>t.y},h.getMaximalFreeRects=function(h){if(!this.overlaps(h))return!1;var i,e=[],s=this.x+this.width,n=this.y+this.height,r=h.x+h.width,y=h.y+h.height;return this.y<h.y&&(i=new t({x:this.x,y:this.y,width:this.width,height:h.y-this.y}),e.push(i)),s>r&&(i=new t({x:r,y:this.y,width:s-r,height:this.height}),e.push(i)),n>y&&(i=new t({x:this.x,y:y,width:this.width,height:n-y}),e.push(i)),this.x<h.x&&(i=new t({x:this.x,y:this.y,width:h.x-this.x,height:this.height}),e.push(i)),e},h.canFit=function(t){return this.width>=t.width&&this.height>=t.height},t});
!function(t,e){if("function"==typeof define&&define.amd)define(["./rect"],e);else if("object"==typeof module&&module.exports)module.exports=e(require("./rect"));else{var i=t.Packery=t.Packery||{};i.Packer=e(i.Rect)}}(window,function(t){"use strict";function e(t,e,i){this.width=t||0,this.height=e||0,this.sortDirection=i||"downwardLeftToRight",this.reset()}var i=e.prototype;i.reset=function(){this.spaces=[];var e=new t({x:0,y:0,width:this.width,height:this.height});this.spaces.push(e),this.sorter=s[this.sortDirection]||s.downwardLeftToRight},i.pack=function(t){for(var e=0;e<this.spaces.length;e++){var i=this.spaces[e];if(i.canFit(t)){this.placeInSpace(t,i);break}}},i.columnPack=function(t){for(var e=0;e<this.spaces.length;e++){var i=this.spaces[e],s=i.x<=t.x&&i.x+i.width>=t.x+t.width&&i.height>=t.height-.01;if(s){t.y=i.y,this.placed(t);break}}},i.rowPack=function(t){for(var e=0;e<this.spaces.length;e++){var i=this.spaces[e],s=i.y<=t.y&&i.y+i.height>=t.y+t.height&&i.width>=t.width-.01;if(s){t.x=i.x,this.placed(t);break}}},i.placeInSpace=function(t,e){t.x=e.x,t.y=e.y,this.placed(t)},i.placed=function(t){for(var e=[],i=0;i<this.spaces.length;i++){var s=this.spaces[i],r=s.getMaximalFreeRects(t);r?e.push.apply(e,r):e.push(s)}this.spaces=e,this.mergeSortSpaces()},i.mergeSortSpaces=function(){e.mergeRects(this.spaces),this.spaces.sort(this.sorter)},i.addSpace=function(t){this.spaces.push(t),this.mergeSortSpaces()},e.mergeRects=function(t){var e=0,i=t[e];t:for(;i;){for(var s=0,r=t[e+s];r;){if(r==i)s++;else{if(r.contains(i)){t.splice(e,1),i=t[e];continue t}i.contains(r)?t.splice(e+s,1):s++}r=t[e+s]}e++,i=t[e]}return t};var s={downwardLeftToRight:function(t,e){return t.y-e.y||t.x-e.x},rightwardTopToBottom:function(t,e){return t.x-e.x||t.y-e.y}};return e});
!function(e,t){"function"==typeof define&&define.amd?define(["outlayer/outlayer","./rect"],t):"object"==typeof module&&module.exports?module.exports=t(require("outlayer"),require("./rect")):e.Packery.Item=t(e.Outlayer,e.Packery.Rect)}(window,function(e,t){"use strict";var i=document.documentElement.style,o="string"==typeof i.transform?"transform":"WebkitTransform",s=function(){e.Item.apply(this,arguments)},r=s.prototype=Object.create(e.Item.prototype),n=r._create;r._create=function(){n.call(this),this.rect=new t};var a=r.moveTo;return r.moveTo=function(e,t){var i=Math.abs(this.position.x-e),o=Math.abs(this.position.y-t),s=this.layout.dragItemCount&&!this.isPlacing&&!this.isTransitioning&&i<1&&o<1;return s?void this.goTo(e,t):void a.apply(this,arguments)},r.enablePlacing=function(){this.removeTransitionStyles(),this.isTransitioning&&o&&(this.element.style[o]="none"),this.isTransitioning=!1,this.getSize(),this.layout._setRectSize(this.element,this.rect),this.isPlacing=!0},r.disablePlacing=function(){this.isPlacing=!1},r.removeElem=function(){this.element.parentNode.removeChild(this.element),this.layout.packer.addSpace(this.rect),this.emitEvent("remove",[this])},r.showDropPlaceholder=function(){var e=this.dropPlaceholder;e||(e=this.dropPlaceholder=document.createElement("div"),e.className="packery-drop-placeholder",e.style.position="absolute"),e.style.width=this.size.width+"px",e.style.height=this.size.height+"px",this.positionDropPlaceholder(),this.layout.element.appendChild(e)},r.positionDropPlaceholder=function(){this.dropPlaceholder.style[o]="translate("+this.rect.x+"px, "+this.rect.y+"px)"},r.hideDropPlaceholder=function(){var e=this.dropPlaceholder.parentNode;e&&e.removeChild(this.dropPlaceholder)},s});
!function(t,i){"function"==typeof define&&define.amd?define(["get-size/get-size","outlayer/outlayer","./rect","./packer","./item"],i):"object"==typeof module&&module.exports?module.exports=i(require("get-size"),require("outlayer"),require("./rect"),require("./packer"),require("./item")):t.Packery=i(t.getSize,t.Outlayer,t.Packery.Rect,t.Packery.Packer,t.Packery.Item)}(window,function(t,i,e,s,r){"use strict";function a(t,i){return t.position.y-i.position.y||t.position.x-i.position.x}function h(t,i){return t.position.x-i.position.x||t.position.y-i.position.y}function n(t,i){var e=i.x-t.x,s=i.y-t.y;return Math.sqrt(e*e+s*s)}e.prototype.canFit=function(t){return this.width>=t.width-1&&this.height>=t.height-1};var o=i.create("packery");o.Item=r;var g=o.prototype;g._create=function(){i.prototype._create.call(this),this.packer=new s,this.shiftPacker=new s,this.isEnabled=!0,this.dragItemCount=0;var t=this;this.handleDraggabilly={dragStart:function(){t.itemDragStart(this.element)},dragMove:function(){t.itemDragMove(this.element,this.position.x,this.position.y)},dragEnd:function(){t.itemDragEnd(this.element)}},this.handleUIDraggable={start:function(i,e){e&&t.itemDragStart(i.currentTarget)},drag:function(i,e){e&&t.itemDragMove(i.currentTarget,e.position.left,e.position.top)},stop:function(i,e){e&&t.itemDragEnd(i.currentTarget)}}},g._resetLayout=function(){this.getSize(),this._getMeasurements();var t,i,e;this._getOption("horizontal")?(t=1/0,i=this.size.innerHeight+this.gutter,e="rightwardTopToBottom"):(t=this.size.innerWidth+this.gutter,i=1/0,e="downwardLeftToRight"),this.packer.width=this.shiftPacker.width=t,this.packer.height=this.shiftPacker.height=i,this.packer.sortDirection=this.shiftPacker.sortDirection=e,this.packer.reset(),this.maxY=0,this.maxX=0},g._getMeasurements=function(){this._getMeasurement("columnWidth","width"),this._getMeasurement("rowHeight","height"),this._getMeasurement("gutter","width")},g._getItemLayoutPosition=function(t){if(this._setRectSize(t.element,t.rect),this.isShifting||this.dragItemCount>0){var i=this._getPackMethod();this.packer[i](t.rect)}else this.packer.pack(t.rect);return this._setMaxXY(t.rect),t.rect},g.shiftLayout=function(){this.isShifting=!0,this.layout(),delete this.isShifting},g._getPackMethod=function(){return this._getOption("horizontal")?"rowPack":"columnPack"},g._setMaxXY=function(t){this.maxX=Math.max(t.x+t.width,this.maxX),this.maxY=Math.max(t.y+t.height,this.maxY)},g._setRectSize=function(i,e){var s=t(i),r=s.outerWidth,a=s.outerHeight;(r||a)&&(r=this._applyGridGutter(r,this.columnWidth),a=this._applyGridGutter(a,this.rowHeight)),e.width=Math.min(r,this.packer.width),e.height=Math.min(a,this.packer.height)},g._applyGridGutter=function(t,i){if(!i)return t+this.gutter;i+=this.gutter;var e=t%i,s=e&&e<1?"round":"ceil";return t=Math[s](t/i)*i},g._getContainerSize=function(){return this._getOption("horizontal")?{width:this.maxX-this.gutter}:{height:this.maxY-this.gutter}},g._manageStamp=function(t){var i,s=this.getItem(t);if(s&&s.isPlacing)i=s.rect;else{var r=this._getElementOffset(t);i=new e({x:this._getOption("originLeft")?r.left:r.right,y:this._getOption("originTop")?r.top:r.bottom})}this._setRectSize(t,i),this.packer.placed(i),this._setMaxXY(i)},g.sortItemsByPosition=function(){var t=this._getOption("horizontal")?h:a;this.items.sort(t)},g.fit=function(t,i,e){var s=this.getItem(t);s&&(this.stamp(s.element),s.enablePlacing(),this.updateShiftTargets(s),i=void 0===i?s.rect.x:i,e=void 0===e?s.rect.y:e,this.shift(s,i,e),this._bindFitEvents(s),s.moveTo(s.rect.x,s.rect.y),this.shiftLayout(),this.unstamp(s.element),this.sortItemsByPosition(),s.disablePlacing())},g._bindFitEvents=function(t){function i(){s++,2==s&&e.dispatchEvent("fitComplete",null,[t])}var e=this,s=0;t.once("layout",i),this.once("layoutComplete",i)},g.resize=function(){this.isResizeBound&&this.needsResizeLayout()&&(this.options.shiftPercentResize?this.resizeShiftPercentLayout():this.layout())},g.needsResizeLayout=function(){var i=t(this.element),e=this._getOption("horizontal")?"innerHeight":"innerWidth";return i[e]!=this.size[e]},g.resizeShiftPercentLayout=function(){var i=this._getItemsForLayout(this.items),e=this._getOption("horizontal"),s=e?"y":"x",r=e?"height":"width",a=e?"rowHeight":"columnWidth",h=e?"innerHeight":"innerWidth",n=this[a];if(n=n&&n+this.gutter){this._getMeasurements();var o=this[a]+this.gutter;i.forEach(function(t){var i=Math.round(t.rect[s]/n);t.rect[s]=i*o})}else{var g=t(this.element)[h]+this.gutter,c=this.packer[r];i.forEach(function(t){t.rect[s]=t.rect[s]/c*g})}this.shiftLayout()},g.itemDragStart=function(t){if(this.isEnabled){this.stamp(t);var i=this.getItem(t);i&&(i.enablePlacing(),i.showDropPlaceholder(),this.dragItemCount++,this.updateShiftTargets(i))}},g.updateShiftTargets=function(t){this.shiftPacker.reset(),this._getBoundingRect();var i=this._getOption("originLeft"),s=this._getOption("originTop");this.stamps.forEach(function(t){var r=this.getItem(t);if(!r||!r.isPlacing){var a=this._getElementOffset(t),h=new e({x:i?a.left:a.right,y:s?a.top:a.bottom});this._setRectSize(t,h),this.shiftPacker.placed(h)}},this);var r=this._getOption("horizontal"),a=r?"rowHeight":"columnWidth",h=r?"height":"width";this.shiftTargetKeys=[],this.shiftTargets=[];var n,o=this[a];if(o=o&&o+this.gutter){var g=Math.ceil(t.rect[h]/o),c=Math.floor((this.shiftPacker[h]+this.gutter)/o);n=(c-g)*o;for(var u=0;u<c;u++){var d=r?0:u*o,f=r?u*o:0;this._addShiftTarget(d,f,n)}}else n=this.shiftPacker[h]+this.gutter-t.rect[h],this._addShiftTarget(0,0,n);var l=this._getItemsForLayout(this.items),m=this._getPackMethod();l.forEach(function(t){var i=t.rect;this._setRectSize(t.element,i),this.shiftPacker[m](i),this._addShiftTarget(i.x,i.y,n);var e=r?i.x+i.width:i.x,s=r?i.y:i.y+i.height;if(this._addShiftTarget(e,s,n),o)for(var a=Math.round(i[h]/o),g=1;g<a;g++){var c=r?e:i.x+o*g,u=r?i.y+o*g:s;this._addShiftTarget(c,u,n)}},this)},g._addShiftTarget=function(t,i,e){var s=this._getOption("horizontal")?i:t;if(!(0!==s&&s>e)){var r=t+","+i,a=this.shiftTargetKeys.indexOf(r)!=-1;a||(this.shiftTargetKeys.push(r),this.shiftTargets.push({x:t,y:i}))}},g.shift=function(t,i,e){var s,r=1/0,a={x:i,y:e};this.shiftTargets.forEach(function(t){var i=n(t,a);i<r&&(s=t,r=i)}),t.rect.x=s.x,t.rect.y=s.y};var c=120;g.itemDragMove=function(t,i,e){function s(){a.shift(r,i,e),r.positionDropPlaceholder(),a.layout()}var r=this.isEnabled&&this.getItem(t);if(r){i-=this.size.paddingLeft,e-=this.size.paddingTop;var a=this,h=new Date;this._itemDragTime&&h-this._itemDragTime<c?(clearTimeout(this.dragTimeout),this.dragTimeout=setTimeout(s,c)):(s(),this._itemDragTime=h)}},g.itemDragEnd=function(t){function i(){s++,2==s&&(e.element.classList.remove("is-positioning-post-drag"),e.hideDropPlaceholder(),r.dispatchEvent("dragItemPositioned",null,[e]))}var e=this.isEnabled&&this.getItem(t);if(e){clearTimeout(this.dragTimeout),e.element.classList.add("is-positioning-post-drag");var s=0,r=this;e.once("layout",i),this.once("layoutComplete",i),e.moveTo(e.rect.x,e.rect.y),this.layout(),this.dragItemCount=Math.max(0,this.dragItemCount-1),this.sortItemsByPosition(),e.disablePlacing(),this.unstamp(e.element)}},g.bindDraggabillyEvents=function(t){this._bindDraggabillyEvents(t,"on")},g.unbindDraggabillyEvents=function(t){this._bindDraggabillyEvents(t,"off")},g._bindDraggabillyEvents=function(t,i){var e=this.handleDraggabilly;t[i]("dragStart",e.dragStart),t[i]("dragMove",e.dragMove),t[i]("dragEnd",e.dragEnd)},g.bindUIDraggableEvents=function(t){this._bindUIDraggableEvents(t,"on")},g.unbindUIDraggableEvents=function(t){this._bindUIDraggableEvents(t,"off")},g._bindUIDraggableEvents=function(t,i){var e=this.handleUIDraggable;t[i]("dragstart",e.start)[i]("drag",e.drag)[i]("dragstop",e.stop)};var u=g.destroy;return g.destroy=function(){u.apply(this,arguments),this.isEnabled=!1},o.Rect=e,o.Packer=s,o});
!function(t,e){"use strict";"function"==typeof define&&define.amd?define(["isotope-layout/js/layout-mode","packery/js/packery"],e):"object"==typeof module&&module.exports?module.exports=e(require("isotope-layout/js/layout-mode"),require("packery")):e(t.Isotope.LayoutMode,t.Packery)}(window,function(t,e){"use strict";var o=t.create("packery"),i=o.prototype,r={_getElementOffset:!0,_getMeasurement:!0};for(var s in e.prototype)r[s]||(i[s]=e.prototype[s]);var n=i._resetLayout;i._resetLayout=function(){this.packer=this.packer||new e.Packer,this.shiftPacker=this.shiftPacker||new e.Packer,n.apply(this,arguments)};var a=i._getItemLayoutPosition;i._getItemLayoutPosition=function(t){return t.rect=t.rect||new e.Rect,a.call(this,t)};var u=i.needsResizeLayout;i.needsResizeLayout=function(){return this._getOption("horizontal")?this.needsVerticalResizeLayout():u.call(this)};var p=i._getOption;return i._getOption=function(t){return"horizontal"==t?void 0!==this.options.isHorizontal?this.options.isHorizontal:this.options.horizontal:p.apply(this.isotope,arguments)},o});
!function(t,e){"use strict";"function"==typeof define&&define.amd?define(["ev-emitter/ev-emitter"],function(i){return e(t,i)}):"object"==typeof module&&module.exports?module.exports=e(t,require("ev-emitter")):t.imagesLoaded=e(t,t.EvEmitter)}("undefined"!=typeof window?window:this,function(t,e){"use strict";function i(t,e){for(var i in e)t[i]=e[i];return t}function o(t){if(Array.isArray(t))return t;var e="object"==typeof t&&"number"==typeof t.length;return e?d.call(t):[t]}function r(t,e,n){if(!(this instanceof r))return new r(t,e,n);var s=t;return"string"==typeof t&&(s=document.querySelectorAll(t)),s?(this.elements=o(s),this.options=i({},this.options),"function"==typeof e?n=e:i(this.options,e),n&&this.on("always",n),this.getImages(),h&&(this.jqDeferred=new h.Deferred),void setTimeout(this.check.bind(this))):void a.error("Bad element for imagesLoaded "+(s||t))}function n(t){this.img=t}function s(t,e){this.url=t,this.element=e,this.img=new Image}var h=t.jQuery,a=t.console,d=Array.prototype.slice;r.prototype=Object.create(e.prototype),r.prototype.options={},r.prototype.getImages=function(){this.images=[],this.elements.forEach(this.addElementImages,this)},r.prototype.addElementImages=function(t){"IMG"==t.nodeName&&this.addImage(t),this.options.background===!0&&this.addElementBackgroundImages(t);var e=t.nodeType;if(e&&m[e]){for(var i=t.querySelectorAll("img"),o=0;o<i.length;o++){var r=i[o];this.addImage(r)}if("string"==typeof this.options.background){var n=t.querySelectorAll(this.options.background);for(o=0;o<n.length;o++){var s=n[o];this.addElementBackgroundImages(s)}}}};var m={1:!0,9:!0,11:!0};return r.prototype.addElementBackgroundImages=function(t){var e=getComputedStyle(t);if(e)for(var i=/url\((['"])?(.*?)\1\)/gi,o=i.exec(e.backgroundImage);null!==o;){var r=o&&o[2];r&&this.addBackground(r,t),o=i.exec(e.backgroundImage)}},r.prototype.addImage=function(t){var e=new n(t);this.images.push(e)},r.prototype.addBackground=function(t,e){var i=new s(t,e);this.images.push(i)},r.prototype.check=function(){function t(t,i,o){setTimeout(function(){e.progress(t,i,o)})}var e=this;return this.progressedCount=0,this.hasAnyBroken=!1,this.images.length?void this.images.forEach(function(e){e.once("progress",t),e.check()}):void this.complete()},r.prototype.progress=function(t,e,i){this.progressedCount++,this.hasAnyBroken=this.hasAnyBroken||!t.isLoaded,this.emitEvent("progress",[this,t,e]),this.jqDeferred&&this.jqDeferred.notify&&this.jqDeferred.notify(this,t),this.progressedCount==this.images.length&&this.complete(),this.options.debug&&a&&a.log("progress: "+i,t,e)},r.prototype.complete=function(){var t=this.hasAnyBroken?"fail":"done";if(this.isComplete=!0,this.emitEvent(t,[this]),this.emitEvent("always",[this]),this.jqDeferred){var e=this.hasAnyBroken?"reject":"resolve";this.jqDeferred[e](this)}},n.prototype=Object.create(e.prototype),n.prototype.check=function(){var t=this.getIsImageComplete();return t?void this.confirm(0!==this.img.naturalWidth,"naturalWidth"):(this.proxyImage=new Image,this.proxyImage.addEventListener("load",this),this.proxyImage.addEventListener("error",this),this.img.addEventListener("load",this),this.img.addEventListener("error",this),void(this.proxyImage.src=this.img.src))},n.prototype.getIsImageComplete=function(){return this.img.complete&&this.img.naturalWidth},n.prototype.confirm=function(t,e){this.isLoaded=t,this.emitEvent("progress",[this,this.img,e])},n.prototype.handleEvent=function(t){var e="on"+t.type;this[e]&&this[e](t)},n.prototype.onload=function(){this.confirm(!0,"onload"),this.unbindEvents()},n.prototype.onerror=function(){this.confirm(!1,"onerror"),this.unbindEvents()},n.prototype.unbindEvents=function(){this.proxyImage.removeEventListener("load",this),this.proxyImage.removeEventListener("error",this),this.img.removeEventListener("load",this),this.img.removeEventListener("error",this)},s.prototype=Object.create(n.prototype),s.prototype.check=function(){this.img.addEventListener("load",this),this.img.addEventListener("error",this),this.img.src=this.url;var t=this.getIsImageComplete();t&&(this.confirm(0!==this.img.naturalWidth,"naturalWidth"),this.unbindEvents())},s.prototype.unbindEvents=function(){this.img.removeEventListener("load",this),this.img.removeEventListener("error",this)},s.prototype.confirm=function(t,e){this.isLoaded=t,this.emitEvent("progress",[this,this.element,e])},r.makeJQueryPlugin=function(e){e=e||t.jQuery,e&&(h=e,h.fn.imagesLoaded=function(t,e){var i=new r(this,t,e);return i.jqDeferred.promise(h(this))})},r.makeJQueryPlugin(),r});
!function(){window.FizzyDocs={},window.filterBind=function(n,t,i,e){n.addEventListener(t,function(n){matchesSelector(n.target,i)&&e(n)})}}();
FizzyDocs["commercial-license-agreement"]=function(e){"use strict";function t(e){var t=o.querySelector(".is-selected");t&&t.classList.remove("is-selected"),e.classList.add("is-selected");var i=e.getAttribute("data-license-option"),n=r[i];l.forEach(function(e){e.element.textContent=n[e.property]})}var r={developer:{title:"Developer","for-official":"one (1) Licensed Developer","for-plain":"one individual Developer"},team:{title:"Team","for-official":"up to eight (8) Licensed Developer(s)","for-plain":"up to 8 Developers"},organization:{title:"Organization","for-official":"an unlimited number of Licensed Developer(s)","for-plain":"an unlimited number of Developers"}},o=e.querySelector(".button-group"),i=e.querySelector("h2"),n=i.cloneNode(!0);n.style.borderTop="none",n.style.marginTop=0,n.id="",n.innerHTML=n.innerHTML.replace("Commercial License",'Commercial <span data-license-property="title"></span> License'),i.textContent="",o.parentNode.insertBefore(n,o.nextSibling);for(var l=[],a=e.querySelectorAll("[data-license-property]"),c=0,s=a.length;c<s;c++){var p=a[c],u={property:p.getAttribute("data-license-property"),element:p};l.push(u)}t(o.querySelector(".button--developer")),filterBind(o,"click",".button",function(e){t(e.target)})};
!function(){var t=0;FizzyDocs["gh-button"]=function(n){function e(t){return t.toString().replace(/(\d)(?=(\d{3})+$)/g,"$1,")}var a=n.href.split("/"),r=a[3],c=a[4],o=n.querySelector(".gh-button__stat__text");t++;var u="ghButtonCallback"+t;window[u]=function(t){var n=e(t.data.stargazers_count);o.textContent=n};var i=document.createElement("script");i.src="https://api.github.com/repos/"+r+"/"+c+"?callback="+u,document.head.appendChild(i)}}();
FizzyDocs["shirt-promo"]=function(e){var t=new Date(2017,9,6),o=Math.round((t-new Date)/864e5),r=e.querySelector(".shirt-promo__title");r.textContent+=". Only on sale for "+o+" more days."};
!function(o){"use strict";o.IsotopeDocs={}}(window);
!function(e){var r="object"==typeof window&&window||"object"==typeof self&&self;"undefined"!=typeof exports?e(exports):r&&(r.hljs=e({}),"function"==typeof define&&define.amd&&define([],function(){return r.hljs}))}(function(e){function r(e){return e.replace(/&/gm,"&amp;").replace(/</gm,"&lt;").replace(/>/gm,"&gt;")}function t(e){return e.nodeName.toLowerCase()}function n(e,r){var t=e&&e.exec(r);return t&&0==t.index}function a(e){return/^(no-?highlight|plain|text)$/i.test(e)}function c(e){var r,t,n,c=e.className+" ";if(c+=e.parentNode?e.parentNode.className:"",t=/\blang(?:uage)?-([\w-]+)\b/i.exec(c))return E(t[1])?t[1]:"no-highlight";for(c=c.split(/\s+/),r=0,n=c.length;n>r;r++)if(E(c[r])||a(c[r]))return c[r]}function i(e,r){var t,n={};for(t in e)n[t]=e[t];if(r)for(t in r)n[t]=r[t];return n}function o(e){var r=[];return function n(e,a){for(var c=e.firstChild;c;c=c.nextSibling)3==c.nodeType?a+=c.nodeValue.length:1==c.nodeType&&(r.push({event:"start",offset:a,node:c}),a=n(c,a),t(c).match(/br|hr|img|input/)||r.push({event:"stop",offset:a,node:c}));return a}(e,0),r}function s(e,n,a){function c(){return e.length&&n.length?e[0].offset!=n[0].offset?e[0].offset<n[0].offset?e:n:"start"==n[0].event?e:n:e.length?e:n}function i(e){function n(e){return" "+e.nodeName+'="'+r(e.value)+'"'}l+="<"+t(e)+Array.prototype.map.call(e.attributes,n).join("")+">"}function o(e){l+="</"+t(e)+">"}function s(e){("start"==e.event?i:o)(e.node)}for(var u=0,l="",f=[];e.length||n.length;){var b=c();if(l+=r(a.substr(u,b[0].offset-u)),u=b[0].offset,b==e){f.reverse().forEach(o);do s(b.splice(0,1)[0]),b=c();while(b==e&&b.length&&b[0].offset==u);f.reverse().forEach(i)}else"start"==b[0].event?f.push(b[0].node):f.pop(),s(b.splice(0,1)[0])}return l+r(a.substr(u))}function u(e){function r(e){return e&&e.source||e}function t(t,n){return new RegExp(r(t),"m"+(e.cI?"i":"")+(n?"g":""))}function n(a,c){if(!a.compiled){if(a.compiled=!0,a.k=a.k||a.bK,a.k){var o={},s=function(r,t){e.cI&&(t=t.toLowerCase()),t.split(" ").forEach(function(e){var t=e.split("|");o[t[0]]=[r,t[1]?Number(t[1]):1]})};"string"==typeof a.k?s("keyword",a.k):Object.keys(a.k).forEach(function(e){s(e,a.k[e])}),a.k=o}a.lR=t(a.l||/\w+/,!0),c&&(a.bK&&(a.b="\\b("+a.bK.split(" ").join("|")+")\\b"),a.b||(a.b=/\B|\b/),a.bR=t(a.b),a.e||a.eW||(a.e=/\B|\b/),a.e&&(a.eR=t(a.e)),a.tE=r(a.e)||"",a.eW&&c.tE&&(a.tE+=(a.e?"|":"")+c.tE)),a.i&&(a.iR=t(a.i)),void 0===a.r&&(a.r=1),a.c||(a.c=[]);var u=[];a.c.forEach(function(e){e.v?e.v.forEach(function(r){u.push(i(e,r))}):u.push("self"==e?a:e)}),a.c=u,a.c.forEach(function(e){n(e,a)}),a.starts&&n(a.starts,c);var l=a.c.map(function(e){return e.bK?"\\.?("+e.b+")\\.?":e.b}).concat([a.tE,a.i]).map(r).filter(Boolean);a.t=l.length?t(l.join("|"),!0):{exec:function(){return null}}}}n(e)}function l(e,t,a,c){function i(e,r){for(var t=0;t<r.c.length;t++)if(n(r.c[t].bR,e))return r.c[t]}function o(e,r){if(n(e.eR,r)){for(;e.endsParent&&e.parent;)e=e.parent;return e}return e.eW?o(e.parent,r):void 0}function s(e,r){return!a&&n(r.iR,e)}function b(e,r){var t=N.cI?r[0].toLowerCase():r[0];return e.k.hasOwnProperty(t)&&e.k[t]}function g(e,r,t,n){var a=n?"":w.classPrefix,c='<span class="'+a,i=t?"":"</span>";return c+=e+'">',c+r+i}function p(){if(!C.k)return r(k);var e="",t=0;C.lR.lastIndex=0;for(var n=C.lR.exec(k);n;){e+=r(k.substr(t,n.index-t));var a=b(C,n);a?(A+=a[1],e+=g(a[0],r(n[0]))):e+=r(n[0]),t=C.lR.lastIndex,n=C.lR.exec(k)}return e+r(k.substr(t))}function d(){var e="string"==typeof C.sL;if(e&&!y[C.sL])return r(k);var t=e?l(C.sL,k,!0,R[C.sL]):f(k,C.sL.length?C.sL:void 0);return C.r>0&&(A+=t.r),e&&(R[C.sL]=t.top),g(t.language,t.value,!1,!0)}function h(){x+=void 0!==C.sL?d():p(),k=""}function m(e,r){x+=e.cN?g(e.cN,"",!0):"",C=Object.create(e,{parent:{value:C}})}function v(e,r){if(k+=e,void 0===r)return h(),0;var t=i(r,C);if(t)return t.skip?k+=r:(t.eB&&(k+=r),h(),t.rB||t.eB||(k=r)),m(t,r),t.rB?0:r.length;var n=o(C,r);if(n){var a=C;a.skip?k+=r:(a.rE||a.eE||(k+=r),h(),a.eE&&(k=r));do C.cN&&(x+="</span>"),C.skip||(A+=C.r),C=C.parent;while(C!=n.parent);return n.starts&&m(n.starts,""),a.rE?0:r.length}if(s(r,C))throw new Error('Illegal lexeme "'+r+'" for mode "'+(C.cN||"<unnamed>")+'"');return k+=r,r.length||1}var N=E(e);if(!N)throw new Error('Unknown language: "'+e+'"');u(N);var M,C=c||N,R={},x="";for(M=C;M!=N;M=M.parent)M.cN&&(x=g(M.cN,"",!0)+x);var k="",A=0;try{for(var S,B,L=0;C.t.lastIndex=L,S=C.t.exec(t),S;)B=v(t.substr(L,S.index-L),S[0]),L=S.index+B;for(v(t.substr(L)),M=C;M.parent;M=M.parent)M.cN&&(x+="</span>");return{r:A,value:x,language:e,top:C}}catch(I){if(-1!=I.message.indexOf("Illegal"))return{r:0,value:r(t)};throw I}}function f(e,t){t=t||w.languages||Object.keys(y);var n={r:0,value:r(e)},a=n;return t.filter(E).forEach(function(r){var t=l(r,e,!1);t.language=r,t.r>a.r&&(a=t),t.r>n.r&&(a=n,n=t)}),a.language&&(n.second_best=a),n}function b(e){return w.tabReplace&&(e=e.replace(/^((<[^>]+>|\t)+)/gm,function(e,r){return r.replace(/\t/g,w.tabReplace)})),w.useBR&&(e=e.replace(/\n/g,"<br>")),e}function g(e,r,t){var n=r?M[r]:t,a=[e.trim()];return e.match(/\bhljs\b/)||a.push("hljs"),-1===e.indexOf(n)&&a.push(n),a.join(" ").trim()}function p(e){var r=c(e);if(!a(r)){var t;w.useBR?(t=document.createElementNS("http://www.w3.org/1999/xhtml","div"),t.innerHTML=e.innerHTML.replace(/\n/g,"").replace(/<br[ \/]*>/g,"\n")):t=e;var n=t.textContent,i=r?l(r,n,!0):f(n),u=o(t);if(u.length){var p=document.createElementNS("http://www.w3.org/1999/xhtml","div");p.innerHTML=i.value,i.value=s(u,o(p),n)}i.value=b(i.value),e.innerHTML=i.value,e.className=g(e.className,r,i.language),e.result={language:i.language,re:i.r},i.second_best&&(e.second_best={language:i.second_best.language,re:i.second_best.r})}}function d(e){w=i(w,e)}function h(){if(!h.called){h.called=!0;var e=document.querySelectorAll("pre code");Array.prototype.forEach.call(e,p)}}function m(){addEventListener("DOMContentLoaded",h,!1),addEventListener("load",h,!1)}function v(r,t){var n=y[r]=t(e);n.aliases&&n.aliases.forEach(function(e){M[e]=r})}function N(){return Object.keys(y)}function E(e){return e=(e||"").toLowerCase(),y[e]||y[M[e]]}var w={classPrefix:"hljs-",tabReplace:null,useBR:!1,languages:void 0},y={},M={};return e.highlight=l,e.highlightAuto=f,e.fixMarkup=b,e.highlightBlock=p,e.configure=d,e.initHighlighting=h,e.initHighlightingOnLoad=m,e.registerLanguage=v,e.listLanguages=N,e.getLanguage=E,e.inherit=i,e.IR="[a-zA-Z]\\w*",e.UIR="[a-zA-Z_]\\w*",e.NR="\\b\\d+(\\.\\d+)?",e.CNR="(-?)(\\b0[xX][a-fA-F0-9]+|(\\b\\d+(\\.\\d*)?|\\.\\d+)([eE][-+]?\\d+)?)",e.BNR="\\b(0b[01]+)",e.RSR="!|!=|!==|%|%=|&|&&|&=|\\*|\\*=|\\+|\\+=|,|-|-=|/=|/|:|;|<<|<<=|<=|<|===|==|=|>>>=|>>=|>=|>>>|>>|>|\\?|\\[|\\{|\\(|\\^|\\^=|\\||\\|=|\\|\\||~",e.BE={b:"\\\\[\\s\\S]",r:0},e.ASM={cN:"string",b:"'",e:"'",i:"\\n",c:[e.BE]},e.QSM={cN:"string",b:'"',e:'"',i:"\\n",c:[e.BE]},e.PWM={b:/\b(a|an|the|are|I'm|isn't|don't|doesn't|won't|but|just|should|pretty|simply|enough|gonna|going|wtf|so|such|will|you|your|like)\b/},e.C=function(r,t,n){var a=e.inherit({cN:"comment",b:r,e:t,c:[]},n||{});return a.c.push(e.PWM),a.c.push({cN:"doctag",b:"(?:TODO|FIXME|NOTE|BUG|XXX):",r:0}),a},e.CLCM=e.C("//","$"),e.CBCM=e.C("/\\*","\\*/"),e.HCM=e.C("#","$"),e.NM={cN:"number",b:e.NR,r:0},e.CNM={cN:"number",b:e.CNR,r:0},e.BNM={cN:"number",b:e.BNR,r:0},e.CSSNM={cN:"number",b:e.NR+"(%|em|ex|ch|rem|vw|vh|vmin|vmax|cm|mm|in|pt|pc|px|deg|grad|rad|turn|s|ms|Hz|kHz|dpi|dpcm|dppx)?",r:0},e.RM={cN:"regexp",b:/\//,e:/\/[gimuy]*/,i:/\n/,c:[e.BE,{b:/\[/,e:/\]/,r:0,c:[e.BE]}]},e.TM={cN:"title",b:e.IR,r:0},e.UTM={cN:"title",b:e.UIR,r:0},e.METHOD_GUARD={b:"\\.\\s*"+e.UIR,r:0},e}),hljs.registerLanguage("css",function(e){var r="[a-zA-Z-][a-zA-Z0-9_-]*",t={b:/[A-Z\_\.\-]+\s*:/,rB:!0,e:";",eW:!0,c:[{cN:"attribute",b:/\S/,e:":",eE:!0,starts:{eW:!0,eE:!0,c:[{b:/[\w-]+\(/,rB:!0,c:[{cN:"built_in",b:/[\w-]+/},{b:/\(/,e:/\)/,c:[e.ASM,e.QSM]}]},e.CSSNM,e.QSM,e.ASM,e.CBCM,{cN:"number",b:"#[0-9A-Fa-f]+"},{cN:"meta",b:"!important"}]}}]};return{cI:!0,i:/[=\/|'\$]/,c:[e.CBCM,{cN:"selector-id",b:/#[A-Za-z0-9_-]+/},{cN:"selector-class",b:/\.[A-Za-z0-9_-]+/},{cN:"selector-attr",b:/\[/,e:/\]/,i:"$"},{cN:"selector-pseudo",b:/:(:)?[a-zA-Z0-9\_\-\+\(\)"'.]+/},{b:"@(font-face|page)",l:"[a-z-]+",k:"font-face page"},{b:"@",e:"[{;]",i:/:/,c:[{cN:"keyword",b:/\w+/},{b:/\s/,eW:!0,eE:!0,r:0,c:[e.ASM,e.QSM,e.CSSNM]}]},{cN:"selector-tag",b:r,r:0},{b:"{",e:"}",i:/\S/,c:[e.CBCM,t]}]}}),hljs.registerLanguage("javascript",function(e){return{aliases:["js","jsx"],k:{keyword:"in of if for while finally var new function do return void else break catch instanceof with throw case default try this switch continue typeof delete let yield const export super debugger as async await static import from as",literal:"true false null undefined NaN Infinity",built_in:"eval isFinite isNaN parseFloat parseInt decodeURI decodeURIComponent encodeURI encodeURIComponent escape unescape Object Function Boolean Error EvalError InternalError RangeError ReferenceError StopIteration SyntaxError TypeError URIError Number Math Date String RegExp Array Float32Array Float64Array Int16Array Int32Array Int8Array Uint16Array Uint32Array Uint8Array Uint8ClampedArray ArrayBuffer DataView JSON Intl arguments require module console window document Symbol Set Map WeakSet WeakMap Proxy Reflect Promise"},c:[{cN:"meta",r:10,b:/^\s*['"]use (strict|asm)['"]/},{cN:"meta",b:/^#!/,e:/$/},e.ASM,e.QSM,{cN:"string",b:"`",e:"`",c:[e.BE,{cN:"subst",b:"\\$\\{",e:"\\}"}]},e.CLCM,e.CBCM,{cN:"number",v:[{b:"\\b(0[bB][01]+)"},{b:"\\b(0[oO][0-7]+)"},{b:e.CNR}],r:0},{b:"("+e.RSR+"|\\b(case|return|throw)\\b)\\s*",k:"return throw case",c:[e.CLCM,e.CBCM,e.RM,{b:/</,e:/(\/\w+|\w+\/)>/,sL:"xml",c:[{b:/<\w+\s*\/>/,skip:!0},{b:/<\w+/,e:/(\/\w+|\w+\/)>/,skip:!0,c:["self"]}]}],r:0},{cN:"function",bK:"function",e:/\{/,eE:!0,c:[e.inherit(e.TM,{b:/[A-Za-z$_][0-9A-Za-z$_]*/}),{cN:"params",b:/\(/,e:/\)/,eB:!0,eE:!0,c:[e.CLCM,e.CBCM]}],i:/\[|%/},{b:/\$[(.]/},e.METHOD_GUARD,{cN:"class",bK:"class",e:/[{;=]/,eE:!0,i:/[:"\[\]]/,c:[{bK:"extends"},e.UTM]},{bK:"constructor",e:/\{/,eE:!0}],i:/#(?!!)/}}),hljs.registerLanguage("json",function(e){var r={literal:"true false null"},t=[e.QSM,e.CNM],n={e:",",eW:!0,eE:!0,c:t,k:r},a={b:"{",e:"}",c:[{cN:"attr",b:/"/,e:/"/,c:[e.BE],i:"\\n"},e.inherit(n,{b:/:/})],i:"\\S"},c={b:"\\[",e:"\\]",c:[e.inherit(n)],i:"\\S"};return t.splice(t.length,0,a,c),{c:t,k:r,i:"\\S"}}),hljs.registerLanguage("xml",function(e){var r="[A-Za-z0-9\\._:-]+",t={eW:!0,i:/</,r:0,c:[{cN:"attr",b:r,r:0},{b:/=\s*/,r:0,c:[{cN:"string",endsParent:!0,v:[{b:/"/,e:/"/},{b:/'/,e:/'/},{b:/[^\s"'=<>`]+/}]}]}]};return{aliases:["html","xhtml","rss","atom","xsl","plist"],cI:!0,c:[{cN:"meta",b:"<!DOCTYPE",e:">",r:10,c:[{b:"\\[",e:"\\]"}]},e.C("<!--","-->",{r:10}),{b:"<\\!\\[CDATA\\[",e:"\\]\\]>",r:10},{b:/<\?(php)?/,e:/\?>/,sL:"php",c:[{b:"/\\*",e:"\\*/",skip:!0}]},{cN:"tag",b:"<style(?=\\s|>|$)",e:">",k:{name:"style"},c:[t],starts:{e:"</style>",rE:!0,sL:["css","xml"]}},{cN:"tag",b:"<script(?=\\s|>|$)",e:">",k:{name:"script"},c:[t],starts:{e:"</script>",rE:!0,sL:["actionscript","javascript","handlebars","xml"]}},{cN:"meta",v:[{b:/<\?xml/,e:/\?>/,r:10},{b:/<\?\w+/,e:/\?>/}]},{cN:"tag",b:"</?",e:"/?>",c:[{cN:"name",b:/[^\/><\s]+/,r:0},t]}]}});
!function(){"use strict";IsotopeDocs.getItemElement=function(){var t=document.createElement("div"),i=Math.random(),e=Math.random(),n=i>.8?"grid-item--width3":i>.6?"grid-item--width2":"",r=e>.8?"grid-item--height3":e>.5?"grid-item--height2":"";return t.className="grid-item "+n+" "+r,t},hljs.configure({classPrefix:""}),$.fn.displayIsotopeCode=function(t,i,e){e=e||0,i="string"==typeof i&&i.indexOf("function")===-1?"'"+i+"'":i;var n="$grid.isotope({ "+t+": "+i+" })",r=n.match(/\n/g);r=r&&r.length||0;for(var o=0;r+o<e;o++)n+="\n";n=hljs.highlight("js",n).value,this.html(n)}}();
IsotopeDocs["gh-button"]=function(t){function e(t){return t.toString().replace(/(\d)(?=(\d{3})+$)/g,"$1,")}var o="metafizzy",a="isotope",n="ghButtonCallback"+Math.floor(1e4*Math.random());window[n]=function(o){var a=e(o.data.stargazers_count);t.querySelector(".gh-button__stat__text").textContent=a};var r=document.createElement("script");r.src="https://api.github.com/repos/"+o+"/"+a+"?callback="+n,document.head.appendChild(r)};
IsotopeDocs["hero-demo"]=function(t){"use strict";var e=$(t),n=e.find(".grid").isotope({itemSelector:".element-item",layoutMode:"fitRows",transitionDuration:"0.6s",getSortData:{name:".name",symbol:".symbol",number:".number parseInt",category:"[data-category]",weight:function(t){var e=$(t).find(".weight").text();return parseFloat(e.replace(/[\(\)]/g,""))}}}),r={numberGreaterThan50:function(){var t=$(this).find(".number").text();return parseInt(t,10)>50},ium:function(){var t=$(this).find(".name").text();return t.match(/ium$/)}},i={numberGreaterThan50:"function() {\n  var number = $(this).find('.number').text();\n  return parseInt( number, 10 ) > 50;\n}",ium:"function() {\n  var name = $(this).find('.name').text();\n  return name.match( /ium$/ );\n}"},o=e.find(".code-display code");e.find(".sort-by").on("click","button",function(){var t=$(this).attr("data-sort-by");n.isotope({sortBy:t}),o.displayIsotopeCode("sortBy",t,5)}),e.find(".filters").on("click","button",function(){var t=$(this).attr("data-filter"),e=r[t]||t,a=i[t]||t;n.isotope({filter:e}),o.displayIsotopeCode("filter",a,5)})};
IsotopeDocs["in-use-grid"]=function(e){"use strict";var i=$(e);i.find(".in-use-grid__item").hide(),i.isotope({itemSelector:"none",masonry:{columnWidth:".grid-sizer",gutter:".gutter-sizer"}}),i.isotope("option",{itemSelector:".in-use-grid__item"}),i.imagesLoaded().progress(function(e,t){var o=$(t.img).parents(".in-use-grid__item");o.show(),i.isotope("appended",o)})};
IsotopeDocs.notification=function(t){"use strict";function n(){var t=new Date,n=e(t.getMinutes()),o=e(t.getSeconds());return[t.getHours(),n,o].join(":")}function e(t){return t<10?"0"+t:t}function o(){t.style[c]="opacity 1.0s",t.style.opacity="0"}var i,s=document.documentElement,c="string"==typeof s.style.transition?"transition":"WebkitTransition";ID.notify=function(e){t.textContent=e+" at "+n(),t.style[c]="none",t.style.display="block",t.style.opacity="1",clearTimeout(i),i=setTimeout(o,1e3)}};
!function(){"use strict";function t(t){this.element=t,this.originalY=this.element.getBoundingClientRect().top+window.pageYOffset,window.addEventListener("scroll",this),this.isFixed=!1,this.onscroll()}function i(t,i,e){var n=t.prototype[i],o=i+"Timeout";t.prototype[i]=function(){if(!this[o]){n.apply(this,arguments);var t=this;this[o]=setTimeout(function(){n.apply(t,arguments),delete t[o]},e||100)}}}IsotopeDocs["page-nav"]=function(i){var e=getSize(i).outerHeight;window.innerWidth<768||e>=window.innerHeight||new t(i)},t.prototype.handleEvent=function(t){var i="on"+t.type;this[i]&&this[i](t)},t.prototype.onscroll=function(){var t=window.pageYOffset>=this.originalY;t!==this.isFixed&&(this.element.classList.toggle("is-fixed"),this.isFixed=t)},i(t,"onscroll",50)}();
IsotopeDocs["animate-item-size"]=function(i){"use strict";var t=$(i),e=t.find(".grid").isotope({masonry:{columnWidth:60}});e.on("click",".animate-item-size-item",function(){$(this).toggleClass("is-expanded"),e.isotope("layout")})};
IsotopeDocs["animate-item-size-responsive"]=function(t){"use strict";function i(t){var i=getSize(t);t.style[o]="none",t.style.width=i.width+"px",t.style.height=i.height+"px"}function e(t){if(o){var i=function(){t.style.width="",t.style.height="",t.removeEventListener(r,i,!1)};t.addEventListener(r,i,!1)}}function n(t,i){var e=getSize(i);t.style.width=e.width+"px",t.style.height=e.height+"px"}var s=document.documentElement.style,o="string"==typeof s.transition?"transition":"WebkitTransition",r={WebkitTransition:"webkitTransitionEnd",transition:"transitionend"}[o],a=$(t),d=a.find(".grid").isotope({itemSelector:".animate-item-size-item",percentPosition:!0,masonry:{columnWidth:".grid-sizer"}});d.on("click",".animate-item-size-item__content",function(){var t=this;i(t);var s=t.parentNode;s.classList.toggle("is-expanded");t.offsetWidth;t.style[o]="",e(t),n(t,s),d.isotope("layout")})};
IsotopeDocs.appended=function(e){"use strict";var o=$(e),t=o.find(".grid").isotope({masonry:{columnWidth:50}});o.find(".append-button").on("click",function(){var e=$([IsotopeDocs.getItemElement(),IsotopeDocs.getItemElement(),IsotopeDocs.getItemElement()]);t.append(e).isotope("appended",e)})};
IsotopeDocs["arrange-complete"]=function(t){"use strict";var o=$(t),n=o.find(".grid").isotope({masonry:{columnWidth:50}});n.on("arrangeComplete",function(t,o){ID.notify("Isotope arrange completed on "+o.length+" items")}),o.find(".button-group").on("click","button",function(){var t=$(this).attr("data-filter");n.isotope({filter:t})})};
IsotopeDocs["combination-filters"]=function(t){"use strict";function o(t){var o="";for(var i in t)o+=t[i];return o}var i=$(t),r=i.find(".grid").isotope({itemSelector:".color-shape",columnWidth:80,transitionDuration:"0.6s"}),n=i.find(".code-display code"),e={};i.on("click",".button",function(){var t=$(this),i=t.parents(".button-group"),a=i.attr("data-filter-group");e[a]=t.attr("data-filter");var s=o(e);r.isotope({filter:s}),n.displayIsotopeCode("filter",s)})};
IsotopeDocs.destroy=function(o){"use strict";var t=$(o),i={masonry:{columnWidth:50}},n=t.find(".grid").isotope(i),s=!0;t.find(".toggle-button").on("click",function(){s?n.isotope("destroy"):n.isotope(i),s=!s})};
IsotopeDocs["filtering-demo"]=function(t){"use strict";var n=$(t),e=n.find(".grid").isotope({itemSelector:".element-item",layoutMode:"fitRows",transitionDuration:"0.6s"}),i={numberGreaterThan50:function(){var t=$(this).find(".number").text();return parseInt(t,10)>50},ium:function(){var t=$(this).find(".name").text();return t.match(/ium$/)}},r={numberGreaterThan50:"function() {\n  var number = $(this).find('.number').text();\n  return parseInt( number, 10 ) > 50;\n}",ium:"function() {\n  var name = $(this).find('.name').text();\n  return name.match( /ium$/ );\n}"},o=n.find(".code-display code");n.find(".filter-button-group").on("click","button",function(){var t=$(this).attr("data-filter"),n=i[t]||t,a=r[t]||t;e.isotope({filter:n}),o.displayIsotopeCode("filter",a)})};
IsotopeDocs["imagesloaded-callback"]=function(e){"use strict";var i=$(e).imagesLoaded(function(){i.isotope({itemSelector:".grid-image-item",percentPosition:!0,masonry:{columnWidth:".grid-sizer"}})})};
IsotopeDocs["imagesloaded-progress"]=function(o){"use strict";var e=$(o).isotope({itemSelector:".grid-image-item",percentPosition:!0,masonry:{columnWidth:".grid-sizer"}});e.imagesLoaded().progress(function(){e.isotope("layout")})};
IsotopeDocs.insert=function(t){"use strict";var n=$(t),r=n.find(".grid").isotope({masonry:{columnWidth:50},filter:function(){var t=$(this).find(".number").text();return parseInt(t,10)%2},sortBy:"number",getSortData:{number:".number parseInt"}});n.find(".insert-button").on("click",function(){for(var t=[],n=0;n<3;n++){var e=IsotopeDocs.getItemElement(),o=Math.floor(100*Math.random());$(e).append('<p class="number">'+o+"</p>"),t.push(e)}r.isotope("insert",t)})};
IsotopeDocs["layout-complete"]=function(o){"use strict";var t=$(o),i=t.find(".grid").isotope({masonry:{columnWidth:50}});i.on("layoutComplete",function(o,t){ID.notify("Isotope layout completed on "+t.length+" items")}),i.on("click",".grid-item",function(){$(this).toggleClass("grid-item--gigante"),i.isotope("layout")})};
IsotopeDocs["layout-demo"]=function(o){"use strict";var i=$(o),t=i.find(".grid").isotope({masonry:{columnWidth:50}});t.on("click",".grid-item",function(){$(this).toggleClass("grid-item--gigante"),t.isotope("layout")})};
IsotopeDocs["layout-modes-demo"]=function(o){"use strict";var t=$(window),i=$(o),a=i.find(".grid").isotope({itemSelector:".grid-splash-item",layoutMode:"masonry",transitionDuration:"0.6s",masonry:{columnWidth:110},cellsByRow:{columnWidth:220,rowHeight:220},masonryHorizontal:{rowHeight:110},cellsByColumn:{columnWidth:220,rowHeight:220}}),e=!1,d=i.find(".code-display code");i.find(".button-group").on("click","button",function(){var o=$(this),i=!!o.attr("data-is-horizontal");if(e!=i){var n=i?{height:.7*t.height()}:{width:"auto"};a.css(n),e=i}var s=o.attr("data-layout-mode");a.isotope({layoutMode:s}),d.displayIsotopeCode("layoutMode",s)})};
IsotopeDocs["multiple-sort-by"]=function(t){"use strict";function o(t){return t.split(",")}var r=$(t),i=r.find(".button-group"),e=r.find(".grid").isotope({layoutMode:"fitRows",itemSelector:".grid-multi-item",getSortData:{color:"[data-color]",number:".number parseInt"},sortBy:["color","number"]});i.on("click","button",function(){e.isotope({sortBy:o(this.getAttribute("data-sort-by"))})})};
IsotopeDocs.prepended=function(e){"use strict";var o=$(e),t=o.find(".grid").isotope({masonry:{columnWidth:50}});o.find(".prepend-button").on("click",function(){var e=$([IsotopeDocs.getItemElement(),IsotopeDocs.getItemElement(),IsotopeDocs.getItemElement()]);t.prepend(e).isotope("prepended",e)})};
IsotopeDocs.remove=function(o){"use strict";var i=$(o),t=i.find(".grid").isotope({masonry:{columnWidth:50}});t.on("click",".grid-item",function(){t.isotope("remove",this).isotope("layout")})};
IsotopeDocs["remove-complete"]=function(o){"use strict";var e=$(o),t=e.find(".grid").isotope({masonry:{columnWidth:50}});t.on("removeComplete",function(o,e){ID.notify("Removed "+e.length+" items")}),t.on("click",".grid-item",function(){t.isotope("remove",this).isotope("layout")})};
IsotopeDocs.shuffle=function(o){"use strict";var f=$(o),i=f.find(".grid").isotope({masonry:{columnWidth:50}});f.find(".shuffle-button").on("click",function(){i.isotope("shuffle")})};
IsotopeDocs["sorting-demo"]=function(t){"use strict";var o=$(t),e=o.find(".sort-by-button-group"),r=o.find(".grid").isotope({itemSelector:".element-item",layoutMode:"fitRows",transitionDuration:"0.6s",getSortData:{name:".name",symbol:".symbol",number:".number parseInt",category:"[data-category]",weight:function(t){var o=$(t).find(".weight").text();return parseFloat(o.replace(/[\(\)]/g,""))}}}),i=o.find(".code-display code");e.on("click","button",function(){var t=$(this).attr("data-sort-by");r.isotope({sortBy:t}),i.displayIsotopeCode("sortBy",t)})};
IsotopeDocs.stagger=function(t){"use strict";var o=$(t),r=o.find(".grid").isotope({layoutMode:"fitRows",stagger:30});o.find(".button-group").on("click",".button",function(t){var o=$(t.currentTarget).attr("data-filter");r.isotope({filter:o})})};
IsotopeDocs["stamp-methods"]=function(t){"use strict";var o=$(t),i=o.find(".grid").isotope({itemSelector:".grid-item",masonry:{columnWidth:50}}),s=i.find(".stamp"),n=!1;o.find(".stamp-button").on("click",function(){n?i.isotope("unstamp",s):i.isotope("stamp",s),i.isotope("layout"),n=!n})};
IsotopeDocs["vertical-list"]=function(t){"use strict";var o=$(t),e=o.find(".vertical-list").isotope({itemSelector:"li",layoutMode:"vertical",transitionDuration:"0.6s",getSortData:{name:".name",symbol:".symbol",number:".number parseInt",category:".category",weight:function(t){var o=$(t).find(".weight").text();return parseFloat(o.replace(/[\(\)]/g,""))}}});o.find(".button-group").on("click","button",function(){var t=$(this).attr("data-sort-by");e.isotope({sortBy:t})})};
IsotopeDocs["visible-hidden-style"]=function(t){"use strict";var i=$(t),o=i.find(".grid").isotope({layoutMode:"fitRows",visibleStyle:{opacity:1},hiddenStyle:{opacity:0}});i.find(".button-group").on("click",".button",function(t){var i=$(t.currentTarget).attr("data-filter");o.isotope({filter:i})})};
!function(){"use strict";$("[data-js]").each(function(t,s){var c=s.getAttribute("data-js"),e=IsotopeDocs[c]||FizzyDocs[c];e&&e(s)}),$(".js-radio-button-group").each(function(t,s){var c=$(s);c.find(":checked").parent().addClass("is-checked"),c.on("click","input, button",function(){c.find(".is-checked").removeClass("is-checked");var t=$(this),s=t.hasClass("button")?t:t.parents(".button");s.addClass("is-checked")})})}();
/*
     _ _      _       _
 ___| (_) ___| | __  (_)___
/ __| | |/ __| |/ /  | / __|
\__ \ | | (__|   < _ | \__ \
|___/_|_|\___|_|\_(_)/ |___/
                   |__/

 Version: 1.5.8
  Author: Ken Wheeler
 Website: http://kenwheeler.github.io
    Docs: http://kenwheeler.github.io/slick
    Repo: http://github.com/kenwheeler/slick
  Issues: http://github.com/kenwheeler/slick/issues

 */
!function(a){"use strict";"function"==typeof define&&define.amd?define(["jquery"],a):"undefined"!=typeof exports?module.exports=a(require("jquery")):a(jQuery)}(function(a){"use strict";var b=window.Slick||{};b=function(){function c(c,d){var f,e=this;e.defaults={accessibility:!0,adaptiveHeight:!1,appendArrows:a(c),appendDots:a(c),arrows:!0,asNavFor:null,prevArrow:'<button type="button" data-role="none" class="slick-prev" aria-label="Previous" tabindex="0" role="button">Previous</button>',nextArrow:'<button type="button" data-role="none" class="slick-next" aria-label="Next" tabindex="0" role="button">Next</button>',autoplay:!1,autoplaySpeed:3e3,centerMode:!1,centerPadding:"50px",cssEase:"ease",customPaging:function(a,b){return'<button type="button" data-role="none" role="button" aria-required="false" tabindex="0">'+(b+1)+"</button>"},dots:!1,dotsClass:"slick-dots",draggable:!0,easing:"linear",edgeFriction:.35,fade:!1,focusOnSelect:!1,infinite:!0,initialSlide:0,lazyLoad:"ondemand",mobileFirst:!1,pauseOnHover:!0,pauseOnDotsHover:!1,respondTo:"window",responsive:null,rows:1,rtl:!1,slide:"",slidesPerRow:1,slidesToShow:1,slidesToScroll:1,speed:500,swipe:!0,swipeToSlide:!1,touchMove:!0,touchThreshold:5,useCSS:!0,variableWidth:!1,vertical:!1,verticalSwiping:!1,waitForAnimate:!0,zIndex:1e3},e.initials={animating:!1,dragging:!1,autoPlayTimer:null,currentDirection:0,currentLeft:null,currentSlide:0,direction:1,$dots:null,listWidth:null,listHeight:null,loadIndex:0,$nextArrow:null,$prevArrow:null,slideCount:null,slideWidth:null,$slideTrack:null,$slides:null,sliding:!1,slideOffset:0,swipeLeft:null,$list:null,touchObject:{},transformsEnabled:!1,unslicked:!1},a.extend(e,e.initials),e.activeBreakpoint=null,e.animType=null,e.animProp=null,e.breakpoints=[],e.breakpointSettings=[],e.cssTransitions=!1,e.hidden="hidden",e.paused=!1,e.positionProp=null,e.respondTo=null,e.rowCount=1,e.shouldClick=!0,e.$slider=a(c),e.$slidesCache=null,e.transformType=null,e.transitionType=null,e.visibilityChange="visibilitychange",e.windowWidth=0,e.windowTimer=null,f=a(c).data("slick")||{},e.options=a.extend({},e.defaults,f,d),e.currentSlide=e.options.initialSlide,e.originalSettings=e.options,"undefined"!=typeof document.mozHidden?(e.hidden="mozHidden",e.visibilityChange="mozvisibilitychange"):"undefined"!=typeof document.webkitHidden&&(e.hidden="webkitHidden",e.visibilityChange="webkitvisibilitychange"),e.autoPlay=a.proxy(e.autoPlay,e),e.autoPlayClear=a.proxy(e.autoPlayClear,e),e.changeSlide=a.proxy(e.changeSlide,e),e.clickHandler=a.proxy(e.clickHandler,e),e.selectHandler=a.proxy(e.selectHandler,e),e.setPosition=a.proxy(e.setPosition,e),e.swipeHandler=a.proxy(e.swipeHandler,e),e.dragHandler=a.proxy(e.dragHandler,e),e.keyHandler=a.proxy(e.keyHandler,e),e.autoPlayIterator=a.proxy(e.autoPlayIterator,e),e.instanceUid=b++,e.htmlExpr=/^(?:\s*(<[\w\W]+>)[^>]*)$/,e.registerBreakpoints(),e.init(!0),e.checkResponsive(!0)}var b=0;return c}(),b.prototype.addSlide=b.prototype.slickAdd=function(b,c,d){var e=this;if("boolean"==typeof c)d=c,c=null;else if(0>c||c>=e.slideCount)return!1;e.unload(),"number"==typeof c?0===c&&0===e.$slides.length?a(b).appendTo(e.$slideTrack):d?a(b).insertBefore(e.$slides.eq(c)):a(b).insertAfter(e.$slides.eq(c)):d===!0?a(b).prependTo(e.$slideTrack):a(b).appendTo(e.$slideTrack),e.$slides=e.$slideTrack.children(this.options.slide),e.$slideTrack.children(this.options.slide).detach(),e.$slideTrack.append(e.$slides),e.$slides.each(function(b,c){a(c).attr("data-slick-index",b)}),e.$slidesCache=e.$slides,e.reinit()},b.prototype.animateHeight=function(){var a=this;if(1===a.options.slidesToShow&&a.options.adaptiveHeight===!0&&a.options.vertical===!1){var b=a.$slides.eq(a.currentSlide).outerHeight(!0);a.$list.animate({height:b},a.options.speed)}},b.prototype.animateSlide=function(b,c){var d={},e=this;e.animateHeight(),e.options.rtl===!0&&e.options.vertical===!1&&(b=-b),e.transformsEnabled===!1?e.options.vertical===!1?e.$slideTrack.animate({left:b},e.options.speed,e.options.easing,c):e.$slideTrack.animate({top:b},e.options.speed,e.options.easing,c):e.cssTransitions===!1?(e.options.rtl===!0&&(e.currentLeft=-e.currentLeft),a({animStart:e.currentLeft}).animate({animStart:b},{duration:e.options.speed,easing:e.options.easing,step:function(a){a=Math.ceil(a),e.options.vertical===!1?(d[e.animType]="translate("+a+"px, 0px)",e.$slideTrack.css(d)):(d[e.animType]="translate(0px,"+a+"px)",e.$slideTrack.css(d))},complete:function(){c&&c.call()}})):(e.applyTransition(),b=Math.ceil(b),d[e.animType]=e.options.vertical===!1?"translate3d("+b+"px, 0px, 0px)":"translate3d(0px,"+b+"px, 0px)",e.$slideTrack.css(d),c&&setTimeout(function(){e.disableTransition(),c.call()},e.options.speed))},b.prototype.asNavFor=function(b){var c=this,d=c.options.asNavFor;d&&null!==d&&(d=a(d).not(c.$slider)),null!==d&&"object"==typeof d&&d.each(function(){var c=a(this).slick("getSlick");c.unslicked||c.slideHandler(b,!0)})},b.prototype.applyTransition=function(a){var b=this,c={};c[b.transitionType]=b.options.fade===!1?b.transformType+" "+b.options.speed+"ms "+b.options.cssEase:"opacity "+b.options.speed+"ms "+b.options.cssEase,b.options.fade===!1?b.$slideTrack.css(c):b.$slides.eq(a).css(c)},b.prototype.autoPlay=function(){var a=this;a.autoPlayTimer&&clearInterval(a.autoPlayTimer),a.slideCount>a.options.slidesToShow&&a.paused!==!0&&(a.autoPlayTimer=setInterval(a.autoPlayIterator,a.options.autoplaySpeed))},b.prototype.autoPlayClear=function(){var a=this;a.autoPlayTimer&&clearInterval(a.autoPlayTimer)},b.prototype.autoPlayIterator=function(){var a=this;a.options.infinite===!1?1===a.direction?(a.currentSlide+1===a.slideCount-1&&(a.direction=0),a.slideHandler(a.currentSlide+a.options.slidesToScroll)):(0===a.currentSlide-1&&(a.direction=1),a.slideHandler(a.currentSlide-a.options.slidesToScroll)):a.slideHandler(a.currentSlide+a.options.slidesToScroll)},b.prototype.buildArrows=function(){var b=this;b.options.arrows===!0&&(b.$prevArrow=a(b.options.prevArrow).addClass("slick-arrow"),b.$nextArrow=a(b.options.nextArrow).addClass("slick-arrow"),b.slideCount>b.options.slidesToShow?(b.$prevArrow.removeClass("slick-hidden").removeAttr("aria-hidden tabindex"),b.$nextArrow.removeClass("slick-hidden").removeAttr("aria-hidden tabindex"),b.htmlExpr.test(b.options.prevArrow)&&b.$prevArrow.prependTo(b.options.appendArrows),b.htmlExpr.test(b.options.nextArrow)&&b.$nextArrow.appendTo(b.options.appendArrows),b.options.infinite!==!0&&b.$prevArrow.addClass("slick-disabled").attr("aria-disabled","true")):b.$prevArrow.add(b.$nextArrow).addClass("slick-hidden").attr({"aria-disabled":"true",tabindex:"-1"}))},b.prototype.buildDots=function(){var c,d,b=this;if(b.options.dots===!0&&b.slideCount>b.options.slidesToShow){for(d='<ul class="'+b.options.dotsClass+'">',c=0;c<=b.getDotCount();c+=1)d+="<li>"+b.options.customPaging.call(this,b,c)+"</li>";d+="</ul>",b.$dots=a(d).appendTo(b.options.appendDots),b.$dots.find("li").first().addClass("slick-active").attr("aria-hidden","false")}},b.prototype.buildOut=function(){var b=this;b.$slides=b.$slider.children(b.options.slide+":not(.slick-cloned)").addClass("slick-slide"),b.slideCount=b.$slides.length,b.$slides.each(function(b,c){a(c).attr("data-slick-index",b).data("originalStyling",a(c).attr("style")||"")}),b.$slidesCache=b.$slides,b.$slider.addClass("slick-slider"),b.$slideTrack=0===b.slideCount?a('<div class="slick-track"/>').appendTo(b.$slider):b.$slides.wrapAll('<div class="slick-track"/>').parent(),b.$list=b.$slideTrack.wrap('<div aria-live="polite" class="slick-list"/>').parent(),b.$slideTrack.css("opacity",0),(b.options.centerMode===!0||b.options.swipeToSlide===!0)&&(b.options.slidesToScroll=1),a("img[data-lazy]",b.$slider).not("[src]").addClass("slick-loading"),b.setupInfinite(),b.buildArrows(),b.buildDots(),b.updateDots(),b.setSlideClasses("number"==typeof b.currentSlide?b.currentSlide:0),b.options.draggable===!0&&b.$list.addClass("draggable")},b.prototype.buildRows=function(){var b,c,d,e,f,g,h,a=this;if(e=document.createDocumentFragment(),g=a.$slider.children(),a.options.rows>1){for(h=a.options.slidesPerRow*a.options.rows,f=Math.ceil(g.length/h),b=0;f>b;b++){var i=document.createElement("div");for(c=0;c<a.options.rows;c++){var j=document.createElement("div");for(d=0;d<a.options.slidesPerRow;d++){var k=b*h+(c*a.options.slidesPerRow+d);g.get(k)&&j.appendChild(g.get(k))}i.appendChild(j)}e.appendChild(i)}a.$slider.html(e),a.$slider.children().children().children().css({width:100/a.options.slidesPerRow+"%",display:"inline-block"})}},b.prototype.checkResponsive=function(b,c){var e,f,g,d=this,h=!1,i=d.$slider.width(),j=window.innerWidth||a(window).width();if("window"===d.respondTo?g=j:"slider"===d.respondTo?g=i:"min"===d.respondTo&&(g=Math.min(j,i)),d.options.responsive&&d.options.responsive.length&&null!==d.options.responsive){f=null;for(e in d.breakpoints)d.breakpoints.hasOwnProperty(e)&&(d.originalSettings.mobileFirst===!1?g<d.breakpoints[e]&&(f=d.breakpoints[e]):g>d.breakpoints[e]&&(f=d.breakpoints[e]));null!==f?null!==d.activeBreakpoint?(f!==d.activeBreakpoint||c)&&(d.activeBreakpoint=f,"unslick"===d.breakpointSettings[f]?d.unslick(f):(d.options=a.extend({},d.originalSettings,d.breakpointSettings[f]),b===!0&&(d.currentSlide=d.options.initialSlide),d.refresh(b)),h=f):(d.activeBreakpoint=f,"unslick"===d.breakpointSettings[f]?d.unslick(f):(d.options=a.extend({},d.originalSettings,d.breakpointSettings[f]),b===!0&&(d.currentSlide=d.options.initialSlide),d.refresh(b)),h=f):null!==d.activeBreakpoint&&(d.activeBreakpoint=null,d.options=d.originalSettings,b===!0&&(d.currentSlide=d.options.initialSlide),d.refresh(b),h=f),b||h===!1||d.$slider.trigger("breakpoint",[d,h])}},b.prototype.changeSlide=function(b,c){var f,g,h,d=this,e=a(b.target);switch(e.is("a")&&b.preventDefault(),e.is("li")||(e=e.closest("li")),h=0!==d.slideCount%d.options.slidesToScroll,f=h?0:(d.slideCount-d.currentSlide)%d.options.slidesToScroll,b.data.message){case"previous":g=0===f?d.options.slidesToScroll:d.options.slidesToShow-f,d.slideCount>d.options.slidesToShow&&d.slideHandler(d.currentSlide-g,!1,c);break;case"next":g=0===f?d.options.slidesToScroll:f,d.slideCount>d.options.slidesToShow&&d.slideHandler(d.currentSlide+g,!1,c);break;case"index":var i=0===b.data.index?0:b.data.index||e.index()*d.options.slidesToScroll;d.slideHandler(d.checkNavigable(i),!1,c),e.children().trigger("focus");break;default:return}},b.prototype.checkNavigable=function(a){var c,d,b=this;if(c=b.getNavigableIndexes(),d=0,a>c[c.length-1])a=c[c.length-1];else for(var e in c){if(a<c[e]){a=d;break}d=c[e]}return a},b.prototype.cleanUpEvents=function(){var b=this;b.options.dots&&null!==b.$dots&&(a("li",b.$dots).off("click.slick",b.changeSlide),b.options.pauseOnDotsHover===!0&&b.options.autoplay===!0&&a("li",b.$dots).off("mouseenter.slick",a.proxy(b.setPaused,b,!0)).off("mouseleave.slick",a.proxy(b.setPaused,b,!1))),b.options.arrows===!0&&b.slideCount>b.options.slidesToShow&&(b.$prevArrow&&b.$prevArrow.off("click.slick",b.changeSlide),b.$nextArrow&&b.$nextArrow.off("click.slick",b.changeSlide)),b.$list.off("touchstart.slick mousedown.slick",b.swipeHandler),b.$list.off("touchmove.slick mousemove.slick",b.swipeHandler),b.$list.off("touchend.slick mouseup.slick",b.swipeHandler),b.$list.off("touchcancel.slick mouseleave.slick",b.swipeHandler),b.$list.off("click.slick",b.clickHandler),a(document).off(b.visibilityChange,b.visibility),b.$list.off("mouseenter.slick",a.proxy(b.setPaused,b,!0)),b.$list.off("mouseleave.slick",a.proxy(b.setPaused,b,!1)),b.options.accessibility===!0&&b.$list.off("keydown.slick",b.keyHandler),b.options.focusOnSelect===!0&&a(b.$slideTrack).children().off("click.slick",b.selectHandler),a(window).off("orientationchange.slick.slick-"+b.instanceUid,b.orientationChange),a(window).off("resize.slick.slick-"+b.instanceUid,b.resize),a("[draggable!=true]",b.$slideTrack).off("dragstart",b.preventDefault),a(window).off("load.slick.slick-"+b.instanceUid,b.setPosition),a(document).off("ready.slick.slick-"+b.instanceUid,b.setPosition)},b.prototype.cleanUpRows=function(){var b,a=this;a.options.rows>1&&(b=a.$slides.children().children(),b.removeAttr("style"),a.$slider.html(b))},b.prototype.clickHandler=function(a){var b=this;b.shouldClick===!1&&(a.stopImmediatePropagation(),a.stopPropagation(),a.preventDefault())},b.prototype.destroy=function(b){var c=this;c.autoPlayClear(),c.touchObject={},c.cleanUpEvents(),a(".slick-cloned",c.$slider).detach(),c.$dots&&c.$dots.remove(),c.$prevArrow&&c.$prevArrow.length&&(c.$prevArrow.removeClass("slick-disabled slick-arrow slick-hidden").removeAttr("aria-hidden aria-disabled tabindex").css("display",""),c.htmlExpr.test(c.options.prevArrow)&&c.$prevArrow.remove()),c.$nextArrow&&c.$nextArrow.length&&(c.$nextArrow.removeClass("slick-disabled slick-arrow slick-hidden").removeAttr("aria-hidden aria-disabled tabindex").css("display",""),c.htmlExpr.test(c.options.nextArrow)&&c.$nextArrow.remove()),c.$slides&&(c.$slides.removeClass("slick-slide slick-active slick-center slick-visible slick-current").removeAttr("aria-hidden").removeAttr("data-slick-index").each(function(){a(this).attr("style",a(this).data("originalStyling"))}),c.$slideTrack.children(this.options.slide).detach(),c.$slideTrack.detach(),c.$list.detach(),c.$slider.append(c.$slides)),c.cleanUpRows(),c.$slider.removeClass("slick-slider"),c.$slider.removeClass("slick-initialized"),c.unslicked=!0,b||c.$slider.trigger("destroy",[c])},b.prototype.disableTransition=function(a){var b=this,c={};c[b.transitionType]="",b.options.fade===!1?b.$slideTrack.css(c):b.$slides.eq(a).css(c)},b.prototype.fadeSlide=function(a,b){var c=this;c.cssTransitions===!1?(c.$slides.eq(a).css({zIndex:c.options.zIndex}),c.$slides.eq(a).animate({opacity:1},c.options.speed,c.options.easing,b)):(c.applyTransition(a),c.$slides.eq(a).css({opacity:1,zIndex:c.options.zIndex}),b&&setTimeout(function(){c.disableTransition(a),b.call()},c.options.speed))},b.prototype.fadeSlideOut=function(a){var b=this;b.cssTransitions===!1?b.$slides.eq(a).animate({opacity:0,zIndex:b.options.zIndex-2},b.options.speed,b.options.easing):(b.applyTransition(a),b.$slides.eq(a).css({opacity:0,zIndex:b.options.zIndex-2}))},b.prototype.filterSlides=b.prototype.slickFilter=function(a){var b=this;null!==a&&(b.unload(),b.$slideTrack.children(this.options.slide).detach(),b.$slidesCache.filter(a).appendTo(b.$slideTrack),b.reinit())},b.prototype.getCurrent=b.prototype.slickCurrentSlide=function(){var a=this;return a.currentSlide},b.prototype.getDotCount=function(){var a=this,b=0,c=0,d=0;if(a.options.infinite===!0)for(;b<a.slideCount;)++d,b=c+a.options.slidesToShow,c+=a.options.slidesToScroll<=a.options.slidesToShow?a.options.slidesToScroll:a.options.slidesToShow;else if(a.options.centerMode===!0)d=a.slideCount;else for(;b<a.slideCount;)++d,b=c+a.options.slidesToShow,c+=a.options.slidesToScroll<=a.options.slidesToShow?a.options.slidesToScroll:a.options.slidesToShow;return d-1},b.prototype.getLeft=function(a){var c,d,f,b=this,e=0;return b.slideOffset=0,d=b.$slides.first().outerHeight(!0),b.options.infinite===!0?(b.slideCount>b.options.slidesToShow&&(b.slideOffset=-1*b.slideWidth*b.options.slidesToShow,e=-1*d*b.options.slidesToShow),0!==b.slideCount%b.options.slidesToScroll&&a+b.options.slidesToScroll>b.slideCount&&b.slideCount>b.options.slidesToShow&&(a>b.slideCount?(b.slideOffset=-1*(b.options.slidesToShow-(a-b.slideCount))*b.slideWidth,e=-1*(b.options.slidesToShow-(a-b.slideCount))*d):(b.slideOffset=-1*b.slideCount%b.options.slidesToScroll*b.slideWidth,e=-1*b.slideCount%b.options.slidesToScroll*d))):a+b.options.slidesToShow>b.slideCount&&(b.slideOffset=(a+b.options.slidesToShow-b.slideCount)*b.slideWidth,e=(a+b.options.slidesToShow-b.slideCount)*d),b.slideCount<=b.options.slidesToShow&&(b.slideOffset=0,e=0),b.options.centerMode===!0&&b.options.infinite===!0?b.slideOffset+=b.slideWidth*Math.floor(b.options.slidesToShow/2)-b.slideWidth:b.options.centerMode===!0&&(b.slideOffset=0,b.slideOffset+=b.slideWidth*Math.floor(b.options.slidesToShow/2)),c=b.options.vertical===!1?-1*a*b.slideWidth+b.slideOffset:-1*a*d+e,b.options.variableWidth===!0&&(f=b.slideCount<=b.options.slidesToShow||b.options.infinite===!1?b.$slideTrack.children(".slick-slide").eq(a):b.$slideTrack.children(".slick-slide").eq(a+b.options.slidesToShow),c=f[0]?-1*f[0].offsetLeft:0,b.options.centerMode===!0&&(f=b.options.infinite===!1?b.$slideTrack.children(".slick-slide").eq(a):b.$slideTrack.children(".slick-slide").eq(a+b.options.slidesToShow+1),c=f[0]?-1*f[0].offsetLeft:0,c+=(b.$list.width()-f.outerWidth())/2)),c},b.prototype.getOption=b.prototype.slickGetOption=function(a){var b=this;return b.options[a]},b.prototype.getNavigableIndexes=function(){var e,a=this,b=0,c=0,d=[];for(a.options.infinite===!1?e=a.slideCount:(b=-1*a.options.slidesToScroll,c=-1*a.options.slidesToScroll,e=2*a.slideCount);e>b;)d.push(b),b=c+a.options.slidesToScroll,c+=a.options.slidesToScroll<=a.options.slidesToShow?a.options.slidesToScroll:a.options.slidesToShow;return d},b.prototype.getSlick=function(){return this},b.prototype.getSlideCount=function(){var c,d,e,b=this;return e=b.options.centerMode===!0?b.slideWidth*Math.floor(b.options.slidesToShow/2):0,b.options.swipeToSlide===!0?(b.$slideTrack.find(".slick-slide").each(function(c,f){return f.offsetLeft-e+a(f).outerWidth()/2>-1*b.swipeLeft?(d=f,!1):void 0}),c=Math.abs(a(d).attr("data-slick-index")-b.currentSlide)||1):b.options.slidesToScroll},b.prototype.goTo=b.prototype.slickGoTo=function(a,b){var c=this;c.changeSlide({data:{message:"index",index:parseInt(a)}},b)},b.prototype.init=function(b){var c=this;a(c.$slider).hasClass("slick-initialized")||(a(c.$slider).addClass("slick-initialized"),c.buildRows(),c.buildOut(),c.setProps(),c.startLoad(),c.loadSlider(),c.initializeEvents(),c.updateArrows(),c.updateDots()),b&&c.$slider.trigger("init",[c]),c.options.accessibility===!0&&c.initADA()},b.prototype.initArrowEvents=function(){var a=this;a.options.arrows===!0&&a.slideCount>a.options.slidesToShow&&(a.$prevArrow.on("click.slick",{message:"previous"},a.changeSlide),a.$nextArrow.on("click.slick",{message:"next"},a.changeSlide))},b.prototype.initDotEvents=function(){var b=this;b.options.dots===!0&&b.slideCount>b.options.slidesToShow&&a("li",b.$dots).on("click.slick",{message:"index"},b.changeSlide),b.options.dots===!0&&b.options.pauseOnDotsHover===!0&&b.options.autoplay===!0&&a("li",b.$dots).on("mouseenter.slick",a.proxy(b.setPaused,b,!0)).on("mouseleave.slick",a.proxy(b.setPaused,b,!1))},b.prototype.initializeEvents=function(){var b=this;b.initArrowEvents(),b.initDotEvents(),b.$list.on("touchstart.slick mousedown.slick",{action:"start"},b.swipeHandler),b.$list.on("touchmove.slick mousemove.slick",{action:"move"},b.swipeHandler),b.$list.on("touchend.slick mouseup.slick",{action:"end"},b.swipeHandler),b.$list.on("touchcancel.slick mouseleave.slick",{action:"end"},b.swipeHandler),b.$list.on("click.slick",b.clickHandler),a(document).on(b.visibilityChange,a.proxy(b.visibility,b)),b.$list.on("mouseenter.slick",a.proxy(b.setPaused,b,!0)),b.$list.on("mouseleave.slick",a.proxy(b.setPaused,b,!1)),b.options.accessibility===!0&&b.$list.on("keydown.slick",b.keyHandler),b.options.focusOnSelect===!0&&a(b.$slideTrack).children().on("click.slick",b.selectHandler),a(window).on("orientationchange.slick.slick-"+b.instanceUid,a.proxy(b.orientationChange,b)),a(window).on("resize.slick.slick-"+b.instanceUid,a.proxy(b.resize,b)),a("[draggable!=true]",b.$slideTrack).on("dragstart",b.preventDefault),a(window).on("load.slick.slick-"+b.instanceUid,b.setPosition),a(document).on("ready.slick.slick-"+b.instanceUid,b.setPosition)},b.prototype.initUI=function(){var a=this;a.options.arrows===!0&&a.slideCount>a.options.slidesToShow&&(a.$prevArrow.show(),a.$nextArrow.show()),a.options.dots===!0&&a.slideCount>a.options.slidesToShow&&a.$dots.show(),a.options.autoplay===!0&&a.autoPlay()},b.prototype.keyHandler=function(a){var b=this;a.target.tagName.match("TEXTAREA|INPUT|SELECT")||(37===a.keyCode&&b.options.accessibility===!0?b.changeSlide({data:{message:"previous"}}):39===a.keyCode&&b.options.accessibility===!0&&b.changeSlide({data:{message:"next"}}))},b.prototype.lazyLoad=function(){function g(b){a("img[data-lazy]",b).each(function(){var b=a(this),c=a(this).attr("data-lazy"),d=document.createElement("img");d.onload=function(){b.animate({opacity:0},100,function(){b.attr("src",c).animate({opacity:1},200,function(){b.removeAttr("data-lazy").removeClass("slick-loading")})})},d.src=c})}var c,d,e,f,b=this;b.options.centerMode===!0?b.options.infinite===!0?(e=b.currentSlide+(b.options.slidesToShow/2+1),f=e+b.options.slidesToShow+2):(e=Math.max(0,b.currentSlide-(b.options.slidesToShow/2+1)),f=2+(b.options.slidesToShow/2+1)+b.currentSlide):(e=b.options.infinite?b.options.slidesToShow+b.currentSlide:b.currentSlide,f=e+b.options.slidesToShow,b.options.fade===!0&&(e>0&&e--,f<=b.slideCount&&f++)),c=b.$slider.find(".slick-slide").slice(e,f),g(c),b.slideCount<=b.options.slidesToShow?(d=b.$slider.find(".slick-slide"),g(d)):b.currentSlide>=b.slideCount-b.options.slidesToShow?(d=b.$slider.find(".slick-cloned").slice(0,b.options.slidesToShow),g(d)):0===b.currentSlide&&(d=b.$slider.find(".slick-cloned").slice(-1*b.options.slidesToShow),g(d))},b.prototype.loadSlider=function(){var a=this;a.setPosition(),a.$slideTrack.css({opacity:1}),a.$slider.removeClass("slick-loading"),a.initUI(),"progressive"===a.options.lazyLoad&&a.progressiveLazyLoad()},b.prototype.next=b.prototype.slickNext=function(){var a=this;a.changeSlide({data:{message:"next"}})},b.prototype.orientationChange=function(){var a=this;a.checkResponsive(),a.setPosition()},b.prototype.pause=b.prototype.slickPause=function(){var a=this;a.autoPlayClear(),a.paused=!0},b.prototype.play=b.prototype.slickPlay=function(){var a=this;a.paused=!1,a.autoPlay()},b.prototype.postSlide=function(a){var b=this;b.$slider.trigger("afterChange",[b,a]),b.animating=!1,b.setPosition(),b.swipeLeft=null,b.options.autoplay===!0&&b.paused===!1&&b.autoPlay(),b.options.accessibility===!0&&b.initADA()},b.prototype.prev=b.prototype.slickPrev=function(){var a=this;a.changeSlide({data:{message:"previous"}})},b.prototype.preventDefault=function(a){a.preventDefault()},b.prototype.progressiveLazyLoad=function(){var c,d,b=this;c=a("img[data-lazy]",b.$slider).length,c>0&&(d=a("img[data-lazy]",b.$slider).first(),d.attr("src",d.attr("data-lazy")).removeClass("slick-loading").load(function(){d.removeAttr("data-lazy"),b.progressiveLazyLoad(),b.options.adaptiveHeight===!0&&b.setPosition()}).error(function(){d.removeAttr("data-lazy"),b.progressiveLazyLoad()}))},b.prototype.refresh=function(b){var c=this,d=c.currentSlide;c.destroy(!0),a.extend(c,c.initials,{currentSlide:d}),c.init(),b||c.changeSlide({data:{message:"index",index:d}},!1)},b.prototype.registerBreakpoints=function(){var c,d,e,b=this,f=b.options.responsive||null;if("array"===a.type(f)&&f.length){b.respondTo=b.options.respondTo||"window";for(c in f)if(e=b.breakpoints.length-1,d=f[c].breakpoint,f.hasOwnProperty(c)){for(;e>=0;)b.breakpoints[e]&&b.breakpoints[e]===d&&b.breakpoints.splice(e,1),e--;b.breakpoints.push(d),b.breakpointSettings[d]=f[c].settings}b.breakpoints.sort(function(a,c){return b.options.mobileFirst?a-c:c-a})}},b.prototype.reinit=function(){var b=this;b.$slides=b.$slideTrack.children(b.options.slide).addClass("slick-slide"),b.slideCount=b.$slides.length,b.currentSlide>=b.slideCount&&0!==b.currentSlide&&(b.currentSlide=b.currentSlide-b.options.slidesToScroll),b.slideCount<=b.options.slidesToShow&&(b.currentSlide=0),b.registerBreakpoints(),b.setProps(),b.setupInfinite(),b.buildArrows(),b.updateArrows(),b.initArrowEvents(),b.buildDots(),b.updateDots(),b.initDotEvents(),b.checkResponsive(!1,!0),b.options.focusOnSelect===!0&&a(b.$slideTrack).children().on("click.slick",b.selectHandler),b.setSlideClasses(0),b.setPosition(),b.$slider.trigger("reInit",[b]),b.options.autoplay===!0&&b.focusHandler()},b.prototype.resize=function(){var b=this;a(window).width()!==b.windowWidth&&(clearTimeout(b.windowDelay),b.windowDelay=window.setTimeout(function(){b.windowWidth=a(window).width(),b.checkResponsive(),b.unslicked||b.setPosition()},50))},b.prototype.removeSlide=b.prototype.slickRemove=function(a,b,c){var d=this;return"boolean"==typeof a?(b=a,a=b===!0?0:d.slideCount-1):a=b===!0?--a:a,d.slideCount<1||0>a||a>d.slideCount-1?!1:(d.unload(),c===!0?d.$slideTrack.children().remove():d.$slideTrack.children(this.options.slide).eq(a).remove(),d.$slides=d.$slideTrack.children(this.options.slide),d.$slideTrack.children(this.options.slide).detach(),d.$slideTrack.append(d.$slides),d.$slidesCache=d.$slides,d.reinit(),void 0)},b.prototype.setCSS=function(a){var d,e,b=this,c={};b.options.rtl===!0&&(a=-a),d="left"==b.positionProp?Math.ceil(a)+"px":"0px",e="top"==b.positionProp?Math.ceil(a)+"px":"0px",c[b.positionProp]=a,b.transformsEnabled===!1?b.$slideTrack.css(c):(c={},b.cssTransitions===!1?(c[b.animType]="translate("+d+", "+e+")",b.$slideTrack.css(c)):(c[b.animType]="translate3d("+d+", "+e+", 0px)",b.$slideTrack.css(c)))},b.prototype.setDimensions=function(){var a=this;a.options.vertical===!1?a.options.centerMode===!0&&a.$list.css({padding:"0px "+a.options.centerPadding}):(a.$list.height(a.$slides.first().outerHeight(!0)*a.options.slidesToShow),a.options.centerMode===!0&&a.$list.css({padding:a.options.centerPadding+" 0px"})),a.listWidth=a.$list.width(),a.listHeight=a.$list.height(),a.options.vertical===!1&&a.options.variableWidth===!1?(a.slideWidth=Math.ceil(a.listWidth/a.options.slidesToShow),a.$slideTrack.width(Math.ceil(a.slideWidth*a.$slideTrack.children(".slick-slide").length))):a.options.variableWidth===!0?a.$slideTrack.width(5e3*a.slideCount):(a.slideWidth=Math.ceil(a.listWidth),a.$slideTrack.height(Math.ceil(a.$slides.first().outerHeight(!0)*a.$slideTrack.children(".slick-slide").length)));var b=a.$slides.first().outerWidth(!0)-a.$slides.first().width();a.options.variableWidth===!1&&a.$slideTrack.children(".slick-slide").width(a.slideWidth-b)},b.prototype.setFade=function(){var c,b=this;b.$slides.each(function(d,e){c=-1*b.slideWidth*d,b.options.rtl===!0?a(e).css({position:"relative",right:c,top:0,zIndex:b.options.zIndex-2,opacity:0}):a(e).css({position:"relative",left:c,top:0,zIndex:b.options.zIndex-2,opacity:0})}),b.$slides.eq(b.currentSlide).css({zIndex:b.options.zIndex-1,opacity:1})},b.prototype.setHeight=function(){var a=this;if(1===a.options.slidesToShow&&a.options.adaptiveHeight===!0&&a.options.vertical===!1){var b=a.$slides.eq(a.currentSlide).outerHeight(!0);a.$list.css("height",b)}},b.prototype.setOption=b.prototype.slickSetOption=function(b,c,d){var f,g,e=this;if("responsive"===b&&"array"===a.type(c))for(g in c)if("array"!==a.type(e.options.responsive))e.options.responsive=[c[g]];else{for(f=e.options.responsive.length-1;f>=0;)e.options.responsive[f].breakpoint===c[g].breakpoint&&e.options.responsive.splice(f,1),f--;e.options.responsive.push(c[g])}else e.options[b]=c;d===!0&&(e.unload(),e.reinit())},b.prototype.setPosition=function(){var a=this;a.setDimensions(),a.setHeight(),a.options.fade===!1?a.setCSS(a.getLeft(a.currentSlide)):a.setFade(),a.$slider.trigger("setPosition",[a])},b.prototype.setProps=function(){var a=this,b=document.body.style;a.positionProp=a.options.vertical===!0?"top":"left","top"===a.positionProp?a.$slider.addClass("slick-vertical"):a.$slider.removeClass("slick-vertical"),(void 0!==b.WebkitTransition||void 0!==b.MozTransition||void 0!==b.msTransition)&&a.options.useCSS===!0&&(a.cssTransitions=!0),a.options.fade&&("number"==typeof a.options.zIndex?a.options.zIndex<3&&(a.options.zIndex=3):a.options.zIndex=a.defaults.zIndex),void 0!==b.OTransform&&(a.animType="OTransform",a.transformType="-o-transform",a.transitionType="OTransition",void 0===b.perspectiveProperty&&void 0===b.webkitPerspective&&(a.animType=!1)),void 0!==b.MozTransform&&(a.animType="MozTransform",a.transformType="-moz-transform",a.transitionType="MozTransition",void 0===b.perspectiveProperty&&void 0===b.MozPerspective&&(a.animType=!1)),void 0!==b.webkitTransform&&(a.animType="webkitTransform",a.transformType="-webkit-transform",a.transitionType="webkitTransition",void 0===b.perspectiveProperty&&void 0===b.webkitPerspective&&(a.animType=!1)),void 0!==b.msTransform&&(a.animType="msTransform",a.transformType="-ms-transform",a.transitionType="msTransition",void 0===b.msTransform&&(a.animType=!1)),void 0!==b.transform&&a.animType!==!1&&(a.animType="transform",a.transformType="transform",a.transitionType="transition"),a.transformsEnabled=null!==a.animType&&a.animType!==!1},b.prototype.setSlideClasses=function(a){var c,d,e,f,b=this;d=b.$slider.find(".slick-slide").removeClass("slick-active slick-center slick-current").attr("aria-hidden","true"),b.$slides.eq(a).addClass("slick-current"),b.options.centerMode===!0?(c=Math.floor(b.options.slidesToShow/2),b.options.infinite===!0&&(a>=c&&a<=b.slideCount-1-c?b.$slides.slice(a-c,a+c+1).addClass("slick-active").attr("aria-hidden","false"):(e=b.options.slidesToShow+a,d.slice(e-c+1,e+c+2).addClass("slick-active").attr("aria-hidden","false")),0===a?d.eq(d.length-1-b.options.slidesToShow).addClass("slick-center"):a===b.slideCount-1&&d.eq(b.options.slidesToShow).addClass("slick-center")),b.$slides.eq(a).addClass("slick-center")):a>=0&&a<=b.slideCount-b.options.slidesToShow?b.$slides.slice(a,a+b.options.slidesToShow).addClass("slick-active").attr("aria-hidden","false"):d.length<=b.options.slidesToShow?d.addClass("slick-active").attr("aria-hidden","false"):(f=b.slideCount%b.options.slidesToShow,e=b.options.infinite===!0?b.options.slidesToShow+a:a,b.options.slidesToShow==b.options.slidesToScroll&&b.slideCount-a<b.options.slidesToShow?d.slice(e-(b.options.slidesToShow-f),e+f).addClass("slick-active").attr("aria-hidden","false"):d.slice(e,e+b.options.slidesToShow).addClass("slick-active").attr("aria-hidden","false")),"ondemand"===b.options.lazyLoad&&b.lazyLoad()},b.prototype.setupInfinite=function(){var c,d,e,b=this;if(b.options.fade===!0&&(b.options.centerMode=!1),b.options.infinite===!0&&b.options.fade===!1&&(d=null,b.slideCount>b.options.slidesToShow)){for(e=b.options.centerMode===!0?b.options.slidesToShow+1:b.options.slidesToShow,c=b.slideCount;c>b.slideCount-e;c-=1)d=c-1,a(b.$slides[d]).clone(!0).attr("id","").attr("data-slick-index",d-b.slideCount).prependTo(b.$slideTrack).addClass("slick-cloned");for(c=0;e>c;c+=1)d=c,a(b.$slides[d]).clone(!0).attr("id","").attr("data-slick-index",d+b.slideCount).appendTo(b.$slideTrack).addClass("slick-cloned");b.$slideTrack.find(".slick-cloned").find("[id]").each(function(){a(this).attr("id","")})}},b.prototype.setPaused=function(a){var b=this;b.options.autoplay===!0&&b.options.pauseOnHover===!0&&(b.paused=a,a?b.autoPlayClear():b.autoPlay())},b.prototype.selectHandler=function(b){var c=this,d=a(b.target).is(".slick-slide")?a(b.target):a(b.target).parents(".slick-slide"),e=parseInt(d.attr("data-slick-index"));return e||(e=0),c.slideCount<=c.options.slidesToShow?(c.setSlideClasses(e),c.asNavFor(e),void 0):(c.slideHandler(e),void 0)},b.prototype.slideHandler=function(a,b,c){var d,e,f,g,h=null,i=this;return b=b||!1,i.animating===!0&&i.options.waitForAnimate===!0||i.options.fade===!0&&i.currentSlide===a||i.slideCount<=i.options.slidesToShow?void 0:(b===!1&&i.asNavFor(a),d=a,h=i.getLeft(d),g=i.getLeft(i.currentSlide),i.currentLeft=null===i.swipeLeft?g:i.swipeLeft,i.options.infinite===!1&&i.options.centerMode===!1&&(0>a||a>i.getDotCount()*i.options.slidesToScroll)?(i.options.fade===!1&&(d=i.currentSlide,c!==!0?i.animateSlide(g,function(){i.postSlide(d)}):i.postSlide(d)),void 0):i.options.infinite===!1&&i.options.centerMode===!0&&(0>a||a>i.slideCount-i.options.slidesToScroll)?(i.options.fade===!1&&(d=i.currentSlide,c!==!0?i.animateSlide(g,function(){i.postSlide(d)}):i.postSlide(d)),void 0):(i.options.autoplay===!0&&clearInterval(i.autoPlayTimer),e=0>d?0!==i.slideCount%i.options.slidesToScroll?i.slideCount-i.slideCount%i.options.slidesToScroll:i.slideCount+d:d>=i.slideCount?0!==i.slideCount%i.options.slidesToScroll?0:d-i.slideCount:d,i.animating=!0,i.$slider.trigger("beforeChange",[i,i.currentSlide,e]),f=i.currentSlide,i.currentSlide=e,i.setSlideClasses(i.currentSlide),i.updateDots(),i.updateArrows(),i.options.fade===!0?(c!==!0?(i.fadeSlideOut(f),i.fadeSlide(e,function(){i.postSlide(e)
})):i.postSlide(e),i.animateHeight(),void 0):(c!==!0?i.animateSlide(h,function(){i.postSlide(e)}):i.postSlide(e),void 0)))},b.prototype.startLoad=function(){var a=this;a.options.arrows===!0&&a.slideCount>a.options.slidesToShow&&(a.$prevArrow.hide(),a.$nextArrow.hide()),a.options.dots===!0&&a.slideCount>a.options.slidesToShow&&a.$dots.hide(),a.$slider.addClass("slick-loading")},b.prototype.swipeDirection=function(){var a,b,c,d,e=this;return a=e.touchObject.startX-e.touchObject.curX,b=e.touchObject.startY-e.touchObject.curY,c=Math.atan2(b,a),d=Math.round(180*c/Math.PI),0>d&&(d=360-Math.abs(d)),45>=d&&d>=0?e.options.rtl===!1?"left":"right":360>=d&&d>=315?e.options.rtl===!1?"left":"right":d>=135&&225>=d?e.options.rtl===!1?"right":"left":e.options.verticalSwiping===!0?d>=35&&135>=d?"left":"right":"vertical"},b.prototype.swipeEnd=function(){var c,b=this;if(b.dragging=!1,b.shouldClick=b.touchObject.swipeLength>10?!1:!0,void 0===b.touchObject.curX)return!1;if(b.touchObject.edgeHit===!0&&b.$slider.trigger("edge",[b,b.swipeDirection()]),b.touchObject.swipeLength>=b.touchObject.minSwipe)switch(b.swipeDirection()){case"left":c=b.options.swipeToSlide?b.checkNavigable(b.currentSlide+b.getSlideCount()):b.currentSlide+b.getSlideCount(),b.slideHandler(c),b.currentDirection=0,b.touchObject={},b.$slider.trigger("swipe",[b,"left"]);break;case"right":c=b.options.swipeToSlide?b.checkNavigable(b.currentSlide-b.getSlideCount()):b.currentSlide-b.getSlideCount(),b.slideHandler(c),b.currentDirection=1,b.touchObject={},b.$slider.trigger("swipe",[b,"right"])}else b.touchObject.startX!==b.touchObject.curX&&(b.slideHandler(b.currentSlide),b.touchObject={})},b.prototype.swipeHandler=function(a){var b=this;if(!(b.options.swipe===!1||"ontouchend"in document&&b.options.swipe===!1||b.options.draggable===!1&&-1!==a.type.indexOf("mouse")))switch(b.touchObject.fingerCount=a.originalEvent&&void 0!==a.originalEvent.touches?a.originalEvent.touches.length:1,b.touchObject.minSwipe=b.listWidth/b.options.touchThreshold,b.options.verticalSwiping===!0&&(b.touchObject.minSwipe=b.listHeight/b.options.touchThreshold),a.data.action){case"start":b.swipeStart(a);break;case"move":b.swipeMove(a);break;case"end":b.swipeEnd(a)}},b.prototype.swipeMove=function(a){var d,e,f,g,h,b=this;return h=void 0!==a.originalEvent?a.originalEvent.touches:null,!b.dragging||h&&1!==h.length?!1:(d=b.getLeft(b.currentSlide),b.touchObject.curX=void 0!==h?h[0].pageX:a.clientX,b.touchObject.curY=void 0!==h?h[0].pageY:a.clientY,b.touchObject.swipeLength=Math.round(Math.sqrt(Math.pow(b.touchObject.curX-b.touchObject.startX,2))),b.options.verticalSwiping===!0&&(b.touchObject.swipeLength=Math.round(Math.sqrt(Math.pow(b.touchObject.curY-b.touchObject.startY,2)))),e=b.swipeDirection(),"vertical"!==e?(void 0!==a.originalEvent&&b.touchObject.swipeLength>4&&a.preventDefault(),g=(b.options.rtl===!1?1:-1)*(b.touchObject.curX>b.touchObject.startX?1:-1),b.options.verticalSwiping===!0&&(g=b.touchObject.curY>b.touchObject.startY?1:-1),f=b.touchObject.swipeLength,b.touchObject.edgeHit=!1,b.options.infinite===!1&&(0===b.currentSlide&&"right"===e||b.currentSlide>=b.getDotCount()&&"left"===e)&&(f=b.touchObject.swipeLength*b.options.edgeFriction,b.touchObject.edgeHit=!0),b.swipeLeft=b.options.vertical===!1?d+f*g:d+f*(b.$list.height()/b.listWidth)*g,b.options.verticalSwiping===!0&&(b.swipeLeft=d+f*g),b.options.fade===!0||b.options.touchMove===!1?!1:b.animating===!0?(b.swipeLeft=null,!1):(b.setCSS(b.swipeLeft),void 0)):void 0)},b.prototype.swipeStart=function(a){var c,b=this;return 1!==b.touchObject.fingerCount||b.slideCount<=b.options.slidesToShow?(b.touchObject={},!1):(void 0!==a.originalEvent&&void 0!==a.originalEvent.touches&&(c=a.originalEvent.touches[0]),b.touchObject.startX=b.touchObject.curX=void 0!==c?c.pageX:a.clientX,b.touchObject.startY=b.touchObject.curY=void 0!==c?c.pageY:a.clientY,b.dragging=!0,void 0)},b.prototype.unfilterSlides=b.prototype.slickUnfilter=function(){var a=this;null!==a.$slidesCache&&(a.unload(),a.$slideTrack.children(this.options.slide).detach(),a.$slidesCache.appendTo(a.$slideTrack),a.reinit())},b.prototype.unload=function(){var b=this;a(".slick-cloned",b.$slider).remove(),b.$dots&&b.$dots.remove(),b.$prevArrow&&b.htmlExpr.test(b.options.prevArrow)&&b.$prevArrow.remove(),b.$nextArrow&&b.htmlExpr.test(b.options.nextArrow)&&b.$nextArrow.remove(),b.$slides.removeClass("slick-slide slick-active slick-visible slick-current").attr("aria-hidden","true").css("width","")},b.prototype.unslick=function(a){var b=this;b.$slider.trigger("unslick",[b,a]),b.destroy()},b.prototype.updateArrows=function(){var b,a=this;b=Math.floor(a.options.slidesToShow/2),a.options.arrows===!0&&a.slideCount>a.options.slidesToShow&&!a.options.infinite&&(a.$prevArrow.removeClass("slick-disabled").attr("aria-disabled","false"),a.$nextArrow.removeClass("slick-disabled").attr("aria-disabled","false"),0===a.currentSlide?(a.$prevArrow.addClass("slick-disabled").attr("aria-disabled","true"),a.$nextArrow.removeClass("slick-disabled").attr("aria-disabled","false")):a.currentSlide>=a.slideCount-a.options.slidesToShow&&a.options.centerMode===!1?(a.$nextArrow.addClass("slick-disabled").attr("aria-disabled","true"),a.$prevArrow.removeClass("slick-disabled").attr("aria-disabled","false")):a.currentSlide>=a.slideCount-1&&a.options.centerMode===!0&&(a.$nextArrow.addClass("slick-disabled").attr("aria-disabled","true"),a.$prevArrow.removeClass("slick-disabled").attr("aria-disabled","false")))},b.prototype.updateDots=function(){var a=this;null!==a.$dots&&(a.$dots.find("li").removeClass("slick-active").attr("aria-hidden","true"),a.$dots.find("li").eq(Math.floor(a.currentSlide/a.options.slidesToScroll)).addClass("slick-active").attr("aria-hidden","false"))},b.prototype.visibility=function(){var a=this;document[a.hidden]?(a.paused=!0,a.autoPlayClear()):a.options.autoplay===!0&&(a.paused=!1,a.autoPlay())},b.prototype.initADA=function(){var b=this;b.$slides.add(b.$slideTrack.find(".slick-cloned")).attr({"aria-hidden":"true",tabindex:"-1"}).find("a, input, button, select").attr({tabindex:"-1"}),b.$slideTrack.attr("role","listbox"),b.$slides.not(b.$slideTrack.find(".slick-cloned")).each(function(c){a(this).attr({role:"option","aria-describedby":"slick-slide"+b.instanceUid+c})}),null!==b.$dots&&b.$dots.attr("role","tablist").find("li").each(function(c){a(this).attr({role:"presentation","aria-selected":"false","aria-controls":"navigation"+b.instanceUid+c,id:"slick-slide"+b.instanceUid+c})}).first().attr("aria-selected","true").end().find("button").attr("role","button").end().closest("div").attr("role","toolbar"),b.activateADA()},b.prototype.activateADA=function(){var a=this,b=a.$slider.find("*").is(":focus");a.$slideTrack.find(".slick-active").attr({"aria-hidden":"false",tabindex:"0"}).find("a, input, button, select").attr({tabindex:"0"}),b&&a.$slideTrack.find(".slick-active").focus()},b.prototype.focusHandler=function(){var b=this;b.$slider.on("focus.slick blur.slick","*",function(c){c.stopImmediatePropagation();var d=a(this);setTimeout(function(){b.isPlay&&(d.is(":focus")?(b.autoPlayClear(),b.paused=!0):(b.paused=!1,b.autoPlay()))},0)})},a.fn.slick=function(){var g,a=this,c=arguments[0],d=Array.prototype.slice.call(arguments,1),e=a.length,f=0;for(f;e>f;f++)if("object"==typeof c||"undefined"==typeof c?a[f].slick=new b(a[f],c):g=a[f].slick[c].apply(a[f].slick,d),"undefined"!=typeof g)return g;return a}});
(function ($) {
	$('.autoplay').slick({
	    slidesToShow: 6,
	    slidesToScroll: 1,
	    autoplay: true,
	    autoplaySpeed: 2000,
	    responsive: [
		    {
		      breakpoint: 1024,
		      settings: {
		        slidesToShow: 4,
		        slidesToScroll: 2,
		        infinite: true,
		        dots: true
		      }
		    },
		    {
		      breakpoint: 768,
		      settings: {
		        slidesToShow: 2,
		        slidesToScroll: 1
		      }
		    },
		]
	});

	$('.slider-about').slick({
	});

    $('.multiple-items').slick({
    	dots: true,
		infinite: true,
		slidesToShow: 3,
		slidesToScroll: 1,
		responsive: [
		    {
		      breakpoint: 1024,
		      settings: {
		        slidesToShow: 2,
		        slidesToScroll: 1,
		        infinite: true,
		        dots: true
		      }
		    },
		    {
		      breakpoint: 768,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1
		      }
		    },
		]
	});

	$('.member-slider').slick({
		infinite: true,
		dots:true,
		slidesToShow: 4,
		slidesToScroll: 1,
		responsive: [
			{
		      breakpoint: 1200,
		      settings: {
		        slidesToShow: 3,
		        slidesToScroll: 1,
		        infinite: true,
		        dots: true
		      }
		    },
		    {
		      breakpoint: 1024,
		      settings: {
		        slidesToShow: 2,
		        slidesToScroll: 1,
		        infinite: true,
		        dots: true
		      }
		    },
		    {
		      breakpoint: 768,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1
		      }
		    },
		]
	});

  	$('.slider-post').slick({
  		prevArrow: $( '.nav-prev' ),
		nextArrow: $( '.nav-next' ),
		infinite: true,
		slidesToShow: 3,
		slidesToScroll: 1,
		responsive: [
		    {
		      breakpoint: 1024,
		      settings: {
		        slidesToShow: 2,
		        slidesToScroll: 1,
		        infinite: true,
		      }
		    },
		    {
		      breakpoint: 768,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1
		      }
		    },
		]
	});

	$('.single-item').slick({
		prevArrow: $( '.prevarrow' ),
		nextArrow: $( '.nextarrow' )
	});

	$('.slider-for').slick({
		slidesToShow: 1,
		arrows: false,
		fade: true,
		asNavFor: '.slider-nav'
	});
	$('.slider-nav').slick({
		slidesToShow: 4,
		asNavFor: '.slider-for',
		dots: false,
		centerMode: true,
		focusOnSelect: true
	});

	$('.single-slider').slick({
		prevArrow: $( '.prev-slider' ),
		nextArrow: $( '.next-slider' ),
		infinite: true,
		slidesToShow: 3,
		slidesToScroll: 1,
		responsive: [
		    {
		      breakpoint: 1024,
		      settings: {
		        slidesToShow: 2,
		        slidesToScroll: 1,
		        infinite: true,
		      }
		    },
		    {
		      breakpoint: 768,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1,
		       	infinite: true
		      }
		    },
		]
	});
		
}(jQuery));
( function($) {
  const filters = Array.from(document.querySelectorAll('.listing-filter'));
  const expandToggle = document.querySelector('.js-expand');
  const filterCheckbox = document.querySelector('.listing-more-filter');

  filters.forEach(filter => {
    filter.addEventListener('click', e => {
      e.preventDefault();
      const currentFilter = e.target.closest('li');
      if (!currentFilter) return;
      const filterLi = Array.from(filter.querySelectorAll('li'));
      filterLi.forEach(item => {
        item.classList.remove('is-active');
      });
      currentFilter.classList.add('is-active');
    });
  });

  if (expandToggle) {
    expandToggle.addEventListener('click', e => {
      e.preventDefault();
      expandToggle.classList.toggle('is-active');
      const height = filterCheckbox.firstElementChild.getBoundingClientRect()
        .height;
      if (expandToggle.classList.contains('is-active')) {
        filterCheckbox.style.height = `${height}px`;
      } else {
        filterCheckbox.style.height = `0`;
      }
    });
  }
} (jQuery));
/*! Lity - v2.3.1 - 2018-04-20
* http://sorgalla.com/lity/
* Copyright (c) 2015-2018 Jan Sorgalla; Licensed MIT */
(function(window, factory) {
    if (typeof define === 'function' && define.amd) {
        define(['jquery'], function($) {
            return factory(window, $);
        });
    } else if (typeof module === 'object' && typeof module.exports === 'object') {
        module.exports = factory(window, require('jquery'));
    } else {
        window.lity = factory(window, window.jQuery || window.Zepto);
    }
}(typeof window !== "undefined" ? window : this, function(window, $) {
    'use strict';

    var document = window.document;

    var _win = $(window);
    var _deferred = $.Deferred;
    var _html = $('html');
    var _instances = [];

    var _attrAriaHidden = 'aria-hidden';
    var _dataAriaHidden = 'lity-' + _attrAriaHidden;

    var _focusableElementsSelector = 'a[href],area[href],input:not([disabled]),select:not([disabled]),textarea:not([disabled]),button:not([disabled]),iframe,object,embed,[contenteditable],[tabindex]:not([tabindex^="-"])';

    var _defaultOptions = {
        esc: true,
        handler: null,
        handlers: {
            image: imageHandler,
            inline: inlineHandler,
            youtube: youtubeHandler,
            vimeo: vimeoHandler,
            googlemaps: googlemapsHandler,
            facebookvideo: facebookvideoHandler,
            iframe: iframeHandler
        },
        template: '<div class="lity" role="dialog" aria-label="Dialog Window (Press escape to close)" tabindex="-1"><div class="lity-wrap" data-lity-close role="document"><div class="lity-loader" aria-hidden="true">Loading...</div><div class="lity-container"><div class="lity-content"></div><button class="lity-close" type="button" aria-label="Close (Press escape to close)" data-lity-close>&times;</button></div></div></div>'
    };

    var _imageRegexp = /(^data:image\/)|(\.(png|jpe?g|gif|svg|webp|bmp|ico|tiff?)(\?\S*)?$)/i;
    var _youtubeRegex = /(youtube(-nocookie)?\.com|youtu\.be)\/(watch\?v=|v\/|u\/|embed\/?)?([\w-]{11})(.*)?/i;
    var _vimeoRegex =  /(vimeo(pro)?.com)\/(?:[^\d]+)?(\d+)\??(.*)?$/;
    var _googlemapsRegex = /((maps|www)\.)?google\.([^\/\?]+)\/?((maps\/?)?\?)(.*)/i;
    var _facebookvideoRegex = /(facebook\.com)\/([a-z0-9_-]*)\/videos\/([0-9]*)(.*)?$/i;

    var _transitionEndEvent = (function() {
        var el = document.createElement('div');

        var transEndEventNames = {
            WebkitTransition: 'webkitTransitionEnd',
            MozTransition: 'transitionend',
            OTransition: 'oTransitionEnd otransitionend',
            transition: 'transitionend'
        };

        for (var name in transEndEventNames) {
            if (el.style[name] !== undefined) {
                return transEndEventNames[name];
            }
        }

        return false;
    })();

    function transitionEnd(element) {
        var deferred = _deferred();

        if (!_transitionEndEvent || !element.length) {
            deferred.resolve();
        } else {
            element.one(_transitionEndEvent, deferred.resolve);
            setTimeout(deferred.resolve, 500);
        }

        return deferred.promise();
    }

    function settings(currSettings, key, value) {
        if (arguments.length === 1) {
            return $.extend({}, currSettings);
        }

        if (typeof key === 'string') {
            if (typeof value === 'undefined') {
                return typeof currSettings[key] === 'undefined'
                    ? null
                    : currSettings[key];
            }

            currSettings[key] = value;
        } else {
            $.extend(currSettings, key);
        }

        return this;
    }

    function parseQueryParams(params) {
        var pairs = decodeURI(params.split('#')[0]).split('&');
        var obj = {}, p;

        for (var i = 0, n = pairs.length; i < n; i++) {
            if (!pairs[i]) {
                continue;
            }

            p = pairs[i].split('=');
            obj[p[0]] = p[1];
        }

        return obj;
    }

    function appendQueryParams(url, params) {
        return url + (url.indexOf('?') > -1 ? '&' : '?') + $.param(params);
    }

    function transferHash(originalUrl, newUrl) {
        var pos = originalUrl.indexOf('#');

        if (-1 === pos) {
            return newUrl;
        }

        if (pos > 0) {
            originalUrl = originalUrl.substr(pos);
        }

        return newUrl + originalUrl;
    }

    function error(msg) {
        return $('<span class="lity-error"/>').append(msg);
    }

    function imageHandler(target, instance) {
        var desc = (instance.opener() && instance.opener().data('lity-desc')) || 'Image with no description';
        var img = $('<img src="' + target + '" alt="' + desc + '"/>');
        var deferred = _deferred();
        var failed = function() {
            deferred.reject(error('Failed loading image'));
        };

        img
            .on('load', function() {
                if (this.naturalWidth === 0) {
                    return failed();
                }

                deferred.resolve(img);
            })
            .on('error', failed)
        ;

        return deferred.promise();
    }

    imageHandler.test = function(target) {
        return _imageRegexp.test(target);
    };

    function inlineHandler(target, instance) {
        var el, placeholder, hasHideClass;

        try {
            el = $(target);
        } catch (e) {
            return false;
        }

        if (!el.length) {
            return false;
        }

        placeholder = $('<i style="display:none !important"/>');
        hasHideClass = el.hasClass('lity-hide');

        instance
            .element()
            .one('lity:remove', function() {
                placeholder
                    .before(el)
                    .remove()
                ;

                if (hasHideClass && !el.closest('.lity-content').length) {
                    el.addClass('lity-hide');
                }
            })
        ;

        return el
            .removeClass('lity-hide')
            .after(placeholder)
        ;
    }

    function youtubeHandler(target) {
        var matches = _youtubeRegex.exec(target);

        if (!matches) {
            return false;
        }

        return iframeHandler(
            transferHash(
                target,
                appendQueryParams(
                    'https://www.youtube' + (matches[2] || '') + '.com/embed/' + matches[4],
                    $.extend(
                        {
                            autoplay: 1
                        },
                        parseQueryParams(matches[5] || '')
                    )
                )
            )
        );
    }

    function vimeoHandler(target) {
        var matches = _vimeoRegex.exec(target);

        if (!matches) {
            return false;
        }

        return iframeHandler(
            transferHash(
                target,
                appendQueryParams(
                    'https://player.vimeo.com/video/' + matches[3],
                    $.extend(
                        {
                            autoplay: 1
                        },
                        parseQueryParams(matches[4] || '')
                    )
                )
            )
        );
    }

    function facebookvideoHandler(target) {
        var matches = _facebookvideoRegex.exec(target);

        if (!matches) {
            return false;
        }

        if (0 !== target.indexOf('http')) {
            target = 'https:' + target;
        }

        return iframeHandler(
            transferHash(
                target,
                appendQueryParams(
                    'https://www.facebook.com/plugins/video.php?href=' + target,
                    $.extend(
                        {
                            autoplay: 1
                        },
                        parseQueryParams(matches[4] || '')
                    )
                )
            )
        );
    }

    function googlemapsHandler(target) {
        var matches = _googlemapsRegex.exec(target);

        if (!matches) {
            return false;
        }

        return iframeHandler(
            transferHash(
                target,
                appendQueryParams(
                    'https://www.google.' + matches[3] + '/maps?' + matches[6],
                    {
                        output: matches[6].indexOf('layer=c') > 0 ? 'svembed' : 'embed'
                    }
                )
            )
        );
    }

    function iframeHandler(target) {
        return '<div class="lity-iframe-container"><iframe frameborder="0" allowfullscreen src="' + target + '"/></div>';
    }

    function winHeight() {
        return document.documentElement.clientHeight
            ? document.documentElement.clientHeight
            : Math.round(_win.height());
    }

    function keydown(e) {
        var current = currentInstance();

        if (!current) {
            return;
        }

        // ESC key
        if (e.keyCode === 27 && !!current.options('esc')) {
            current.close();
        }

        // TAB key
        if (e.keyCode === 9) {
            handleTabKey(e, current);
        }
    }

    function handleTabKey(e, instance) {
        var focusableElements = instance.element().find(_focusableElementsSelector);
        var focusedIndex = focusableElements.index(document.activeElement);

        if (e.shiftKey && focusedIndex <= 0) {
            focusableElements.get(focusableElements.length - 1).focus();
            e.preventDefault();
        } else if (!e.shiftKey && focusedIndex === focusableElements.length - 1) {
            focusableElements.get(0).focus();
            e.preventDefault();
        }
    }

    function resize() {
        $.each(_instances, function(i, instance) {
            instance.resize();
        });
    }

    function registerInstance(instanceToRegister) {
        if (1 === _instances.unshift(instanceToRegister)) {
            _html.addClass('lity-active');

            _win
                .on({
                    resize: resize,
                    keydown: keydown
                })
            ;
        }

        $('body > *').not(instanceToRegister.element())
            .addClass('lity-hidden')
            .each(function() {
                var el = $(this);

                if (undefined !== el.data(_dataAriaHidden)) {
                    return;
                }

                el.data(_dataAriaHidden, el.attr(_attrAriaHidden) || null);
            })
            .attr(_attrAriaHidden, 'true')
        ;
    }

    function removeInstance(instanceToRemove) {
        var show;

        instanceToRemove
            .element()
            .attr(_attrAriaHidden, 'true')
        ;

        if (1 === _instances.length) {
            _html.removeClass('lity-active');

            _win
                .off({
                    resize: resize,
                    keydown: keydown
                })
            ;
        }

        _instances = $.grep(_instances, function(instance) {
            return instanceToRemove !== instance;
        });

        if (!!_instances.length) {
            show = _instances[0].element();
        } else {
            show = $('.lity-hidden');
        }

        show
            .removeClass('lity-hidden')
            .each(function() {
                var el = $(this), oldAttr = el.data(_dataAriaHidden);

                if (!oldAttr) {
                    el.removeAttr(_attrAriaHidden);
                } else {
                    el.attr(_attrAriaHidden, oldAttr);
                }

                el.removeData(_dataAriaHidden);
            })
        ;
    }

    function currentInstance() {
        if (0 === _instances.length) {
            return null;
        }

        return _instances[0];
    }

    function factory(target, instance, handlers, preferredHandler) {
        var handler = 'inline', content;

        var currentHandlers = $.extend({}, handlers);

        if (preferredHandler && currentHandlers[preferredHandler]) {
            content = currentHandlers[preferredHandler](target, instance);
            handler = preferredHandler;
        } else {
            // Run inline and iframe handlers after all other handlers
            $.each(['inline', 'iframe'], function(i, name) {
                delete currentHandlers[name];

                currentHandlers[name] = handlers[name];
            });

            $.each(currentHandlers, function(name, currentHandler) {
                // Handler might be "removed" by setting callback to null
                if (!currentHandler) {
                    return true;
                }

                if (
                    currentHandler.test &&
                    !currentHandler.test(target, instance)
                ) {
                    return true;
                }

                content = currentHandler(target, instance);

                if (false !== content) {
                    handler = name;
                    return false;
                }
            });
        }

        return {handler: handler, content: content || ''};
    }

    function Lity(target, options, opener, activeElement) {
        var self = this;
        var result;
        var isReady = false;
        var isClosed = false;
        var element;
        var content;

        options = $.extend(
            {},
            _defaultOptions,
            options
        );

        element = $(options.template);

        // -- API --

        self.element = function() {
            return element;
        };

        self.opener = function() {
            return opener;
        };

        self.options  = $.proxy(settings, self, options);
        self.handlers = $.proxy(settings, self, options.handlers);

        self.resize = function() {
            if (!isReady || isClosed) {
                return;
            }

            content
                .css('max-height', winHeight() + 'px')
                .trigger('lity:resize', [self])
            ;
        };

        self.close = function() {
            if (!isReady || isClosed) {
                return;
            }

            isClosed = true;

            removeInstance(self);

            var deferred = _deferred();

            // We return focus only if the current focus is inside this instance
            if (
                activeElement &&
                (
                    document.activeElement === element[0] ||
                    $.contains(element[0], document.activeElement)
                )
            ) {
                try {
                    activeElement.focus();
                } catch (e) {
                    // Ignore exceptions, eg. for SVG elements which can't be
                    // focused in IE11
                }
            }

            content.trigger('lity:close', [self]);

            element
                .removeClass('lity-opened')
                .addClass('lity-closed')
            ;

            transitionEnd(content.add(element))
                .always(function() {
                    content.trigger('lity:remove', [self]);
                    element.remove();
                    element = undefined;
                    deferred.resolve();
                })
            ;

            return deferred.promise();
        };

        // -- Initialization --

        result = factory(target, self, options.handlers, options.handler);

        element
            .attr(_attrAriaHidden, 'false')
            .addClass('lity-loading lity-opened lity-' + result.handler)
            .appendTo('body')
            .focus()
            .on('click', '[data-lity-close]', function(e) {
                if ($(e.target).is('[data-lity-close]')) {
                    self.close();
                }
            })
            .trigger('lity:open', [self])
        ;

        registerInstance(self);

        $.when(result.content)
            .always(ready)
        ;

        function ready(result) {
            content = $(result)
                .css('max-height', winHeight() + 'px')
            ;

            element
                .find('.lity-loader')
                .each(function() {
                    var loader = $(this);

                    transitionEnd(loader)
                        .always(function() {
                            loader.remove();
                        })
                    ;
                })
            ;

            element
                .removeClass('lity-loading')
                .find('.lity-content')
                .empty()
                .append(content)
            ;

            isReady = true;

            content
                .trigger('lity:ready', [self])
            ;
        }
    }

    function lity(target, options, opener) {
        if (!target.preventDefault) {
            opener = $(opener);
        } else {
            target.preventDefault();
            opener = $(this);
            target = opener.data('lity-target') || opener.attr('href') || opener.attr('src');
        }

        var instance = new Lity(
            target,
            $.extend(
                {},
                opener.data('lity-options') || opener.data('lity'),
                options
            ),
            opener,
            document.activeElement
        );

        if (!target.preventDefault) {
            return instance;
        }
    }

    lity.version  = '2.3.1';
    lity.options  = $.proxy(settings, lity, _defaultOptions);
    lity.handlers = $.proxy(settings, lity, _defaultOptions.handlers);
    lity.current  = currentInstance;

    $(document).on('click.lity', '[data-lity]', lity);

    return lity;
}));

/**!
 * lightgallery.js | 1.0.0 | October 5th 2016
 * http://sachinchoolur.github.io/lightgallery.js/
 * Copyright (c) 2016 Sachin N; 
 * @license GPLv3 
 */(function(f){if(typeof exports==="object"&&typeof module!=="undefined"){module.exports=f()}else if(typeof define==="function"&&define.amd){define([],f)}else{var g;if(typeof window!=="undefined"){g=window}else if(typeof global!=="undefined"){g=global}else if(typeof self!=="undefined"){g=self}else{g=this}g.Lightgallery = f()}})(function(){var define,module,exports;return (function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
(function (global, factory) {
    if (typeof define === "function" && define.amd) {
        define(['exports'], factory);
    } else if (typeof exports !== "undefined") {
        factory(exports);
    } else {
        var mod = {
            exports: {}
        };
        factory(mod.exports);
        global.lgUtils = mod.exports;
    }
})(this, function (exports) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });

    /*
     *@todo remove function from window and document. Update on and off functions
     */
    window.getAttribute = function (label) {
        return window[label];
    };

    window.setAttribute = function (label, value) {
        window[label] = value;
    };

    document.getAttribute = function (label) {
        return document[label];
    };

    document.setAttribute = function (label, value) {
        document[label] = value;
    };

    var utils = {
        wrap: function wrap(el, className) {
            if (!el) {
                return;
            }

            var wrapper = document.createElement('div');
            wrapper.className = className;
            el.parentNode.insertBefore(wrapper, el);
            el.parentNode.removeChild(el);
            wrapper.appendChild(el);
        },

        addClass: function addClass(el, className) {
            if (!el) {
                return;
            }

            if (el.classList) {
                el.classList.add(className);
            } else {
                el.className += ' ' + className;
            }
        },

        removeClass: function removeClass(el, className) {
            if (!el) {
                return;
            }

            if (el.classList) {
                el.classList.remove(className);
            } else {
                el.className = el.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
            }
        },

        hasClass: function hasClass(el, className) {
            if (el.classList) {
                return el.classList.contains(className);
            } else {
                return new RegExp('(^| )' + className + '( |$)', 'gi').test(el.className);
            }

            return false;
        },

        // ex Transform
        // ex TransitionTimingFunction
        setVendor: function setVendor(el, property, value) {
            if (!el) {
                return;
            }

            el.style[property.charAt(0).toLowerCase() + property.slice(1)] = value;
            el.style['webkit' + property] = value;
            el.style['moz' + property] = value;
            el.style['ms' + property] = value;
            el.style['o' + property] = value;
        },

        trigger: function trigger(el, event) {
            var detail = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;

            if (!el) {
                return;
            }

            var customEvent = new CustomEvent(event, {
                detail: detail
            });
            el.dispatchEvent(customEvent);
        },

        Listener: {
            uid: 0
        },
        on: function on(el, events, fn) {
            if (!el) {
                return;
            }

            events.split(' ').forEach(function (event) {
                var _id = el.getAttribute('lg-event-uid') || '';
                utils.Listener.uid++;
                _id += '&' + utils.Listener.uid;
                el.setAttribute('lg-event-uid', _id);
                utils.Listener[event + utils.Listener.uid] = fn;
                el.addEventListener(event.split('.')[0], fn, false);
            });
        },

        off: function off(el, event) {
            if (!el) {
                return;
            }

            var _id = el.getAttribute('lg-event-uid');
            if (_id) {
                _id = _id.split('&');
                for (var i = 0; i < _id.length; i++) {
                    if (_id[i]) {
                        var _event = event + _id[i];
                        if (_event.substring(0, 1) === '.') {
                            for (var key in utils.Listener) {
                                if (utils.Listener.hasOwnProperty(key)) {
                                    if (key.split('.').indexOf(_event.split('.')[1]) > -1) {
                                        el.removeEventListener(key.split('.')[0], utils.Listener[key]);
                                        el.setAttribute('lg-event-uid', el.getAttribute('lg-event-uid').replace('&' + _id[i], ''));
                                        delete utils.Listener[key];
                                    }
                                }
                            }
                        } else {
                            el.removeEventListener(_event.split('.')[0], utils.Listener[_event]);
                            el.setAttribute('lg-event-uid', el.getAttribute('lg-event-uid').replace('&' + _id[i], ''));
                            delete utils.Listener[_event];
                        }
                    }
                }
            }
        },

        param: function param(obj) {
            return Object.keys(obj).map(function (k) {
                return encodeURIComponent(k) + '=' + encodeURIComponent(obj[k]);
            }).join('&');
        }
    };

    exports.default = utils;
});

},{}],2:[function(require,module,exports){
(function (global, factory) {
    if (typeof define === "function" && define.amd) {
        define(['./lg-utils'], factory);
    } else if (typeof exports !== "undefined") {
        factory(require('./lg-utils'));
    } else {
        var mod = {
            exports: {}
        };
        factory(global.lgUtils);
        global.lightgallery = mod.exports;
    }
})(this, function (_lgUtils) {
    'use strict';

    var _lgUtils2 = _interopRequireDefault(_lgUtils);

    function _interopRequireDefault(obj) {
        return obj && obj.__esModule ? obj : {
            default: obj
        };
    }

    var _extends = Object.assign || function (target) {
        for (var i = 1; i < arguments.length; i++) {
            var source = arguments[i];

            for (var key in source) {
                if (Object.prototype.hasOwnProperty.call(source, key)) {
                    target[key] = source[key];
                }
            }
        }

        return target;
    };

    /** Polyfill the CustomEvent() constructor functionality in Internet Explorer 9 and higher */
    (function () {

        if (typeof window.CustomEvent === 'function') {
            return false;
        }

        function CustomEvent(event, params) {
            params = params || {
                bubbles: false,
                cancelable: false,
                detail: undefined
            };
            var evt = document.createEvent('CustomEvent');
            evt.initCustomEvent(event, params.bubbles, params.cancelable, params.detail);
            return evt;
        }

        CustomEvent.prototype = window.Event.prototype;

        window.CustomEvent = CustomEvent;
    })();

    window.utils = _lgUtils2.default;
    window.lgData = {
        uid: 0
    };

    window.lgModules = {};
    var defaults = {

        mode: 'lg-slide',

        // Ex : 'ease'
        cssEasing: 'ease',

        //'for jquery animation'
        easing: 'linear',
        speed: 600,
        height: '100%',
        width: '100%',
        addClass: '',
        startClass: 'lg-start-zoom',
        backdropDuration: 150,
        hideBarsDelay: 6000,

        useLeft: false,

        closable: true,
        loop: true,
        escKey: true,
        keyPress: true,
        controls: true,
        slideEndAnimatoin: true,
        hideControlOnEnd: false,
        mousewheel: false,

        getCaptionFromTitleOrAlt: true,

        // .lg-item || '.lg-sub-html'
        appendSubHtmlTo: '.lg-sub-html',

        subHtmlSelectorRelative: false,

        /**
         * @desc number of preload slides
         * will exicute only after the current slide is fully loaded.
         *
         * @ex you clicked on 4th image and if preload = 1 then 3rd slide and 5th
         * slide will be loaded in the background after the 4th slide is fully loaded..
         * if preload is 2 then 2nd 3rd 5th 6th slides will be preloaded.. ... ...
         *
         */
        preload: 1,
        showAfterLoad: true,
        selector: '',
        selectWithin: '',
        nextHtml: '',
        prevHtml: '',

        // 0, 1
        index: false,

        iframeMaxWidth: '100%',

        download: true,
        counter: true,
        appendCounterTo: '.lg-toolbar',

        swipeThreshold: 50,
        enableSwipe: true,
        enableDrag: true,

        dynamic: false,
        dynamicEl: [],
        galleryId: 1
    };

    function Plugin(element, options) {

        // Current lightGallery element
        this.el = element;

        // lightGallery settings
        this.s = _extends({}, defaults, options);

        // When using dynamic mode, ensure dynamicEl is an array
        if (this.s.dynamic && this.s.dynamicEl !== 'undefined' && this.s.dynamicEl.constructor === Array && !this.s.dynamicEl.length) {
            throw 'When using dynamic mode, you must also define dynamicEl as an Array.';
        }

        // lightGallery modules
        this.modules = {};

        // false when lightgallery complete first slide;
        this.lGalleryOn = false;

        this.lgBusy = false;

        // Timeout function for hiding controls;
        this.hideBartimeout = false;

        // To determine browser supports for touch events;
        this.isTouch = 'ontouchstart' in document.documentElement;

        // Disable hideControlOnEnd if sildeEndAnimation is true
        if (this.s.slideEndAnimatoin) {
            this.s.hideControlOnEnd = false;
        }

        this.items = [];

        // Gallery items
        if (this.s.dynamic) {
            this.items = this.s.dynamicEl;
        } else {
            if (this.s.selector === 'this') {
                this.items.push(this.el);
            } else if (this.s.selector !== '') {
                if (this.s.selectWithin) {
                    this.items = document.querySelector(this.s.selectWithin).querySelectorAll(this.s.selector);
                } else {
                    this.items = this.el.querySelectorAll(this.s.selector);
                }
            } else {
                this.items = this.el.children;
            }
        }

        // .lg-item

        this.___slide = '';

        // .lg-outer
        this.outer = '';

        this.init();

        return this;
    }

    Plugin.prototype.init = function () {

        var _this = this;

        // s.preload should not be more than $item.length
        if (_this.s.preload > _this.items.length) {
            _this.s.preload = _this.items.length;
        }

        // if dynamic option is enabled execute immediately
        var _hash = window.location.hash;
        if (_hash.indexOf('lg=' + this.s.galleryId) > 0) {

            _this.index = parseInt(_hash.split('&slide=')[1], 10);

            _lgUtils2.default.addClass(document.body, 'lg-from-hash');
            if (!_lgUtils2.default.hasClass(document.body, 'lg-on')) {
                _lgUtils2.default.addClass(document.body, 'lg-on');
                setTimeout(function () {
                    _this.build(_this.index);
                });
            }
        }

        if (_this.s.dynamic) {

            _lgUtils2.default.trigger(this.el, 'onBeforeOpen');

            _this.index = _this.s.index || 0;

            // prevent accidental double execution
            if (!_lgUtils2.default.hasClass(document.body, 'lg-on')) {
                _lgUtils2.default.addClass(document.body, 'lg-on');
                setTimeout(function () {
                    _this.build(_this.index);
                });
            }
        } else {

            for (var i = 0; i < _this.items.length; i++) {

                /*jshint loopfunc: true */
                (function (index) {

                    // Using different namespace for click because click event should not unbind if selector is same object('this')
                    _lgUtils2.default.on(_this.items[index], 'click.lgcustom', function (e) {

                        e.preventDefault();

                        _lgUtils2.default.trigger(_this.el, 'onBeforeOpen');

                        _this.index = _this.s.index || index;

                        if (!_lgUtils2.default.hasClass(document.body, 'lg-on')) {
                            _this.build(_this.index);
                            _lgUtils2.default.addClass(document.body, 'lg-on');
                        }
                    });
                })(i);
            }
        }
    };

    Plugin.prototype.build = function (index) {

        var _this = this;

        _this.structure();

        for (var key in window.lgModules) {
            _this.modules[key] = new window.lgModules[key](_this.el);
        }

        // initiate slide function
        _this.slide(index, false, false);

        if (_this.s.keyPress) {
            _this.keyPress();
        }

        if (_this.items.length > 1) {

            _this.arrow();

            setTimeout(function () {
                _this.enableDrag();
                _this.enableSwipe();
            }, 50);

            if (_this.s.mousewheel) {
                _this.mousewheel();
            }
        }

        _this.counter();

        _this.closeGallery();

        _lgUtils2.default.trigger(_this.el, 'onAfterOpen');

        // Hide controllers if mouse doesn't move for some period
        _lgUtils2.default.on(_this.outer, 'mousemove.lg click.lg touchstart.lg', function () {

            _lgUtils2.default.removeClass(_this.outer, 'lg-hide-items');

            clearTimeout(_this.hideBartimeout);

            // Timeout will be cleared on each slide movement also
            _this.hideBartimeout = setTimeout(function () {
                _lgUtils2.default.addClass(_this.outer, 'lg-hide-items');
            }, _this.s.hideBarsDelay);
        });
    };

    Plugin.prototype.structure = function () {
        var list = '';
        var controls = '';
        var i = 0;
        var subHtmlCont = '';
        var template;
        var _this = this;

        document.body.insertAdjacentHTML('beforeend', '<div class="lg-backdrop"></div>');
        _lgUtils2.default.setVendor(document.querySelector('.lg-backdrop'), 'TransitionDuration', this.s.backdropDuration + 'ms');

        // Create gallery items
        for (i = 0; i < this.items.length; i++) {
            list += '<div class="lg-item"></div>';
        }

        // Create controlls
        if (this.s.controls && this.items.length > 1) {
            controls = '<div class="lg-actions">' + '<div class="lg-prev lg-icon">' + this.s.prevHtml + '</div>' + '<div class="lg-next lg-icon">' + this.s.nextHtml + '</div>' + '</div>';
        }

        if (this.s.appendSubHtmlTo === '.lg-sub-html') {
            subHtmlCont = '<div class="lg-sub-html"></div>';
        }

        template = '<div class="lg-outer ' + this.s.addClass + ' ' + this.s.startClass + '">' + '<div class="lg" style="width:' + this.s.width + '; height:' + this.s.height + '">' + '<div class="lg-inner">' + list + '</div>' + '<div class="lg-toolbar group">' + '<span class="lg-close lg-icon"></span>' + '</div>' + controls + subHtmlCont + '</div>' + '</div>';

        document.body.insertAdjacentHTML('beforeend', template);
        this.outer = document.querySelector('.lg-outer');
        this.___slide = this.outer.querySelectorAll('.lg-item');

        if (this.s.useLeft) {
            _lgUtils2.default.addClass(this.outer, 'lg-use-left');

            // Set mode lg-slide if use left is true;
            this.s.mode = 'lg-slide';
        } else {
            _lgUtils2.default.addClass(this.outer, 'lg-use-css3');
        }

        // For fixed height gallery
        _this.setTop();
        _lgUtils2.default.on(window, 'resize.lg orientationchange.lg', function () {
            setTimeout(function () {
                _this.setTop();
            }, 100);
        });

        // add class lg-current to remove initial transition
        _lgUtils2.default.addClass(this.___slide[this.index], 'lg-current');

        // add Class for css support and transition mode
        if (this.doCss()) {
            _lgUtils2.default.addClass(this.outer, 'lg-css3');
        } else {
            _lgUtils2.default.addClass(this.outer, 'lg-css');

            // Set speed 0 because no animation will happen if browser doesn't support css3
            this.s.speed = 0;
        }

        _lgUtils2.default.addClass(this.outer, this.s.mode);

        if (this.s.enableDrag && this.items.length > 1) {
            _lgUtils2.default.addClass(this.outer, 'lg-grab');
        }

        if (this.s.showAfterLoad) {
            _lgUtils2.default.addClass(this.outer, 'lg-show-after-load');
        }

        if (this.doCss()) {
            var inner = this.outer.querySelector('.lg-inner');
            _lgUtils2.default.setVendor(inner, 'TransitionTimingFunction', this.s.cssEasing);
            _lgUtils2.default.setVendor(inner, 'TransitionDuration', this.s.speed + 'ms');
        }

        setTimeout(function () {
            _lgUtils2.default.addClass(document.querySelector('.lg-backdrop'), 'in');
        });

        setTimeout(function () {
            _lgUtils2.default.addClass(_this.outer, 'lg-visible');
        }, this.s.backdropDuration);

        if (this.s.download) {
            this.outer.querySelector('.lg-toolbar').insertAdjacentHTML('beforeend', '<a id="lg-download" target="_blank" download class="lg-download lg-icon"></a>');
        }

        // Store the current scroll top value to scroll back after closing the gallery..
        this.prevScrollTop = document.documentElement.scrollTop || document.body.scrollTop;
    };

    // For fixed height gallery
    Plugin.prototype.setTop = function () {
        if (this.s.height !== '100%') {
            var wH = window.innerHeight;
            var top = (wH - parseInt(this.s.height, 10)) / 2;
            var lGallery = this.outer.querySelector('.lg');
            if (wH >= parseInt(this.s.height, 10)) {
                lGallery.style.top = top + 'px';
            } else {
                lGallery.style.top = '0px';
            }
        }
    };

    // Find css3 support
    Plugin.prototype.doCss = function () {
        // check for css animation support
        var support = function support() {
            var transition = ['transition', 'MozTransition', 'WebkitTransition', 'OTransition', 'msTransition', 'KhtmlTransition'];
            var root = document.documentElement;
            var i = 0;
            for (i = 0; i < transition.length; i++) {
                if (transition[i] in root.style) {
                    return true;
                }
            }
        };

        if (support()) {
            return true;
        }

        return false;
    };

    /**
     *  @desc Check the given src is video
     *  @param {String} src
     *  @return {Object} video type
     *  Ex:{ youtube  :  ["//www.youtube.com/watch?v=c0asJgSyxcY", "c0asJgSyxcY"] }
     */
    Plugin.prototype.isVideo = function (src, index) {

        if (!src) {
            throw new Error("Make sure that slide " + index + " has an image/video src");
        }

        var html;
        if (this.s.dynamic) {
            html = this.s.dynamicEl[index].html;
        } else {
            html = this.items[index].getAttribute('data-html');
        }

        if (!src && html) {
            return {
                html5: true
            };
        }

        var youtube = src.match(/\/\/(?:www\.)?youtu(?:\.be|be\.com)\/(?:watch\?v=|embed\/)?([a-z0-9\-\_\%]+)/i);
        var vimeo = src.match(/\/\/(?:www\.)?vimeo.com\/([0-9a-z\-_]+)/i);
        var dailymotion = src.match(/\/\/(?:www\.)?dai.ly\/([0-9a-z\-_]+)/i);
        var vk = src.match(/\/\/(?:www\.)?(?:vk\.com|vkontakte\.ru)\/(?:video_ext\.php\?)(.*)/i);

        if (youtube) {
            return {
                youtube: youtube
            };
        } else if (vimeo) {
            return {
                vimeo: vimeo
            };
        } else if (dailymotion) {
            return {
                dailymotion: dailymotion
            };
        } else if (vk) {
            return {
                vk: vk
            };
        }
    };

    /**
     *  @desc Create image counter
     *  Ex: 1/10
     */
    Plugin.prototype.counter = function () {
        if (this.s.counter) {
            this.outer.querySelector(this.s.appendCounterTo).insertAdjacentHTML('beforeend', '<div id="lg-counter"><span id="lg-counter-current">' + (parseInt(this.index, 10) + 1) + '</span> / <span id="lg-counter-all">' + this.items.length + '</span></div>');
        }
    };

    /**
     *  @desc add sub-html into the slide
     *  @param {Number} index - index of the slide
     */
    Plugin.prototype.addHtml = function (index) {
        var subHtml = null;
        var currentEle;
        if (this.s.dynamic) {
            subHtml = this.s.dynamicEl[index].subHtml;
        } else {
            currentEle = this.items[index];
            subHtml = currentEle.getAttribute('data-sub-html');
            if (this.s.getCaptionFromTitleOrAlt && !subHtml) {
                subHtml = currentEle.getAttribute('title');
                if (subHtml && currentEle.querySelector('img')) {
                    subHtml = currentEle.querySelector('img').getAttribute('alt');
                }
            }
        }

        if (typeof subHtml !== 'undefined' && subHtml !== null) {

            // get first letter of subhtml
            // if first letter starts with . or # get the html form the jQuery object
            var fL = subHtml.substring(0, 1);
            if (fL === '.' || fL === '#') {
                if (this.s.subHtmlSelectorRelative && !this.s.dynamic) {
                    subHtml = currentEle.querySelector(subHtml).innerHTML;
                } else {
                    subHtml = document.querySelector(subHtml).innerHTML;
                }
            }
        } else {
            subHtml = '';
        }

        if (this.s.appendSubHtmlTo === '.lg-sub-html') {
            this.outer.querySelector(this.s.appendSubHtmlTo).innerHTML = subHtml;
        } else {
            this.___slide[index].insertAdjacentHTML('beforeend', subHtml);
        }

        // Add lg-empty-html class if title doesn't exist
        if (typeof subHtml !== 'undefined' && subHtml !== null) {
            if (subHtml === '') {
                _lgUtils2.default.addClass(this.outer.querySelector(this.s.appendSubHtmlTo), 'lg-empty-html');
            } else {
                _lgUtils2.default.removeClass(this.outer.querySelector(this.s.appendSubHtmlTo), 'lg-empty-html');
            }
        }

        _lgUtils2.default.trigger(this.el, 'onAfterAppendSubHtml', {
            index: index
        });
    };

    /**
     *  @desc Preload slides
     *  @param {Number} index - index of the slide
     */
    Plugin.prototype.preload = function (index) {
        var i = 1;
        var j = 1;
        for (i = 1; i <= this.s.preload; i++) {
            if (i >= this.items.length - index) {
                break;
            }

            this.loadContent(index + i, false, 0);
        }

        for (j = 1; j <= this.s.preload; j++) {
            if (index - j < 0) {
                break;
            }

            this.loadContent(index - j, false, 0);
        }
    };

    /**
     *  @desc Load slide content into slide.
     *  @param {Number} index - index of the slide.
     *  @param {Boolean} rec - if true call loadcontent() function again.
     *  @param {Boolean} delay - delay for adding complete class. it is 0 except first time.
     */
    Plugin.prototype.loadContent = function (index, rec, delay) {

        var _this = this;
        var _hasPoster = false;
        var _img;
        var _src;
        var _poster;
        var _srcset;
        var _sizes;
        var _html;
        var getResponsiveSrc = function getResponsiveSrc(srcItms) {
            var rsWidth = [];
            var rsSrc = [];
            for (var i = 0; i < srcItms.length; i++) {
                var __src = srcItms[i].split(' ');

                // Manage empty space
                if (__src[0] === '') {
                    __src.splice(0, 1);
                }

                rsSrc.push(__src[0]);
                rsWidth.push(__src[1]);
            }

            var wWidth = window.innerWidth;
            for (var j = 0; j < rsWidth.length; j++) {
                if (parseInt(rsWidth[j], 10) > wWidth) {
                    _src = rsSrc[j];
                    break;
                }
            }
        };

        if (_this.s.dynamic) {

            if (_this.s.dynamicEl[index].poster) {
                _hasPoster = true;
                _poster = _this.s.dynamicEl[index].poster;
            }

            _html = _this.s.dynamicEl[index].html;
            _src = _this.s.dynamicEl[index].src;

            if (_this.s.dynamicEl[index].responsive) {
                var srcDyItms = _this.s.dynamicEl[index].responsive.split(',');
                getResponsiveSrc(srcDyItms);
            }

            _srcset = _this.s.dynamicEl[index].srcset;
            _sizes = _this.s.dynamicEl[index].sizes;
        } else {

            if (_this.items[index].getAttribute('data-poster')) {
                _hasPoster = true;
                _poster = _this.items[index].getAttribute('data-poster');
            }

            _html = _this.items[index].getAttribute('data-html');
            _src = _this.items[index].getAttribute('href') || _this.items[index].getAttribute('data-src');

            if (_this.items[index].getAttribute('data-responsive')) {
                var srcItms = _this.items[index].getAttribute('data-responsive').split(',');
                getResponsiveSrc(srcItms);
            }

            _srcset = _this.items[index].getAttribute('data-srcset');
            _sizes = _this.items[index].getAttribute('data-sizes');
        }

        //if (_src || _srcset || _sizes || _poster) {

        var iframe = false;
        if (_this.s.dynamic) {
            if (_this.s.dynamicEl[index].iframe) {
                iframe = true;
            }
        } else {
            if (_this.items[index].getAttribute('data-iframe') === 'true') {
                iframe = true;
            }
        }

        var _isVideo = _this.isVideo(_src, index);
        if (!_lgUtils2.default.hasClass(_this.___slide[index], 'lg-loaded')) {
            if (iframe) {
                _this.___slide[index].insertAdjacentHTML('afterbegin', '<div class="lg-video-cont" style="max-width:' + _this.s.iframeMaxWidth + '"><div class="lg-video"><iframe class="lg-object" frameborder="0" src="' + _src + '"  allowfullscreen="true"></iframe></div></div>');
            } else if (_hasPoster) {
                var videoClass = '';
                if (_isVideo && _isVideo.youtube) {
                    videoClass = 'lg-has-youtube';
                } else if (_isVideo && _isVideo.vimeo) {
                    videoClass = 'lg-has-vimeo';
                } else {
                    videoClass = 'lg-has-html5';
                }

                _this.___slide[index].insertAdjacentHTML('beforeend', '<div class="lg-video-cont ' + videoClass + ' "><div class="lg-video"><span class="lg-video-play"></span><img class="lg-object lg-has-poster" src="' + _poster + '" /></div></div>');
            } else if (_isVideo) {
                _this.___slide[index].insertAdjacentHTML('beforeend', '<div class="lg-video-cont "><div class="lg-video"></div></div>');
                _lgUtils2.default.trigger(_this.el, 'hasVideo', {
                    index: index,
                    src: _src,
                    html: _html
                });
            } else {
                _this.___slide[index].insertAdjacentHTML('beforeend', '<div class="lg-img-wrap"><img class="lg-object lg-image" src="' + _src + '" /></div>');
            }

            _lgUtils2.default.trigger(_this.el, 'onAferAppendSlide', {
                index: index
            });

            _img = _this.___slide[index].querySelector('.lg-object');
            if (_sizes) {
                _img.setAttribute('sizes', _sizes);
            }

            if (_srcset) {
                _img.setAttribute('srcset', _srcset);
                try {
                    picturefill({
                        elements: [_img[0]]
                    });
                } catch (e) {
                    console.error('Make sure you have included Picturefill version 2');
                }
            }

            if (this.s.appendSubHtmlTo !== '.lg-sub-html') {
                _this.addHtml(index);
            }

            _lgUtils2.default.addClass(_this.___slide[index], 'lg-loaded');
        }

        _lgUtils2.default.on(_this.___slide[index].querySelector('.lg-object'), 'load.lg error.lg', function () {

            // For first time add some delay for displaying the start animation.
            var _speed = 0;

            // Do not change the delay value because it is required for zoom plugin.
            // If gallery opened from direct url (hash) speed value should be 0
            if (delay && !_lgUtils2.default.hasClass(document.body, 'lg-from-hash')) {
                _speed = delay;
            }

            setTimeout(function () {
                _lgUtils2.default.addClass(_this.___slide[index], 'lg-complete');

                _lgUtils2.default.trigger(_this.el, 'onSlideItemLoad', {
                    index: index,
                    delay: delay || 0
                });
            }, _speed);
        });

        // @todo check load state for html5 videos
        if (_isVideo && _isVideo.html5 && !_hasPoster) {
            _lgUtils2.default.addClass(_this.___slide[index], 'lg-complete');
        }

        if (rec === true) {
            if (!_lgUtils2.default.hasClass(_this.___slide[index], 'lg-complete')) {
                _lgUtils2.default.on(_this.___slide[index].querySelector('.lg-object'), 'load.lg error.lg', function () {
                    _this.preload(index);
                });
            } else {
                _this.preload(index);
            }
        }

        //}
    };

    /**
    *   @desc slide function for lightgallery
        ** Slide() gets call on start
        ** ** Set lg.on true once slide() function gets called.
        ** Call loadContent() on slide() function inside setTimeout
        ** ** On first slide we do not want any animation like slide of fade
        ** ** So on first slide( if lg.on if false that is first slide) loadContent() should start loading immediately
        ** ** Else loadContent() should wait for the transition to complete.
        ** ** So set timeout s.speed + 50
    <=> ** loadContent() will load slide content in to the particular slide
        ** ** It has recursion (rec) parameter. if rec === true loadContent() will call preload() function.
        ** ** preload will execute only when the previous slide is fully loaded (images iframe)
        ** ** avoid simultaneous image load
    <=> ** Preload() will check for s.preload value and call loadContent() again accoring to preload value
        ** loadContent()  <====> Preload();
    
    *   @param {Number} index - index of the slide
    *   @param {Boolean} fromTouch - true if slide function called via touch event or mouse drag
    *   @param {Boolean} fromThumb - true if slide function called via thumbnail click
    */
    Plugin.prototype.slide = function (index, fromTouch, fromThumb) {

        var _prevIndex = 0;
        for (var i = 0; i < this.___slide.length; i++) {
            if (_lgUtils2.default.hasClass(this.___slide[i], 'lg-current')) {
                _prevIndex = i;
                break;
            }
        }

        var _this = this;

        // Prevent if multiple call
        // Required for hsh plugin
        if (_this.lGalleryOn && _prevIndex === index) {
            return;
        }

        var _length = this.___slide.length;
        var _time = _this.lGalleryOn ? this.s.speed : 0;
        var _next = false;
        var _prev = false;

        if (!_this.lgBusy) {

            if (this.s.download) {
                var _src;
                if (_this.s.dynamic) {
                    _src = _this.s.dynamicEl[index].downloadUrl !== false && (_this.s.dynamicEl[index].downloadUrl || _this.s.dynamicEl[index].src);
                } else {
                    _src = _this.items[index].getAttribute('data-download-url') !== 'false' && (_this.items[index].getAttribute('data-download-url') || _this.items[index].getAttribute('href') || _this.items[index].getAttribute('data-src'));
                }

                if (_src) {
                    document.getElementById('lg-download').setAttribute('href', _src);
                    _lgUtils2.default.removeClass(_this.outer, 'lg-hide-download');
                } else {
                    _lgUtils2.default.addClass(_this.outer, 'lg-hide-download');
                }
            }

            _lgUtils2.default.trigger(_this.el, 'onBeforeSlide', {
                prevIndex: _prevIndex,
                index: index,
                fromTouch: fromTouch,
                fromThumb: fromThumb
            });

            _this.lgBusy = true;

            clearTimeout(_this.hideBartimeout);

            // Add title if this.s.appendSubHtmlTo === lg-sub-html
            if (this.s.appendSubHtmlTo === '.lg-sub-html') {

                // wait for slide animation to complete
                setTimeout(function () {
                    _this.addHtml(index);
                }, _time);
            }

            this.arrowDisable(index);

            if (!fromTouch) {

                // remove all transitions
                _lgUtils2.default.addClass(_this.outer, 'lg-no-trans');

                for (var j = 0; j < this.___slide.length; j++) {
                    _lgUtils2.default.removeClass(this.___slide[j], 'lg-prev-slide');
                    _lgUtils2.default.removeClass(this.___slide[j], 'lg-next-slide');
                }

                if (index < _prevIndex) {
                    _prev = true;
                    if (index === 0 && _prevIndex === _length - 1 && !fromThumb) {
                        _prev = false;
                        _next = true;
                    }
                } else if (index > _prevIndex) {
                    _next = true;
                    if (index === _length - 1 && _prevIndex === 0 && !fromThumb) {
                        _prev = true;
                        _next = false;
                    }
                }

                if (_prev) {

                    //prevslide
                    _lgUtils2.default.addClass(this.___slide[index], 'lg-prev-slide');
                    _lgUtils2.default.addClass(this.___slide[_prevIndex], 'lg-next-slide');
                } else if (_next) {

                    // next slide
                    _lgUtils2.default.addClass(this.___slide[index], 'lg-next-slide');
                    _lgUtils2.default.addClass(this.___slide[_prevIndex], 'lg-prev-slide');
                }

                // give 50 ms for browser to add/remove class
                setTimeout(function () {
                    _lgUtils2.default.removeClass(_this.outer.querySelector('.lg-current'), 'lg-current');

                    //_this.$slide.eq(_prevIndex).removeClass('lg-current');
                    _lgUtils2.default.addClass(_this.___slide[index], 'lg-current');

                    // reset all transitions
                    _lgUtils2.default.removeClass(_this.outer, 'lg-no-trans');
                }, 50);
            } else {

                var touchPrev = index - 1;
                var touchNext = index + 1;

                if (index === 0 && _prevIndex === _length - 1) {

                    // next slide
                    touchNext = 0;
                    touchPrev = _length - 1;
                } else if (index === _length - 1 && _prevIndex === 0) {

                    // prev slide
                    touchNext = 0;
                    touchPrev = _length - 1;
                }

                _lgUtils2.default.removeClass(_this.outer.querySelector('.lg-prev-slide'), 'lg-prev-slide');
                _lgUtils2.default.removeClass(_this.outer.querySelector('.lg-current'), 'lg-current');
                _lgUtils2.default.removeClass(_this.outer.querySelector('.lg-next-slide'), 'lg-next-slide');
                _lgUtils2.default.addClass(_this.___slide[touchPrev], 'lg-prev-slide');
                _lgUtils2.default.addClass(_this.___slide[touchNext], 'lg-next-slide');
                _lgUtils2.default.addClass(_this.___slide[index], 'lg-current');
            }

            if (_this.lGalleryOn) {
                setTimeout(function () {
                    _this.loadContent(index, true, 0);
                }, this.s.speed + 50);

                setTimeout(function () {
                    _this.lgBusy = false;
                    _lgUtils2.default.trigger(_this.el, 'onAfterSlide', {
                        prevIndex: _prevIndex,
                        index: index,
                        fromTouch: fromTouch,
                        fromThumb: fromThumb
                    });
                }, this.s.speed);
            } else {
                _this.loadContent(index, true, _this.s.backdropDuration);

                _this.lgBusy = false;
                _lgUtils2.default.trigger(_this.el, 'onAfterSlide', {
                    prevIndex: _prevIndex,
                    index: index,
                    fromTouch: fromTouch,
                    fromThumb: fromThumb
                });
            }

            _this.lGalleryOn = true;

            if (this.s.counter) {
                if (document.getElementById('lg-counter-current')) {
                    document.getElementById('lg-counter-current').innerHTML = index + 1;
                }
            }
        }
    };

    /**
     *  @desc Go to next slide
     *  @param {Boolean} fromTouch - true if slide function called via touch event
     */
    Plugin.prototype.goToNextSlide = function (fromTouch) {
        var _this = this;
        if (!_this.lgBusy) {
            if (_this.index + 1 < _this.___slide.length) {
                _this.index++;
                _lgUtils2.default.trigger(_this.el, 'onBeforeNextSlide', {
                    index: _this.index
                });
                _this.slide(_this.index, fromTouch, false);
            } else {
                if (_this.s.loop) {
                    _this.index = 0;
                    _lgUtils2.default.trigger(_this.el, 'onBeforeNextSlide', {
                        index: _this.index
                    });
                    _this.slide(_this.index, fromTouch, false);
                } else if (_this.s.slideEndAnimatoin) {
                    _lgUtils2.default.addClass(_this.outer, 'lg-right-end');
                    setTimeout(function () {
                        _lgUtils2.default.removeClass(_this.outer, 'lg-right-end');
                    }, 400);
                }
            }
        }
    };

    /**
     *  @desc Go to previous slide
     *  @param {Boolean} fromTouch - true if slide function called via touch event
     */
    Plugin.prototype.goToPrevSlide = function (fromTouch) {
        var _this = this;
        if (!_this.lgBusy) {
            if (_this.index > 0) {
                _this.index--;
                _lgUtils2.default.trigger(_this.el, 'onBeforePrevSlide', {
                    index: _this.index,
                    fromTouch: fromTouch
                });
                _this.slide(_this.index, fromTouch, false);
            } else {
                if (_this.s.loop) {
                    _this.index = _this.items.length - 1;
                    _lgUtils2.default.trigger(_this.el, 'onBeforePrevSlide', {
                        index: _this.index,
                        fromTouch: fromTouch
                    });
                    _this.slide(_this.index, fromTouch, false);
                } else if (_this.s.slideEndAnimatoin) {
                    _lgUtils2.default.addClass(_this.outer, 'lg-left-end');
                    setTimeout(function () {
                        _lgUtils2.default.removeClass(_this.outer, 'lg-left-end');
                    }, 400);
                }
            }
        }
    };

    Plugin.prototype.keyPress = function () {
        var _this = this;
        if (this.items.length > 1) {
            _lgUtils2.default.on(window, 'keyup.lg', function (e) {
                if (_this.items.length > 1) {
                    if (e.keyCode === 37) {
                        e.preventDefault();
                        _this.goToPrevSlide();
                    }

                    if (e.keyCode === 39) {
                        e.preventDefault();
                        _this.goToNextSlide();
                    }
                }
            });
        }

        _lgUtils2.default.on(window, 'keydown.lg', function (e) {
            if (_this.s.escKey === true && e.keyCode === 27) {
                e.preventDefault();
                if (!_lgUtils2.default.hasClass(_this.outer, 'lg-thumb-open')) {
                    _this.destroy();
                } else {
                    _lgUtils2.default.removeClass(_this.outer, 'lg-thumb-open');
                }
            }
        });
    };

    Plugin.prototype.arrow = function () {
        var _this = this;
        _lgUtils2.default.on(this.outer.querySelector('.lg-prev'), 'click.lg', function () {
            _this.goToPrevSlide();
        });

        _lgUtils2.default.on(this.outer.querySelector('.lg-next'), 'click.lg', function () {
            _this.goToNextSlide();
        });
    };

    Plugin.prototype.arrowDisable = function (index) {

        // Disable arrows if s.hideControlOnEnd is true
        if (!this.s.loop && this.s.hideControlOnEnd) {
            var next = this.outer.querySelector('.lg-next');
            var prev = this.outer.querySelector('.lg-prev');
            if (index + 1 < this.___slide.length) {
                next.removeAttribute('disabled');
                _lgUtils2.default.removeClass(next, 'disabled');
            } else {
                next.setAttribute('disabled', 'disabled');
                _lgUtils2.default.addClass(next, 'disabled');
            }

            if (index > 0) {
                prev.removeAttribute('disabled');
                _lgUtils2.default.removeClass(prev, 'disabled');
            } else {
                next.setAttribute('disabled', 'disabled');
                _lgUtils2.default.addClass(next, 'disabled');
            }
        }
    };

    Plugin.prototype.setTranslate = function (el, xValue, yValue) {
        // jQuery supports Automatic CSS prefixing since jQuery 1.8.0
        if (this.s.useLeft) {
            el.style.left = xValue;
        } else {
            _lgUtils2.default.setVendor(el, 'Transform', 'translate3d(' + xValue + 'px, ' + yValue + 'px, 0px)');
        }
    };

    Plugin.prototype.touchMove = function (startCoords, endCoords) {

        var distance = endCoords - startCoords;

        if (Math.abs(distance) > 15) {
            // reset opacity and transition duration
            _lgUtils2.default.addClass(this.outer, 'lg-dragging');

            // move current slide
            this.setTranslate(this.___slide[this.index], distance, 0);

            // move next and prev slide with current slide
            this.setTranslate(document.querySelector('.lg-prev-slide'), -this.___slide[this.index].clientWidth + distance, 0);
            this.setTranslate(document.querySelector('.lg-next-slide'), this.___slide[this.index].clientWidth + distance, 0);
        }
    };

    Plugin.prototype.touchEnd = function (distance) {
        var _this = this;

        // keep slide animation for any mode while dragg/swipe
        if (_this.s.mode !== 'lg-slide') {
            _lgUtils2.default.addClass(_this.outer, 'lg-slide');
        }

        for (var i = 0; i < this.___slide.length; i++) {
            if (!_lgUtils2.default.hasClass(this.___slide[i], 'lg-current') && !_lgUtils2.default.hasClass(this.___slide[i], 'lg-prev-slide') && !_lgUtils2.default.hasClass(this.___slide[i], 'lg-next-slide')) {
                this.___slide[i].style.opacity = '0';
            }
        }

        // set transition duration
        setTimeout(function () {
            _lgUtils2.default.removeClass(_this.outer, 'lg-dragging');
            if (distance < 0 && Math.abs(distance) > _this.s.swipeThreshold) {
                _this.goToNextSlide(true);
            } else if (distance > 0 && Math.abs(distance) > _this.s.swipeThreshold) {
                _this.goToPrevSlide(true);
            } else if (Math.abs(distance) < 5) {

                // Trigger click if distance is less than 5 pix
                _lgUtils2.default.trigger(_this.el, 'onSlideClick');
            }

            for (var i = 0; i < _this.___slide.length; i++) {
                _this.___slide[i].removeAttribute('style');
            }
        });

        // remove slide class once drag/swipe is completed if mode is not slide
        setTimeout(function () {
            if (!_lgUtils2.default.hasClass(_this.outer, 'lg-dragging') && _this.s.mode !== 'lg-slide') {
                _lgUtils2.default.removeClass(_this.outer, 'lg-slide');
            }
        }, _this.s.speed + 100);
    };

    Plugin.prototype.enableSwipe = function () {
        var _this = this;
        var startCoords = 0;
        var endCoords = 0;
        var isMoved = false;

        if (_this.s.enableSwipe && _this.isTouch && _this.doCss()) {

            for (var i = 0; i < _this.___slide.length; i++) {
                /*jshint loopfunc: true */
                _lgUtils2.default.on(_this.___slide[i], 'touchstart.lg', function (e) {
                    if (!_lgUtils2.default.hasClass(_this.outer, 'lg-zoomed') && !_this.lgBusy) {
                        e.preventDefault();
                        _this.manageSwipeClass();
                        startCoords = e.targetTouches[0].pageX;
                    }
                });
            }

            for (var j = 0; j < _this.___slide.length; j++) {
                /*jshint loopfunc: true */
                _lgUtils2.default.on(_this.___slide[j], 'touchmove.lg', function (e) {
                    if (!_lgUtils2.default.hasClass(_this.outer, 'lg-zoomed')) {
                        e.preventDefault();
                        endCoords = e.targetTouches[0].pageX;
                        _this.touchMove(startCoords, endCoords);
                        isMoved = true;
                    }
                });
            }

            for (var k = 0; k < _this.___slide.length; k++) {
                /*jshint loopfunc: true */
                _lgUtils2.default.on(_this.___slide[k], 'touchend.lg', function () {
                    if (!_lgUtils2.default.hasClass(_this.outer, 'lg-zoomed')) {
                        if (isMoved) {
                            isMoved = false;
                            _this.touchEnd(endCoords - startCoords);
                        } else {
                            _lgUtils2.default.trigger(_this.el, 'onSlideClick');
                        }
                    }
                });
            }
        }
    };

    Plugin.prototype.enableDrag = function () {
        var _this = this;
        var startCoords = 0;
        var endCoords = 0;
        var isDraging = false;
        var isMoved = false;
        if (_this.s.enableDrag && !_this.isTouch && _this.doCss()) {
            for (var i = 0; i < _this.___slide.length; i++) {
                /*jshint loopfunc: true */
                _lgUtils2.default.on(_this.___slide[i], 'mousedown.lg', function (e) {
                    // execute only on .lg-object
                    if (!_lgUtils2.default.hasClass(_this.outer, 'lg-zoomed')) {
                        if (_lgUtils2.default.hasClass(e.target, 'lg-object') || _lgUtils2.default.hasClass(e.target, 'lg-video-play')) {
                            e.preventDefault();

                            if (!_this.lgBusy) {
                                _this.manageSwipeClass();
                                startCoords = e.pageX;
                                isDraging = true;

                                // ** Fix for webkit cursor issue https://code.google.com/p/chromium/issues/detail?id=26723
                                _this.outer.scrollLeft += 1;
                                _this.outer.scrollLeft -= 1;

                                // *

                                _lgUtils2.default.removeClass(_this.outer, 'lg-grab');
                                _lgUtils2.default.addClass(_this.outer, 'lg-grabbing');

                                _lgUtils2.default.trigger(_this.el, 'onDragstart');
                            }
                        }
                    }
                });
            }

            _lgUtils2.default.on(window, 'mousemove.lg', function (e) {
                if (isDraging) {
                    isMoved = true;
                    endCoords = e.pageX;
                    _this.touchMove(startCoords, endCoords);
                    _lgUtils2.default.trigger(_this.el, 'onDragmove');
                }
            });

            _lgUtils2.default.on(window, 'mouseup.lg', function (e) {
                if (isMoved) {
                    isMoved = false;
                    _this.touchEnd(endCoords - startCoords);
                    _lgUtils2.default.trigger(_this.el, 'onDragend');
                } else if (_lgUtils2.default.hasClass(e.target, 'lg-object') || _lgUtils2.default.hasClass(e.target, 'lg-video-play')) {
                    _lgUtils2.default.trigger(_this.el, 'onSlideClick');
                }

                // Prevent execution on click
                if (isDraging) {
                    isDraging = false;
                    _lgUtils2.default.removeClass(_this.outer, 'lg-grabbing');
                    _lgUtils2.default.addClass(_this.outer, 'lg-grab');
                }
            });
        }
    };

    Plugin.prototype.manageSwipeClass = function () {
        var touchNext = this.index + 1;
        var touchPrev = this.index - 1;
        var length = this.___slide.length;
        if (this.s.loop) {
            if (this.index === 0) {
                touchPrev = length - 1;
            } else if (this.index === length - 1) {
                touchNext = 0;
            }
        }

        for (var i = 0; i < this.___slide.length; i++) {
            _lgUtils2.default.removeClass(this.___slide[i], 'lg-next-slide');
            _lgUtils2.default.removeClass(this.___slide[i], 'lg-prev-slide');
        }

        if (touchPrev > -1) {
            _lgUtils2.default.addClass(this.___slide[touchPrev], 'lg-prev-slide');
        }

        _lgUtils2.default.addClass(this.___slide[touchNext], 'lg-next-slide');
    };

    Plugin.prototype.mousewheel = function () {
        var _this = this;
        _lgUtils2.default.on(_this.outer, 'mousewheel.lg', function (e) {

            if (!e.deltaY) {
                return;
            }

            if (e.deltaY > 0) {
                _this.goToPrevSlide();
            } else {
                _this.goToNextSlide();
            }

            e.preventDefault();
        });
    };

    Plugin.prototype.closeGallery = function () {

        var _this = this;
        var mousedown = false;
        _lgUtils2.default.on(this.outer.querySelector('.lg-close'), 'click.lg', function () {
            _this.destroy();
        });

        if (_this.s.closable) {

            // If you drag the slide and release outside gallery gets close on chrome
            // for preventing this check mousedown and mouseup happened on .lg-item or lg-outer
            _lgUtils2.default.on(_this.outer, 'mousedown.lg', function (e) {

                if (_lgUtils2.default.hasClass(e.target, 'lg-outer') || _lgUtils2.default.hasClass(e.target, 'lg-item') || _lgUtils2.default.hasClass(e.target, 'lg-img-wrap')) {
                    mousedown = true;
                } else {
                    mousedown = false;
                }
            });

            _lgUtils2.default.on(_this.outer, 'mouseup.lg', function (e) {

                if (_lgUtils2.default.hasClass(e.target, 'lg-outer') || _lgUtils2.default.hasClass(e.target, 'lg-item') || _lgUtils2.default.hasClass(e.target, 'lg-img-wrap') && mousedown) {
                    if (!_lgUtils2.default.hasClass(_this.outer, 'lg-dragging')) {
                        _this.destroy();
                    }
                }
            });
        }
    };

    Plugin.prototype.destroy = function (d) {

        var _this = this;

        if (!d) {
            _lgUtils2.default.trigger(_this.el, 'onBeforeClose');
        }

        document.body.scrollTop = _this.prevScrollTop;
        document.documentElement.scrollTop = _this.prevScrollTop;

        /**
         * if d is false or undefined destroy will only close the gallery
         * plugins instance remains with the element
         *
         * if d is true destroy will completely remove the plugin
         */

        if (d) {
            if (!_this.s.dynamic) {
                // only when not using dynamic mode is $items a jquery collection

                for (var i = 0; i < this.items.length; i++) {
                    _lgUtils2.default.off(this.items[i], '.lg');
                    _lgUtils2.default.off(this.items[i], '.lgcustom');
                }
            }

            var lguid = _this.el.getAttribute('lg-uid');
            delete window.lgData[lguid];
            _this.el.removeAttribute('lg-uid');
        }

        // Unbind all events added by lightGallery
        _lgUtils2.default.off(this.el, '.lgtm');

        // Distroy all lightGallery modules
        for (var key in window.lgModules) {
            if (_this.modules[key]) {
                _this.modules[key].destroy();
            }
        }

        this.lGalleryOn = false;

        clearTimeout(_this.hideBartimeout);
        this.hideBartimeout = false;
        _lgUtils2.default.off(window, '.lg');
        _lgUtils2.default.removeClass(document.body, 'lg-on');
        _lgUtils2.default.removeClass(document.body, 'lg-from-hash');

        if (_this.outer) {
            _lgUtils2.default.removeClass(_this.outer, 'lg-visible');
        }

        _lgUtils2.default.removeClass(document.querySelector('.lg-backdrop'), 'in');
        setTimeout(function () {
            try {
                if (_this.outer) {
                    _this.outer.parentNode.removeChild(_this.outer);
                }

                if (document.querySelector('.lg-backdrop')) {
                    document.querySelector('.lg-backdrop').parentNode.removeChild(document.querySelector('.lg-backdrop'));
                }

                if (!d) {
                    _lgUtils2.default.trigger(_this.el, 'onCloseAfter');
                }
            } catch (err) {}
        }, _this.s.backdropDuration + 50);
    };

    window.lightGallery = function (el, options) {
        if (!el) {
            return;
        }

        try {
            if (!el.getAttribute('lg-uid')) {
                var uid = 'lg' + window.lgData.uid++;
                window.lgData[uid] = new Plugin(el, options);
                el.setAttribute('lg-uid', uid);
            } else {
                try {
                    window.lgData[el.getAttribute('lg-uid')].init();
                } catch (err) {
                    console.error('lightGallery has not initiated properly');
                }
            }
        } catch (err) {
            console.error('lightGallery has not initiated properly');
        }
    };
});

},{"./lg-utils":1}]},{},[2])(2)
});
const ratingTotal = 5;
const ratingEls = Array.from(document.querySelectorAll('.stars-outer'));
const starsContainer = document.querySelector('.stars');
const stars = Array.from(document.querySelectorAll('.star'));

ratingEls.forEach(ratingEl => {
  const rating = ratingEl.getAttribute('data-rating');
  const ratingInner = ratingEl.children[0];
  const ratingPercentage = (rating / ratingTotal) * 100;
  const ratingPercentageRounded = `${Math.round(ratingPercentage / 10) * 10}%`;

  ratingInner.style.width = ratingPercentageRounded;
});
if (starsContainer) {
  starsContainer.addEventListener('mouseover', e => {
    const currentStar = e.target.closest('.star');
    if (!currentStar) return;
    const rating = parseInt(currentStar.getAttribute('data-rating'), 10);
    stars.forEach((star, index) => {
      if (index < rating) {
        star.classList.add('hover');
      } else {
        star.classList.remove('hover');
      }
    });
  });

  starsContainer.addEventListener('mouseout', () => {
    stars.forEach(star => {
      star.classList.remove('hover');
    });
  });

  starsContainer.addEventListener('click', e => {
    const currentStar = e.target.closest('.star');
    if (!currentStar) return;
    const rating = parseInt(currentStar.getAttribute('data-rating'), 10);
    stars.forEach(star => {
      star.classList.remove('is-selected');
    });

    for (let i = 0; i < rating; i += 1) {
      stars[i].classList.add('is-selected');
    }
  });
}
( function($) {
    $(document).ready(function () {

        $(window).scroll(function () {
            if ($(this).scrollTop() > 100) {
                $('.back-to-top').addClass('is-visible');
            } else {
                $('.back-to-top').removeClass('is-visible');
            }
        });

        $('.back-to-top').click(function () {
            $("html, body").animate({
                scrollTop: 0
            }, 600);
            return false;
        });

    });
} (jQuery));
( function($) {
      /*! BEFORE LOAD
  ------------------------------------------------->*/
  window.addEventListener('beforeunload', function () {
    if (document.getElementsByClassName('is-page-loading')[0]) {
      var themeContainer = document.getElementById('theme-container');

      if (themeContainer) {
        themeContainer.classList.add('is-loading');
        themeContainer.classList.remove('is-ready');
      }
    }
  });


  /* LOAD
   ***************************************************/
  window.addEventListener('load', function () {

    /*! PRELOADER `done()`
    ------------------------------------------------->*/
    if (document.getElementsByClassName('is-page-loading')[0]) {
      var themeContainer = document.getElementById('theme-container');

      if (themeContainer) {
        themeContainer.classList.remove('is-loading');
        themeContainer.classList.add('is-ready');
      }
    }
  });

} (jQuery));
(function($) {
  $.fn.stickynav = function(options) {

    const DEFAULT_SELECTORS = {
      navActiveClass:    'active',   // Selected nav item modifier class
      navStickyClass:    'sticky',   // Sticky nav modifier class
      sectionSelector:   'section'   // Section id, class or tag selector
    };

    // Merge options with defaults
    options = $.extend({}, DEFAULT_SELECTORS, options);

    // Set jQuery DOM elements
    const $nav = this;
    const $navLinks = $nav.find('a');
    const $sections = $(options.sectionSelector);

    const navHeight = $nav.height();
    const scrollTopOffset = $sections.first().height() / 2;

    let currentScrollPosition = 0;
    let offsetNumbers = [0];


    function initialise() {
      calculateOffsets();
      bindEvents();
    }

    function bindEvents() {
      $navLinks.on('click', onClick);
      $(window).on('scroll', throttle(onScroll, 20));
    }

    function onClick(e) {
      e.preventDefault();
      const targetEl = $(this).attr('href');

      if ($(targetEl).length) {
        selectNavItem(this);

          $('html, body').animate({
            scrollTop: $(targetEl).offset().top - navHeight
          });
        }
    }

    function onScroll() {
      var scrollTop = $(document).scrollTop() + navHeight,
          closestPosition = findClosestNumber(scrollTop, offsetNumbers);

      // select navbar item
      if (closestPosition !== currentScrollPosition) {
        selectNavItem('.section-offset-' + closestPosition);
        currentScrollPosition = closestPosition;
      }

      // fix navbar
      if (scrollTop > scrollTopOffset) {
        $nav.addClass(options.navStickyClass);
      } else {
        $nav.removeClass(options.navStickyClass);
      }
    }

    function findClosestNumber(num, arr) {
      return arr.reduce(function(prev, curr) {
        return (Math.abs(curr - num) < Math.abs(prev - num) ? curr : prev);
      });
    }

    function calculateOffsets() {
      $sections.each(function(index) {
        const el = $(this)[0];
        const offsetTop = getOffsetTop(el);

        offsetNumbers.push(offsetTop);
        getNavItem(el).addClass('section-offset-' + offsetTop);
      });
    }

    function getOffsetTop(el) {
        const rect = el.getBoundingClientRect(),
              scrollTop = window.pageYOffset || document.documentElement.scrollTop;

        return Math.round(rect.top + scrollTop);
    }

    function getNavItem(el) {
      return $('nav a[href="#' + $(el).attr('id') + '"]');
    }

    function selectNavItem(el) {
      if (!$nav.hasClass(options.navStickyClass)) {
        $nav.addClass(options.navStickyClass);
      }

      $navLinks.removeClass(options.navActiveClass);
      $(el).addClass(options.navActiveClass);
    }

    function throttle(func, delay) {
      let timer = 0;

      return function() {
        const context = this,
        args = [].slice.call(arguments);

        clearTimeout(timer);
        timer = setTimeout(function() {
          func.apply(context, args);
        }, delay);
      };
    }

    initialise();
  };

}(jQuery));
function datePicker() {
	$('#date').dateDropper();
}

function navListing() {
	$('.listing-nav').stickynav();

    $('.navbar-toggler').click(() => {
      $('.navbar-collapse').toggleClass('show');
    });
}

function lightBox( select ) {
	lightGallery(document.querySelector( select ));
}

function viewPhoto() {
	var button = $( '.goto-views-preview' ),
		gallery = document.querySelector( '#lightgallery' ),
		firstGallery = gallery ? gallery.querySelector( ':first-child' ) : false;

	if ( ! button.length || ! firstGallery ) {
		return;
	}

	button.on( 'click', function() {
		firstGallery.click();
	} );
}

(function($){
	$(document).ready(function() {
		datePicker();
	    navListing();
	    lightBox( '#lightgallery' );
	    viewPhoto();
	});
}(jQuery));