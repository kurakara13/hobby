<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ulasan extends Model
{
    protected $table = 'ulasan';

    public function user(){

      return $this->belongsTo('App\User', 'id_user');
    }
}
