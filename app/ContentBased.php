<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContentBased extends Model
{
    protected $table = 'content';
}
