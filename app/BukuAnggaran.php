<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BukuAnggaran extends Model
{
    protected $table = 'buku_anggaran';

    public function kegiatan(){

      return $this->hasMany('App\Kegiatan', 'id_anggaran');
    }

    public function realisasi(){

      return $this->hasMany('App\Realisasi', 'id_anggaran');
    }
}
