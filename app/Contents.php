<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contents extends Model
{
    protected $table = 'contents';

    public function get_loc(){

      return $this->hasMany('App\Rating', 'id_contents');
    }

    public function user(){

      return $this->belongsTo('App\User', 'id_user');
    }

    public function ulasan(){

      return $this->hasMany('App\Ulasan', 'id_contents');
    }

    public function acara(){

      return $this->hasOne('App\Acara', 'id_contents');
    }

    public function tempat(){

      return $this->hasOne('App\Tempat', 'id_contents');
    }
}
