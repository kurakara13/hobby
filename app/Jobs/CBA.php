<?php

 namespace App\Jobs;

 class CBA extends Recommend
 {

   const USER_ID = '__USER__';

   protected $data;
   //
   // public function __construct()
   // {
   //   $this->data[self::USER_ID] = $this->processUser($user);
   //   $this->data = array_merge($this->data, $this->processObjects($objects));
   // }

   static function getRecommendation($user, $objects){
     $data[self::USER_ID] = CBA::processUser($user);
     $dataObjects = CBA::processObjects($objects);
     $data = array_merge($data, $dataObjects);
     $result = [];
     $a = 0;
     foreach ($data as $k => $v) {
       if($k !== self::USER_ID) {
         $result[$k] = CBA::similarityDistance($data, self::USER_ID, $k);
         $objects[$a]->content_based = $result[$k];
         $a++;
       }
     }
     // arsort($result);
     // dd($objects);
     return $objects;
   }

   static function processUser($user)
 	{
 		$result = [];
 		foreach ($user as $tag) {
 			$result[$tag] = 1.0;
 		}
 		return $result;
 	}

 	static function processObjects($objects)
 	{
 		$result = [];
    // dd($objects);
 		foreach ($objects as $object => $tags) {
      foreach (explode(',',$tags->hobi) as $tag) {
 				$result[$object][$tag] = 1.0	;
 			}
 		}
 		return $result;
 	}

 }
