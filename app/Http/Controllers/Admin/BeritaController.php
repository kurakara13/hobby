<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ContentBased;
use App\Hobby;
use App\Contents;
use App\UserProfile;
use App\Jobs\CBA;
use Auth;

class BeritaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $berita = Contents::where('type', 'Berita')->where('id_user', Auth::user()->id)->get();

        return view('admin.berita.index', compact('berita'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $hobby = Hobby::get();

      return view('admin.berita.create', compact('hobby'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->gambar != null) {
          $filename = time() . '.' . $request->gambar->getClientOriginalExtension();
          $request->gambar->move(public_path('images/contents'), $filename);
        }else {
          $filename = 'no-img.png';
        }

        $contents = new Contents;
        $contents->id_user = Auth::user()->id;
        $contents->judul = $request->judul;
        $contents->deskripsi = $request->deskripsi;
        $contents->hobi = implode(',', $request->hobby);
        $contents->type = 'Berita';
        $contents->gambar = $filename;
        $contents->save();

        return redirect()->route('admin.berita.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
