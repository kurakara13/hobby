<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\BukuAnggaran;
use App\ContentBased;
use App\Hobby;
use App\Jobs\CBA;

class DashboardController extends AdminController
{
    public function index(){
      $content = ContentBased::limit(4)->get();
      $hobby = Hobby::get();

      return view('admin.dashboard', compact('hobby', 'content'));
    }
}
