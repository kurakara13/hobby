<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\BukuAnggaran;
use App\ContentBased;
use App\Hobby;
use App\Jobs\CBA;

class HomeController extends AdminController
{
    public function index(){
      $content = ContentBased::limit(4)->get();
      $hobby = Hobby::get();

      return view('user.home2', compact('hobby', 'content'));
    }

    public function place(){
      $content = ContentBased::paginate(9);
      $hobby = Hobby::get();
      $contentBased = ContentBased::limit(5)->get();

      return view('user.tempat', compact('content', 'hobby', 'contentBased'));
    }

    public function content_based_post(Request $req){
      $berita = ContentBased::limit(20)->get();
      $userHobby = 'Auth::->hobby';
      $contentBased = CBA::getRecommendation($userHobby, $content);

      // $ranNum = array(1,2,3,4,5,6);
      $hobby = Hobby::get();
      // $ranName = [];
      // foreach ($hobby as $key => $value) {
      //   array_push($ranName, $value->nama);
      // }
      // foreach ($content as $item) {
      //   $indexranNum = array_rand($ranNum);
      //   $tes = array_rand($ranName, $ranNum[$indexranNum]);
      //   $newArray = [];
      //   if(is_array($tes)){
      //     for ($i=0; $i < count($tes); $i++) {
      //       array_push($newArray, $ranName[$tes[$i]]);
      //     }
      //   }else {
      //     array_push($newArray, $ranName[$tes]);
      //   }
      //   $databaru = implode(',',$newArray);
      //
      //   $newContent = ContentBased::find($item->id);
      //   $newContent->hobby = $databaru;
      //   $newContent->save();
      // }
      // dd('sukses');


      return view('user.content_recomended', compact('content', 'hobby', 'contentBased'));
    }

}
