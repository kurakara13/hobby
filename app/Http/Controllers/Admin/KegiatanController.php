<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Kegiatan;
use App\BukuAnggaran;
use App\Realisasi;

class KegiatanController extends AdminController
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $kegiatan = new Kegiatan;
        $kegiatan->id_anggaran = $request->id_anggaran;
        $kegiatan->nama_kegiatan = $request->nama_kegiatan;
        $kegiatan->deskripsi_kegiatan = $request->deskripsi_kegiatan;
        $kegiatan->pagu_awal_kegiatan = $request->pagu_awal;
        $kegiatan->kode = $request->kode;
        $kegiatan->save();

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id_anggaran, $id_kegiatan)
    {
        $anggaran = BukuAnggaran::find($id_anggaran);
        $kegiatan = Kegiatan::with('realisasi')->find($id_kegiatan);
        $realisasi = Realisasi::where('id_kegiatan', $id_kegiatan)->get();

        return view('admin.realisasi', compact('anggaran', 'kegiatan', 'realisasi'));
    }

    public function delete_realisasi($id_anggaran, $id_kegiatan)
    {
        $anggaran = BukuAnggaran::find($id_anggaran);
        $kegiatan = Kegiatan::with('realisasi')->find($id_kegiatan);
        $realisasi = Realisasi::where('id_kegiatan', $id_kegiatan)->get();

        return view('admin.realisasi', compact('anggaran', 'kegiatan', 'realisasi'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $kegiatan = Kegiatan::find($id);
        $kegiatan->id_anggaran = $request->id_anggaran;
        $kegiatan->nama_kegiatan = $request->nama_kegiatan;
        $kegiatan->deskripsi_kegiatan = $request->deskripsi_kegiatan;
        $kegiatan->pagu_awal_kegiatan = $request->pagu_awal;
        $kegiatan->kode = $request->kode;
        $kegiatan->created_at = $request->tgl_buat;
        $kegiatan->save();

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $realisasi = Realisasi::where('id_kegiatan', $id)->get();
        foreach ($realisasi as $item) {
          $data = Realisasi::find($item->id);
          $data->delete();
        }

        $kegiatan = Kegiatan::find($id);
        $kegiatan->delete();

        return redirect()->back();
    }
}
