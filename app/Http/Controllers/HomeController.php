<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ContentBased;
use App\Hobby;
use App\UserProfile;
use App\Jobs\CBA;
use App\Contents;
use App\Ulasan;
use Auth;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $berita = Contents::where('type', 'Berita')->limit(4)->get();
        $acara = Contents::where('type', 'Acara')->limit(4)->get();
        $tempat = Contents::where('type', 'Tempat')->limit(4)->get();
        $hobby = Hobby::get();

        return view('home', compact('hobby', 'berita', 'acara', 'tempat'));
    }

    public function news()
    {
        $userHobby = explode(',', 'Hiking,Menulis');
        $berita = Contents::with('get_loc')->where('type', 'Berita')->get();
        $contentBased = CBA::getRecommendation($userHobby, $berita);

        return view('berita', ['berita' => $contentBased->sortByDesc('content_based')]);
    }

    public function event()
    {
        $userHobby = explode(',', 'Hiking,Menulis');
        $acara = Contents::with('get_loc')->where('type', 'Acara')->get();
        $contentBased = CBA::getRecommendation($userHobby, $acara);

        return view('acara', ['acara' => $contentBased->sortByDesc('content_based')]);
    }

    public function place()
    {
        $userHobby = explode(',', 'Hiking,Menulis');
        $tempat = Contents::with('get_loc')->where('type', 'Tempat')->get();
        $contentBased = CBA::getRecommendation($userHobby, $tempat);

        return view('tempat', ['tempat' => $contentBased->sortByDesc('content_based')]);
    }

    public function community()
    {
        $userHobby = explode(',', 'Hiking,Menulis');
        $komunitas = Contents::with('get_loc')->where('type', 'Komunitas')->get();
        $contentBased = CBA::getRecommendation($userHobby, $komunitas);

        return view('komunitas', ['komunitas' => $contentBased->sortByDesc('content_based')]);
    }

    public function place_detail($id)
    {
        $tempat = Contents::get();
        $tempatDetail = Contents::with('user', 'ulasan.user', 'tempat')->where('type', 'Tempat')->find($id);
        $ulasan = $tempatDetail->ulasan()->paginate(1);
        $hobby = Hobby::get();

        return view('tempat_detail', compact('tempat', 'tempatDetail', 'ulasan', 'hobby'));
    }

    public function method()
    {
        $userProfile = UserProfile::where('id_user', Auth::user()->id)->first();
        $userHobby = explode(',', $userProfile->hobby);
        $berita = Contents::with('get_loc')->where('type', 'Berita')->get();
        $contentBased = CBA::getRecommendation($userHobby, $berita);

        return view('admin.method', ['user' => $userProfile, 'contentBased' => $contentBased->sortByDesc('content_based')]);
    }

    public function news_detail($id)
    {
        $berita = Contents::get();
        $beritaDetail = Contents::with('user', 'ulasan.user')->find($id);
        $ulasan = $beritaDetail->ulasan()->paginate(1);
        $hobby = Hobby::get();

        return view('berita_detail', compact('berita', 'beritaDetail', 'ulasan', 'hobby'));
    }

    public function community_detail($id)
    {
        $komunitas = Contents::get();
        $komunitasDetail = Contents::with('user', 'ulasan.user')->find($id);
        $ulasan = $komunitasDetail->ulasan()->paginate(1);
        $hobby = Hobby::get();

        return view('komunitas_detail', compact('komunitas', 'komunitasDetail', 'ulasan', 'hobby'));
    }

    public function event_detail($id)
    {
        $acara = Contents::get();
        $acaraDetail = Contents::with('user', 'ulasan.user', 'acara')->where('type', 'Acara')->find($id);
        $ulasan = $acaraDetail->ulasan()->paginate(1);
        $hobby = Hobby::get();

        return view('acara_detail', compact('acara', 'acaraDetail', 'ulasan', 'hobby'));
    }

    public function aboutus()
    {

        return view('tentang_kami');
    }
}
