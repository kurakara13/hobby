<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ContentBased;
use App\Hobby;
use App\Contents;
use App\Acara;
use App\UserProfile;
use App\Jobs\CBA;
use Auth;

class AcaraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $acara = Contents::where('type', 'Acara')->where('id_user', Auth::user()->id)->get();

      return view('user.acara.index', compact('acara'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       $hobby = Hobby::get();

       return view('user.acara.create', compact('hobby'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      if ($request->gambar != null) {
        $filename = time() . '.' . $request->gambar->getClientOriginalExtension();
        $request->gambar->move(public_path('images/contents'), $filename);
      }else {
        $filename = 'no-img.png';
      }

      $contents = new Contents;
      $contents->id_user = Auth::user()->id;
      $contents->judul = $request->judul;
      $contents->deskripsi = $request->deskripsi;
      $contents->hobi = implode(',', $request->hobby);
      $contents->type = 'Acara';
      $contents->gambar = $filename;
      $contents->save();

      $acara = new Acara;
      $acara->id_contents = $contents->id;
      $acara->max = $request->max;
      $acara->biaya = $request->biaya;
      $acara->catatan_biaya = $request->catatan_biaya;
      $acara->status = '0';
      $acara->save();

      return redirect()->route('user.acara.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
