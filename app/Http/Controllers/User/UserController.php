<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Middleware\User;
use Auth;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware(User::class);
    }
}
