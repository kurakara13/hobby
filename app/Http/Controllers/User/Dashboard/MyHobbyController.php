<?php

namespace App\Http\Controllers\User\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\User\UserController;
use App\Hobby;
use App\UserProfile;
use Auth;

class MyHobbyController extends UserController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $userProfile = UserProfile::where('id_user', Auth::user()->id)->first();
      $userHobby = [];
      if($userProfile){
        $userHobby = explode(',', $userProfile->hobby);
      }

      $hobby = Hobby::whereNotIn('nama', $userHobby)->get();
      $hobbyUser = Hobby::whereIn('nama', $userHobby)->get();

      return view('user.dashboard.myhobby.index', compact('userProfile', 'hobbyUser', 'hobby'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $userProfile = UserProfile::where('id_user', Auth::user()->id)->first();
        if($userProfile){
          $userProfile->hobby = $userProfile->hobby.','.$request->hobby;
          $userProfile->save();
        }else {
          $userProfileNew = new UserProfile;
          $userProfileNew->id_user = Auth::user()->id;
          $userProfileNew->hobby = $request->hobby;
          $userProfileNew->save();
        }

        return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $hobby = Hobby::find($id);
        $userProfile = UserProfile::where('id_user', Auth::user()->id)->first();
        $array = explode(',', $userProfile->hobby);
        if(($key = array_search($hobby->nama, $array))  !== false){
          unset($array[$key]);
        }

        $userProfile->hobby = implode(',', $array);
        $userProfile->save();

        return redirect()->back();

    }
}
