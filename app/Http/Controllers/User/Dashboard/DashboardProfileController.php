<?php

namespace App\Http\Controllers\User\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\User\UserController;
use App\Hobby;
use App\UserProfile;
use Auth;

class DashboardProfileController extends UserController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $userProfile = UserProfile::where('id_user', Auth::user()->id)->first();
      $userHobby = [];
      if($userProfile){
        $userHobby = explode(',', $userProfile->hobby);
      }

      $hobby = Hobby::whereNotIn('nama', $userHobby)->get();

      return view('user.dashboard.profile', compact('userProfile', 'userHobby', 'hobby'));
    }

    /**
     *  Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function add_hobby(Request $request)
    {
      $userProfile = UserProfile::where('id_user', Auth::user()->id)->first();
      if($userProfile){
        $userProfile->hobby = $userProfile->hobby.','.$request->hobby;
        $userProfile->save();
      }else {
        $userProfileNew = new UserProfile;
        $userProfileNew->id_user = Auth::user()->id;
        $userProfileNew->hobby = $request->hobby;
        $userProfileNew->save();
      }

      return redirect()->back();
    }

    /**
     *  Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function delete_hobby(Request $request)
    {
      $userProfile = UserProfile::where('id_user', Auth::user()->id)->first();
      $hobby = explode(',', $userProfile->hobby);
      $hobby = array_diff($hobby, [$request->hobby]);
      $userProfile->hobby = implode(',', $hobby);
      $userProfile->save();

      return redirect()->back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
