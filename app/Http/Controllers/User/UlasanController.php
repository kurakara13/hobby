<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Ulasan;
use Auth;

class UlasanController extends Controller
{
    public function store(Request $request){

      $ulasan = new Ulasan;
      $ulasan->id_user = Auth::user()->id;
      $ulasan->id_contents = $request->content_id;
      $ulasan->rating = $request->rating;
      $ulasan->deskripsi = $request->comment;
      $ulasan->save();

      return redirect()->back();
    }

    public function update(Request $request, $id){

      $ulasan = Ulasan::find($id);
      $ulasan->rating = $request->rating;
      $ulasan->deskripsi = $request->comment;
      $ulasan->save();

      return redirect()->back();
    }

    public function destroy($id){

      $ulasan = Ulasan::find($id);
      $ulasan->delete();

      return redirect()->back();
    }
}
