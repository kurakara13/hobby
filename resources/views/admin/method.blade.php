@extends('layouts.app')

@section('style')
<style>

</style>
@stop

@section('content')
<div class="page-tours bg-header page-tours-grid-3-left pd-tour">
	<div class="container">
		<h1 class="title__page">Content Based</h1>
		<p>Hobby User : {{$user->hobby}}</p>
		<div class="row">
			<!-- end-col -->
      <div class="col-lg-12">
        <div class="row">
					@foreach($contentBased as $item)
          <!-- end-col -->
          <div class="col-md-4">
            <div class="listing__wrapper">
              <div class="listing__thubmail">
                <a href="tour-single.html">
                  <div class="item-grid__image-overlay"></div>
                  <img src="{{asset('images/contents/'.$item->gambar)}}" style="width:100%;height:200px" alt="tours">
                </a>
                <div class="listing__favorite">
                  <span class="wishlist-img btn-radius ion-images"></span>
                  <span class="wishlist-img btn-radius ion-ios-heart-outline"></span>
                </div>
              </div>
              <div class="listing_detail">
                <h3><a href="{{route('news.detail', $item->id)}}" class="listing__title">{{substr($item->judul,0,20)}}...</a></h3>
                <div class="listing__review">
                  <ul class="min-list inline-list list-meta">
                    <li class="review-dots">Rating : <span class="review__start img-sale" style="width:10%;height:10%">0</span></li>
                    <li><span class="d-review"> Likes <span class="c-green">0</span></span></li>
                  </ul>
                </div>
                <div class="listing-entry__meta">
                  <span class="entry__location"><i class="fa fa-sun"></i> {{$item->hobi}}</span>
                </div>
                <div class="listing-entry__meta">
                  <span class="entry__location"><i class="fa fa-sun"></i> Content Based :<br><i class="fa fa-sun"></i> {{$item->content_based}}</span>
                </div>
              </div>
            </div>
          </div>
          <!-- end-col -->
					@endforeach
        </div>
        <!-- end-row -->
      </div>
			<!-- end-col -->
		</div>
		<!-- end-row -->

	</div>
	<!-- end-container -->
</div>
@endsection

@section('script')
<script>
</script>
@stop
