<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from haintheme.com/demo/html/cititour/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 18 Jun 2019 17:00:23 GMT -->
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>MyHobby</title>

	<link rel="stylesheet" href="{{asset('assets/scripts/jquery-ui.css')}}" type="text/css" media="all" />
	<link href="https://fonts.googleapis.com/css?family=Lato&amp;Montserrat:400,700" rel="stylesheet">
	<link href="{{asset('assets/styles/datedropper.css')}}" rel="stylesheet" type="text/css" />
	<link href="{{asset('assets/styles/rangeslider.css')}}" rel="stylesheet" type="text/css" />
	<link href="{{asset('assets/styles/fakeloader.css')}}" rel="stylesheet" type="text/css" />
	<link href="{{asset('assets/styles/lightgallery.css')}}" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="{{asset('assets/styles/style.css')}}">
</head>
<body class="is-page-loading">
	<div id="page-loader" class=""></div>
		<div id="theme-container"><div class="page-login">
	<div class="login_singup ">
		<div class="login_logo center">
			<h1 class="logo-login">
            	<a href="index.html">MyHobby</a>
          	</h1>
		</div>
		<div class="form-login-container">
      <form method="POST" class="form-login" action="{{ route('login') }}">
        @csrf
				<div class="form-login__title center">
					<h2 class="title-login login">Log In</h2>
				</div>
				<div class="form-login__input">
					<label for="login-user">Username</label>
          <input type="text" id="ms-form-user" class="form-input btn-radius @error('username') is-invalid @enderror" name="username" value="{{ old('username') }}" required autocomplete="username" autofocus>

          @error('username')
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
              </span>
          @enderror
				</div>
				<div class="form-login__input">
					<label for="login-password">Passsword</label>
          <input type="password" id="ms-form-pass" class="form-input btn-radius @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

          @error('password')
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
              </span>
          @enderror
        </div>
				<div class="form-login__input form-icheck">
					<div class="form-group-login">
						<div class="squaredcheck">
					      <input type="checkbox" value="None" id="squaredcheck" class="checkbox1" name="check" checked="">
					      <label for="squaredcheck"><span class="main-listing__form-label">Remember Me</span></label>
					    </div>
					    <div class="link-forget">
					    	<a class="login-footer-link" href="#">Forgot Password</a>
					    </div>
					</div>
				</div>
				<div class="form-login__button">
					<button class="button-submit btn-submit btn-radius">Log In</button>
				</div>
				<div class="form-login__footer center">
					<span class="login-footer"> Don't have an account ? <a class="login-footer-link" href="register.html"> Register here</a></span>
				</div>
			</form>
		</div>
		<ul class="center">
			<li>
				<a href="#">About</a>
			</li>
			<li>
				<a href="#">Tours</a>
			</li>
			<li>
				<a href="#">Destinations</a>
			</li>
			<li>
				<a href="#">Blog</a>
			</li>
			<li>
				<a href="#">Help</a>
			</li>
		</ul>
	</div>
</div>
		</div>
		<span class="is-loading-effect"></span>
			<script src="{{asset('assets/scripts/jquery.min.js')}}" type="text/javascript"></script>
			<script src="{{asset('assets/scripts/jquery-ui.min.js')}}" type="text/javascript"></script>
			<script src="{{asset('assets/scripts/app.js')}}"></script>
	</body>

<!-- Mirrored from haintheme.com/demo/html/cititour/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 18 Jun 2019 17:00:23 GMT -->
</html>
