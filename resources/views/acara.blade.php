@extends('layouts.app')

@section('style')
<style>

</style>
@stop

@section('content')
<div class="page-tours bg-header page-tours-grid-3-left pd-tour">
	<div class="container">
		<div class="breadcrumbs-container">
		  <ul class="breadcrumbs min-list inline-list">
		    <li class="breadcrumbs__item">
		      <a href="index.html" class="breadcrumbs__link">
		        <span class="breadcrumbs__title">Home</span>
		      </a>
		    </li>
		    <li class="breadcrumbs__item">
		      <span class="breadcrumbs__page c-gray">Acara</span>
		    </li>
		  </ul><!-- .breadcrumbs -->
		</div>
		<h1 class="title__page">Acara</h1>
		<div class="header-form">
      <form action="#">
				<div class="form-search d-lg-flex form-search__home1">
					<div class="form-iner-2 p-rt">
						<input class="input-home search-info" type="text" name="text" placeholder="Cari Yang Sesui Hobi Kamu ...">
					</div>
					<div class="form-iner form-submit__home1">
						<button type="submit" class="btn-submit button-home">Search now</button>
					</div>
				</div>
			</form>
		</div>
		<div class="row">
      <div class="col-lg-3">
        <aside class="sidebar widget-sidebar">
          <section class="widget widget-list-tours">
            <h3 class="widget-title">Sort By</h3>
            <div class="widget-main-select">
              <div class="squaredcheck">
                <input type="radio" value="None" id="squaredcheck" class="checkbox1" name="check" checked />
                <label for="squaredcheck"><span class="main-listing__form-label">Recommended</span></label>
              </div>
              <div class="squaredcheck">
                <input type="radio" value="None" id="squaredcheck1" class="checkbox1" name="check"  />
                <label for="squaredcheck1"><span class="main-listing__form-label">Newest</span></label>
              </div>
              <div class="squaredcheck">
                <input type="radio" value="None" id="squaredcheck2" class="checkbox1" name="check"/>
                <label for="squaredcheck2"><span class="main-listing__form-label">Likes</span></label>
              </div>
              <div class="squaredcheck">
                <input type="radio" value="None" id="squaredcheck3" class="checkbox1" name="check" />
                <label for="squaredcheck3"><span class="main-listing__form-label">Rating</span></label>
              </div>
            </div>
          </section>
          <section class="widget widget-list-tours">
            <h3 class="widget-title">Hobby</h3>
            <ul>
              <li>
                @foreach(App\Hobby::get() as $item)
                <a href="#">
                  <span>{{$item->nama}}</span>
                  <span class="main-listing__form-desc">({{rand(0,10)}})</span>
                </a>
                @endforeach
              </li>
            </ul>
          </section>
          <!-- end-widget -->
        </aside>
      </div>
			<!-- end-col -->
      <div class="col-lg-9">
        <div class="row">
          <div class="col-md-12 listing-search-container d-lg-flex">
            <div class="listing-view">
              <span class="c-dusty-gray">Showing 1-12 of 135 tours</span>
            </div>
          </div>
					@foreach($acara as $item)
          <!-- end-col -->
          <div class="col-md-4">
            <div class="listing__wrapper">
              <div class="listing__thubmail">
                <a href="tour-single.html">
                  <div class="item-grid__image-overlay"></div>
                  <img src="{{asset('images/contents/'.$item->gambar)}}" style="width:100%;height:200px" alt="tours">
                </a>
                <div class="listing__favorite">
                  <span class="wishlist-img btn-radius ion-images"></span>
                  <span class="wishlist-img btn-radius ion-ios-heart-outline"></span>
                </div>
              </div>
              <div class="listing_detail">
                <h3><a href="{{route('event.detail', $item->id)}}" class="listing__title">{{substr($item->judul,0,20)}} ...</a></h3>
                <div class="listing__review">
                  <ul class="min-list inline-list list-meta">
                    <li class="review-dots">Rating : <span class="review__start img-sale" style="width:10%;height:10%">0</span></li>
                    <li><span class="d-review"> Likes <span class="c-green">0</span></span></li>
                  </ul>
                </div>
                <div class="listing-entry__meta">
                  <span class="entry__location"><i class="fa fa-sun"></i> {{$item->hobi}}</span>
                </div>
              </div>
            </div>
          </div>
          <!-- end-col -->
					@endforeach
        </div>
        <!-- end-row -->
      </div>
			<!-- end-col -->
		</div>
		<!-- end-row -->

	</div>
	<!-- end-container -->
</div>
@endsection

@section('script')
<script>
</script>
@stop
