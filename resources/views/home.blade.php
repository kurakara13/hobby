@extends('layouts.app')

@section('style')
<style>

</style>
@stop

@section('content')
<section class="page-banner bg-header page-banner--layout-home">
	<div class="header-title container">
		<div class="pages-title">
			<h1 class="title__page c-white">Temukan Hobi Anda</h1>
			<p>Berkumpul bersama orang-orang dengan hobi sama lebih menyenangkan</p>
		</div>
		<div class="header-form">
			<form action="#">
				<div class="form-search d-lg-flex form-search__home1">
					<div class="form-iner-2 p-rt">
						<input class="input-home search-info" type="text" name="text" placeholder="Vietnamese">
					</div>
					<div class="form-iner form-submit__home1">
						<button type="submit" class="btn-submit button-home" style="width:100%">Search now</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</section>
<div class="pages-content tour-grid-4"  style="background: #f8f8f8">
  <div class="main-content pd-content">
    <section class="section section-header">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <h1 class="page-section__title center">Tempat Populer</h1>
          </div>
          <div class="all">
						@foreach($tempat as $item)
            <div class="col-md-6 col-xl-3 isotope-item festival">
              <div class="listing__wrapper">
                <div class="listing__thubmail">
                  <a href="tour-single.html">
										<img src="{{asset('images/contents/'.$item->gambar)}}" style="width:100%;height:200px" alt="tours">
                    <div class="item-grid__image-overlay"></div>
                  </a>
                  <div class="listing__favorite">
                    <span class="wishlist-img btn-radius ion-images"></span>
                    <span class="wishlist-img btn-radius"><i class="ion-ios-heart-outline"></i></span>
                  </div>
                </div>
                <div class="listing_detail">
                  <h3><a class="listing__title" href="{{route('place.detail', $item->id)}}"> {{substr($item->judul,0,25)}} ... </a></h3>
									<div class="listing__review">
	                  <ul class="min-list inline-list list-meta">
	                    <li class="review-dots">Rating : <span class="review__start img-sale" style="width:10%;height:10%">0</span></li>
	                    <li><span class="d-review"> Likes <span class="c-green">0</span></span></li>
	                  </ul>
	                </div>
	                <div class="listing-entry__meta">
	                  <span class="entry__location"><i class="fa fa-sun"></i> {{$item->hobi}}</span>
	                </div>
                </div>
              </div>
            </div>
						@endforeach
					</div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <h1 class="page-section__title center">Acara</h1>
          </div>

          <div class="all">
						@foreach($acara as $item)
            <div class="col-md-6 col-xl-3 isotope-item festival">
              <div class="listing__wrapper">
                <div class="listing__thubmail">
                  <a href="tour-single.html">
										<img src="{{asset('images/contents/'.$item->gambar)}}" style="width:100%;height:200px" alt="tours">
                    <div class="item-grid__image-overlay"></div>
                  </a>
                  <div class="listing__favorite">
                    <span class="wishlist-img btn-radius ion-images"></span>
                    <span class="wishlist-img btn-radius"><i class="ion-ios-heart-outline"></i></span>
                  </div>
                </div>
                <div class="listing_detail">
									<h3><a class="listing__title" href="{{route('event.detail', $item->id)}}"> {{substr($item->judul,0,25)}} ... </a></h3>
									<div class="listing__review">
	                  <ul class="min-list inline-list list-meta">
	                    <li class="review-dots">Rating : <span class="review__start img-sale" style="width:10%;height:10%">0</span></li>
	                    <li><span class="d-review"> Likes <span class="c-green">0</span></span></li>
	                  </ul>
	                </div>
	                <div class="listing-entry__meta">
	                  <span class="entry__location"><i class="fa fa-sun"></i> {{$item->hobi}}</span>
	                </div>
                </div>
              </div>
            </div>
						@endforeach
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <h1 class="page-section__title center">Berita</h1>
          </div>

          <div class="all">
						@foreach($berita as $item)
						<div class="col-md-6 col-xl-3 isotope-item festival">
							<div class="listing__wrapper">
								<div class="listing__thubmail">
									<a href="tour-single.html">
										<img src="{{asset('images/contents/'.$item->gambar)}}" style="width:100%;height:200px" alt="tours">
										<div class="item-grid__image-overlay"></div>
									</a>
									<div class="listing__favorite">
										<span class="wishlist-img btn-radius ion-images"></span>
										<span class="wishlist-img btn-radius"><i class="ion-ios-heart-outline"></i></span>
									</div>
								</div>
								<div class="listing_detail">
									<h3><a class="listing__title" href="{{route('news.detail', $item->id)}}"> {{substr($item->judul,0,25)}} ... </a></h3>
									<div class="listing__review">
										<ul class="min-list inline-list list-meta">
											<li class="review-dots">Rating : <span class="review__start img-sale" style="width:10%;height:10%">0</span></li>
											<li><span class="d-review"> Likes <span class="c-green">0</span></span></li>
										</ul>
									</div>
									<div class="listing-entry__meta">
										<span class="entry__location"><i class="fa fa-sun"></i> {{$item->hobi}}</span>
									</div>
								</div>
							</div>
						</div>
						@endforeach
          </div>
        </div>
      </div>

    </section>
  </div>
</div>
@stop

@section('script')
<script>
$(document).ready(function(){

});
</script>
@stop
