<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="{{asset('vendors/gentelella/images/favicon.ico')}}" type="image/ico" />

    <title>Monitoring Anggaran | </title>

    <!-- Bootstrap -->
    <link href="{{asset('vendors/gentelella/vendors/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{asset('vendors/gentelella/vendors/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <!-- NProgress -->
    <link href="{{asset('vendors/gentelella/vendors/nprogress/nprogress.css')}}" rel="stylesheet">
    <!-- iCheck -->
    <link href="{{asset('vendors/gentelella/vendors/iCheck/skins/flat/green.css')}}" rel="stylesheet">

    @yield('css')

    <!-- Custom Theme Style -->
    <link href="{{asset('vendors/gentelella/build/css/custom.min.css')}}" rel="stylesheet">

    @yield('style')

  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"><i class="fa fa-paw"></i> <span>Anggaran!</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="{{asset('vendors/gentelella/images/img.jpg')}}" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>
                  @if(Auth::user()->role == 0)
                    Admin
                  @else
                    User
                  @endif
                  ,</span>
                <h2>{{Auth::user()->name}}</h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                  @if(Auth::user()->role == 0)
                  <li class="{{request()->routeIs('admin.home') ? 'active' : ''}}"><a href="{{route('admin.home')}}"><i class="fa fa-home"></i> Dashboard </a></li>
                  <li class="{{request()->routeIs('admin.anggaran.index', 'admin.anggaran.show') ? 'active' : ''}}"><a href="{{route('admin.anggaran.index')}}"><i class="fa fa-edit"></i> Buku Anggaran</a></li>
                  <li class="{{request()->routeIs('admin.anggaran.destroy', 'admin.anggaran.delete', 'admin.anggaran.delete.kegiatan', 'admin.kegiatan.delete') ? 'active' : ''}}"><a href="{{route('admin.anggaran.delete')}}"><i class="fa fa-edit"></i> Hapus Buku Anggaran</a></li>
                  @else
                  <li class="{{request()->routeIs('user.home') ? 'active' : ''}}"><a href="{{route('user.home')}}"><i class="fa fa-home"></i> Dashboard </a></li>
                  <li class="{{request()->routeIs('user.anggaran', 'user.kegiatan', 'user.realisasi') ? 'active' : ''}}"><a href="{{route('user.anggaran')}}"><i class="fa fa-edit"></i> Buku Anggaran</a></li>
                  @endif
                </ul>
              </div>

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="{{asset('vendors/gentelella/images/img.jpg')}}" alt="">{{Auth::user()->name}}
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="javascript:;"> Profile</a></li>
                    <li>
                      <a href="{{ route('logout') }}"
                         onclick="event.preventDefault();
                                       document.getElementById('logout-form').submit();">
                          <i class="fa fa-sign-out pull-right"></i> {{ __('Logout') }}
                      </a>

                      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                          @csrf
                      </form>
                    </li>
                  </ul>
                </li>
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          @yield('content')
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="{{asset('vendors/gentelella/vendors/jquery/dist/jquery.min.js')}}"></script>
    <!-- Bootstrap -->
    <script src="{{asset('vendors/gentelella/vendors/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <!-- FastClick -->
    <script src="{{asset('vendors/gentelella/vendors/fastclick/lib/fastclick.js')}}"></script>
    <!-- NProgress -->
    <script src="{{asset('vendors/gentelella/vendors/nprogress/nprogress.js')}}"></script>
    <!-- iCheck -->
    <script src="{{asset('vendors/gentelella/vendors/iCheck/icheck.min.js')}}"></script>

    @yield('script')

    <!-- Custom Theme Scripts -->
    <script src="{{asset('vendors/gentelella/build/js/custom.js?v=1')}}"></script>

    @yield('js')

  </body>
</html>
