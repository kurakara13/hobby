@if(Auth::user()->role == 1)
<div class="sidebar-dashboard dashboard-bg">
  <div class="item-list tour-booking">
    <ul class="min-list list-dashboard">
      <li>
        <a href="dashboard-my-booking.html">DASHBOARD</a>
      </li>
    </ul>
  </div>
  <div class="item-list my-acount">
    <h2 class="my-acount__title min border-sidebar__list">My Acount</h2>
    <ul class="min-list list-dashboard">
      <li class="">
        <a href="#">MY PROFILE </a>
      </li>
      <li>
        <a href="dashboard-edit-profile.html">EDIT PROFILE</a>
      </li>
      <li class="{{request()->routeIs('user.hobby.index', 'user.hobby.store') ? 'active-li' : ''}}">
        <a href="{{route('user.hobby.index')}}">MY HOBBY</a>
      </li>
      <li>
        <a href="dashboard-change-password.html">CHANGE PASSWORD</a>
      </li>
    </ul>
  </div>
  <div class="item-list tour-booking">
    <h2 class="my-acount__title min border-sidebar__list">Your Post</h2>
    <ul class="min-list list-dashboard">
      <li class="{{request()->routeIs('user.acara.index', 'user.acara.create') ? 'active-li' : ''}}">
        <a href="{{route('user.acara.index')}}">ACARA</a>
      </li>
      <li class="{{request()->routeIs('user.tempat.index', 'user.tempat.create') ? 'active-li' : ''}}">
        <a href="{{route('user.tempat.index')}}">TEMPAT</a>
      </li>
      <li class="{{request()->routeIs('user.komunitas.index', 'user.komunitas.create') ? 'active-li' : ''}}">
        <a href="{{route('user.komunitas.index')}}">KOMUNITAS</a>
      </li>
    </ul>
  </div>
  <div class="item-list tour-booking">
    <h2 class="my-acount__title min border-sidebar__list">Booking</h2>
    <ul class="min-list list-dashboard">
      <li>
        <a href="dashboard-my-booking.html">MY BOOKINGS</a>
      </li>
      <li>
        <a href="dashboard-wishlist.html">WISHLIST</a>
      </li>
    </ul>
  </div>
  <div class="sing-out min border-sidebar__list">
    <a class="sing-out__link" href="login.html">SIGN OUT</a>
  </div>
</div>
@else
<div class="sidebar-dashboard dashboard-bg">
  <div class="item-list tour-booking">
    <ul class="min-list list-dashboard">
      <li>
        <a href="dashboard-my-booking.html">DASHBOARD</a>
      </li>
    </ul>
  </div>
  <div class="item-list my-acount">
    <h2 class="my-acount__title min border-sidebar__list">My Acount</h2>
    <ul class="min-list list-dashboard">
      <li class="">
        <a href="#">MY PROFILE </a>
      </li>
      <li>
        <a href="dashboard-edit-profile.html">EDIT PROFILE</a>
      </li>
      <li class="{{request()->routeIs('admin.hobby.index', 'admin.hobby.store') ? 'active-li' : ''}}">
        <a href="{{route('user.hobby.index')}}">MY HOBBY</a>
      </li>
      <li>
        <a href="dashboard-change-password.html">CHANGE PASSWORD</a>
      </li>
    </ul>
  </div>
  <div class="item-list tour-booking">
    <h2 class="my-acount__title min border-sidebar__list">Your Post</h2>
    <ul class="min-list list-dashboard">
      <li class="{{request()->routeIs('admin.berita.index', 'admin.berita.create') ? 'active-li' : ''}}">
        <a href="{{route('admin.berita.index')}}">BERITA</a>
      </li>
    </ul>
  </div>
  <div class="item-list tour-booking">
    <h2 class="my-acount__title min border-sidebar__list">Booking</h2>
    <ul class="min-list list-dashboard">
      <li>
        <a href="dashboard-my-booking.html">LIST BOOKINGS</a>
      </li>
      <li>
        <a href="dashboard-review.html">PAYMENTS</a>
      </li>
    </ul>
  </div>
  <div class="sing-out min border-sidebar__list">
    <a class="sing-out__link" href="login.html">SIGN OUT</a>
  </div>
</div>
@endif
