<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from haintheme.com/demo/html/cititour/index-2.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 18 Jun 2019 16:53:54 GMT -->
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>MyHobby</title>

	<link rel="stylesheet" href="{{asset('assets/scripts/jquery-ui.css')}}" type="text/css" media="all" />
	<link href="https://fonts.googleapis.com/css?family=Lato&amp;Montserrat:400,700" rel="stylesheet">
  <link href="{{asset('assets/css/all.css')}}" rel="stylesheet" type="text/css" />
	<link href="{{asset('assets/styles/datedropper.css')}}" rel="stylesheet" type="text/css" />
	<link href="{{asset('assets/styles/rangeslider.css')}}" rel="stylesheet" type="text/css" />
	<link href="{{asset('assets/styles/fakeloader.css')}}" rel="stylesheet" type="text/css" />
	<link href="{{asset('assets/styles/lightgallery.css')}}" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="{{asset('assets/styles/style.css')}}">
  @yield('style')
  <style>
  @media (min-width: 992px){
    .footer-top {
        margin-bottom: 0px;
        padding-top: 0px;
    }
    .header-title {
        padding: 89px 0 89px 0;
    }
  }


  .form-iner-2 .input-home {
      padding: 10px;
      color: #3e3e3e;
      border-radius: 4px;
      margin: 5px 0;
      border: none;
  }


  .form-iner-2 > input {
      width: 100%;
  }

  @media (min-width: 992px){
    .form-iner-2:not(.form-submit__home1) {
        width: calc( ( 100% - 220px ));
    }

    .form-iner-2 .input-home {
        padding: 40px 80px 40px 40px;
        border: none;
        border-radius: 0;
        max-width: 100%;
    }

    .form-iner-2 .search-info {
        border-top-left-radius: 5px !important;
        border-bottom-left-radius: 5px !important;
        border-bottom-left-radius: 0px;
        border-top-left-radius: 0px;
    }
    .form-iner-2 > input {
        width: auto;
    }
  }

  .squaredcheck input[type=radio] {
      visibility: hidden;
  }

  .squaredcheck input[type=radio]:checked + label {
      background: #e14b23;
  }
  </style>
</head>
<body class="is-page-loading">
	<div id="page-loader" class=""></div>
	<div id="theme-container">
    <header id="masthead" class="site-header full-width border-bottom_header background-white">
    	<div class="header-menu full-width-1">
    		<div class="header-nav d-flex menu-color-gray">
        	<div class="site-header__logo">
        		<h1 class="logo-head logo-black">
        			<a href="index.html" class="c-black">MyHobby</a>
        		</h1>
        	</div><!-- .site-header__logo -->

        	<div class="social-menu-header">
        		<div class="d-lg-none nav-mobile">
        			<a href="#" class="nav-toggle js-nav-toggle">
        				<span></span>
        			</a><!-- .nav-toggle -->
      	    </div>
            <nav id="menu-nav">
              <ul class="list-menu min-list main-navigation">
                <li class="head-menu">
                  <a href="{{route('home')}}">Home</a>
                </li>
                <li class="head-menu">
                  <a href="{{route('aboutus')}}">Tentang Kami</a>
                </li>
                <li class="head-menu">
                  <a href="{{route('event')}}">Acara</a>
                </li>
                <li class="head-menu">
                  <a href="{{route('place')}}">Tempat</a>
                </li>
                <li class="head-menu">
                  <a href="{{route('news')}}">Berita</a>
                </li>
                <li class="head-menu">
                  <a href="{{route('community')}}">Komunitas</a>
                </li>
								@guest
								@else
								@if(Auth::user()->role == 0)
								<li class="head-menu">
									<a href="{{route('admin.method')}}">Method</a>
								</li>
								@endif
								@endguest
              </ul>
            </nav>
        	</div>

        	<div class="social-menu-left inline-list list-menu">
        		<ul class="min-list">
              @guest
              <li><a href="{{route('login')}}">Login</a></li>
              <li><a href="{{route('register')}}">Sign Up</a></li>
              @else
							@if(Auth::user()->role == 1)
							<li><a href="{{route('user.dashboard')}}">Dashboard</a></li>
							@else
							<li><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
							@endif
							<li>
                <a href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();">
                    {{ __('Logout') }}
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
              </li>
              @endguest
        		</ul>
        	</div>
        </div>
    <!-- .site-header__menu -->
    	</div>
    </header>

    @yield('content')

    <!-- end-pages -->
    <footer id="colophone" class="site-footer">
    	<div class="t-center site-footer__primary">
    		<div class="footer-top">
    			<div class="container">
    				<div class="row">
    					<div class="col-md-6 col-lg-3">
    						<h3 class="footer-title">Hobi</h3>
    						<ul class="min-list footer-list">
                  @foreach(App\Hobby::get() as $item)
    							<li><a href="#">{{$item->nama}}</a></li>
                  @endforeach
    						</ul>
    					</div>

    					<div class="col-md-6 col-lg-3">
    						<h3 class="footer-title">Account</h3>
    						<ul class="min-list footer-list">
    							<li><a href="#">Acara</a></li>
    							<li><a href="#">Berita</a></li>
    							<li><a href="#">Tempat</a></li>
    							<li><a href="#">Komunitas</a></li>
    						</ul>
    					</div>

    					<div class="col-md-6 col-lg-6">
    						<h3 class="footer-title">Subscribe</h3>
    						<form action="#" class="footer-form">
    							<input type="email" class="email-footer form-input" placeholder="your@email.com" />
    							<button type="submit" class="btn-submit submit-footer btn-radius" style="width:100%"> Subscribe</button>
    						</form>
    						<p class="title-sale"> I want emails from MyHobby with travel and product information, promotions, advertisements, third-party offers, and surveys. I can unsubcribe any time using the unsubcribe link at the end of all emails. Contact MyHobby <a href="#">here</a> . Read MyHobby <a href="#">Privacy Policy</a> .</p>
    					</div>
    				</div>
    			</div>
    		</div>
    		<div class="footer-bottom site-footer">
    			<div class="container">
    				<div class="d-lg-flex  justify-content-between align-items-center footer-bottom-top">
    					<h1 class="logo-head logo-foot">
    			            <a href="index.html" class="c-black">MyHobi</a>
    		            </h1>
    					<ul class="social-menu min-list inline-list">
                <li><a href="#">Tentang Kami</a></li>
    						<li><a href="#">Acara</a></li>
    						<li><a href="#">Berita</a></li>
    						<li><a href="#">Tempat</a></li>
    						<li><a href="#">Komunitas</a></li>
    					</ul>
    					<div class="social-footer">
    						<ul class="min-list inline-list">
    							<li><a href="#" class="ion-font-size ion-social-facebook"></a></li>
    							<li><a href="#" class="ion-font-size ion-social-twitter"></a></li>
    							<li><a href="#" class="ion-font-size ion-social-instagram"></a></li>
    							<li><a href="#" class="ion-font-size ion-social-youtube"></a></li>
    						</ul>
    					</div>
    				</div>
    				<div class="footer-copyright">
    					<p>&amp; 2018 MyHobby. All rights reserved. No pary of this site may be reproduced wirhout our weittem permission.</p>
    				</div>
    			</div>
    		</div>
    	</div>
    </footer><!-- #colophone -->
    <a href="#" class="back-to-top"><span class="ion-ios-arrow-up"></span></a>
	</div>
	<span class="is-loading-effect"></span>
		<script src="{{asset('assets/scripts/jquery.min.js')}}" type="text/javascript"></script>
		<script src="{{asset('assets/scripts/jquery-ui.min.js')}}" type="text/javascript"></script>
		<script src="{{asset('assets/scripts/app.js')}}"></script>
    @yield('script')
</body>

<!-- Mirrored from haintheme.com/demo/html/cititour/index-2.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 18 Jun 2019 16:54:14 GMT -->
</html>


<!-- <!DOCTYPE html> -->
<!-- <html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1"> -->

    <!-- CSRF Token -->
    <!-- <meta name="csrf-token" content="{{ csrf_token() }}"> -->

    <!-- <title>{{ config('app.name', 'Laravel') }}</title> -->

    <!-- Scripts -->
    <!-- <script src="{{ asset('js/app.js')}}') }}" defer></script> -->

    <!-- Fonts -->
    <!-- <link rel="dns-prefetch" href="//fonts.gstatic.com"> -->
    <!-- <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css"> -->

    <!-- Styles -->
    <!-- <link href="{{ asset('css/app.css')}}') }}" rel="stylesheet"> -->
<!-- </head> -->
<!-- <body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent"> -->
                    <!-- Left Side Of Navbar -->
                    <!-- <ul class="navbar-nav mr-auto">

                    </ul> -->

                    <!-- Right Side Of Navbar -->
                    <!-- <ul class="navbar-nav ml-auto"> -->
                        <!-- Authentication Links -->
                        <!-- @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
        </main>
    </div>
</body>
</html> -->
