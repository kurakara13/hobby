@extends('layouts.app')

@section('style')
<style>

</style>
@stop

@section('content')
<div class="main-content-single content-slider-single">
	<div class="container">
		<div class="row">
			<div class="col-lg-8">
				<div class="content-area"  style="margin-bottom:50px">
					<section class="page-banner">
						<div class="post_page_blog-sidebar">
							<h2 class="title-single-sidebar">{{$komunitasDetail->judul}}</h2>
							<div class="post-socical">
								<ul class="min-list inline-list">
									<li class="menu-item">
										 <img class="item-thubmail-2" src="{{asset('assets/image/uploads/id-bl.png')}}" alt="id"><span class="socical-title">{{$komunitasDetail->user->name}}</span>
									</li>
									<li class="menu-item">
										<img class="item-thubmail" src="{{asset('assets/image/bl-date.png')}}" alt="id"><span class="socical-title">{{date('D M Y',strtotime($komunitasDetail->created_at))}}</span>
									</li>
									<li class="menu-item">
										<img class="item-thubmail" src="{{asset('assets/image/bl-cm.png')}}" alt="id"><span class="socical-title">{{$komunitasDetail->ulasan->count()}}</span>
									</li>
									<li class="menu-item">
										#<span class="socical-title" style="margin:0">{{$komunitasDetail->type}}</span>
									</li>
								</ul>
							</div>
						</div>
					</section>
					<div class="post-blog__single">
						<div class="post-blog__image">
							<img src="{{asset('images/contents/'.$komunitasDetail->gambar)}}" style="width:100%;height:400px" alt="tours">
						</div>
						<div class="">
							<div class="entry-header">
								<h2 class="entry-header__title">{{$komunitasDetail->judul}}</h2>
							</div>
							<div class="entry-content">
								{!!$komunitasDetail->deskripsi!!}
							</div>
							<div class="entry-meta d-md-flex justify-content-md-between">
								<div class="entry-tags">
									<span class="tags-title">Hobi:</span>
									@foreach(explode(',', $komunitasDetail->hobi) as $item)
									<a href="#">{{$item}} | </a>
									@endforeach
								</div>
							</div>
						</div>
					</div>
					<div class="comment-single bg-white" style="padding:20px">
						<nav class="listing-nav collapse navbar-collapse" id="myNavbar">
							<ul class="min-list inline-list">
								<li>
									<a class="listing-nav-link nav-link" href="#review">Reviews</a>
								</li>
								@if(Auth::user())
								<li>
									<a class="listing-nav-link nav-link" href="#write">Write a review</a>
								</li>
								@endif
							</ul>
						</nav>
						<!-- end-nav -->
						<section class="list-comment_wrapper listing-section__single" id="review">
							<ul class="min-list">
								@foreach($ulasan as $item)
								<li>
									<div class="list-comment__Wrapper d-flex bd-bottom first-comment">
										<!-- <div class="img-thubmail">
											<img src="{{asset('assets/image/uploads/id-comment.png')}}" alt="tours">
										</div> -->
										<div class="comment_detail">
											<h4 class="entry-title__comment">{{$item->user->name}} {{$item->id_user == Auth::user()->id ? '(Yours)' : ''}}</h4>
											<span class="entry-time"> {{$komunitasDetail->created_at}}</span>
											<p class="entry-description">{{$item->deskripsi}}</p>
											<div class="comment-star" style="max-width:200px">
												<span>Rating:</span>
												<span>
													@for($i=0;$i<$item->rating;$i++)
													<i class="ion-ios-star"></i>
													@endfor
													@for($i=0;$i<(5-($item->rating));$i++)
													<i class="ion-ios-star" style="color:#dddddd"></i>
													@endfor
												</span>
											</div>
										</div>
									</div>
								</li>
								@endforeach
							</ul>
						</section>
							<!-- end-comment -->
							<div class="pagination_tours-reviews d-flex justify-content-center pagination_blog">
								{{$ulasan->links()}}
							</div>
							<!-- end-pagination -->

							<section class="form-comment listing-section__single" id="write">
								@if(Auth::user())
									@if($komunitasDetail->ulasan->where('id_user', Auth::user()->id)->count() > 0)
									@php($ulasanUser = $komunitasDetail->ulasan->where('id_user', Auth::user()->id)->first())
									<div class="wrap-rating in-post">
										<div class="form-group">
											<div class="head" style="display:flex">
												<h4 class="form-label">Edit Your Review :</h4>
												<button class="btn-radius btn-submit" onclick="$('#delete-ul').submit()" style="padding:7px;position:absolute;right:75px">Hapus Ulasan</button>
												<form id="delete-ul" action="{{route('user.ulasan.destroy', $ulasanUser->id)}}" method="post">
													@method('DELETE')
													@csrf
												</form>
											</div>
											<ul class="min-list inline-list stars">
												<li class="star" id="rating-1" data-rating="1" onclick="$('#rating').val(1)">
													<i class="ion-ios-star"></i>
												</li>
												<li class="star" id="rating-2" data-rating="2" onclick="$('#rating').val(2)">
													<i class="ion-ios-star"></i>
												</li>
												<li class="star" id="rating-3" data-rating="3" onclick="$('#rating').val(3)">
													<i class="ion-ios-star"></i>
												</li>
												<li class="star" id="rating-4" data-rating="4" onclick="$('#rating').val(4)">
													<i class="ion-ios-star"></i>
												</li>
												<li class="star" id="rating-5" data-rating="5" onclick="$('#rating').val(5)">
													<i class="ion-ios-star"></i>
												</li>
											</ul>
										</div>
									</div>
									<form action="{{route('user.ulasan.update', $ulasanUser->id)}}" method="post">
										@method('PUT')
										@csrf
										<input type="hidden" name="content_id" value="{{$komunitasDetail->id}}">
										<input type="hidden" id="rating" name="rating" value="{{$ulasanUser->rating}}">
										<div class="form-comment">
											<label class="title-form" for="comment" >Your review <span class="c-sale">*</span></label>
											<textarea name="comment" class="form-area input-border" id="comment">{{$ulasanUser->deskripsi}}</textarea>
										</div>
										<div class="form-submit">
											<button type="submit" class="btn-radius btn-submit">Submit</button>
										</div>
									</form>
									@else
									<div class="wrap-rating in-post">
										<div class="form-group">
											<h4 class="form-label">Choose your rate :</h4>
											<ul class="min-list inline-list stars">
												<li class="star" data-rating="1" onclick="$('#rating').val(1)">
													<i class="ion-ios-star"></i>
												</li>
												<li class="star" data-rating="2" onclick="$('#rating').val(2)">
													<i class="ion-ios-star"></i>
												</li>
												<li class="star" data-rating="3" onclick="$('#rating').val(3)">
													<i class="ion-ios-star"></i>
												</li>
												<li class="star" data-rating="4" onclick="$('#rating').val(4)">
													<i class="ion-ios-star"></i>
												</li>
												<li class="star" data-rating="5" onclick="$('#rating').val(5)">
													<i class="ion-ios-star"></i>
												</li>
											</ul>
										</div>
									</div>
									<form action="{{route('user.ulasan.store')}}" method="post">
										@csrf
										<input type="hidden" name="content_id" value="{{$komunitasDetail->id}}">
										<input type="hidden" id="rating" name="rating" value="0">
										<div class="form-comment">
											<label class="title-form" for="comment" >Your review <span class="c-sale">*</span></label>
											<textarea name="comment" class="form-area input-border" id="comment"></textarea>
										</div>
										<div class="form-submit">
											<button type="submit" class="btn-radius btn-submit">Submit</button>
										</div>
									</form>
									@endif
								@else
								<div class="text-center" style="text-align:center">
									<h2><a href="{{route('login')}}">Login</a> Terlebih Dahulu Untuk Memberi Ulasan</h2>
								</div>
								@endif
							</section>
							<!-- end-form-comment -->
						<!-- end-review -->
					</div>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="sidebar widget-area">
					<section class="widget widget-sale">
						<div class="price-form">
							<!-- <span class="price-form__step"> Tersedia <i class="fa fa-info-circle" aria-hidden="true"></i></span> -->
							<div class="to">
								<span class="price-form__step"> {{date('D M Y',strtotime($komunitasDetail->updated_at))}}</span>
							</div>
							<div class="price-form__price-list" style="display: block;">
								<h2 class="title__page price__wrap">{{$komunitasDetail->status == 0 ? 'Belum Terverifikasi' : 'Terverifikasi'}}</h2>
							</div>
						</div>
					</section>
					<section class="widget widget-form bg-white" style="text-align:center">
						@guest
						<h3><a href="{{route('login')}}">Login</a> Untuk Menghubungi Komunitas</h3>
						@else
						<form action="http://haintheme.com/">
							<div class="form-submit">
								<button class="btn-submit btn-radius button-submit">Hubungi Sekarang</button>
							</div>
						</form>
						@endif
					</section>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('script')
<script>
function rating(id){
	$('#rating-'+id).click();
}

$('.pagination').addClass('min-list inline-list bg-navigation')
$('.pagination li').first().children().text('').addClass('ion-ios-arrow-back');
$('.pagination li').last().children().text('').addClass('ion-ios-arrow-forward');
var text = $('.pagination').find('.active').children().text();
$('.pagination').find('.active').children().replaceWith('<a class="page-link">'+text+'</a>');
</script>
@if(Auth::user())
	@if($komunitasDetail->ulasan->where('id_user', Auth::user()->id)->count() > 0)
	<script>
	rating({{$komunitasDetail->ulasan->where('id_user', Auth::user()->id)->first()->rating}});
	</script>
	@endif
@endif
@stop
