@extends('layouts.app')

@section('style')
<style>
.wo_user_side_info{
  padding: 0;
    white-space: nowrap;
    margin: 0;
}
.wo_user_side_info li {
    display: inline-block;
    margin: 0 auto;
    width: 32.13%;
    text-align: center;
    border-right: 1px solid #e9e9e9;
}

.wo_user_side_info .menu_list {
    display: block;
    padding: 7px 5px;
    transition: all .15s ease-in-out;
    text-transform: capitalize;
    text-decoration: none;
}
.wo_user_side_info li .split-link {
    display: block;
}

.wo_user_side_info li a {
    color:black
}

.pub-focus .post.publisher-box {
    z-index: 1031;
    position: relative;
}

.panel-white {
    box-shadow: 0 1px 0 0 #e3e4e8, 0 0 0 1px #f1f1f1;
}

.panel {
    margin-bottom: 20px;
    background-color: #fff;
    /* border: 1px solid transparent; */
    border-radius: 3px;
}

#post-textarea {
    position: relative;
}

.publisher-box .post-avatar {
    width: 35px;
    height: 35px;
    border-radius: 50%;
    position: absolute;
    left: 10px;
    top: 10px;
    z-index: 22;
}

img {
    vertical-align: middle;
}

.publisher-box textarea.postText {
    padding: 16px 55px;
    line-height: 1.628571;
    height: 55px;
}

textarea.postText {
    border: none;
    padding-top: 12px;
    padding-left: 50px;
    height: 45px;
    box-shadow: none;
}

#loading_indicator {
    position: absolute;
    display: none;
    right: 5px;
    top: 5px;
    color: #999;
}

.publisher-box .publisher-box-footer {
    padding: 5px;
    display: none;
}

.publisher-box .publisher-box-footer .pub-footer-upper {
    position: relative;
}

.publisher-box .publisher-box-footer .pub-footer-upper {
    margin: 0;
    padding: 4px 1px;
}

.pull-left {
    float: left!important;
}

.publisher-box .publisher-box-footer .poster-left-buttons {
    margin: 0 5.3px;
}

.publisher-box .publisher-box-footer .poster-left-buttons {
    margin: 0 7.8px;
    text-align: center;
    border-radius: 50%;
    transition: all .1s linear;
}

.publisher-box .publisher-box-footer .poster-left-buttons .btn {
    /* padding: 7px; */
    padding: 0;
    border: 0;
    width: 35px;
    height: 35px;
    display: flex;
    align-items: center;
    justify-content: center;
}
.btn-file {
    position: relative;
    overflow: hidden;
}

.btn-file input[type=file] {
    position: absolute;
    top: 0;
    right: 0;
    min-width: 100%;
    min-height: 100%;
    text-align: right;
    opacity: 0;
    outline: 0;
    background: #fff;
    cursor: inherit;
    display: block;
}

.publisher-box .publisher-box-footer .poster-left-buttons span#postRecordingTime {
    position: relative;
    font-size: 11px;
    font-weight: 700;
    margin-top: -10px;
    display: block;
    pointer-events: none;
}
.hidden {
    display: none!important;
}

.all_colors {
    display: none;
    padding: 0px 9px 8px;
    position: absolute;
    background: white;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: 1;
}

.all_colors > svg {
    position: absolute;
    right: 0;
    top: 6px;
    cursor: pointer;
}

.list-group-item .menu-item{
    margin-left: 10px;
    position: relative;
    top: -5px;
}
</style>
@stop

@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-lg-2 d-none d-lg-block" style="padding-right:0">
      <ul class="list-group">
    		<li class="list-group-item">
    			<a href="#">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="color:#047cac;">
              <path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path>
              <circle cx="12" cy="7" r="4"></circle>
            </svg>
    				<span class="menu-item">
              My Profile
            </span>
          </a>
    		</li>
    		<li class="list-group-item">
    			<a href="#">
    				<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="color:#8bc34a;">
              <rect x="3" y="3" width="18" height="18" rx="2" ry="2"></rect>
              <circle cx="8.5" cy="8.5" r="1.5"></circle>
              <polyline points="21 15 16 10 5 21"></polyline>
            </svg>
    				<span class="menu-item">
              Albums
            </span>
          </a>
    		</li>
    		<li class="list-group-item">
          <a href="#">
    				<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="color:#673ab7;">
              <path d="M4 3h16a2 2 0 0 1 2 2v6a10 10 0 0 1-10 10A10 10 0 0 1 2 11V5a2 2 0 0 1 2-2z"></path>
              <polyline points="8 10 12 14 16 10"></polyline>
            </svg>
    				<span class="menu-item">
              Saved Posts
            </span>
          </a>
    		</li>
				<li class="list-group-item">
          <a href="#">
    				<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="color:#009688;">
              <circle cx="12" cy="12" r="10"></circle><circle cx="12" cy="12" r="6"></circle>
              <circle cx="12" cy="12" r="2"></circle>
            </svg>
    				<span class="menu-item">
              Pokes
            </span>
    			</a>
    		</li>
				<li class="list-group-item">
          <a href="#">
    				<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="color:#03A9F4;">
              <rect x="3" y="3" width="7" height="7"></rect>
              <rect x="14" y="3" width="7" height="7"></rect>
              <rect x="14" y="14" width="7" height="7"></rect>
              <rect x="3" y="14" width="7" height="7"></rect>
            </svg>
    				<span class="menu-item">
              My Groups
            </span>
          </a>
    		</li>
				<li class="list-group-item">
          <a href="#">
    				<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="color:#f79f58;">
              <path d="M4 15s1-1 4-1 5 2 8 2 4-1 4-1V3s-1 1-4 1-5-2-8-2-4 1-4 1z"></path>
              <line x1="4" y1="22" x2="4" y2="15"></line>
            </svg>
            <span class="menu-item">
              My Pages
            </span>
          </a>
    		</li>
				<li class="list-group-item">
          <a href="#">
    				<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="color:#f35d4d;">
              <path d="M4 19.5A2.5 2.5 0 0 1 6.5 17H20"></path>
              <path d="M6.5 2H20v20H6.5A2.5 2.5 0 0 1 4 19.5v-15A2.5 2.5 0 0 1 6.5 2z"></path>
            </svg>
            <span class="menu-item">
              Blog
            </span>
          </a>
    		</li>
    		<li class="list-group-item">
          <a href="#">
    				<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="color:#009da0;">
              <path d="M2 3h6a4 4 0 0 1 4 4v14a3 3 0 0 0-3-3H2z"></path>
              <path d="M22 3h-6a4 4 0 0 0-4 4v14a3 3 0 0 1 3-3h7z"></path>
            </svg>
            <span class="menu-item">
              My articles
            </span>
          </a>
    		</li>
				<li class="list-group-item">
          <a href="#">
    				<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="color: #7d8250;">
              <path d="M6 2L3 6v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2V6l-3-4z"></path>
              <line x1="3" y1="6" x2="21" y2="6"></line><path d="M16 10a4 4 0 0 1-8 0"></path>
            </svg>
            <span class="menu-item">
              Market
            </span>
          </a>
    		</li>
				<li class="list-group-item">
          <a href="#">
    				<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="color:#6a7f9a;">
              <circle cx="9" cy="21" r="1"></circle>
              <circle cx="20" cy="21" r="1"></circle><path d="M1 1h4l2.68 13.39a2 2 0 0 0 2 1.61h9.72a2 2 0 0 0 2-1.61L23 6H6"></path>
            </svg>
            <span class="menu-item">
              My Products
            </span>
          </a>
    		</li>
        <li class="list-group-item">
          <a href="#">
    				<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="color:#8BC34A;">
              <circle cx="11" cy="11" r="8"></circle>
              <line x1="21" y1="21" x2="16.65" y2="16.65"></line>
            </svg>
            <span class="menu-item">
              Explore
            </span>
          </a>
    		</li>
				<li class="list-group-item">
          <a href="#">
    				<svg class="feather feather-file-text" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" aria-hidden="true" style="color:#8d73cc;">
              <path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path>
              <polyline points="14 2 14 8 20 8"></polyline>
              <line x1="16" y1="13" x2="8" y2="13"></line>
              <line x1="16" y1="17" x2="8" y2="17"></line>
              <polyline points="10 9 9 9 8 9"></polyline>
            </svg>
            <span class="menu-item">
              Popular Posts
            </span>
          </a>
    		</li>
    		<li class="list-group-item">
          <a href="#">
    				<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="color:#f25e4e;">
              <rect x="3" y="4" width="18" height="18" rx="2" ry="2"></rect>
              <line x1="16" y1="2" x2="16" y2="6"></line>
              <line x1="8" y1="2" x2="8" y2="6"></line>
              <line x1="3" y1="10" x2="21" y2="10"></line>
            </svg>
            <span class="menu-item">
              Events
            </span>
          </a>
    		</li>
				<li class="list-group-item">
    			<a href="#">
    				<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="color:#03A9F4;">
              <circle cx="12" cy="12" r="10"></circle>
              <line x1="22" y1="12" x2="18" y2="12"></line>
              <line x1="6" y1="12" x2="2" y2="12"></line>
              <line x1="12" y1="6" x2="12" y2="2"></line>
              <line x1="12" y1="22" x2="12" y2="18"></line>
            </svg>
            <span class="menu-item">
              Games
            </span>
          </a>
    		</li>
			</ul>
    </div>
    <div class="col-md-7">
      <div class="row">
        <div class="col-md-12">
          <form action="#" method="post" class="post publisher-box" id="publisher-box-focus">
          	<div class="panel panel-white post panel-shadow">
          		<div id="post-textarea" onclick="Wo_ShowPosInfo();">
          			<div class="wo_pub_txtara_combo">
  								<img class="post-avatar" src="{{asset('vendors/materialstyle/assets/img/demo/avatar1.jpg')}}">
          				<textarea name="postText" class="form-control postText ui-autocomplete-input" cols="10" placeholder="What's going on? #Hashtag.. @Mention.." dir="auto" autocomplete="off" opened="1" style="height: 75px;"></textarea>
          			</div>
          		</div>

          		<!--Publisher Box Footer-->
          		<div class="publisher-box-footer" style="display: block;">
          		<div class="row pub-footer-upper">
          			<!--Uploaded Image-->
          			<div class="pull-left">
          				<div class="poster-left-buttons">
          					<div class="input-group">
          						<span class="btn btn-file">
          							<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path fill="#4db3f6" d="M8.5,13.5L11,16.5L14.5,12L19,18H5M21,19V5C21,3.89 20.1,3 19,3H5A2,2 0 0,0 3,5V19A2,2 0 0,0 5,21H19A2,2 0 0,0 21,19Z"></path></svg>
          							<input type="file" id="publisher-photos" accept="image/x-png, image/gif, image/jpeg" name="postPhotos[]" multiple="multiple">
          						</span>
          					</div>
          				</div>
          			</div>

          			<!--Uploaded Video-->
    						<div class="pull-left">
          				<div class="poster-left-buttons">
          					<div class="input-group">
          						<span class="btn btn-file">
          							<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 512 512"><path fill="#71a257" d="M450.6 153.6c-3.3 0-6.5.9-9.3 2.7l-86.5 54.6c-2.5 1.6-4 4.3-4 7.2v76c0 2.9 1.5 5.6 4 7.2l86.5 54.6c2.8 1.7 6 2.7 9.3 2.7h20.8c4.8 0 8.6-3.8 8.6-8.5v-188c0-4.7-3.9-8.5-8.6-8.5h-20.8zM273.5 384h-190C55.2 384 32 360.8 32 332.6V179.4c0-28.3 23.2-51.4 51.4-51.4h190c28.3 0 51.4 23.2 51.4 51.4v153.1c.1 28.3-23 51.5-51.3 51.5z"></path></svg>
          							<input type="file" id="publisher-video" name="postVideo" accept="video/*">
          						</span>
          					</div>
          				</div>
          			</div>

          			<!--Add GIFs-->
    						<div class="pull-left gif-form">
          				<div class="poster-left-buttons">
          					<div class="input-group">
          						<span class="btn btn-file btngif" style="padding: 0px;">
          							<svg fill="#9d87d2" height="34" viewBox="0 0 24 24" width="34" xmlns="http://www.w3.org/2000/svg"><path d="M0 0h24v24H0z" fill="none"></path><defs> <path d="M24 24H0V0h24v24z" id="a"></path> </defs> <clipPath id="b"> <use overflow="visible" xlink:href="#a"></use> </clipPath> <path clip-path="url(#b)" d="M11.5 9H13v6h-1.5zM9 9H6c-.6 0-1 .5-1 1v4c0 .5.4 1 1 1h3c.6 0 1-.5 1-1v-2H8.5v1.5h-2v-3H10V10c0-.5-.4-1-1-1zm10 1.5V9h-4.5v6H16v-2h2v-1.5h-2v-1z"></path></svg>
          						</span>
          					</div>
          				</div>
          			</div>

          			<!--Record Audio-->
    						<div class="pull-left" id="recordPostAudioWrapper">
          				<div class="poster-left-buttons">
          					<div class="input-group">
          						<span class="btn btn-file" id="recordPostAudio" data-record="0">
          							<svg xmlns="http://www.w3.org/2000/svg" width="23" height="23" viewBox="0 0 24 24"><path fill="#ff3a55" d="M12,2A3,3 0 0,1 15,5V11A3,3 0 0,1 12,14A3,3 0 0,1 9,11V5A3,3 0 0,1 12,2M19,11C19,14.53 16.39,17.44 13,17.93V21H11V17.93C7.61,17.44 5,14.53 5,11H7A5,5 0 0,0 12,16A5,5 0 0,0 17,11H19Z"></path></svg>
          						</span>
          						<span id="postRecordingTime" class="hidden">00:00</span>
          					</div>
          				</div>
          			</div>

          			<!--Add Feeling-->
          			<div class="pull-left emo-form">
          				<div class="poster-left-buttons">
          					<div class="input-group">
          						<span class="btn btn-file">
          							<svg xmlns="http://www.w3.org/2000/svg" width="23" height="23" viewBox="0 0 24 24"><path fill="#f3c038" d="M12,2C6.486,2,2,6.486,2,12s4.486,10,10,10c5.514,0,10-4.486,10-10S17.514,2,12,2z M8.5,8C9.328,8,10,8.896,10,10	s-0.672,2-1.5,2S7,11.104,7,10S7.672,8,8.5,8z M12,18c-1.905,0-3.654-0.874-4.8-2.399l1.599-1.201C9.563,15.417,10.73,16,12,16	c1.27,0,2.436-0.583,3.2-1.601l1.6,1.201C15.653,17.126,13.904,18,12,18z M15.5,12c-0.828,0-1.5-0.896-1.5-2s0.672-2,1.5-2	S17,8.896,17,10S16.328,12,15.5,12z"></path></svg>
          						</span>
          					</div>
          				</div>
          			</div>

          			<!--Uploaded File-->
    						<div class="pull-left">
          				<div class="poster-left-buttons">
          					<div class="input-group">
          						<span class="btn btn-file">
          							<svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 512 512"><path fill="#6bcfef" d="M312 155h91c2.8 0 5-2.2 5-5 0-8.9-3.9-17.3-10.7-22.9L321 63.5c-5.8-4.8-13-7.4-20.6-7.4-4.1 0-7.4 3.3-7.4 7.4V136c0 10.5 8.5 19 19 19z"></path><path fill="#6bcfef" d="M267 136V56H136c-17.6 0-32 14.4-32 32v336c0 17.6 14.4 32 32 32h240c17.6 0 32-14.4 32-32V181h-96c-24.8 0-45-20.2-45-45z"></path></svg>
          							<input type="file" id="publisher-file" name="postFile">
          						</span>
          					</div>
          				</div>
          			</div>

          			<!--Create Product-->
    						<div class="pull-left">
          				<div class="poster-left-buttons">
          					<div class="input-group">
          						<a href="#" data-toggle="modal" data-target="#create-product-modal" data-backdrop="static" data-keyboard="false" class="btn btn-file">
          							<svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" viewBox="0 0 24 24"><path fill="#f07729" d="M3.572,5.572l4.506,10.813C8.233,16.757,8.597,17,9.001,17H18c0.417,0,0.79-0.259,0.937-0.648l3-8 c0.115-0.308,0.072-0.651-0.114-0.921C21.635,7.161,21.328,7,21,7H6.333L4.923,3.615C4.768,3.243,4.404,3,4,3H2v2h1L3.572,5.572z"></path><circle fill="#f07729" cx="10.5" cy="20.5" r="1.5"></circle><circle fill="#f07729" cx="16.438" cy="20.5" r="1.5"></circle></svg>
          						</a>
          					</div>
          				</div>
          			</div>

          			<!--Create Poll-->
          			<div class="pull-left">
          				<div class="poster-left-buttons">
          					<div class="input-group">
          						<span class="btn btn-file poll-form">
          							<svg xmlns="http://www.w3.org/2000/svg" width="26" height="26" viewBox="0 0 24 24"><path fill="#31a38c" d="M17,17H15V13H17M13,17H11V7H13M9,17H7V10H9M19,3H5C3.89,3 3,3.89 3,5V19A2,2 0 0,0 5,21H19A2,2 0 0,0 21,19V5C21,3.89 20.1,3 19,3Z"></path></svg>
          						</span>
          					</div>
          				</div>
          			</div>

          			<!--Add Location-->
          			<div class="pull-left">
          				<div class="poster-left-buttons">
          					<div class="input-group">
          						<span class="btn btn-file map-form">
          							<svg xmlns="http://www.w3.org/2000/svg" width="23" height="23" viewBox="0 0 24 24"><path fill="#f35369" d="M12,11.5A2.5,2.5 0 0,1 9.5,9A2.5,2.5 0 0,1 12,6.5A2.5,2.5 0 0,1 14.5,9A2.5,2.5 0 0,1 12,11.5M12,2A7,7 0 0,0 5,9C5,14.25 12,22 12,22C12,22 19,14.25 19,9A7,7 0 0,0 12,2Z"></path></svg>
          						</span>
          					</div>
          				</div>
          			</div>

          			<!--Uploaded Music-->
    						<div class="pull-left">
          				<div class="poster-left-buttons">
          					<div class="input-group">
          						<span class="btn btn-file">
          							<svg xmlns="http://www.w3.org/2000/svg" width="26" height="26" viewBox="0 0 24 24"><path fill="#4db3f6" d="M16,9V7H12V12.5C11.58,12.19 11.07,12 10.5,12A2.5,2.5 0 0,0 8,14.5A2.5,2.5 0 0,0 10.5,17A2.5,2.5 0 0,0 13,14.5V9H16M12,2A10,10 0 0,1 22,12A10,10 0 0,1 12,22A10,10 0 0,1 2,12A10,10 0 0,1 12,2Z"></path></svg>
          							<input type="file" id="publisher-music" name="postMusic" accept="audio/*">
          						</span>
          					</div>
          				</div>
          			</div>

          			<!--Add Bg Color-->
    						<div class="pull-left">
          				<div class="poster-left-buttons" onclick="Wo_ShowColors()">
          					<div class="input-group">
          						<span class="btn btn-file">
          							<svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 24 24"><path fill="#673ab7" d="M17.5,12A1.5,1.5 0 0,1 16,10.5A1.5,1.5 0 0,1 17.5,9A1.5,1.5 0 0,1 19,10.5A1.5,1.5 0 0,1 17.5,12M14.5,8A1.5,1.5 0 0,1 13,6.5A1.5,1.5 0 0,1 14.5,5A1.5,1.5 0 0,1 16,6.5A1.5,1.5 0 0,1 14.5,8M9.5,8A1.5,1.5 0 0,1 8,6.5A1.5,1.5 0 0,1 9.5,5A1.5,1.5 0 0,1 11,6.5A1.5,1.5 0 0,1 9.5,8M6.5,12A1.5,1.5 0 0,1 5,10.5A1.5,1.5 0 0,1 6.5,9A1.5,1.5 0 0,1 8,10.5A1.5,1.5 0 0,1 6.5,12M12,3A9,9 0 0,0 3,12A9,9 0 0,0 12,21A1.5,1.5 0 0,0 13.5,19.5C13.5,19.11 13.35,18.76 13.11,18.5C12.88,18.23 12.73,17.88 12.73,17.5A1.5,1.5 0 0,1 14.23,16H16A5,5 0 0,0 21,11C21,6.58 16.97,3 12,3Z"></path></svg>
          						</span>
          					</div>
          				</div>
          			</div>
                <div class="pull-right" style="    margin-left: 50px;">
                  <button type="button "class="btn btn-raised btn-twitter">
          					<span>Post</span>
          				</button>
      					</div>
			         </div>
          		 <div class="pub-footer-bottom">
          			<div class="">
          			</div>
          			<div class="clear"></div>
          		 </div>
              </div>
             </div>
          </form>
          <ul class="ms-timeline" style="margin-left:10px">
            <li class="ms-timeline-item wow materialUp">
              <div class="ms-timeline-date">
                <i class="ms-timeline-point"></i>
              </div>
              <div class="card card-primary">
                <div class="card-header" style="display:flex">
                  <img src="{{asset('vendors/materialstyle/assets/img/demo/avatar6.jpg')}}" style="width:50px;height:50px;border-radius:50%" class="ms-timeline-point-img">
                  <div class="" style="margin-left:20px;width:70%">
                    <h3 class="card-title">Farhan</h3>
                    <h3 class="card-title">02-01-2019 12:00:00</h3>
                  </div>
                  <div>
                    <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-sm mr-1 btn-circle-white color-danger">
                      <i class="zmdi zmdi-favorite" style="margin:0"></i>
                    </a>
                    <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-sm ml-1 btn-circle-white color-success">
                      <i class="zmdi zmdi-share" style="margin:0"></i>
                    </a>
                  </div>
                </div>
                <div class="card-block">
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Necessitatibus officiis autem magni et, nisi eveniet nulla magnam tenetur voluptatem dolore, assumenda delectus error porro animi architecto dolorum quod veniam nesciunt.
                  <hr>
                  <div>
                    <a href="javascript:void(0)" class="btn btn-primary btn-raised btn-block animate-icon" style="float: left;width: 150px;">Read More...
                    </a>
                    <div class="" style="float:right">
                      <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-sm ml-1 btn-circle-white color-warning">
                        <i class="zmdi zmdi-star"></i>
                      </a>
                      <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-sm ml-1 btn-circle-white color-warning">
                        <i class="zmdi zmdi-star"></i>
                      </a>
                      <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-sm ml-1 btn-circle-white color-warning">
                        <i class="zmdi zmdi-star"></i>
                      </a>
                      <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-sm ml-1 btn-circle-white">
                        <i class="zmdi zmdi-star" style="color: #e1dfdf"></i>
                      </a>
                      <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-sm ml-1 btn-circle-white">
                        <i class="zmdi zmdi-star" style="color: #e1dfdf"></i>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </li>
            <li class="ms-timeline-item wow materialUp">
              <div class="ms-timeline-date">
                <i class="ms-timeline-point bg-royal"></i>
              </div>
              <div class="card card-royal">
                <div class="card-header" style="display:flex">
                  <img src="{{asset('vendors/materialstyle/assets/img/demo/avatar6.jpg')}}" style="width:50px;height:50px;border-radius:50%" class="ms-timeline-point-img">
                  <div class="" style="margin-left:20px;width:70%">
                    <h3 class="card-title">Farhan</h3>
                    <h3 class="card-title">02-01-2019 12:00:00</h3>
                  </div>
                  <div>
                    <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-sm mr-1 btn-circle-white">
                      <i class="zmdi zmdi-favorite" style="color: #e1dfdf;margin:0"></i>
                    </a>
                    <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-sm ml-1 btn-circle-white color-success">
                      <i class="zmdi zmdi-share" style="margin:0"></i>
                    </a>
                  </div>
                </div>
                <div class="card-block">
                  <div class="row">
                    <div class="col-sm-4">
                      <img src="{{asset('vendors/materialstyle/assets/img/demo/office1.jpg')}}" alt="" class="img-fluid"> </div>
                    <div class="col-sm-8">
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum, praesentium, quam! Quia fugiat aperiam.</p>
                      <p>Perspiciatis soluta voluptate dolore officiis libero repellat cupiditate explicabo atque facere aliquam.</p>
                      <hr>
                      <div>
                        <a href="javascript:void(0)" class="btn btn-primary btn-raised btn-block animate-icon" style="float: left;width: 150px;">Read More...
                        </a>
                        <div class="" style="float:right">
                          <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-sm ml-1 btn-circle-white color-warning">
                            <i class="zmdi zmdi-star"></i>
                          </a>
                          <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-sm ml-1 btn-circle-white color-warning">
                            <i class="zmdi zmdi-star"></i>
                          </a>
                          <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-sm ml-1 btn-circle-white color-warning">
                            <i class="zmdi zmdi-star"></i>
                          </a>
                          <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-sm ml-1 btn-circle-white">
                            <i class="zmdi zmdi-star" style="color: #e1dfdf"></i>
                          </a>
                          <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-sm ml-1 btn-circle-white">
                            <i class="zmdi zmdi-star" style="color: #e1dfdf"></i>
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </li>
            <li class="ms-timeline-item wow materialUp">
              <div class="ms-timeline-date">
                <i class="ms-timeline-point bg-warning"></i>
              </div>
              <div class="card card-warning">
                <div class="card-header" style="display:flex">
                  <img src="{{asset('vendors/materialstyle/assets/img/demo/avatar6.jpg')}}" style="width:50px;height:50px;border-radius:50%" class="ms-timeline-point-img">
                  <div class="" style="margin-left:20px;width:70%">
                    <h3 class="card-title">Farhan</h3>
                    <h3 class="card-title">02-01-2019 12:00:00</h3>
                  </div>
                  <div>
                    <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-sm mr-1 btn-circle-white">
                      <i class="zmdi zmdi-favorite" style="color: #e1dfdf;margin:0"></i>
                    </a>
                    <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-sm ml-1 btn-circle-white color-success">
                      <i class="zmdi zmdi-share" style="margin:0"></i>
                    </a>
                  </div>
                </div>
                <div class="card-block">
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nam, nulla recusandae blanditiis architecto soluta culpa obcaecati quis earum atque consequuntur.</p>
                  <div class="row">
                    <div class="col-sm-4">
                      <img src="{{asset('vendors/materialstyle/assets/img/demo/office2.jpg')}}" alt="" class="img-fluid"> </div>
                    <div class="col-sm-4">
                      <img src="{{asset('vendors/materialstyle/assets/img/demo/office3.jpg')}}" alt="" class="img-fluid"> </div>
                    <div class="col-sm-4">
                      <img src="{{asset('vendors/materialstyle/assets/img/demo/office4.jpg')}}" alt="" class="img-fluid"> </div>
                  </div>
                  <br>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis, ipsum voluptates eius placeat dolorum reprehenderit ducimus accusamus magni aspernatur at dolore assumenda quae suscipit enim veritatis obcaecati molestias laudantium
                    maxime!</p>
                  <hr>
                  <div>
                    <a href="javascript:void(0)" class="btn btn-primary btn-raised btn-block animate-icon" style="float: left;width: 180px;">Read More...
                    </a>
                    <div class="" style="float:right">
                      <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-sm ml-1 btn-circle-white color-warning">
                        <i class="zmdi zmdi-star"></i>
                      </a>
                      <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-sm ml-1 btn-circle-white color-warning">
                        <i class="zmdi zmdi-star"></i>
                      </a>
                      <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-sm ml-1 btn-circle-white color-warning">
                        <i class="zmdi zmdi-star"></i>
                      </a>
                      <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-sm ml-1 btn-circle-white">
                        <i class="zmdi zmdi-star" style="color: #e1dfdf"></i>
                      </a>
                      <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-sm ml-1 btn-circle-white">
                        <i class="zmdi zmdi-star" style="color: #e1dfdf"></i>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </div>
    <div class="col-lg-3 d-none d-lg-block">
      <div class="card animated fadeInUp animation-delay-7">
        <div class="ms-hero-bg-info ms-hero-img-mountain">
          <h3 class="color-white index-1 text-center no-m pt-4">Victoria Smith</h3>
          <div class="color-medium index-1 text-center np-m">@vic_smith</div>
          <img src="{{asset('vendors/materialstyle/assets/img/demo/avatar1.jpg')}}" alt="..." class="img-avatar-circle"> </div>
        <div class="card-block pt-4 text-center">
          <hr>
          <ul class="wo_user_side_info" style="list-style:none">
        		<li>
        			<a class="menu_list" href="#" data-ajax="?link1=timeline&amp;u=kurakara13">
        				<span class="split-link"><b>posts</b></span>
        				<span id="user_post_count">0</span>
        			</a>
        		</li>

        							<li>
        			<a class="menu_list" href="#" data-ajax="?link1=timeline&amp;u=kurakara13&amp;type=following">
        				<span class="split-link"><b>Following</b></span>
        				<span>19</span>
        			</a>
        		</li>
        		<li>
        			<a class="menu_list" href="#" data-ajax="?link1=timeline&amp;u=kurakara13&amp;type=followers">
        				<span class="split-link"><b>Followers</b></span>
        				<span>0</span>
        			</a>
        		</li>
      		</ul>
          <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs mt-1 mr-1 no-mr-md btn-google">
            <i class="zmdi zmdi-google"></i>
          </a>
          <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs mt-1 mr-1 no-mr-md btn-facebook">
            <i class="zmdi zmdi-facebook"></i>
          </a>
          <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs mt-1 mr-1 no-mr-md btn-twitter">
            <i class="zmdi zmdi-twitter"></i>
          </a>
          <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs mt-1 mr-1 no-mr-md btn-instagram">
            <i class="zmdi zmdi-instagram"></i>
          </a>
        </div>
      </div>
      <div class="card card-primary animated fadeInUp animation-delay-7">
        <div class="card-header">
          <h3 class="card-title">
            <i class="zmdi zmdi-apps"></i> Navigation</h3>
        </div>
        <div class="card-tabs">
          <ul class="nav nav-tabs nav-tabs-transparent indicator-primary nav-tabs-full nav-tabs-4" role="tablist">
            <li class="nav-item current">
              <a href="#favorite" aria-controls="favorite" role="tab" data-toggle="tab" class="nav-link withoutripple active" aria-selected="true">
                <i class="no-mr zmdi zmdi-star"></i>
              </a>
            </li>
            <li class="nav-item">
              <a href="#categories" aria-controls="categories" role="tab" data-toggle="tab" class="nav-link withoutripple" aria-selected="false">
                <i class="no-mr zmdi zmdi-folder"></i>
              </a>
            </li>
            <li class="nav-item">
              <a href="#archives" aria-controls="archives" role="tab" data-toggle="tab" class="nav-link withoutripple" aria-selected="false">
                <i class="no-mr zmdi zmdi-time"></i>
              </a>
            </li>
            <li class="nav-item">
              <a href="#tags" aria-controls="tags" role="tab" data-toggle="tab" class="nav-link withoutripple" aria-selected="false">
                <i class="no-mr zmdi zmdi-tag-more"></i>
              </a>
            </li>
          <span id="i2" class="ms-tabs-indicator" style="left: 0px; width: 87.5px;"></span></ul>
        </div>
        <div class="tab-content">
          <div role="tabpanel" class="tab-pane fade active show" id="favorite">
            <div class="card-block">
              <div class="ms-media-list">
                <div class="media mb-2">
                  <div class="media-left media-middle">
                    <a href="#">
                      <img class="d-flex mr-3 media-object media-object-circle" src="{{asset('vendors/materialstyle/assets/img/demo/p75.jpg')}}" alt="..."> </a>
                  </div>
                  <div class="media-body">
                    <a href="javascript:void(0)" class="media-heading">Lorem ipsum dolor sit amet in consectetur adipisicing</a>
                    <div class="media-footer text-small">
                      <span class="mr-1">
                        <i class="zmdi zmdi-time color-info mr-05"></i> August 18, 2016</span>
                      <span>
                        <i class="zmdi zmdi-folder-outline color-success mr-05"></i>
                        <a href="javascript:void(0)">Design</a>
                      </span>
                    </div>
                  </div>
                </div>
                <div class="media mb-2">
                  <div class="media-left media-middle">
                    <a href="#">
                      <img class="d-flex mr-3 media-object media-object-circle" src="{{asset('vendors/materialstyle/assets/img/demo/p75.jpg')}}" alt="..."> </a>
                  </div>
                  <div class="media-body">
                    <a href="javascript:void(0)" class="media-heading">Nemo enim ipsam voluptatem quia voluptas sit aspernatur</a>
                    <div class="media-footer text-small">
                      <span class="mr-1">
                        <i class="zmdi zmdi-time color-info mr-05"></i> August 18, 2016</span>
                      <span>
                        <i class="zmdi zmdi-folder-outline color-danger mr-05"></i>
                        <a href="javascript:void(0)">Productivity</a>
                      </span>
                    </div>
                  </div>
                </div>
                <div class="media">
                  <div class="media-left media-middle">
                    <a href="#">
                      <img class="d-flex mr-3 media-object media-object-circle" src="{{asset('vendors/materialstyle/assets/img/demo/p75.jpg')}}" alt="..."> </a>
                  </div>
                  <div class="media-body">
                    <a href="javascript:void(0)" class="media-heading">inventore veritatis et vitae quasi architecto beatae </a>
                    <div class="media-footer text-small">
                      <span class="mr-1">
                        <i class="zmdi zmdi-time color-info mr-05"></i> August 18, 2016</span>
                      <span>
                        <i class="zmdi zmdi-folder-outline color-royal-light mr-05"></i>
                        <a href="javascript:void(0)">Resources</a>
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div role="tabpanel" class="tab-pane fade" id="categories">
            <div class="list-group">
              <a href="javascript:void(0)" class="list-group-item list-group-item-action withripple">
                <i class=" color-info zmdi zmdi-cocktail"></i>Design
                <span class="ml-auto badge-pill badge-pill-info">25</span>
              </a>
              <a href="javascript:void(0)" class="list-group-item list-group-item-action withripple">
                <i class=" color-warning zmdi zmdi-collection-image"></i> Graphics
                <span class="ml-auto badge-pill badge-pill-warning">14</span>
              </a>
              <a href="javascript:void(0)" class="list-group-item list-group-item-action withripple">
                <i class=" color-danger zmdi zmdi-case-check"></i> Productivity
                <span class="ml-auto badge-pill badge-pill-danger">7</span>
              </a>
              <a href="javascript:void(0)" class="list-group-item list-group-item-action withripple">
                <i class=" color-royal zmdi zmdi-folder-star-alt"></i>Resources
                <span class="ml-auto badge-pill badge-pill-royal">67</span>
              </a>
              <a href="javascript:void(0)" class="list-group-item list-group-item-action withripple">
                <i class=" color-success zmdi zmdi-play-circle-outline"></i>Multimedia
                <span class="ml-auto badge-pill badge-pill-success">32</span>
              </a>
            </div>
          </div>
          <div role="tabpanel" class="tab-pane fade" id="archives">
            <div class="list-group">
              <a href="javascript:void(0)" class="list-group-item list-group-item-action withripple">
                <i class="zmdi zmdi-calendar"></i> January 2016
                <span class="ml-auto badge-pill">25</span>
              </a>
              <a href="javascript:void(0)" class="list-group-item list-group-item-action withripple">
                <i class="zmdi zmdi-calendar"></i> February 2016
                <span class="ml-auto badge-pill">14</span>
              </a>
              <a href="javascript:void(0)" class="list-group-item list-group-item-action withripple">
                <i class="zmdi zmdi-calendar"></i> March 2016
                <span class="ml-auto badge-pill">9</span>
              </a>
              <a href="javascript:void(0)" class="list-group-item list-group-item-action withripple">
                <i class="zmdi zmdi-calendar"></i> April 2016
                <span class="ml-auto badge-pill">12</span>
              </a>
              <a href="javascript:void(0)" class="list-group-item list-group-item-action withripple">
                <i class="zmdi zmdi-calendar"></i> June 2016
                <span class="ml-auto badge-pill">65</span>
              </a>
            </div>
          </div>
          <div role="tabpanel" class="tab-pane fade" id="tags">
            <div class="card-block text-center">
              <a href="javascript:void(0)" class="ms-tag ms-tag-primary">Design</a>
              <a href="javascript:void(0)" class="ms-tag ms-tag-primary">Productivity</a>
              <a href="javascript:void(0)" class="ms-tag ms-tag-primary">Web</a>
              <a href="javascript:void(0)" class="ms-tag ms-tag-primary">Resources</a>
              <a href="javascript:void(0)" class="ms-tag ms-tag-primary">Multimedia</a>
              <a href="javascript:void(0)" class="ms-tag ms-tag-primary">HTML5</a>
              <a href="javascript:void(0)" class="ms-tag ms-tag-primary">CSS3</a>
              <a href="javascript:void(0)" class="ms-tag ms-tag-primary">Javascript</a>
              <a href="javascript:void(0)" class="ms-tag ms-tag-primary">Jquery</a>
              <a href="javascript:void(0)" class="ms-tag ms-tag-primary">Bootstrap</a>
              <a href="javascript:void(0)" class="ms-tag ms-tag-primary">Angular</a>
              <a href="javascript:void(0)" class="ms-tag ms-tag-primary">Gulp</a>
              <a href="javascript:void(0)" class="ms-tag ms-tag-primary">Atom</a>
              <a href="javascript:void(0)" class="ms-tag ms-tag-primary">Fonts</a>
              <a href="javascript:void(0)" class="ms-tag ms-tag-primary">Pictures</a>
              <a href="javascript:void(0)" class="ms-tag ms-tag-primary">Developers</a>
              <a href="javascript:void(0)" class="ms-tag ms-tag-primary">Code</a>
              <a href="javascript:void(0)" class="ms-tag ms-tag-primary">SASS</a>
              <a href="javascript:void(0)" class="ms-tag ms-tag-primary">Less</a>
            </div>
          </div>
        </div>
      </div>
      <div class="card card-success animated fadeInUp animation-delay-7">
        <div class="card-header">
          <h3 class="card-title">
            <i class="zmdi zmdi-play-circle-outline"></i> Feature Video</h3>
        </div>
        <div tabindex="0" class="plyr plyr--vimeo plyr--video"><div class="plyr__video-wrapper plyr__video-embed"><div id="vimeo-95"></div></div></div>
      </div>
      <div class="card card-primary animated fadeInUp animation-delay-7">
        <div class="card-header">
          <h3 class="card-title">
            <i class="zmdi zmdi-widgets"></i> Text Widget</h3>
        </div>
        <div class="card-block">
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat ipsam non eaque est architecto doloribus, labore nesciunt laudantium, ex id ea, cum facilis similique tenetur fugit nemo id minima possimus.</p>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('script')
<script>
$(document).ready(function(){

  var highestBox = 0;
  var highestBoxTitle = 0;
  var highestBoxHobby = 0;

  // Select and loop the container element of the elements you want to equalise
  $('.content .col-lg-4').each(function(){
    if($(this).find('.title').height() > highestBoxTitle) {
      highestBoxTitle = $(this).find('.title').height();
    }
  });

  $('.title').height(highestBoxTitle);

  $('.content .col-lg-4').each(function(){
    if($(this).find('.desc').height() > highestBox) {
      highestBox = $(this).find('.desc').height();
    }
  });

  $('.desc').height(highestBox);


  $('.content .col-lg-4').each(function(){
    if($(this).find('.hobby').height() > highestBoxHobby) {
      highestBoxHobby = $(this).find('.hobby').height();
    }
  });

  $('.hobby').height(highestBoxHobby);


});
</script>
<style>
.wo_pub_txtara_combo .form-group{
  margin:0;
}
@stop
