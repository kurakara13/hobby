@extends('layouts.app')

@section('kategori')
<li class="nav-item dropdown">
  <a href="#" class="nav-link dropdown-toggle animated fadeIn animation-delay-7" data-toggle="dropdown" data-hover="dropdown" role="button" aria-haspopup="true" aria-expanded="false" data-name="blog">Kategori
    <i class="zmdi zmdi-chevron-down"></i>
  </a>
  <ul class="dropdown-menu">
    @foreach($hobby as $item)
    <li>
      <a class="dropdown-item" href="#">
        <i class="zmdi zmdi-view-compact"></i> {{$item->nama}}</a>
    </li>
    @endforeach
  </ul>
</li>
@stop

@section('content')

<div class="ms-hero-page ms-hero-img-keyboard ms-hero-bg-primary mb-6">
  <div class="container">
    <div class="text-center">
      <span class="ms-logo ms-logo-lg ms-logo-white center-block mb-2 mt-2 animated zoomInDown animation-delay-5">H</span>
      <h1 class="no-m ms-site-title color-white center-block ms-site-title-lg mt-2 animated zoomInDown animation-delay-5">Hobby Your Style</h1>
      <p class="lead lead-lg color-white text-center center-block mt-2 mw-800 fw-300 animated fadeInUp animation-delay-7">Follow our blog and discover the latest content and trends in the market. In addition you will discover the best
        <span class="colorStar">tricks</span> and
        <span class="colorStar">discounts</span>.</p>
    </div>
  </div>
</div>
<div class="container">
  <div class="row masonry-container">
    @foreach($hobby as $item)
    <div class="col-lg-4 col-md-6 masonry-item wow fadeInUp animation-delay-2">
      <article class="card card-info mb-4 wow materialUp animation-delay-5">
        <figure class="ms-thumbnail ms-thumbnail-left">
          <img src="{{asset('vendors/materialstyle/assets/img/demo/'.$item->gambar)}}" style="height:225px" alt="" class="img-fluid">
          <figcaption class="ms-thumbnail-caption text-center">
            <div class="ms-thumbnail-caption-content">
              <h3 class="ms-thumbnail-caption-title">{{$item->nama}}</h3>
              <div class="mt-2">
                <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-sm mr-1 btn-circle-white color-danger">
                  <i class="zmdi zmdi-favorite"></i>
                </a>
                <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-sm ml-1 btn-circle-white color-success">
                  <i class="zmdi zmdi-share"></i>
                </a>
              </div>
            </div>
          </figcaption>
        </figure>
        <div class="card-block">
          <h2>
            <a href=javascript:void(0)>{{$item->nama}}</a>
          </h2>
        </div>
      </article>
    </div>
    @endforeach
  </div>
</div>
@endsection
