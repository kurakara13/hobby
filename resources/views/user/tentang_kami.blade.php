@extends('layouts.app')

@section('style')
<style>

</style>
@stop

@section('content')
<header class="ms-hero bg-primary-dark pb-4 pt-4 mb-6">
  <div class="container">
    <div class="text-center">
      <span class="ms-logo ms-logo-lg ms-logo-white center-block mb-2 mt-2 animated zoomInDown animation-delay-5">H</span>
      <h1 class="no-m ms-site-title color-white center-block ms-site-title-lg mt-2 animated zoomInDown animation-delay-5">My
        <span>Hobby</span>
      </h1>
    </div>
  </div>
</header>
<div class="container">
  <div class="card wow fadeInUp">
    <div class="card-block card-block-big">
      <h2 class="text-center fw-500">Tentang Kami</h2>
      <p class="lead text-center center-block mw-800">Discover our projects and the rigorous process of creation. Our principles are creativity, design, experience and knowledge.</p>
      <div class="row mt-4">
        <div class="col-lg-6 order-lg-2 text-justify">
          <h3 class="color-primary">Know us</h3>
          <p class="dropcaps">Fugit recusandae dolores, hic suscipit necessitatibus nobis culpa, quidem inventore molestias autem eligendi provident, natus eius reprehenderit repudiandae velit! Aut qui deleniti autem nobis laborum sapiente tempore, facere cumque tempora
            laudantium porro illo officiis culpa labore magnam itaque eos quia asperiores officia voluptas voluptatem est molestias architecto magni corporis. Cupiditate, voluptates, saepe.</p>
          <p>Explicabo numquam quam amet vel laboriosam odio eaque quos esse cumque, nam deserunt ducimus odit libero asperiores necessitatibus, soluta corporis dignissimos. Delectus temporibus iusto debitis obcaecati accusantium, dolorum ad doloremque
            maiores optio ut magni praesentium cupiditate dolore.</p>
          <p>Nulla delectus tempora ab ipsum molestias dolorem explicabo nihil. Officia quia iusto sint, iure nemo vitae aperiam, corrupti aliquid fugit, qui dolore voluptatibus eos quisquam obcaecati, omnis iste illum optio. Maxime atque hic vero.
            Doloribus eius sit laborum fugiat necessitatibus tempora facilis facere cumque, ipsum nam, temporibus, perferendis non ratione!</p>
        </div>
        <div class="col-lg-6 order-lg-1">
          <h3 class="color-primary">Our Principles</h3>
          <ol class="service-list list-unstyled">
            <li>Lorem ipsum dolor sit amet,
              <strong>consectetur adipisicing elit</strong>. Nihil suscipit cupiditate expedita hic earum vero sint, recusandae itaque, rem distinctio.</li>
            <li>Totam porro sit, obcaecati quos quae iure tenetur, soluta voluptatem sapiente rerum ipsam delectus corporis voluptates voluptate, nulla mollitia pariatur.</li>
            <li>Enim quas nesciunt sequi odit, ut
              <a href="#">quisquam vitae commodi</a> animi placeat nihil saepe magnam aliquam, vero harum quae doloribus aut nostrum veniam alias!</li>
            <li>Lorem ipsum dolor sit amet,
              <strong>consectetur adipisicing elit</strong>. Nihil suscipit cupiditate expedita hic earum vero sint, recusandae itaque, rem distinctio.</li>
          </ol>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('script')
<script>

</script>
@stop
