@extends('layouts.app')

@section('kategori')
<li class="nav-item dropdown">
  <a href="#" class="nav-link dropdown-toggle animated fadeIn animation-delay-7" data-toggle="dropdown" data-hover="dropdown" role="button" aria-haspopup="true" aria-expanded="false" data-name="blog">Kategori
    <i class="zmdi zmdi-chevron-down"></i>
  </a>
  <ul class="dropdown-menu">
    @foreach($hobby as $item)
    <li>
      <a class="dropdown-item" href="#">
        <i class="zmdi zmdi-view-compact"></i> {{$item->nama}}</a>
    </li>
    @endforeach
  </ul>
</li>
@stop

@section('content')
<div class="ms-hero-page ms-hero-img-keyboard ms-hero-bg-primary mb-6">
  <div class="container">
    <div class="text-center">
      <span class="ms-logo ms-logo-lg ms-logo-white center-block mb-2 mt-2 animated zoomInDown animation-delay-5">H</span>
      <h1 class="no-m ms-site-title color-white center-block ms-site-title-lg mt-2 animated zoomInDown animation-delay-5">Recomended Content</h1>
      <p class="lead lead-lg color-white text-center center-block mt-2 mw-800 fw-300 animated fadeInUp animation-delay-7">Follow our blog and discover the latest content and trends in the market. In addition you will discover the best
        <span class="colorStar">tricks</span> and
        <span class="colorStar">discounts</span>.</p>
    </div>
  </div>
</div>
<div class="container">
  <div class="row">
    <div class="col-lg-8">
      @foreach($contentBased as $index => $item)
      <article class="card wow fadeInLeft animation-delay-5 mb-4">
        <div class="card-block">
          <div class="row">
            <div class="col-xl-6">
              <img src="{{asset('images/blog-02.jpg')}}" alt="" class="img-fluid mb-4"> </div>
            <div class="col-xl-6">
              <h3 class="no-mt">
                <a href="javascript:void(0)">{{$content[$index]->judul}}</a>
              </h3>
              <p class="mb-4">{{$content[$index]->deskripsi}}</p>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-8">
              Akurasi : <a href="javascript:void(0)">{{$item}}</a>
              <br>
              Hobby :
              @foreach(explode(',',$content[$index]->hobby) as $hobbyValue)
                {{$hobbyValue}} |
              @endforeach
            </div>
            <div class="col-lg-4 text-right">
              <a href="javascript:void(0)" class="btn btn-primary btn-raised btn-block animate-icon">Read More
                <i class="ml-1 no-mr zmdi zmdi-long-arrow-right"></i>
              </a>
            </div>
          </div>
        </div>
      </article>
      @endforeach
      <nav aria-label="Page navigation">
        <ul class="pagination pagination-plain">
          <li class="page-item">
            <a class="page-link" href="javascript:void(0)" aria-label="Previous">
              <span aria-hidden="true">&laquo;</span>
            </a>
          </li>
          <li class="page-item active">
            <a class="page-link" href="javascript:void(0)">1</a>
          </li>
          <li class="page-item">
            <a class="page-link" href="javascript:void(0)">2</a>
          </li>
          <li class="page-item">
            <a class="page-link" href="javascript:void(0)">3</a>
          </li>
          <li class="page-item">
            <a class="page-link" href="javascript:void(0)">4</a>
          </li>
          <li class="page-item">
            <a class="page-link" href="javascript:void(0)" aria-label="Next">
              <span aria-hidden="true">&raquo;</span>
            </a>
          </li>
        </ul>
      </nav>
    </div>
    <div class="col-lg-4">
      <div class="card animated fadeInUp animation-delay-7">
        <div class="ms-hero-bg-info ms-hero-img-mountain">
          <h3 class="color-white index-1 text-center no-m pt-4">Victoria Smith</h3>
          <div class="color-medium index-1 text-center np-m">@vic_smith</div>
          <img src="assets/img/demo/avatar1.jpg" alt="..." class="img-avatar-circle"> </div>
        <div class="card-block pt-4 text-center">
          <h3 class="color-primary">Ketegori User</h3>
          <form class="form-horizontal" method="post" action="{{route('user.content.based.post')}}" autocomplete="off">
            @csrf
            <fieldset>
              <div class="form-group row justify-content-end">
                <div class="col-lg-12">
                  <select id="select222" multiple="multiple" name="hobby[]" class="selectpicker form-control" data-dropup-auto="false">
                    @foreach($hobby as $item)
                    <option>{{$item->nama}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="form-group row justify-content-end">
                <div class="col-lg-10">
                  <button type="submit" class="btn btn-raised btn-primary">Submit</button>
                </div>
              </div>
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
