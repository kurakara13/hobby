@extends('layouts.app')

@section('style')
<style>
@media (min-width: 992px){
  .page-dashboard {
      padding: 100px 0 100px 0;
  }

  .sidebar-dashboard {
      max-height: max-content;
  }

  .header-page {
    margin-bottom: 50px;
  }

  .btn-submit {
    padding: 7px;
  }
}
</style>
@stop

@section('content')
<div class="page-dashboard">
  <div class="container">
  	<div class="breadcrumbs-container breadcrumbs-top">
  	  <ul class="breadcrumbs min-list inline-list">
  	    <li class="breadcrumbs__item">
  	      <a href="index.html" class="breadcrumbs__link">
  	        <span class="breadcrumbs__title">Home</span>
  	      </a>
  	    </li>

  	    <li class="breadcrumbs__item">
  	      <a href="#" class="breadcrumbs__link">
  	        <span class="breadcrumbs__title">Dashboard</span>
  	      </a>
  	    </li>

  	    <li class="breadcrumbs__item">
  	      <span class="breadcrumbs__page c-gray">Komunitas</span>
  	    </li>
  	  </ul><!-- .breadcrumbs -->
  	</div>
  	<h1 class="header-page">Komunitas</h1>
  	<div class="row">
  		<div class=" col-lg-4">
  			@include('layouts.partials')
  		</div>
  		<!-- end-col -->
  		<div class="col-lg-8">
        <div class="content-dashboard_wishlist dashboard-bg dashboard-pd">
          <div class="title-review">
            <h2>KOMUNITAS</h2>
          </div>
          <div class="list-my-booking">
            <div class="item-booking">
              <a href="{{route('user.komunitas.create')}}" style="float:right"><button class="btn-submit submit-footer btn-radius">Tambah Komunitas</button></a>
              <h3 class="title-my-booking">List Komunitas Anda</h3>
              <div class="list-items__wrapp">
                @forelse($komunitas as $index => $item)
                <div class="list-items__wrapper d-lg-flex">
                  <div class="wishlist_wrapper d-flex">
                    <div class="wishlist_thubmail">
                      <!-- <p class="review__start img-sale" style="padding:6px;padding-left: 9px;width: 35px;height: 35px;">4.5</p> -->
                      <a href="#"><img src="{{asset('images/contents/'.$item->gambar)}}" style="width:80px;height:80px;" alt="tours">
                        <!-- <span class="review__start img-sale" style="">4.5</span> -->
                      </a>
                    </div>
                    <div class="wishlist_detail">
                      <h3> <a href="tour-single.html" class="wishlist_title">{{$item->judul}}</a></h3>
                      <ul class="inline-list">
                        @foreach(explode(',', $item->hobi) as $hobby)
                        <li class="item-detail-descrip">{{$hobby}} |</li>
                        @endforeach
                      </ul>
                    </div>
                  </div>
                  <div class="wishlist_action" style="    padding-right: 10px;">
                    <div class="d-lg-flex justify-content-between" style="line-height:3">
                      <a class="wishlist-link" href="#" style="font-size:25px"><i class="fa fa-trash"></i></a>
                      <a class="ion-ios" href="#" style="font-size:25px"><i class="fa fa-edit"></i></a>
                    </div>
                  </div>
                </div>
                @empty
                <div>
                  <h3 style="text-align:center">Tidak Ada Komunitas Yang Anda Buat</h3>
                </div>
                @endforelse
              </div>
            </div>
            <div class="pagination_tours-reviews center">
              <ul class="min-list inline-list">
                <li><a href="#" class="ion-ios-arrow-back"></a></li>
                <li class="active"><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">...</a></li>
                <li><a href="#">15</a></li>
                <li><a href="#" class="ion-ios-arrow-forward"></a></li>
              </ul>
            </div>
          </div>
        </div>
  		</div>
  		<!-- end-col -->
  	</div>
  	<!-- end-row -->
  </div>
  <!-- end-container -->
</div>
@endsection

@section('script')
<script>
</script>
@stop
