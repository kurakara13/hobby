@extends('layouts.app')

@section('style')
<style>
.no-star{
  color: #e1dfdf;
}
</style>
@stop

@section('content')
<div class="container">
  <div class="row">
    <div class="col-lg-8">
      <div class="card animated fadeInLeftTiny animation-delay-5">
        <div class="card-block card-block-big">
          <h1 class="no-mt">Repellat sequi id reiciendis ipsum placeat accusantium debitis</h1>
          <div class="mb-4">
            <img src="{{asset('vendors/materialstyle/assets/img/demo/avatar50.jpg')}}" alt="..." class="img-circle mr-1"> by
            <a href="javascript:void(0)">Victoria</a> in
            <a href="javascript:void(0)" class="ms-tag ms-tag-info">Design</a>
            <span class="ml-1 d-none d-sm-inline">
              <i class="zmdi zmdi-time mr-05 color-info"></i>
              <span class="color-medium-dark">April 15, 2015</span>
            </span>
            <span class="ml-1">
              <i class="zmdi zmdi-comments color-royal mr-05"></i> 25</span>
          </div>
          <img src="{{asset('vendors/materialstyle/assets/img/demo/post4.jpg')}}" alt="" class="img-fluid mb-4">
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Optio delectus
            <strong>eveniet quas exercitationem explicabo</strong> dolore quibusdam voluptatum saepe consequatur nostrum ducimus, repellendus molestiae natus ipsa, suscipit nihil nisi dolorem sit, sed earum enim quos deserunt. Ipsam, deleniti magni ullam
            incidunt dolores sit libero rerum reprehenderit velit? Id
            <a href="#">veniam cum mollitia</a> minima porro quae dicta deserunt eligendi excepturi totam. Voluptatem illum rem, exercitationem. Neque illo magnam deserunt perspiciatis explicabo tempora culpa molestias, pariatur, nostrum a, reprehenderit.</p>
          <blockquote class="blockquote blockquote-big">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante ultricies nisi vel augue quam semper libero.</p>
            <footer>Someone famous in
              <cite title="Source Title">Source Title</cite>
            </footer>
          </blockquote>
          <p>Nihil autem, consectetur mollitia est neque aperiam expedita reprehenderit quia in atque. Perferendis nulla asperiores dolores veritatis blanditiis. Quia architecto distinctio natus ad asperiores explicabo cum aliquid officiis, similique,
            quidem voluptatem odio illum possimus mollitia. Tempore optio soluta iusto maxime, cupiditate aut, assumenda facilis atque enim dicta, esse mollitia itaque voluptatem doloremque doloribus similique ad consequuntur asperiores. Ducimus
            reiciendis voluptatum fugit praesentium, labore nobis unde quia ullam quidem dolore et cumque. Est ratione similique rem.</p>
          <h3 class="color-primary">Cupiditate necessitatibus inventore facilis</h3>
          <p>Deleniti, ut. Reiciendis cumque, autem quas, est rem nam inventore numquam, voluptatum eius vero repellat aliquam.
            <strong>Ut incidunt ab veniam fugiat beatae sequi tempora fuga</strong>, impedit unde inventore dignissimos! Beatae omnis amet tempora similique dolores error officia aliquam et cupiditate necessitatibus inventore facilis natus optio adipisci
            doloribus nulla voluptate sint fuga eum, dolorum aperiam quasi. Earum nemo ab ratione quia placeat voluptatem eos magni officia illo fugit consequatur, nihil aliquam nostrum esse officiis! Provident, a.</p>
          <img src="{{asset('vendors/materialstyle/assets/img/demo/avatar2.jpg')}}"
            alt="" class="imgborder ml-2 mb-2 pull-right">
          <p>Dolor animi enim tempora. Nostrum velit iure aut dicta non at, rem ea ipsum quia! Eum magni earum accusamus dolores, accusantium, necessitatibus debitis natus expedita iure. Modi repellendus excepturi hic veniam et autem asperiores optio
            non dolore corrupti distinctio earum perferendis porro velit aperiam exercitationem explicabo dolores quaerat inventore sapiente vitae harum, doloribus. Nulla, inventore. Non totam sed ducimus iusto, excepturi consectetur, facere maxime
            quisquam, vitae eius iste ullam veritatis dolor enim repudiandae reprehenderit asperiores!</p>
            <hr>
            <div class="comments-page">
              <div class="comment-list">
                <div class="comment-item-post" style="margin-bottom:15px">
                  <div class="e-txt" style="line-height: 20px;width:100%;display:flex;margin-bottom:10px">
                    <img src="{{asset('vendors/materialstyle/assets/img/demo/avatar1.jpg')}}" alt="..." class="img-avatar-circle" style="margin:0;width:40px;height:40px;transform: none;margin-right: 10px;border: solid 3px #fff;">
                    <div class="" style="line-height: 17px;">
                      <small class="card-title">Farhan</small><br>
                      @for($i=0;$i<5;$i++)
                      <a href="javascript:void(0)" class="color-warning">
                        <i class="zmdi zmdi-star no-star" style="font-size:20px;"></i>
                      </a>
                      @endfor
                    </div>
                  </div>
                  <div class="description" style="background: whitesmoke;padding: 10px;border-radius: 10px;">
                    <textarea class="form-control" placeholder="Write Something..."></textarea>
                  </div>
                </div>
                <hr>
                <div class="comment-item" style="display:flex;margin-bottom: 15px;">
                  <img src="{{asset('vendors/materialstyle/assets/img/demo/avatar1.jpg')}}" alt="..." class="img-avatar-circle" style="margin:0;width:40px;height:40px;transform: none;    position: relative;top: 0px;    right: -14px;border: solid 3px #fff;">
                  <div class="e-txt" style="margin-left:30px;line-height: 20px;width:100%;">
                    <div class="" style="line-height: 17px;">
                      <small class="card-title">Farhan</small><br>
                      @for($i=0;$i<5;$i++)
                      <a href="javascript:void(0)" class="color-warning">
                        <i class="zmdi zmdi-star {{$i < 2 ? '' : 'no-star'}}" style="font-size:20px;"></i>
                      </a>
                      @endfor
                    </div>
                    <div class="description" style="background: whitesmoke;padding: 10px;border-radius: 10px;">
                      <p>where place of that?</p>
                    </div>
                    <div class="ic" style="width:100%;display:flex">
                      <div class="" style="padding:10px">
                        <small class="card-title">02-01-2019 12:00:00</small>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="comment-item" style="display:flex;margin-bottom:15px">
                  <img src="{{asset('vendors/materialstyle/assets/img/demo/avatar1.jpg')}}" alt="..." class="img-avatar-circle" style="margin:0;width:40px;height:40px;transform: none;    position: relative;top: 0px;    right: -14px;border: solid 3px #fff;">
                  <div class="e-txt" style="margin-left:30px;line-height: 20px;width:100%;">
                    <div class="" style="line-height: 17px;">
                      <small class="card-title">Farhan</small><br>
                      @for($i=0;$i<5;$i++)
                      <a href="javascript:void(0)" class="color-warning">
                        <i class="zmdi zmdi-star" style="font-size:20px;"></i>
                      </a>
                      @endfor
                    </div>
                    <div class="description" style="background: whitesmoke;padding: 10px;border-radius: 10px;">
                      <p>Good</p>
                    </div>
                    <div class="ic" style="width:100%;display:flex">
                      <div class="" style="padding:10px">
                        <small class="card-title">02-01-2019 12:00:00</small>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>
    <div class="col-lg-4">
      <div class="card animated fadeInUp animation-delay-7">
        <div class="ms-hero-bg-info ms-hero-img-mountain">
          <h3 class="color-white index-1 text-center no-m pt-4">Victoria Smith</h3>
          <div class="color-medium index-1 text-center np-m">@vic_smith</div>
          <img src="{{asset('vendors/materialstyle/assets/img/demo/avatar1.jpg')}}" alt="..." class="img-avatar-circle"> </div>
        <div class="card-block pt-4 text-center">
          <h3 class="color-primary">About me</h3>
          <p>Lorem ipsum dolor sit amet, consectetur alter adipisicing elit. Facilis, natuse inse voluptates officia repudiandae beatae magni es magnam autem molestias.</p>
          <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs mt-1 mr-1 no-mr-md btn-google">
            <i class="zmdi zmdi-google"></i>
          </a>
          <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs mt-1 mr-1 no-mr-md btn-facebook">
            <i class="zmdi zmdi-facebook"></i>
          </a>
          <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs mt-1 mr-1 no-mr-md btn-twitter">
            <i class="zmdi zmdi-twitter"></i>
          </a>
          <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs mt-1 mr-1 no-mr-md btn-instagram">
            <i class="zmdi zmdi-instagram"></i>
          </a>
        </div>
      </div>
      <div class="card card-success animated fadeInUp animation-delay-7">
        <div class="card-header">
          <i class="fa fa-list-alt" aria-hidden="true"></i> Tersedia</div>
        <div class="card-block">
          <h3>
            <strong>Total:</strong>
            <span class="color-success">Rp. 80,000,00</span>
          </h3>
          <a href="javascript:void(0)" class="btn btn-raised btn-success btn-block btn-raised mt-2 no-mb">
            <i class="zmdi zmdi-shopping-cart-plus"></i> Pesan Sekarang</a>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('script')
<script>
$(document).ready(function(){
});
</script>
@stop
