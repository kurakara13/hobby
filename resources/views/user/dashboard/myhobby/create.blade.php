@extends('layouts.app')

@section('style')
<style>
@media (min-width: 992px){
  .page-dashboard {
      padding: 100px 0 100px 0;
  }

  .sidebar-dashboard {
      max-height: max-content;
  }

  .header-page {
    margin-bottom: 50px;
  }

  .btn-submit {
    padding: 7px;
  }
}
</style>

<link href="https://cdn.jsdelivr.net/npm/froala-editor@3.0.0/css/froala_editor.pkgd.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/froala-editor@3.0.0/js/froala_editor.pkgd.min.js"></script>
@stop

@section('content')
<div class="page-dashboard">
  <div class="container">
  	<div class="breadcrumbs-container breadcrumbs-top">
  	  <ul class="breadcrumbs min-list inline-list">
  	    <li class="breadcrumbs__item">
  	      <a href="index.html" class="breadcrumbs__link">
  	        <span class="breadcrumbs__title">Home</span>
  	      </a>
  	    </li>

  	    <li class="breadcrumbs__item">
  	      <a href="#" class="breadcrumbs__link">
  	        <span class="breadcrumbs__title">Dashboard</span>
  	      </a>
  	    </li>

        <li class="breadcrumbs__item">
  	      <a href="#" class="breadcrumbs__link">
  	        <span class="breadcrumbs__title">Berita</span>
  	      </a>
  	    </li>

  	    <li class="breadcrumbs__item">
  	      <span class="breadcrumbs__page c-gray">Tambah</span>
  	    </li>
  	  </ul><!-- .breadcrumbs -->
  	</div>
  	<h1 class="header-page">Tambah Berita</h1>
  	<div class="row">
  		<div class=" col-lg-4">
  			@include('layouts.partials')
  		</div>
  		<!-- end-col -->
  		<div class="col-lg-8">
        <div class="content-dashboard_wishlist dashboard-bg dashboard-pd">
          <div class="title-review">
            <h2>Berita</h2>
          </div>
          <div class="dashboard_edit-profile">
						<div class="content-dashboard_profile">
							<div class="row">
								<div class="col-md-12">
									<div class="profile_info">
										<div class="profile_form-login">
											<form action="{{route('admin.berita.store')}}" method="post" class="form-login"  enctype="multipart/form-data">
                        @csrf
												<h3 class="title_profile-info c-sale">Detail Berita</h3>
												<div class="profile-personal">
													<div class="row">
														<div class="col-md-12">
															<div class="form-login__input">
																<label class="label-login" for="login-name">Judul <span class="c-sale">*</span></label>
																<input type="text" name="judul" id="login-name" class="form-input input-border text-input">
															</div>
														</div>
                            <div class="form-login__input">
                              <div class="col-md-12">
                                <label class="label-login" for="login-name">Gambar Berita <span class="c-sale">*</span></label>
                                <input type="file" name="gambar" class="form-input input-border text-input">
                              </div>
                            </div>
														<div class="col-md-12">
															<div class="form-login__input">
																<label class="label-login"  for="login-country">Deskripsi</label>
                                <textarea id="example" name="deskripsi"></textarea>
															</div>
														</div>
                            <div class="col-md-12">
															<div class="form-login__input">
																<label class="label-login" for="login-name">Hobby <span class="c-sale">*</span></label>
																<select name="hobby[]" class="form-input input-border text-input" multiple>
                                  @foreach($hobby as $item)
                                  <option value="{{$item->nama}}">{{$item->nama}}</option>
                                  @endforeach
                                </select>
                                <p style="padding:5px;color:red">*Tekan Ctrl untuk memilih lebih dari 1</p>
															</div>
														</div>
                            <div class="col-md-12">
                              <div class="form-login__button">
                                <button type="submit" class="btn-submit btn-radius">Save Changes</button>
                              </div>
                            </div>
													</div>
												</div>
											</form>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
        </div>
  		</div>
  		<!-- end-col -->
  	</div>
  	<!-- end-row -->
  </div>
  <!-- end-container -->
</div>
@endsection

@section('script')
<script>
var editor = new FroalaEditor('#example', {
  toolbarButtons: {
    'moreText': {
      'buttons': ['bold', 'italic', 'underline']
    },
    'moreParagraph': {
      'buttons': ['alignLeft', 'alignCenter', 'formatOLSimple', 'alignRight', 'alignJustify', 'formatOL', 'formatUL', 'paragraphFormat', 'paragraphStyle', 'lineHeight', 'outdent', 'indent', 'quote']
    },
    'moreRich': {
      'buttons': ['insertImage']
    },
    'moreMisc': {
      'buttons': ['undo', 'redo', 'fullscreen', 'print', 'getPDF', 'spellChecker', 'selectAll', 'html', 'help'],
      'align': 'right',
      'buttonsVisible': 2
    }
  }
});
</script>
<style>

/* .fr-wrapper > div:first-child{
  display: none;
} */


/* .second-toolbar{
  display: none;
} */
</style>
@stop
