@extends('layouts.app')

@section('style')
<style>
@media (min-width: 992px){
  .page-dashboard {
      padding: 100px 0 100px 0;
  }

  .sidebar-dashboard {
      max-height: max-content;
  }

  .header-page {
    margin-bottom: 50px;
  }

  .btn-submit {
    padding: 7px;
  }
}
</style>
@stop

@section('content')
<div class="page-dashboard">
  <div class="container">
  	<div class="breadcrumbs-container breadcrumbs-top">
  	  <ul class="breadcrumbs min-list inline-list">
  	    <li class="breadcrumbs__item">
  	      <a href="index.html" class="breadcrumbs__link">
  	        <span class="breadcrumbs__title">Home</span>
  	      </a>
  	    </li>

  	    <li class="breadcrumbs__item">
  	      <a href="#" class="breadcrumbs__link">
  	        <span class="breadcrumbs__title">Dashboard</span>
  	      </a>
  	    </li>

  	    <li class="breadcrumbs__item">
  	      <span class="breadcrumbs__page c-gray">My Profile</span>
  	    </li>
  	  </ul><!-- .breadcrumbs -->
  	</div>
  	<h1 class="header-page">Dashboard</h1>
  	<div class="row">
  		<div class=" col-lg-4">
  			@include('layouts.partials')
  		</div>
  		<!-- end-col -->
  		<div class="col-lg-8">
        <div class="content-dashboard_wishlist dashboard-bg dashboard-pd">
          <div class="title-review">
            <h2>My Hobby</h2>
          </div>
          <div class="list-my-booking">
            <div class="item-booking">
              <form action="{{route('user.hobby.store')}}" method="post">
                @csrf
                <button type="submit" class="btn-submit submit-footer btn-radius" style="float:right;height:40px">Tambah Hobby</button>
                <select name="hobby" class="form-input input-border text-input"  style="float:right;width:auto;height:40px;margin-right:20px">
                  @foreach($hobby as $item)
                  <option value="{{$item->nama}}">{{$item->nama}}</option>
                  @endforeach
                </select>
              </form>
              <h3 class="title-my-booking">List Hobby</h3>
              <div class="list-items__wrapp">
                @forelse($hobbyUser as $index => $item)
                <div class="list-items__wrapper d-lg-flex">
                  <div class="wishlist_wrapper d-flex">
                    <div class="wishlist_thubmail">
                      <a href="#"><img src="{{asset('assets/img/'.$item->gambar)}}" style="width:100px;height:100px;border-radius:50%" alt="tours"></a>
                    </div>
                    <div class="wishlist_detail">
                      <h3> <a href="tour-single.html" class="wishlist_title" style="line-height: 5;">{{$item->nama}}</a></h3>
                    </div>
                  </div>
                  <div class="wishlist_action" style="    padding-right: 10px;">
                    <div class="d-lg-flex justify-content-between" style="line-height:3">
                      <a class="wishlist-link" onclick="$('#destroy_hobby-{{$item->id}}').submit()" href="#" style="font-size:25px"><i class="fa fa-trash"></i></a>
                      <form id="destroy_hobby-{{$item->id}}" method="post" action="{{route('user.hobby.destroy', $item->id)}}">
                        @csrf
                        @method('DELETE')
                      </form>
                    </div>
                  </div>
                </div>
                @empty
                <div>
                  <h3 style="text-align:center">Tidak Ada Hobi Yang Anda Pilih</h3>
                </div>
                @endforelse
              </div>
            </div>
            <div class="pagination_tours-reviews center">
              <ul class="min-list inline-list">
                <li><a href="#" class="ion-ios-arrow-back"></a></li>
                <li class="active"><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">...</a></li>
                <li><a href="#">15</a></li>
                <li><a href="#" class="ion-ios-arrow-forward"></a></li>
              </ul>
            </div>
          </div>
        </div>
  		</div>
  		<!-- end-col -->
  	</div>
  	<!-- end-row -->
  </div>
  <!-- end-container -->
</div>
@endsection

@section('script')
<script>
</script>
@stop
