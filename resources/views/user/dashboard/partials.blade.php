<div class="sidebar-dashboard dashboard-bg">
  <div class="item-list my-acount">
    <h2 class="my-acount__title">My Acount</h2>
    <ul class="min-list list-dashboard">
      <li class="">
        <a href="#">MY PROFILE </a>
      </li>
      <li>
        <a href="dashboard-edit-profile.html">EDIT PROFILE</a>
      </li>
      <li class="{{request()->routeIs('user.dashboard.hobby.index', 'user.dashboard.hobby.store') ? 'active-li' : ''}}">
        <a href="{{route('user.dashboard.hobby.index')}}">MY HOBBY</a>
      </li>
      <li>
        <a href="dashboard-change-password.html">CHANGE PASSWORD</a>
      </li>
    </ul>
  </div>
  <div class="item-list tour-booking">
    <h2 class="my-acount__title min border-sidebar__list">Your Post</h2>
    <ul class="min-list list-dashboard">
      <li>
        <a href="dashboard-my-booking.html">ACARA</a>
      </li>
      <li class="{{request()->routeIs('user.dashboard.berita.index', 'user.dashboard.berita.create') ? 'active-li' : ''}}">
        <a href="{{route('user.dashboard.berita.index')}}">BERITA</a>
      </li>
      <li>
        <a href="dashboard-wishlist.html">TEMPAT</a>
      </li>
      <li>
        <a href="dashboard-wishlist.html">KOMUNITAS</a>
      </li>
    </ul>
  </div>
  <div class="item-list tour-booking">
    <h2 class="my-acount__title min border-sidebar__list">Booking</h2>
    <ul class="min-list list-dashboard">
      <li>
        <a href="dashboard-my-booking.html">MY BOOKINGS</a>
      </li>
      <li>
        <a href="dashboard-review.html">REVIEWS</a>
      </li>
      <li>
        <a href="dashboard-wishlist.html">WISHLIST</a>
      </li>
    </ul>
  </div>
  <div class="sing-out min border-sidebar__list">
    <a class="sing-out__link" href="login.html">SIGN OUT</a>
  </div>
</div>
