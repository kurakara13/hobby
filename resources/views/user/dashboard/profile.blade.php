@extends('layouts.app')

@section('style')
<style>
@media (min-width: 992px){
  .page-dashboard {
      padding: 100px 0 100px 0;
  }

  .sidebar-dashboard {
      max-height: max-content;
  }
}
</style>
@stop

@section('content')
<div class="page-dashboard">
  <div class="container">
  	<div class="breadcrumbs-container breadcrumbs-top">
  	  <ul class="breadcrumbs min-list inline-list">
  	    <li class="breadcrumbs__item">
  	      <a href="index.html" class="breadcrumbs__link">
  	        <span class="breadcrumbs__title">Home</span>
  	      </a>
  	    </li>

  	    <li class="breadcrumbs__item">
  	      <a href="#" class="breadcrumbs__link">
  	        <span class="breadcrumbs__title">Dashboard</span>
  	      </a>
  	    </li>

  	    <li class="breadcrumbs__item">
  	      <span class="breadcrumbs__page c-gray">My Profile</span>
  	    </li>
  	  </ul><!-- .breadcrumbs -->
  	</div>
  	<h1 class="header-page">Dashboard</h1>
  	<div class="row">
  		<div class=" col-lg-4">
  			@include('user.dashboard.partials')
  		</div>
  		<!-- end-col -->
  		<div class="col-lg-8">
  			<div class="content-dashboard dashboard-bg dashboard-pd">
  				<div class="list-profile">
  					<div class="title-list-profile">
  						<div class="list-title">
  							<h2 class="title-head">Scott Greene <span class="title__states">/ United States</span></h2>
  						</div>
  						<a class="c-blue link-edit" href="#">Edit Profile</a>
  					</div>
  					<div class="list-info">
  						<div class="row">
  							<div class="col-md-3">
  								<img src="assets/image/uploads/avatar.png" alt="avatar">
  							</div>
  							<div class="col-md-9">
  								<div class="table-personal">
  									<table>
  										  <tr>
  										    <td class="title-table c-sale">Personal Info</td>
  										  </tr>
  										  <tr>
  										    <td class="test c-gra">Name</td>
  										    <td class="test c-black">Scott Greene</td>
  										    <td class="test">&ensp;</td>
  										  </tr>
  										  <tr>
  										    <td class="test c-gra">Birth Date</td>
  										    <td class="test c-black">May 24,1996</td>
  										    <td class="test">&ensp;</td>
  										  </tr>
  										  <tr>
  										    <td class="test c-gra">Gender</td>
  										    <td class="test c-black">Male</td>
  										    <td class="test">&ensp;</td>
  										  </tr>
  										  <tr>
  										    <td class="test c-gra">Country</td>
  										    <td class="test c-black">United Stated</td>
  										    <td class="test">&ensp;</td>
  										  </tr>
  									</table>
  								</div>
  								<div class="table-contact">
  									<table>
  									  <tr>
  									    <td class="title-table c-sale">Contact Info</td>
  									  </tr>
  									  <tr>
  									    <td class="test c-gra">Email</td>
  									    <td class="test c-blue t-bold">scottgreene@gmail.com</td>
  									    <td class="test">&ensp;</td>
  									  </tr>
  									  <tr>
  									    <td class="test c-gra">Phone</td>
  									    <td class="test c-blue t-bold">+1 382 5836 28</td>
  									    <td class="test"></td>
  									  </tr>
  									  <tr>
  									    <td class="test c-gra">Contact Address</td>
  									    <td class="test c-black">45th King Str,Manhattan,NYC</td>
  									    <td class="test">&ensp;</td>
  									  </tr>
  									</table>
  								</div>
  							</div>
  						</div>
  					</div>
  				</div>
  			</div>
  		</div>
  		<!-- end-col -->
  	</div>
  	<!-- end-row -->
  </div>
  <!-- end-container -->
</div>
@endsection

@section('script')
<script>
</script>
@stop
