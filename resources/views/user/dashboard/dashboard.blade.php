@extends('layouts.app')

@section('style')
<style>
@media (min-width: 992px){
  .page-dashboard {
      padding: 100px 0 100px 0;
  }

  .sidebar-dashboard {
      max-height: max-content;
  }
}
</style>
@stop

@section('content')
<div class="page-dashboard">
  <div class="container">
  	<div class="breadcrumbs-container breadcrumbs-top">
  	  <ul class="breadcrumbs min-list inline-list">
  	    <li class="breadcrumbs__item">
  	      <a href="index.html" class="breadcrumbs__link">
  	        <span class="breadcrumbs__title">Home</span>
  	      </a>
  	    </li>

  	    <li class="breadcrumbs__item">
          <span class="breadcrumbs__page c-gray">Dashboard</span>
  	    </li>

  	  </ul><!-- .breadcrumbs -->
  	</div>
  	<h1 class="header-page">Dashboard</h1>
  	<div class="row">
  		<div class=" col-lg-4">
  			@include('layouts.partials')
  		</div>
  		<!-- end-col -->
  		<div class="col-lg-8">
  			<div class="content-dashboard dashboard-bg dashboard-pd">

  			</div>
  		</div>
  		<!-- end-col -->
  	</div>
  	<!-- end-row -->
  </div>
  <!-- end-container -->
</div>
@endsection

@section('script')
<script>
</script>
@stop
