@extends('layouts.app')

@section('style')
<style>
.no-star{
  color: #e1dfdf;
}
</style>
@stop

@section('content')
<div class="ms-hero-page ms-hero-img-keyboard ms-hero-bg-primary mb-6">
  <div class="container">
    <div class="text-center">
      <span class="ms-logo ms-logo-lg ms-logo-white center-block mb-2 mt-2 animated zoomInDown animation-delay-5">H</span>
      <h1 class="no-m ms-site-title color-white center-block ms-site-title-lg mt-2 animated zoomInDown animation-delay-5">Recomended Tempat</h1>
    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col-lg-3 d-none d-lg-block">
      <div class="card card-primary">
        <div class="card-header">
          <h3 class="card-title">
            <i class="zmdi zmdi-filter-list"></i>Navigation</h3>
        </div>
        <div class="card-block no-pb">
          <h4 class="no-m color-primary">Sort</h4>
          <div class="form-group mt-1">
            <div class="radio no-mb">
              <label>
                <input type="radio" name="optionsRadios" id="optionsRadios0" value="option0" checked="" class="filter" data-filter="all"> Recommended </label>
            </div>
            <div class="radio no-mb">
              <label>
                <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" class="filter" data-filter=".category-1"> Newest </label>
            </div>
            <div class="radio no-mb">
              <label>
                <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" class="filter" data-filter=".category-1"> Most Likes </label>
            </div>
            <div class="radio no-mb">
              <label>
                <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" class="filter" data-filter=".category-1"> Most Shared </label>
            </div>
          </div>
          <h4 class="mt-2 color-primary no-mb">Hobi</h4>
          <div class="form-group mt-1">
            @foreach($hobby as $key)
            <div class="radio no-mb">
              <label>
                <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" class="filter" data-filter=".category-1"> {{$key->nama}} </label>
            </div>
            @endforeach
          </div>
        </div>
        <div class="card-block pr-0">
          <form class="form-horizontal">
            <div class="form-group no-mt">
              <h4 class="no-m color-primary mb-2">Search</h4>
              <input class="form-control" type="text" placeholder="Search Content" style="width:90%">
            </div>
          </form>
        </div>
      </div>
    </div>
    <div class="col-md-9">
      <div class="row content">
        @foreach($content as $item)
        <div class="col-lg-4">
          <article class="card wow fadeInLeft animation-delay-5 mb-4">
            <div class="card-block">
              <div class="row">
                <div class="col-xl-12">
                  <img src="{{asset('images/blog-02.jpg')}}" alt="" class="img-fluid mb-4" style="margin-bottom: 10px !important">
                  <a href="javascript:void(0)" class="color-danger">
                    <i class="zmdi zmdi-favorite" style="font-size:20px"></i> <span style="color:#00000073;margin-left:5px;font-size:14px">2,019</span>
                  </a>
                  @for($i=0;$i<5;$i++)
                  <a href="javascript:void(0)" class="color-warning pull-right">
                    <i class="zmdi zmdi-star {{$i < 2 ? 'no-star' : ''}}" style="font-size:20px;"></i>
                  </a>
                  @endfor
                  <hr style="margin: 5px 0;">
                </div>
                <div class="col-xl-12" style="text-align:justify">
                  <h3 class="no-mt title">
                    <a href="{{route('user.post.detail', 1)}}" style="font-size:20px;">{{substr($item->judul,0,40)}}...</a>
                  </h3>
                </div>
                <div class="col-xl-12" style="text-align:justify">
                  <div class="desc">
                    <p href="javascript:void(0)">{{substr($item->deskripsi,0,100)}}...</p>
                  </div>
                </div>
              </div>
              <div class="row">
                <!-- <div class="col-lg-12 hobby">
                  Hobby :
                  @foreach(explode(',',$item->hobby) as $hobbyValue)
                    {{$hobbyValue}} |
                  @endforeach
                </div> -->
                <div class="col-lg-12">
                  <a href="{{route('user.post.detail', 1)}}" class="btn btn-primary btn-raised btn-block animate-icon">Selengkapnya
                    <i class="ml-1 no-mr zmdi zmdi-long-arrow-right"></i>
                  </a>
                </div>
              </div>
            </div>
          </article>
        </div>
        @endforeach
      </div>
      {{ $content->links() }}
    </div>
  </div>
</div>
@endsection

@section('script')
<script>
$(document).ready(function(){

  var highestBox = 0;
  var highestBoxTitle = 0;
  var highestBoxHobby = 0;

  // Select and loop the container element of the elements you want to equalise
  $('.content .col-lg-4').each(function(){
    if($(this).find('.title').height() > highestBoxTitle) {
      highestBoxTitle = $(this).find('.title').height();
    }
  });

  $('.title').height(highestBoxTitle);

  $('.content .col-lg-4').each(function(){
    if($(this).find('.desc').height() > highestBox) {
      highestBox = $(this).find('.desc').height();
    }
  });

  $('.desc').height(highestBox);


  $('.content .col-lg-4').each(function(){
    if($(this).find('.hobby').height() > highestBoxHobby) {
      highestBoxHobby = $(this).find('.hobby').height();
    }
  });

  $('.hobby').height(highestBoxHobby);


});
</script>
@stop
