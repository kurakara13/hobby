@extends('layouts.app')

@section('style')
<style>

</style>
@stop

@section('content')
<section class="page-banner bg-header page-banner--layout-blog__modem">
	<div class="header-title_about container">
	</div>
</section>
<div class="page-about">
	<section class="section-about section-head">
		<div class="container">
			<div class="entry-top d-flex">
				<div class="entry-top__wrap">
					<h1 class="entry-title">Tentang Kami</h1>
				</div>
			</div>
			<div class="entry-bottom">
				<div class="row">
					<div class="col-md-12 section-about section-content  bg-header">
						<div class="intro-video">
							<div class="intro-video__wrapper">
								<h2 class="entry-title">Our Story</h2>
								<h3>Suspendisse gravida elementum lacus, amae suada tortor sollicitudin ut. Donec pharetra metus lectus, ut eleifend eros sol licitu din.</h3>
								<p>Proin maximus ut augue ut finibus. In non est eu libero rutrum fringilla. Mauris dictum, turpis in convallis tincidunt, est dui varius est, eget ultrices magna quam eu tellus.</p>
								<p>Maecenas pellentesque aliquet arcu, vel elementum magna facilisis vitae. Nam in cursus lorem. </p>
								<p>Donec ac tellus nisl. Sed vo lutpat quis orci nec placerat fusce ex magna. Quisque venenatis, eros in auctor dictum, ipsum nisi imperdiet sem, at pulvinar nisl ante id sem.</p>
							</div>
						</div>
					</div>
					<!-- end-col -->
				</div>
				<!-- end-row -->
			</div>
		</div>
		<!-- end-container -->
	</section>
</div>
@stop

@section('script')
<script>
$(document).ready(function(){

});
</script>
@stop
